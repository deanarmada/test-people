from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time
import inspect


# Create your tests here.
# python manage.py test login

# Test in Session Authentication Upon Entry
# Test in Token Authentication Upon Entry
# Submit button must be disabled unless both username and password or now valid(fields are required) 

class LogInReusablesScript(StaticLiveServerTestCase):

	# Command: python manage.py dumpdata auth.User --indent=2 > login/fixtures/login_users.json
	# Command: python manage.py dumpdata bio.bio --indent=2 > login/fixtures/bio.json
	fixtures = ['login_users.json', 'bio.json']

	def _setupLogIn(self):
		self.browser.get(self.live_server_url) # .get is tells the browser to go to a new page
		# Type a user's user name in the field
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('adgc')
		# Type a user's password in the field
		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys('adgcadgc')
		# Click the login button
		login = self.browser.find_element_by_tag_name('button')
		login.click()
		time.sleep(2)

	def _setupLogOut(self):
		# User click the tiny arrow down button near the picture on the right side of the nav to show the dropdown menu
		dropdown_caret = self.browser.find_element_by_id('caret')
		dropdown_caret.click()
		# User searches for the logout button and clicks it
		log_out_link = self.browser.find_element_by_link_text('Log Out') # Finds element's' with all that text in a link
		log_out_link.click()

class LogInTestFT(StaticLiveServerTestCase):

	# fixtures is the same as getting data and in this case we get it from a json file
	# bio.json fore the code on the upper right
	# Not inherited from LogInReusablesScript for testing Empty Lists/Records
	fixtures = ['login_users.json', 'bio.json']

	def setUp(self):
		# self.browser is the selenium object which represents the web browser, aka the WebDriver.
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.headerText = 'MANSHIP PEOPLE'
		self.defaultDirectURL = '/crew-menu/bio/'

	def tearDown(self):
		self.browser.quit()

	def _setup_input_login(self):
		# Type a user's user name in the field
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('adgc')
		# Type a user's password in the field
		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys('adgcadgc')

	def _setup_login(self):
		self.browser.get(self.live_server_url) # .get is tells the browser to go to a new page
		self._setup_input_login()

	def _setup_login_with_access(self):
		self.browser.get(self.live_server_url) # .get is tells the browser to go to a new page
		self._setup_input_login()
		login = self.browser.find_element_by_tag_name('button')
		login.click()
		time.sleep(2)

	def test_BasicLogin(self):
		LogInReusablesScript._setupLogIn(self)

		# It must be redirected to the Crew Menu with the header text MANSHIP PEOPLE
		headerText = self.headerText
		header = self.browser.find_element_by_tag_name('strong')
		self.assertIn(headerText, header.text)
		time.sleep(2) # sleep is needed to assert MANSHIP PEOPLE
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Log-in can press enter
	# 1.) Problem, login function on formcontroller in main.js only works when you click the log-in button
	# 2.) Solution, created a directive of ng-enter in main.js and then applied it in the tag with the login function as effect
	def test_BasicLoginUsingEnter(self):
		self._setup_login()
		# Press Enter on password field
		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys(Keys.RETURN)
		time.sleep(2)

		# It must be redirected to the Crew Menu with the header text MANSHIP PEOPLE
		headerText = self.headerText
		header = self.browser.find_element_by_tag_name('strong')
		self.assertIn('MANSHIP PEOPLE', header.text)
		time.sleep(2)
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Must be able to authenticate on right username and password
	def test_WrongUsernamePassword(self):
		self.browser.get(self.live_server_url) # .get is tells the browser to go to a new page
		# Type a wrong user name in the field
		username_field = self.browser.find_element_by_name('username')
		username_field.send_keys('adg')
		# Type a wrong password in the field
		password_field = self.browser.find_element_by_name('password')
		password_field.send_keys('adgadg')
		# Try logging in with the wrong credential's
		login = self.browser.find_element_by_tag_name('button')
		login.click()
		time.sleep(2)

		# login-notifications are not located if no error beacause of ng-if
		# Check if an error notifications show
		login_notifications = self.browser.find_element_by_id('login-notifications')
		self.assertIn('* Invalid username or password', login_notifications.text)
		time.sleep(2)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Log-in Notifications must close
	# TOCOMPLETE! 
	def test_CloseErrorNotification(self):
		self.test_WrongUsernamePassword()
		time.sleep(2)
		self.browser.find_element_by_id("hideLogInNotifications").click()
		time.sleep(2)
		body = self.browser.find_element_by_tag_name('body')
		self.assertNotIn('Error Notifications!', body.text)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

		# Alternative way to pass not founds
		# try:
		# 	login_notifications = self.browser.find_element_by_id('login-notifications')
		# except:
		# 	pass
		## self.fail('Surprise!')

		# Example of using self.client, this will return error since it finds even those false of ng-ifs
		# login_notifications = self.browser.find_element_by_id('login-notifications')
		# response = self.client.get('/')
		# self.assertNotIn('Error Notifications!', response.content.decode('utf-8'))

	# Redirect immediately to a logged-in interface if user visits the login page with still a valid session and token
	# When a user tries to go to a session authenticated page he must be redirected to the login page,
	def test_InvalidSessionRedirectLogin(self):
		# Try to visit a url inside the login
		self.browser.get(self.live_server_url+self.defaultDirectURL)
		time.sleep(2)
		# Will be redirected back to the login and check if there is a input field named redirect hidden
		redirect_field = self.browser.find_element_by_name('redirect')
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	## There must be an error notification that shows upon redirection
	## Upon redirection to login page when he logs-in the user must go straight to the page that he is trying to access immediately
	def test_SessionRedirect(self):
		self.test_InvalidSessionRedirectLogin()
		time.sleep(2)
		login_notifications = self.browser.find_element_by_id('login-notifications')
		# An Session Expired Error Notification must show after trying to log-in on pages with required authentications
		self.assertIn('* Session expired, Please login again', login_notifications.text)
		self._setup_input_login()
		login = self.browser.find_element_by_tag_name('button')
		login.click()
		time.sleep(2)

		# It must be redirected to the Bio Entries with the header text BIO ENTRIES
		headerText = 'BIO ENTRIES'
		header = self.browser.find_element_by_tag_name('strong')
		self.assertIn(headerText, header.text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	def test_logOut(self):
		LogInReusablesScript._setupLogIn(self)
		LogInReusablesScript._setupLogOut(self)
		time.sleep(2)
		self.assertIn('Log-In Page', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))