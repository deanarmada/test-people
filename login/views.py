from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as logIn, logout as logOut
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect


from test_people.api.views import login, logout

# from global_declarations.variables import _today

# Create your views here.

def index(request):
	user = request.user
	if user.is_authenticated():
		return HttpResponseRedirect('crew-menu')
	next_redirect = ""
	if "next" in request.GET:
		next_redirect = request.GET['next']
	template = 'login/index.html'
	# template = 'pure_html/login/index.html'
	context_dict = {}
	context_dict['next_redirect'] = next_redirect
	return render(request, template, context_dict)
	# return HttpResponse("Index")

@login_required
def log_out(request):
	logOut(request)
	# return HttpResponse("Logout")
	return logout(request)

# Return came from the login api passed as a response to front-end
def validation(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user:
			if user.is_active:
				logIn(request, user)
		else:
			pass
		# authenticate syntax
		# login syntax
	else:
		pass

	return login(request)

def testcodes(request):
	if request.method:
		print (request.method)
		print (request.POST)
		print (request.FILES)
	return HttpResponse("DEAN")