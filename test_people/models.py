# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from ckeditor.fields import RichTextField

#django-mptt
from mptt.models import MPTTModel, TreeForeignKey

class AreaCodes(models.Model):
    area_name = models.CharField(max_length=100, blank=True, null=True)
    code = models.PositiveIntegerField(default=0)
    country = models.ForeignKey('Countries', on_delete=models.DO_NOTHING, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'area_codes'

class AccessControl(models.Model):
    access_control_sys_status = models.CharField(max_length=45, blank=True, null=True)
    access_control_code = models.CharField(max_length=45, blank=True, null=True)
    access_control_name = models.CharField(max_length=45, blank=True, null=True)
    access_control_group_access = models.ForeignKey('GroupAccess', on_delete=models.DO_NOTHING, db_column='access_control_group_access', blank=True, null=True)
    access_control_add = models.CharField(max_length=1, blank=True, null=True)
    access_control_del = models.CharField(max_length=1, blank=True, null=True)
    access_control_update = models.CharField(max_length=1, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'access_control'


class AdvanceProvider(models.Model):
    id = models.IntegerField(primary_key=True)
    advance_provider_sys_status = models.CharField(max_length=45, blank=True, null=True)
    advance_provider_code = models.CharField(max_length=1, blank=True, null=True)
    advance_provider_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='advance_provider_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'advance_provider'

class Agents(models.Model):
    id = models.IntegerField(primary_key=True)
    agent_sys_status = models.CharField(max_length=45, blank=True, null=True)
    agent_code = models.CharField(max_length=4, blank=True, null=True)
    agent_full = models.CharField(max_length=45, blank=True, null=True)
    agent_address_1 = models.CharField(max_length=45, blank=True, null=True)
    agent_address_2 = models.CharField(max_length=45, blank=True, null=True)
    agent_address_3 = models.CharField(max_length=45, blank=True, null=True)
    agent_address_4 = models.CharField(max_length=45, blank=True, null=True)
    agent_country = models.ForeignKey('Countries', on_delete=models.DO_NOTHING, db_column='agent_country', blank=True, null=True)
    agent_phone = models.CharField(max_length=45, blank=True, null=True)
    agent_telex = models.CharField(max_length=45, blank=True, null=True)
    agent_fax = models.CharField(max_length=45, blank=True, null=True)
    agent_comment = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='agent_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'agents'

class Banks(models.Model):
    id = models.IntegerField(primary_key=True)
    bank_sys_status = models.CharField(max_length=45, blank=True, null=True)
    bank_code = models.CharField(max_length=4, blank=True, null=True)
    bank_full = models.CharField(max_length=45, blank=True, null=True)
    bank_actual_code = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='bank_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'banks'

class Barangay(models.Model):
    barangay = models.CharField(max_length=60, blank=True, null=True)
    municipality = models.ForeignKey('Municipality', blank=True, null=True, default=None)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'barangay'
        ordering = ['barangay']

    def __str__(self):
        return self.barangay.upper()

class Zip(models.Model):
    zip = models.PositiveIntegerField(default=None)
    barangay = models.ForeignKey(Barangay, blank=True, null=True, default=None)
    municipality = models.ForeignKey('Municipality',)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'zip'

    def __str__(self):
        return str(self.zip)

class BloodTypes(models.Model):
    blood_type = models.CharField(max_length=60, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'blood_types'

class Countries(models.Model):
    country_name = models.CharField(max_length=100)
    country_code = models.CharField(max_length=45)
    country_nationality = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='country_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.country_name

    class Meta:
        ordering = ('country_name',)
        db_table = 'countries'


class Cities(models.Model):
    city_state = models.ForeignKey('States', on_delete=models.DO_NOTHING, db_column='city_state')
    city_name = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='city_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'cities'

class CivilStatuses(models.Model):
    civil_status_code = models.CharField(max_length=45, blank=True, null=True)
    civil_status_name = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='civil_status_updated_by')
    date_updated = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'civil_statuses'

class ComponentScales(models.Model):
    id = models.IntegerField(primary_key=True)
    component_scale_sys_status = models.CharField(max_length=45, blank=True, null=True)
    component_scale_code = models.CharField(max_length=2, blank=True, null=True)
    component_scale_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='component_scale_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'component_scales'        

class Components(models.Model):
    id = models.IntegerField(primary_key=True)
    component_code = models.CharField(max_length=3, blank=True, null=True)
    component_full = models.CharField(max_length=45, blank=True, null=True)
    component_component_scale = models.ForeignKey('ComponentScales', on_delete=models.DO_NOTHING, db_column='component_component_scale', blank=True, null=True)
    component_sort = models.CharField(max_length=45, blank=True, null=True)
    component_table = models.CharField(max_length=45, blank=True, null=True)
    component_skip = models.CharField(max_length=45, blank=True, null=True)
    component_taxable = models.CharField(max_length=45, blank=True, null=True)
    component_whopays = models.CharField(max_length=45, blank=True, null=True)
    component_is_tax = models.CharField(max_length=45, blank=True, null=True)
    component_print_monthly = models.CharField(max_length=45, blank=True, null=True)
    component_print_contract_gca = models.CharField(max_length=45, blank=True, null=True)
    component_print_payroll = models.CharField(max_length=45, blank=True, null=True)
    component_print_r_payroll = models.CharField(max_length=45, blank=True, null=True)
    component_print_retro = models.CharField(max_length=45, blank=True, null=True)
    component_print_article = models.CharField(max_length=45, blank=True, null=True)
    component_print_clone = models.CharField(max_length=45, blank=True, null=True)
    component_monthly_amount = models.CharField(max_length=45, blank=True, null=True)
    component_total_amount = models.CharField(max_length=45, blank=True, null=True)
    component_field_depend_1 = models.CharField(max_length=45, blank=True, null=True)
    component_field_depend_2 = models.CharField(max_length=45, blank=True, null=True)
    component_field_change_1 = models.CharField(max_length=45, blank=True, null=True)
    component_field_change_2 = models.CharField(max_length=45, blank=True, null=True)
    component_field_special = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='component_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'components'        


class Degrees(models.Model):
    degree_name = models.CharField(max_length=100, blank=True, null=True) # Changed by Dean from 45 to 100 because of import reasons(45 is too ) at 05-11-2016
    degree_code = models.CharField(max_length=25, blank=True, null=True)
    # Commented by Dean because of belief of confusing relation? 20 Schools has BSIT degrees so there are 20 values with different schools with the same course? at 05-11-2016
    # degree_school = models.ForeignKey('Schools', on_delete=models.DO_NOTHING, db_column='degree_school')
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='degree_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'degrees'

    def __str__(self):
        return self.degree_name

class Dialects(models.Model):
    language = models.ForeignKey('Languages', on_delete=models.DO_NOTHING)
    dialect = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'dialects'        

class EmployeeTypes(models.Model):
    employee_type = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'employee_types'

class EmploymentTypes(models.Model):
    employment_type = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'employment_types'        

class Exchange(models.Model):
    id = models.IntegerField(primary_key=True)
    exchange_sys_status = models.CharField(max_length=45, blank=True, null=True)
    exchange_code = models.CharField(max_length=45, blank=True, null=True)
    exchange_money = models.ForeignKey('Money', on_delete=models.DO_NOTHING, db_column='exchange_money', blank=True, null=True)
    exchange_date = models.DateField(blank=True, null=True)
    exchange_rate = models.CharField(max_length=45, blank=True, null=True)
    exchange_divide_or_multiply = models.CharField(max_length=45, blank=True, null=True)
    exchange_decimal_point = models.IntegerField(blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='exchange_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'exchange'

class GroupAccess(models.Model):
    group_name = models.CharField(max_length=45)
    group_code = models.CharField(max_length=4, blank=True, null=True)
    group_sys_status = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'group_access'

class Hobbies(models.Model):
    hobby = models.CharField(max_length=100)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'hobbies'

class Languages(models.Model):
    language = models.CharField(max_length=100)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'languages'        

class ManningAgents(models.Model):
    id = models.IntegerField(primary_key=True)
    manning_agent_sys_status = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_code = models.CharField(max_length=4, blank=True, null=True)
    manning_agent_full = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_adddress_1 = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_adddress_2 = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_adddress_3 = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_adddress_4 = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_law_country = models.IntegerField(blank=True, null=True)
    manning_agent_phone = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_tax_agent = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_tax_account = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_sss = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_aoh = models.CharField(max_length=45, blank=True, null=True)
    manning_agent_official_signer = models.ForeignKey('OfficialSigners', on_delete=models.DO_NOTHING, db_column='manning_agent_official_signer', blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='manning_agent_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'manning_agents'

class Money(models.Model):
    id = models.IntegerField(primary_key=True)
    money_sys_status = models.CharField(max_length=45, blank=True, null=True)
    money_currency = models.CharField(max_length=45, blank=True, null=True)
    money_description = models.CharField(max_length=45, blank=True, null=True)
    money_decimal_point = models.IntegerField(blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='money_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'money'

class MobileNetworks(models.Model):
    network = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'mobile_networks'

class MobileNetworkPrefixes(models.Model):
    network = models.ForeignKey(MobileNetworks, on_delete=models.DO_NOTHING,)
    prefix = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'mobile_network_prefixes'

class Municipality(models.Model):
    province = models.ForeignKey('Provinces', on_delete=models.DO_NOTHING, blank=True, null=True)
    municipality = models.CharField(max_length=80, default=None)
    has_barangay = models.BooleanField(default=False)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.municipality.upper()        

    class Meta:
        db_table = 'municipality'

class NameSuffixes(models.Model):
    suffix = models.CharField(max_length=50, unique=True, default=None)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'name_suffixes'        

class OfficialSigners(models.Model):
    id = models.IntegerField(primary_key=True)
    official_signer_sys_status = models.CharField(max_length=45, blank=True, null=True)
    official_signer_code = models.CharField(max_length=4, blank=True, null=True)
    official_signer_full = models.CharField(max_length=45, blank=True, null=True)
    official_signer_position = models.CharField(max_length=45, blank=True, null=True)
    official_signer_tax_account_number = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='official_signer_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'official_signers'

class Options(models.Model):
    option_sys_status = models.CharField(max_length=45, blank=True, null=True)
    option_name = models.CharField(max_length=45, blank=True, null=True)
    option_value = models.CharField(max_length=50, blank=True, null=True)
    option_type = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'options'

class Provinces(models.Model):
    region = models.ForeignKey('Regions', on_delete=models.DO_NOTHING, blank=True, null=True)
    province = models.CharField(max_length=50, unique=True, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.province

    class Meta:
        ordering = ['province']
        db_table = 'provinces'

class RankGroupDetails(models.Model):
    id = models.IntegerField(primary_key=True)
    rank_group_detail_sys_status = models.CharField(max_length=45, blank=True, null=True)
    rank_group_detail_record = models.CharField(max_length=8, blank=True, null=True)
    rank_group_detail_rank_group = models.ForeignKey('RankGroups', on_delete=models.DO_NOTHING, db_column='rank_group_detail_rank_group', blank=True, null=True)
    rank_group_detail_rank = models.ForeignKey('Ranks', on_delete=models.DO_NOTHING, db_column='rank_group_detail_rank', blank=True, null=True)
    rank_group_detail_status = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='rank_group_detail_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'rank_group_details'

class RankGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    rank_group_sys_status = models.CharField(max_length=45, blank=True, null=True)
    rank_group_code = models.CharField(max_length=8, blank=True, null=True)
    rank_group_full = models.CharField(max_length=45, blank=True, null=True)
    rank_group_group_by = models.CharField(max_length=45, blank=True, null=True)
    rank_group_status = models.CharField(max_length=45, blank=True, null=True)
    rank_group_prinicipal = models.ForeignKey('bio.Principals', on_delete=models.DO_NOTHING, db_column='rank_group_prinicipal', blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='rank_group_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'rank_groups'


class Ranks(models.Model):
    rank_code = models.CharField(max_length=45)
    rank_sys_status = models.CharField(max_length=45, blank=True, null=True)
    rank_description = models.CharField(max_length=45)
    rank_department = models.CharField(max_length=45)
    rank_type = models.CharField(max_length=5)
    rank_order = models.IntegerField()
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='rank_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'ranks'

class Regions(models.Model):
    region = models.CharField(max_length=50, default=None)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.region

    class Meta:
        db_table = 'regions'


class RelationshipTypes(models.Model):
    relationship_type_sys_status = models.CharField(max_length=45, blank=True, null=True)
    relationship_code = models.CharField(max_length=45)
    relationship_name = models.CharField(max_length=45)
    relationship_gender = models.CharField(max_length=3, null=True, blank=True) # Added by Dean to add a conditional on the relations template if Female then show maiden name fields at 05-11-2016
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='relationship_type_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.relationship_name

    class Meta:
        db_table = 'relationship_types'        

class Religions(models.Model):
    religion = models.CharField(max_length=100, default=None)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.religion

    class Meta:
        db_table = 'religions'

class Schools(models.Model):
    school_name = models.CharField(max_length=100, blank=True, null=True)
    school_code = models.CharField(max_length=25, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='school_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'schools'

class SocialMedia(models.Model):
    social_media_name = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by',)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'social_medias'


class SssTaxFacts(models.Model):
    id = models.IntegerField(primary_key=True)
    sss_tax_fact_sys_status = models.CharField(max_length=45, blank=True, null=True)
    sss_tax_code = models.CharField(max_length=1, blank=True, null=True)
    sss_tax_last_update = models.DateField(blank=True, null=True)
    sss_tax_employee_sss_ded = models.IntegerField(blank=True, null=True)
    sss_tax_employer_sss_cont = models.IntegerField(blank=True, null=True)
    sss_tax_employee_med_ded = models.IntegerField(blank=True, null=True)
    sss_tax_employer_med_cont = models.IntegerField(blank=True, null=True)
    sss_tax_employer_ecc_cont = models.IntegerField(blank=True, null=True)
    sss_tax_sss_ec = models.IntegerField(blank=True, null=True)
    sss_tax_employee_pagibig_ded = models.IntegerField(blank=True, null=True)
    sss_tax_employee_pagibig_cont = models.IntegerField(blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='sss_facts_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'sss_tax_facts'

class States(models.Model):
    state_country = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, db_column='state_country')
    state_name = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='state_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'states'

class Titles(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'titles'        

class TaxCategories(models.Model):
    id = models.IntegerField(primary_key=True)
    tax_category_sys_status = models.CharField(max_length=45, blank=True, null=True)
    tax_category_code = models.CharField(max_length=1, blank=True, null=True)
    tax_category_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='tax_category_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'tax_categories'


class TaxStatuses(models.Model):
    tax_name = models.CharField(max_length=45, blank=True, null=True)
    tax_description = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='tax_status_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.tax_name)

    class Meta:
        db_table = 'tax_statuses'

class VesselCategories(models.Model):
    vessel_category_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_category_code = models.CharField(max_length=45, blank=True, null=True)
    vessel_category_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_category_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'vessel_categories'                

class VesselTypes(MPTTModel):
    vessel_type_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_type_full = models.CharField(max_length=45, blank=True, null=True)
    vessel_type_code = models.CharField(max_length=10, blank=True, null=True)
    vessel_type_parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    vessel_type_description = models.CharField(max_length=255, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_type_updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'vessel_types'

    class MPTTMeta:
        parent_attr = 'vessel_type_parent'

class VesselTypeSizes(models.Model):
    vessel_type_size_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_type_size_code = models.CharField(max_length=10, blank=True, null=True)
    vessel_type_size_full = models.CharField(max_length=80, blank=True, null=True)
    vessel_type_size_grt_min = models.IntegerField(blank=True, null=True)
    vessel_type_size_grt_max = models.IntegerField(blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'vessel_type_sizes'

# Creates the Notification Email
class EmailNotificationTemplate(models.Model):
    template = models.CharField(max_length=45)
    greetings = RichTextField()
    message = RichTextField()

    def __str__(self):
        return "%s" % self.greetings

class UIDRange(models.Model):
    is_foreigner = models.BooleanField(default=False)
    is_hellespont_group = models.BooleanField(default=False)
    next_uid = models.PositiveIntegerField(default=0)
    min_range = models.PositiveIntegerField(default=0)
    max_range = models.PositiveIntegerField(default=0)
    group_ids = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return "%s" % self.next_uid

    class Meta:
        db_table = 'uid_range'

    #(min <= value <= max) and value >= next_uid