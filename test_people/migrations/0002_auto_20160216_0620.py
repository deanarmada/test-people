# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-16 06:20
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('bio', '0003_auto_20160216_0620'),
        ('test_people', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvanceProvider',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('advance_provider_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('advance_provider_code', models.CharField(blank=True, max_length=1, null=True)),
                ('advance_provider_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'advance_provider',
            },
        ),
        migrations.CreateModel(
            name='Agents',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('agent_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_code', models.CharField(blank=True, max_length=4, null=True)),
                ('agent_full', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_address_1', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_address_2', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_address_3', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_address_4', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_phone', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_telex', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_fax', models.CharField(blank=True, max_length=45, null=True)),
                ('agent_comment', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'agents',
            },
        ),
        migrations.CreateModel(
            name='Banks',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('bank_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('bank_code', models.CharField(blank=True, max_length=4, null=True)),
                ('bank_full', models.CharField(blank=True, max_length=45, null=True)),
                ('bank_actual_code', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'banks',
            },
        ),
        migrations.CreateModel(
            name='Components',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('component_code', models.CharField(blank=True, max_length=3, null=True)),
                ('component_full', models.CharField(blank=True, max_length=45, null=True)),
                ('component_sort', models.CharField(blank=True, max_length=45, null=True)),
                ('component_table', models.CharField(blank=True, max_length=45, null=True)),
                ('component_skip', models.CharField(blank=True, max_length=45, null=True)),
                ('component_taxable', models.CharField(blank=True, max_length=45, null=True)),
                ('component_whopays', models.CharField(blank=True, max_length=45, null=True)),
                ('component_is_tax', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_monthly', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_contract_gca', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_payroll', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_r_payroll', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_retro', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_article', models.CharField(blank=True, max_length=45, null=True)),
                ('component_print_clone', models.CharField(blank=True, max_length=45, null=True)),
                ('component_monthly_amount', models.CharField(blank=True, max_length=45, null=True)),
                ('component_total_amount', models.CharField(blank=True, max_length=45, null=True)),
                ('component_field_depend_1', models.CharField(blank=True, max_length=45, null=True)),
                ('component_field_depend_2', models.CharField(blank=True, max_length=45, null=True)),
                ('component_field_change_1', models.CharField(blank=True, max_length=45, null=True)),
                ('component_field_change_2', models.CharField(blank=True, max_length=45, null=True)),
                ('component_field_special', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'components',
            },
        ),
        migrations.CreateModel(
            name='ComponentScales',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('component_scale_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('component_scale_code', models.CharField(blank=True, max_length=2, null=True)),
                ('component_scale_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'component_scales',
            },
        ),
        migrations.CreateModel(
            name='Degrees',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('degree_name', models.CharField(blank=True, max_length=45, null=True)),
            ],
            options={
                'db_table': 'degrees',
            },
        ),
        migrations.CreateModel(
            name='Exchange',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('exchange_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('exchange_code', models.CharField(blank=True, max_length=45, null=True)),
                ('exchange_date', models.DateField(blank=True, null=True)),
                ('exchange_rate', models.CharField(blank=True, max_length=45, null=True)),
                ('exchange_divide_or_multiply', models.CharField(blank=True, max_length=45, null=True)),
                ('exchange_decimal_point', models.IntegerField(blank=True, null=True)),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'exchange',
            },
        ),
        migrations.CreateModel(
            name='ManningAgents',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('manning_agent_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_code', models.CharField(blank=True, max_length=4, null=True)),
                ('manning_agent_full', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_adddress_1', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_adddress_2', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_adddress_3', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_adddress_4', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_law_country', models.IntegerField(blank=True, null=True)),
                ('manning_agent_phone', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_tax_agent', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_tax_account', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_sss', models.CharField(blank=True, max_length=45, null=True)),
                ('manning_agent_aoh', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'manning_agents',
            },
        ),
        migrations.CreateModel(
            name='Money',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('money_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('money_currency', models.CharField(blank=True, max_length=45, null=True)),
                ('money_description', models.CharField(blank=True, max_length=45, null=True)),
                ('money_decimal_point', models.IntegerField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'money',
            },
        ),
        migrations.CreateModel(
            name='OfficialSigners',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('official_signer_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('official_signer_code', models.CharField(blank=True, max_length=4, null=True)),
                ('official_signer_full', models.CharField(blank=True, max_length=45, null=True)),
                ('official_signer_position', models.CharField(blank=True, max_length=45, null=True)),
                ('official_signer_tax_account_number', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'official_signers',
            },
        ),
        migrations.CreateModel(
            name='RankGroupDetails',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('rank_group_detail_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('rank_group_detail_record', models.CharField(blank=True, max_length=8, null=True)),
                ('rank_group_detail_status', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'rank_group_details',
            },
        ),
        migrations.CreateModel(
            name='RankGroups',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('rank_group_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('rank_group_code', models.CharField(blank=True, max_length=8, null=True)),
                ('rank_group_full', models.CharField(blank=True, max_length=45, null=True)),
                ('rank_group_group_by', models.CharField(blank=True, max_length=45, null=True)),
                ('rank_group_status', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'rank_groups',
            },
        ),
        migrations.CreateModel(
            name='Schools',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('school_name', models.CharField(blank=True, max_length=45, null=True)),
                ('school_type', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'schools',
            },
        ),
        migrations.CreateModel(
            name='SssTaxFacts',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('sss_tax_fact_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('sss_tax_code', models.CharField(blank=True, max_length=1, null=True)),
                ('sss_tax_last_update', models.DateField(blank=True, null=True)),
                ('sss_tax_employee_sss_ded', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employer_sss_cont', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employee_med_ded', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employer_med_cont', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employer_ecc_cont', models.IntegerField(blank=True, null=True)),
                ('sss_tax_sss_ec', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employee_pagibig_ded', models.IntegerField(blank=True, null=True)),
                ('sss_tax_employee_pagibig_cont', models.IntegerField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'sss_tax_facts',
            },
        ),
        migrations.CreateModel(
            name='TaxCategories',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('tax_category_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('tax_category_code', models.CharField(blank=True, max_length=1, null=True)),
                ('tax_category_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'tax_categories',
            },
        ),
        migrations.CreateModel(
            name='VesselCategories',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_category_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_category_code', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_category_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'vessel_categories',
            },
        ),
        migrations.CreateModel(
            name='VesselTypes',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_type_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_type_code', models.CharField(blank=True, max_length=4, null=True)),
                ('vessel_type_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'vessel_types',
            },
        ),
        migrations.RemoveField(
            model_name='nauticaldegrees',
            name='nautical_schools',
        ),
        migrations.RemoveField(
            model_name='otherdegrees',
            name='other_schools',
        ),
        migrations.RenameField(
            model_name='civilstatuses',
            old_name='code',
            new_name='civil_status_code',
        ),
        migrations.RenameField(
            model_name='civilstatuses',
            old_name='name',
            new_name='civil_status_name',
        ),
        migrations.RenameField(
            model_name='ranks',
            old_name='code',
            new_name='rank_code',
        ),
        migrations.RenameField(
            model_name='ranks',
            old_name='department',
            new_name='rank_department',
        ),
        migrations.RenameField(
            model_name='ranks',
            old_name='description',
            new_name='rank_description',
        ),
        migrations.RenameField(
            model_name='relationshiptypes',
            old_name='code',
            new_name='relationship_code',
        ),
        migrations.RenameField(
            model_name='relationshiptypes',
            old_name='name',
            new_name='relationship_name',
        ),
        migrations.RenameField(
            model_name='taxstatuses',
            old_name='description',
            new_name='tax_description',
        ),
        migrations.RenameField(
            model_name='taxstatuses',
            old_name='name',
            new_name='tax_name',
        ),
        migrations.RemoveField(
            model_name='cities',
            name='states',
        ),
        migrations.RemoveField(
            model_name='states',
            name='countries',
        ),
        migrations.RemoveField(
            model_name='taxstatuses',
            name='date_created',
        ),
        migrations.AddField(
            model_name='cities',
            name='city_state',
            field=models.ForeignKey(db_column='city_state', default='', on_delete=django.db.models.deletion.DO_NOTHING, to='test_people.States'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='states',
            name='state_country',
            field=models.ForeignKey(db_column='state_country', default=0, on_delete=django.db.models.deletion.DO_NOTHING, to='test_people.Countries'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taxstatuses',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', default=0, on_delete=django.db.models.deletion.DO_NOTHING, related_name='tax_status_updated_by', to='bio.Bio'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cities',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='city_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='civilstatuses',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='civil_status_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='countries',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='country_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='ranks',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='rank_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='relationshiptypes',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='relationship_type_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='states',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='state_updated_by', to='bio.Bio'),
        ),
        migrations.AlterField(
            model_name='taxstatuses',
            name='date_updated',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 16, 6, 20, 38, 556820, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='NauticalDegrees',
        ),
        migrations.DeleteModel(
            name='NauticalSchools',
        ),
    ]
