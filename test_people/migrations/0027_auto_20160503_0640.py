# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-03 06:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0026_auto_20160429_1004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobilenetworkprefixes',
            name='prefix',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
