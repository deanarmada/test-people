# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-05 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0029_auto_20160505_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='municipality',
            name='municipality',
            field=models.CharField(default=None, max_length=80),
        ),
    ]
