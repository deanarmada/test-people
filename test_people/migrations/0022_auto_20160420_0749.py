# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-04-20 07:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0021_auto_20160419_0938'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='provinces',
            options={'ordering': ['province']},
        ),
        migrations.RenameField(
            model_name='provinces',
            old_name='provinces',
            new_name='province',
        ),
    ]
