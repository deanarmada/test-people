# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-07-04 05:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0039_uidrange_is_hellespont_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='schools',
            name='school_code',
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
    ]
