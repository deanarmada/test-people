# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-14 06:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0013_emailnotificationtemplate_template'),
    ]

    operations = [
        migrations.AddField(
            model_name='options',
            name='option_sys_status',
            field=models.CharField(blank=True, max_length=45, null=True),
        ),
        migrations.AddField(
            model_name='options',
            name='option_value',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
