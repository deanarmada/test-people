# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-04 05:52
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('test_people', '0010_relationshiptypes_relationship_type_sys_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='degrees',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 3, 4, 5, 50, 0, 385675, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='degrees',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='degree_updated_by', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='accesscontrol',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='advanceprovider',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='advanceprovider',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='advance_provider_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='agents',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='agents',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='agent_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='banks',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='banks',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='bank_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='cities',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='cities',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='city_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='civilstatuses',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='civilstatuses',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='civil_status_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='components',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='components',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='component_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='componentscales',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='componentscales',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='component_scale_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='countries',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='countries',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='country_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='exchange',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 3, 4, 5, 50, 45, 15058, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='exchange',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='exchange_updated_by', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='manningagents',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='manningagents',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='manning_agent_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='money',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='money',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='money_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='officialsigners',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='officialsigners',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='official_signer_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='rankgroupdetails',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 3, 4, 5, 51, 29, 148272, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rankgroupdetails',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='rank_group_detail_updated_by', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rankgroups',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='rankgroups',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='rank_group_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='schools',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='schools',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='school_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='ssstaxfacts',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='ssstaxfacts',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='sss_facts_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='states',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='states',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='state_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='taxcategories',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='taxcategories',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='tax_category_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='taxstatuses',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='taxstatuses',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='tax_status_updated_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='vesselcategories',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2016, 3, 4, 5, 52, 32, 284273, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vesselcategories',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='vessel_category_updated_by', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vesseltypes',
            name='date_updated',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='vesseltypes',
            name='updated_by',
            field=models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, related_name='vessel_type_updated_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
