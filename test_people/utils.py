# class PeopleAccess():
# 	from test_people.models import GroupAccess, AccessControl

# 	def has_permission(self, obj):
# 		pass

from rest_framework.authentication import SessionAuthentication 

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening



def password_generator(size=8):
	import string
	from random import choice
	characters = string.digits + string.ascii_letters # + string.punctuation  + string.digits
	return "".join(choice(characters) for x in range(size))

'''
	Author : Allan Bernabe
	Parameter : Serializer object
	Description: Get rules on Django searializer class and convert it to a dictionary
'''
def rules_generator(obj):
    #parameter: Django REST Serializer
    fields = {}

    for key, value in obj.fields.items():
    	field_type = value.__class__.__name__
    	field_type_arg = value.__dict__
    	try:
    		field_type_arg.pop('error_messages')
    		field_type_arg.pop('source_attrs')
    		field_type_arg.pop('default')
    		field_type_arg.pop('initial')
    		field_type_arg.pop('label')
    		field_type_arg.pop('help_text')
    		field_type_arg.pop('read_only')
    		field_type_arg.pop('field_name')
    		field_type_arg.pop('write_only')
    		field_type_arg.pop('_kwargs')
    		field_type_arg.pop('_creation_counter')
    		field_type_arg.pop('_args')
    		field_type_arg.pop('source')
    		field_type_arg.pop('parent')
    		field_type_arg.pop('style')
    		field_type_arg.pop('_validators')
    		field_type_arg.pop('_context')
    		field_type_arg.pop('partial')
    	except KeyError:
    		pass
    	
    	
    	fields.update({
    		key: {
    			'rules' : field_type_arg
    		}	
		})

    return fields

'''
	Author : Allan Bernabe
	Parameter : Dictionary
	e.g: filters = {
		'field1' : 'value1',
		'~|field2' : 'value2',
		'&~|field3' : 'value3',
		'1~|field4' : 'value4',
		'2&~|field5' : 'value5',
	}
	#produces SQL : WHERE (NOT field4 = value4 AND NOT field5 = value5) AND NOT field3 = value3 OR NOT field3 = value 3 OR field1 = value 1
	Description: Set "Where" statement using Q objects. It can combine "Or" or "And" statement
'''
def filter_builder(filters):
	#python
	import re, collections
	#django
	from django.db.models import Q

	query = Q() #default filtering
	condition = {
		'&~' : 'AND',
		'&' : 'AND',
		'~' : 'OR'
	}

	if filters == None:
		return query 

	# List of dictionary container
	queries = []
	filters = collections.OrderedDict(sorted(filters.items())) #sort
	#print(filters)
	#return

	#collections.OrderedDict(sorted(d.items()))

	for key, value in filters.items():
		key = re.search('(\d)(&~|~|&)(\|)(\w+)|(\d)(\|)(\w+)|(\w+)|(~)(\|)(\w+)', key)
		if key == None:
			queries.append({'Q' : query, 'condition' : 'OR'}) #Send default
			break

		#TODO Optimize
		if '~' == key.group(2) or '&~' == key.group(2): #(not "or" or "and") with priority 1~|field, 2&~|field
			queries.append({'Q' : ~Q(**{key.group(4):value}), 'condition' : condition.get(key.group(2))  })
		elif '&' == key.group(2): # (and) with priority e.g. 1&|field
			queries.append({'Q' : Q(**{key.group(4):value}), 'condition' : condition.get(key.group(2)) })
		elif key.group(7) != None: #with priority e.g. 1|field
			queries.append({'Q' : Q(**{key.group(7):value}), 'condition' : 'OR'})
		elif key.group(8) != None: #without priority e.g field
			queries.append({'Q' : Q(**{key.group(8):value}), 'condition' : 'OR'})
		elif '~' == key.group(9): # (not) without priority e.g ~|field
			queries.append({'Q' : ~Q(**{key.group(11):value}), 'condition' : 'OR'})

	#queries = [Q(**{key:value}) for key, value in filters.items()] #reference

    # Take first Q object from the list
	query = queries.pop(0).get('Q')
    
    # "Or" or "And" the Q object with the ones remaining in the list
	for item in queries:
		if item.get('condition') == 'AND':
			query &= item.get('Q')
		else:
			query |= item.get('Q')

	return query

def tree_builder(model, queryset, defers):
	import json, datetime
	fields = model._meta.get_all_field_names()

	for defer in defers:
	    fields.remove(defer)

	def recursive_node_to_dict(node, count =0):
	    result = {}

	    for field in fields:
	        try:
	            value = getattr(node, field)
	            if (isinstance(value, datetime.date)):
	                value = value.strftime('%Y-%m-%d %H:%M')

	            if (field == 'updated_by'):
	                try:
	                    user_code = value.bio.user_code
	                except:
	                    user_code = value.username

	                value = {
	                    'user_code' : user_code,
	                    'first_name' : value.first_name,
	                    'last_name' : value.last_name,
	                }

	            result.update({
	                field: value
	            })
	        except:
	            continue

	    children = [recursive_node_to_dict(c) for c in node.get_children()]
	    if children:
	    	result['children'] = children
	    
	    return result
	
	root_nodes = queryset.get_descendants(include_self=True).get_cached_trees()
	#root_nodes = queryset

	dicts = []
	for n in root_nodes:
	    dicts.append(recursive_node_to_dict(n))

	return dicts