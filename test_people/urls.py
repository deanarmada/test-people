"""test_people URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings

from login.views import index, log_out, validation, testcodes

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # START authentication URLS
    url(r'^$', index, name='home'),
    url(r'^testcodes/$', testcodes, name='testcodes'),
    url(r'^logout/$', log_out, name='logout'),
    url(r'^validation/$', validation, name='validation'),
    # END authentication URLS

    # START first level apps
    # url(r'^testing-sample-app/', include('testing_sample_app.urls')),
    url(r'^crew-menu/', include('crew_menu.urls')),
    # END first level apps

    # START api level apps
    url(r'^api/people/', include('test_people.api.urls')),
    # END api level apps
]

# activating media urls
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)