from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, widgets, fields

from . models import *
from crew_menu.bio.models import *

class NullImporter(widgets.ForeignKeyWidget):
	def __init__(self, *args, **kwargs):
		self.extra = kwargs.pop('extra', {})
		super(NullImporter, self).__init__(*args, **kwargs)

	def clean(self, value):
		val = value
		# print(val)
		# print(type(val))
		# print('xxxxxxxxxxx')
		if val in [None, '']:
			val = ''
			return None

		if self.extra.get('row', None) is not None:
			row = self.extra.get('row').get_row()
			filters = {}
			print(row) #Bug? you need to print?
			#print('---------') #Bug? you need to print?
			for key, value in self.extra.get('kwargs').items():
				filters.update({key : row[value]})
			print(filters)
			return self.model.objects.get(**filters)

		return self.model.objects.get(**{self.field: val})

class FieldTest(fields.Field):
	def __init__(self, *args, **kwargs):
		self.data = None
		super(FieldTest, self).__init__(*args, **kwargs)

	def clean(self, data):
		self.data = data
		return data

	def get_row(self):
		return self.data


class CountriesResource(resources.ModelResource):
	class Meta:
		model = Countries

class CountriesImport(ImportExportModelAdmin):
	resource_class = CountriesResource


class RegionResource(resources.ModelResource):
	class Meta:
		model = Regions

class RegionImport(ImportExportModelAdmin):
	resource_class = RegionResource

class MunicipalityResource(resources.ModelResource):
	province = fields.Field(column_name='province', attribute='province', widget=NullImporter(Provinces, 'province', ))

	class Meta:
		model = Municipality
		field = ['id', 'municipality']

class MunicipalityImport(ImportExportModelAdmin):
	resource_class = MunicipalityResource
	list_display = ('municipality',)

class BarangayResource(resources.ModelResource):
	row = FieldTest(column_name='barangay', attribute='barangay',) #hack to get value? from a row?
	extra = {
		'row' : row,
		'kwargs' : {
			'barangay' : 'barangay',
			'municipality__municipality' : 'municipality',						
			'municipality__province__province' : 'province',						
		}
	}

	municipality = fields.Field(column_name='municipality', attribute='municipality', widget=NullImporter(Municipality, 'municipality', extra=extra))

	class Meta:
		model = Barangay

class BarangayImport(ImportExportModelAdmin):
	resource_class = BarangayResource
	list_per_page = 2010

class ZipResource(resources.ModelResource):
	row = FieldTest(column_name='zip', attribute='zip',) #hack to get value? from a row?
	extra = {
		'row' : row,
		'kwargs' : {
			'municipality' : 'municipality',			
			'province__province' : 'province',						
		}
	}

	extra_2 = {
		'row' : row,
		'kwargs' : {
			'municipality__province__province' : 'province',			
			'municipality__municipality' : 'municipality',			
			'barangay' : 'barangay'			
		}
	}
	
	municipality = fields.Field(column_name='municipality', attribute='municipality', widget=NullImporter(Municipality, 'municipality', extra=extra))
	barangay = fields.Field(column_name='barangay', attribute='barangay', widget=NullImporter(Barangay, 'barangay', extra=extra_2))

	class Meta:
		model = Zip

class ZipImport(ImportExportModelAdmin):
	resource_class = ZipResource

class ProvinceResource(resources.ModelResource):
	region = fields.Field(column_name='region', attribute='region', widget=NullImporter(Regions, 'region', ))

	class Meta:
		model = Provinces

class ProvinceImport(ImportExportModelAdmin):
	resource_class = ProvinceResource

class EmployeeTypesResource(resources.ModelResource):
	class Meta:
		model = EmployeeTypes

class EmployeeTypesImport(ImportExportModelAdmin):
	resource_class = EmployeeTypesResource

class EmploymentTypesResource(resources.ModelResource):
	class Meta:
		model = EmploymentTypes

class EmploymentTypesImport(ImportExportModelAdmin):
	resource_class = EmploymentTypesResource

class RanksResource(resources.ModelResource):
	class Meta:
		model = Ranks

class RanksImport(ImportExportModelAdmin):
	resource_class = RanksResource

class NameSuffixesResource(resources.ModelResource):
	class Meta:
		model = NameSuffixes

class NameSuffixesImport(ImportExportModelAdmin):
	resource_class = NameSuffixesResource

class BloodTypesResource(resources.ModelResource):
	class Meta:
		model = BloodTypes

class BloodTypesImport(ImportExportModelAdmin):
	resource_class = BloodTypesResource

class ReligionsResource(resources.ModelResource):
	class Meta:
		model = Religions

class ReligionsImport(ImportExportModelAdmin):
	resource_class = ReligionsResource

class CivilStatusesResource(resources.ModelResource):
	class Meta:
		model = CivilStatuses

class CivilStatusesImport(ImportExportModelAdmin):
	resource_class = CivilStatusesResource

class TaxStatusesResource(resources.ModelResource):
	class Meta:
		model = TaxStatuses

class TaxStatusesImport(ImportExportModelAdmin):
	resource_class = TaxStatusesResource

class AreaCodesResource(resources.ModelResource):
	country = fields.Field(column_name='country', attribute='country', widget=NullImporter(Countries, 'country_name', ))
	class Meta:
		model = AreaCodes

class AreaCodesImport(ImportExportModelAdmin):
	resource_class = AreaCodesResource

class MobileNetworksResource(resources.ModelResource):
	class Meta:
		model = MobileNetworks

class MobileNetworksImport(ImportExportModelAdmin):
	resource_class = MobileNetworksResource

class MobileNetworkPrefixesResource(resources.ModelResource):
	network = fields.Field(column_name='network', attribute='network', widget=NullImporter(MobileNetworks, 'network', ))
	class Meta:
		model = MobileNetworkPrefixes

class MobileNetworkPrefixesImport(ImportExportModelAdmin):
	resource_class = MobileNetworkPrefixesResource

class LanguagesResource(resources.ModelResource):
	class Meta:
		model = Languages

class LanguagesImport(ImportExportModelAdmin):
	resource_class = LanguagesResource

class DialectsResource(resources.ModelResource):
	language = fields.Field(column_name='language', attribute='language', widget=NullImporter(Languages, 'language', ))
	class Meta:
		model = Dialects

class DialectsImport(ImportExportModelAdmin):
	resource_class = DialectsResource

class HobbiesResource(resources.ModelResource):
	class Meta:
		model = Hobbies

class HobbiesImport(ImportExportModelAdmin):
	resource_class = HobbiesResource

class TitlesResource(resources.ModelResource):
	class Meta:
		model = Titles

class TitlesImport(ImportExportModelAdmin):
	resource_class = TitlesResource

class RelationshipTypesResource(resources.ModelResource):
	class Meta:
		model = RelationshipTypes

class RelationshipTypesImport(ImportExportModelAdmin):
	resource_class = RelationshipTypesResource

class DegreesResource(resources.ModelResource):
	class Meta:
		model = Degrees

class DegreesImport(ImportExportModelAdmin):
	resource_class = DegreesResource

class SchoolsResource(resources.ModelResource):
	class Meta:
		model = Schools

class SchoolsImport(ImportExportModelAdmin):
	resource_class = SchoolsResource

class SocialMediaResource(resources.ModelResource):
	class Meta:
		model = SocialMedia

class SocialMediaImport(ImportExportModelAdmin):
	resource_class = SocialMediaResource

# Register your models here.
admin.site.register(Ranks, RanksImport)
admin.site.register(EmailNotificationTemplate)
admin.site.register(CivilStatuses, CivilStatusesImport)
admin.site.register(TaxStatuses, TaxStatusesImport)
admin.site.register(RelationshipTypes, RelationshipTypesImport)
admin.site.register(Countries, CountriesImport)
admin.site.register(Municipality, MunicipalityImport)
admin.site.register(Barangay, BarangayImport)
admin.site.register(Zip, ZipImport)
admin.site.register(Regions, RegionImport)
admin.site.register(Provinces, ProvinceImport)
admin.site.register(EmployeeTypes, EmployeeTypesImport)
admin.site.register(EmploymentTypes, EmploymentTypesImport)
admin.site.register(NameSuffixes, NameSuffixesImport)
admin.site.register(BloodTypes, BloodTypesImport)
admin.site.register(Religions, ReligionsImport)
admin.site.register(AreaCodes, AreaCodesImport)
admin.site.register(MobileNetworks, MobileNetworksImport)
admin.site.register(MobileNetworkPrefixes, MobileNetworkPrefixesImport)
admin.site.register(Languages, LanguagesImport)
admin.site.register(Hobbies, HobbiesImport)
admin.site.register(Dialects, DialectsImport)
admin.site.register(Titles, TitlesImport)
admin.site.register(Degrees, DegreesImport)
admin.site.register(Schools, SchoolsImport)
admin.site.register(SocialMedia, SocialMediaImport)
admin.site.register(UIDRange)