"""
Django settings for test_people project.

Generated by 'django-admin startproject' using Django 1.9.1.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_xj(v*rt^a!bep%q+bks^r%nsbhsiz+q3h942u$#x)h*&fqtze'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # START third-party packages app
    'rest_framework',
    'rest_framework.authtoken',
    'ckeditor', # used for flexible editing in Text Field
    'import_export', # used on import and exporting countries
    # END third-party packages app
    # START django manage.py built apps
    'crew_menu',
    'crew_menu.bio',
    'crew_menu.vessels',
    'crew_menu.crew_onboard_list',
    'crew_menu.crew_admin',
    'crew_menu.crew_admin.rank_management',
    'crew_menu.crew_admin.user_management',
    'crew_menu.crew_admin.principal_management',
    'crew_menu.crew_admin.vessel_type_management',
    # 'testing_sample_app',
    'login',
    'test_people',
    # 'djangular', # django-angular form validation
    # END django manage.py built apps
    # START extra-apps
    ## global_declarations
    # END extra-apps
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'test_people.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.media', # To Add Media URLS on templates
            ],
        },
    },
]

WSGI_APPLICATION = 'test_people.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'people',                      # Or path to database file if using sqlite3.
        'USER': 'postgres',
        'PASSWORD': 'sample',
        'HOST': 'localhost',                      # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
        'PORT': '', 
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

# UNCOMMENT STATIC ROOT to run collectstatic
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# COMMENT STATIC_PATH to run collecstatic
STATIC_PATH = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# COMMENT STATICFIELS_DIRS to run collecstatic
STATICFILES_DIRS = (STATIC_PATH, )

# Used in defining date formats
# Syntax tells that the defined date formats file is in /formats/en/formats.py 
FORMAT_MODULE_PATH = [ 'formats', ]

# Default Rest FrameWork Settings 
REST_FRAMEWORK = {
    # Output: 2016-02-19 03:50
    'DATETIME_FORMAT': '%Y-%m-%d %H:%M',
    # Output: 2016-02-19
    'DATE_FORMAT': '%Y-%m-%d',
    # Input in this format: 2016-02-19
    'DATE_INPUT_FORMATS': ('%Y-%m-%d', ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        #'rest_framework.authentication.SessionAuthentication'
    )
}

# LOGIN_URL = '/?error=Session Expired, Please login again'
LOGIN_URL = '/'

# START CUSTOM EMAIL CONFIGURATIONS
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'deanarmada@gmail.com'
EMAIL_HOST_PASSWORD = 'd3@narmada13'
DEFAULT_FROM_EMAIL = 'deanarmada@gmail.com'
DEFAULT_TO_EMAIL = 'deanarmada@gmail.com'

CSRF_COOKIE_SECURE = False