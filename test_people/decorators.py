#Rest Framework
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

def token_required(func):
    def inner(request, *args, **kwargs):
        if request.method == 'OPTIONS':
            return func(request, *args, **kwargs)
        auth_header = request.META.get('HTTP_AUTHORIZATION', None)
        if auth_header is not None:
            tokens = auth_header.split(' ')
            if tokens[0] == 'Token':
                token = tokens[1]
                try:
                    request.token = Token.objects.select_related('user').get(key=token)
                    return func(request, *args, **kwargs)
                except Token.DoesNotExist:
                     return Response({
                         'error': 'Token not found'
                     }, status=401)

        return Response({
            'error': 'Invalid Header'
        }, status=401)
    return inner

