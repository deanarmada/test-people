from rest_framework import serializers
from test_people.models import *

from test_people.api.users.serializers import *

class RankSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter
		super(RankSerializer, self).__init__(*args, **kwargs)
		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

	rank_order = serializers.IntegerField(max_value=99)
	rank_code = serializers.CharField(min_length=2, max_length=3)
	rank_description = serializers.CharField(max_length=24)
	updated_by = serializers.SerializerMethodField()

	def validate_rank_code(self, value):
		rank = Ranks.objects.filter(~Q(rank_sys_status='D'), ~Q(rank_sys_status=''), rank_code = value.upper()) #not D and not empty string 
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			rank = rank.filter(~Q(id = self.instance.id))

		if len(rank) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def create(self, validated_data):
		query_dict = self.initial_data
		data = {}

		for x in query_dict:
			data[x] = query_dict[x]

		data['updated_by']= User.objects.get(id=query_dict['updated_by'])
		data['rank_code'] = query_dict.get('rank_code', '').upper()
		return Ranks.objects.create(**data)

	def update(self,  instance, validated_data):
		query_dict = self.initial_data
		
		for x in query_dict:
			if x == 'updated_by':
				setattr(instance, x, User.objects.get(id=query_dict['updated_by']))
				continue

			setattr(instance, x, query_dict[x])

		instance.rank_code = query_dict.get('rank_code', '').upper()
		instance.save()
		
		return instance

	def get_fields(self, *args, **kwargs):
	    fields = super(RankSerializer, self).get_fields(*args, **kwargs)
	    user_fields = self.context.get('user_fields', None)

	    #if (user_fields):
	    #	fields['updated_by'].Meta.fields = user_fields

	    return fields

	def get_rank_type(self, obj):
		label = {
			'O' : 'Officer',
			'R' : 'Ratings',
			'C' : 'Crew'
		}

		return label[obj.rank_type]

	def get_updated_by(self, obj):
		try:
			user_code = obj.updated_by.bio.user_code
		except:
			user_code = obj.updated_by.username

		return {
			'user_code' : user_code,
			'first_name' : obj.updated_by.first_name,
			'last_name' : obj.updated_by.last_name,
		}

	class Meta:
		model = Ranks
		fields = '__all__'