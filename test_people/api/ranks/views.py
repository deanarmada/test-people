#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.db.models import Q
from django.utils import timezone

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required

#Start Ranks API
''' Parameters:
        required:
            - rank_code
            - rank_description
            - rank_department
            - rank_type
            - rank_order
            - updated_by 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        rank = RankSerializer(data=request.data, context={'user_fields': ('first_name', 'last_name')})
        if rank.is_valid():
            rank.save()

            return Response({
                'errors' : [],
                'data' : rank.data,
            })

        return Response({
            'errors' : rank.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        try:
            #rank = tuple(Ranks.objects.filter(pk=id).values()) #can be a way; keep as reference
            rank = Ranks.objects.filter(pk=id)
            rank = RankSerializer(rank, many=True, context={'user_fields': ('first_name', 'last_name')})
        except Ranks.DoesNotExist:
             return Response({
                 'error': [{
                    'message': 'Object not found'
                 }]
             }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            'errors' : [],
            'data' : rank.data,
        })


''' Parameters:
        required: 
            - rank_code
            - rank_description
            - rank_department
            - rank_type
            - rank_order
            - updated_by 
        optional:
            - fields: #If you wish to update selected field(s). Request must be json format e.g. {"rank_code" : "CA"}
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    #TODO: add per field update?
    if request.method == 'POST':
        fields = request.data.get('fields')
        if fields != None:
            #update selected fields?
            try:
                rank = Ranks.objects.get(pk=request.data['id'])
                for field in fields: #loop thru each set fields
                    if field == 'updated_by': #hardcoded?
                        setattr(rank, field, User.objects.get(id=fields[field]))
                        continue

                    setattr(rank, field, fields[field])

                rank.save()

                return Response({
                    'error': [],
                    'data': [fields]
                })
            except rank.DoesNotExist:
                 return Response({
                     'error': [{
                        'message': 'Object not found' #TODO: make constant for repeating messages
                     }]
                 }, status=status.HTTP_400_BAD_REQUEST)

        rank = Ranks.objects.get(pk=request.data['id'])
        rank = RankSerializer(rank, data=request.data, context={'user_fields': ('first_name', 'last_name')})
        
        if rank.is_valid():
            rank.save()

            return Response({
                'errors' : [],
                'data' : rank.data,
            })

        return Response({
            'errors' : rank.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)


''' Parameters:
        required: None
        Optional: 
         - limit
         - offset
         - order_by: #If you wish to sort field(s). Request must be a json e.g. ["rank_code"]
         - filters: #If you wish to filter field(s). Request must be a json e.g. {
                "rank_code__contains" : "CA",
                "rank_description__contains": "CA"
            }
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def ranks(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["rank_order"] #default ordering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        ranks = Ranks.objects.filter(filter_builder(filters), Q(rank_sys_status=None) | Q(rank_sys_status='')).order_by(*order_by) #default filter active rank?
        #print (ranks.query) #See generated SQL query
        total = ranks.count()
        if limit != None or offset != None:
            ranks = ranks[int(offset):int(limit)]

        extra_field = {
            'rank_type' : serializers.SerializerMethodField(),
        }
        
        serializer = RankSerializer(ranks, many=True, extra_field = extra_field, context={'user_fields': ('first_name', 'last_name')})

        return Response({
            'errors' : [],
            'data' : serializer.data,
            'order_by' : order_by,
            'total' : total,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    if request.method == 'POST':
        rank_id = request.data.get('id')
        logged_id = request.data.get('updated_by', request.token.user.id)
        try:
            ranks = Ranks.objects.filter(id__in=rank_id)
            ranks.update(rank_sys_status = 'D', updated_by = logged_id, date_updated=timezone.now()) #TODO: get from options table
        except Ranks.DoesNotExist:
            return Response({
                'errors' : [{
                    'message' : 'Rank doesn\'t exists'
                }],
                'data' : [],
            }, status=status.HTTP_400_BAD_REQUEST)

        ranks = RankSerializer(ranks, many=True, context={'user_fields': ('first_name', 'last_name')})

        return Response({
            'errors' : [],
            'data' : ranks.data
        })
        
@api_view(['GET'])
@csrf_exempt
def rules(request):
    if request.method == 'GET':
        from test_people.utils import rules_generator

        return Response({
            'errors' : [],
            'data' : rules_generator(RankSerializer())
        })
#End Ranks API