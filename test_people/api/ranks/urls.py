from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.ranks, name='api_rank_list' ),
	url(r'^add/$', views.add, name='api_rank_add' ),
	url(r'^update/$', views.update, name='api_rank_update' ),
	url(r'^delete/$', views.delete, name='api_rank_delete' ),
	url(r'^detail/$', views.detail, name='api_rank_detail' ),
	url(r'^rules/$', views.rules, name='api_rank_rules' ),
]