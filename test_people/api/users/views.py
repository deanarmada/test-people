#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.utils import timezone

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required

''' Parameters:
        required:
            - username
            - user_code
            - password
            - confirm_password
            - first_name
            - last_name
            - email
            - updated_by *The ID of the user that is logged in
        optional:
            - middle_name
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))

        user = UserFormSerializer(data=request.data, context={'logged_user': logged_user})
        if user.is_valid():
            user.save()
            user_copy = user.data.copy() #copy to manipulate

            return Response({
                'errors' : [],
                'data' : user_copy,
            })

        return Response({
            'errors' : user.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)
''' Parameters:
        required:
            - username
            - user_code
            - first_name
            - last_name
            - email
            - updated_by *The ID of the user that is logged in
        optional:
            - middle_name
            - password
            - confirm_password
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    if request.method == 'POST':
        user = User.objects.extra({
            'user_code' : 'bio.user_code',
            'middle_name' : 'bio.middle_name',
            'date_updated' : 'bio.date_updated',
            'updated_by' : "'**'",
        }).select_related('bio').filter(pk=request.data['id'])
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
        fields = request.data.get('fields', None)

        if fields is not None:
            return _update(request, fields=fields, logged_user=logged_user, user=user) #update field directly

        user = UserFormSerializer(user, data=request.data, context={'logged_user': logged_user})
        if user.is_valid():
            user.save()
            user_copy = user.data.copy() #copy to manipulate

            return Response({
                'errors' : [],
                'data' : user_copy,
            })

        return Response({
            'errors' : user.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def users(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ['-is_active', 'user_code'] #default ordering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        users = User.objects.extra({
            'user_code' : 'bio.user_code',
            'middle_name' : 'bio.middle_name',
            'date_updated' : 'bio.date_updated',
            'updated_by' : "'**'",
        }).select_related('bio').filter(filter_builder(filters), bio__isnull=False).order_by(*order_by) #only select accounts thats having BIO record
        #print (users.query) #See generated SQL query

        total = len(users)
        if limit != None or offset != None:
            users = users[int(offset):int(limit)]

        extra_field = {
            'is_active' : serializers.SerializerMethodField()
        }

        users = UserFormSerializer(users, many=True, extra_field = extra_field)

        return Response({
            'errors' : [],
            'data' : users.data,
            'order_by' : order_by,
            'total' : total,
        })

@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        try:
            user = User.objects.extra({
                'user_code' : 'bio.user_code',
                'middle_name' : 'bio.middle_name',
                'date_updated' : 'bio.date_updated',
                'updated_by' : "'**'",
            }).select_related('bio').filter(id=id, bio__isnull=False) #only select accounts thats having BIO record

            user = UserFormSerializer(user, many=True)
        except User.DoesNotExist:
             return Response({
                 'error': [{
                    'message': 'Object not found'
                 }]
             }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            'errors' : [],
            'data' : user.data,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - logged_id : User logged id 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    if request.method == 'POST':
        id = request.data.get('id')
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))

        users = User.objects.extra({
            'updated_by' : "'**'",
            'user_code' : 'bio.user_code',
            'date_updated' : 'bio.date_updated'
        }).select_related('bio').filter(id__in=id)
        users.update(is_active=False)

        #No way to update related one to one field?
        for user in users:
            Bio.objects.filter(pk=user.bio.pk).update(updated_by=logged_user, date_updated=timezone.now())

        users = users.filter() #re query?

        user = UserFormSerializer(users, many=True, context={
            'logged_user': logged_user
        }, extra_field = {
            'is_active' : serializers.BooleanField()
        },fields=('id', 'user_code', 'is_active', 'updated_by', 'date_updated'))

        return Response({
            'errors' : [],
            'data' : user.data
        })        

@api_view(['GET'])
@csrf_exempt
def rules(request):
    if request.method == 'GET':
        from test_people.utils import rules_generator

        return Response({
            'errors' : [],
            'data' : rules_generator(UserFormSerializer())
        })        

def  _update(request, **kwargs):
    user = kwargs['user']
    logged_user = kwargs['logged_user']
    fields = kwargs['fields']
    password = fields.get('password', None)

    #update selected fields?
    bio_fields = ['middle_name' ,'user_code', 'updated_by']
    bio_to_update = {'date_updated' : timezone.now()} #updating dictionary
    items = fields.copy() #copy fields

    for key, value in items.items(): #must be dictionary
        if key in bio_fields:
            bio_to_update.update({key : value})
            fields.pop(key) #remove to fields

    if bio_to_update.get('updated_by', None) is not None: #support to override default logged user
        bio_to_update['updated_by'] = User.objects.get(pk=bio_to_update.get('updated_by'))
    else:
        bio_to_update.update({'updated_by' : logged_user}) #adding default

    if password is not None:
        from django.contrib.auth.hashers import make_password
        fields['password'] = make_password(password)

    user.update(**fields) #save auth user
    Bio.objects.filter(pk=user[0].bio.pk).update(**bio_to_update) #save bio

    user = UserFormSerializer(user, many=True)
    
    return Response({
        'errors' : [],
        'data' : user.data,
    })

        #FUTURE REFERENCE
        # user_data = []
        #manually create a new dictionary
        # users = [{
        #     'id' : 13,
        #     # 'bio_fields' : {
        #     #     'user_code' : 'BATA'
        #     #     'middle_name' : 'Altamira'
        #     # }
        # }] #reference

        # for item in users.values():
        #     user_code = ''
        #     middle_name = ''
        #     updated_by_first_name = ''
        #     updated_by_last_name = ''

        #     try:
        #         bio = Bio.objects.get(auth_user_id=item.get('id', 1))
        #         user_code = bio.user_code
        #         middle_name = bio.middle_name or ''
        #         updated_by_first_name = bio.updated_by.first_name
        #         updated_by_last_name = bio.updated_by.last_name
        #     except Bio.DoesNotExist:
        #         pass

        #     user_data.append({
        #         #Bio fields        
        #         'user_code' : user_code,
        #         'middle_name' : middle_name,
        #         #User fields
        #         'id' : item.get('id', 1),
        #         'first_name' : item.get('first_name', ""),
        #         'last_name' : item.get('last_name', ""),
        #         'updated_by_first_name':  updated_by_first_name, 
        #         'updated_by_last_name': updated_by_last_name 
        #     })


        # #serializer = RankSerializer(ranks, many=True, context={'user_fields': ('first_name', 'last_name')})

        # return Response({
        #     'errors' : [],
        #     'data' : user_data,
        #     'total' : total,
        # })