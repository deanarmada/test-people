#django
from django.core.validators import EMPTY_VALUES
from django.db.models import Q

#Rest Framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

#People
from test_people.utils import password_generator
from crew_menu.bio.models import *

#Model is from django admin
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class UserFormSerializer(serializers.Serializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter
		self.bio = None #default

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(UserFormSerializer, self).__init__(*args, **kwargs)

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)

	class UserCodeValidator(object):
	    def __init__(self, base):
	        self.base = base #just to run validator ??? I think its a bug
	        self.parent = None

	    def __call__(self, value):
	    	bio = Bio.objects.filter(user_code = value.lower())

	    	if self.parent.instance is not None: #means that it's in update mode
	    		#don't include current record to validate
	    		bio = bio.filter(~Q(auth_user_id = self.parent.instance[0].id))

	    	if len(bio) != 0:
	    		raise serializers.ValidationError('This field must be unique.')

	    def set_context(self, value): #Documentation on django rest framework is b*llsh*t it says parameter must be "serializers_field" but it is actually "value"
	    	self.parent = value.parent

	id = serializers.IntegerField(read_only=True)
	username = serializers.CharField(max_length=30)
	last_name = serializers.CharField(max_length=30)
	first_name = serializers.CharField(max_length=30)
	middle_name = serializers.CharField(max_length=50, required=False, allow_null=True, allow_blank=True)
	email = serializers.EmailField(max_length=50)
	user_code = serializers.CharField(max_length=4, min_length=4, validators=[UserCodeValidator(base=1)])
	updated_by = serializers.SerializerMethodField()
	date_updated = serializers.DateTimeField(read_only=True)
	is_active = serializers.BooleanField(required=False)

	# def validate_confirm_password(self, value):
	# 	password = self.initial_data.get('password', '')
	# 	if password.strip() != value.strip(): #strip() to ensure whitespaces on both end are removed
	# 		raise serializers.ValidationError("Password does not match the confirm password")
	# 	return value

	def get_is_active(self, obj):
		return 'Active' if obj.is_active else 'Inactive'
 
	def validate_username(self, value):
		user = User.objects.filter(username = value.upper())
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			user = user.filter(~Q(id = self.instance[0].id))

		if len(user) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def validate_email(self, value):
		user = User.objects.filter(email = value)
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			user = user.filter(~Q(id = self.instance[0].id))

		if len(user) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def create(self, validated_data):
		from test_people.asynchronous_mail import send_mail #import mailing class
		from test_people import settings
		#django
		from django.template.loader import render_to_string
		from django.template import Context, Template

		validated_data_copy = validated_data.copy() #future use?
		validated_data.update({'password' : password_generator(8)})

		#deleting keys that are not meant to be in auth user record
		validated_data['username'] = validated_data.get('user_code', '').lower()
		validated_data.pop("user_code", None)
		validated_data.pop("middle_name", None)
		auth_user = User.objects.create_user(**validated_data) #create new account
		
		initial_bio_data = {
			"user_code": validated_data_copy.get('user_code', '').lower(),
			"middle_name": validated_data_copy.get('middle_name', None),
			"updated_by": self.context.get('logged_user'),
			"auth_user_id": auth_user.id
		}

		#TODO: Set groups?

		#TODO: make it access by user instance e.g. auth_user.bio.create()?
		bio = Bio.objects.create(**initial_bio_data) #add initial data into Bio table
		
		# START SEND EMAIL SCRIPT
		email_notification = EmailNotificationTemplate.objects.get(template='password-notifications')
		# START HTML rendering from the database to email
		greetings_template = Template(email_notification.greetings)
		greetings = Context({'code': initial_bio_data['user_code']})
		greetings = greetings_template.render(greetings)
		message_template = Template(email_notification.message)
		message = Context({'password':validated_data.get('password')})
		message = message_template.render(message)
		
		email_data = {}
		email_data['email_greetings'] = greetings
		email_data['email_body'] = message
		# email_data serves as a dictionary with key value pairs to be used to store data fetched from the database

		msg_html = render_to_string('email-templates/password-notifications.html', email_data) #Template(html).render(Context(email_data)) #reference
		# END HTML rendering from the database to email

		#email_receievers = [validated_data.get('email')]
		email_receievers = ['dev@manship.com']
		# SEND EMAIL SYNTAX
		send_mail(greetings, '', settings.EMAIL_HOST_USER, email_receievers, fail_silently=False, html_message=msg_html)
		# END SEND EMAIL SCRIPT

		extra = {
			'user_code': "bio.user_code",
			'middle_name' : "bio.middle_name",
			'date_updated' : "bio.date_updated",
		}

		return User.objects.select_related('bio').extra(select=extra).get(pk=auth_user.id)

	def update(self,  instance, validated_data):
		#from django.contrib.auth.hashers import make_password #import password maker
		validated_data_copy = validated_data.copy() #future use?
		#deleting keys that are not meant to be in auth user record
		validated_data['username'] = validated_data.get('user_code', '').lower()
		validated_data.pop("user_code", None)
		validated_data.pop("middle_name", None)
		validated_data.update({'password' : instance[0].password}) #retain password
			
		auth_user = User(pk=instance[0].id, **validated_data)
		auth_user.save()

		bio = Bio.objects.get(auth_user_id=auth_user.id)

		initial_bio_data = {
			"user_code": validated_data_copy.get('user_code', '').lower(),
			"middle_name": validated_data_copy.get('middle_name', bio.middle_name),
			"updated_by": self.context.get('logged_user'),
			"auth_user_id": auth_user.pk
		}

		#TODO: Set groups?

		bio = Bio(pk=bio.id, **initial_bio_data) #updating initial data into Bio table
		bio.save()

		extra = {
			'user_code': "bio.user_code",
			'middle_name' : "bio.middle_name",
			'date_updated' : "bio.date_updated",
		}

		return User.objects.select_related('bio').extra(select=extra).get(pk=instance[0].id)

	def get_updated_by(self, obj):

		try:	
			user_code = obj.bio.updated_by.bio.user_code
		except:
			user_code = obj.bio.updated_by.username

		return {
		 	"first_name" : obj.bio.updated_by.first_name, 
		 	"last_name" : obj.bio.updated_by.last_name,
		 	"user_code" : user_code
		}

