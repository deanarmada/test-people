from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.users, name='api_user_list' ),
	url(r'^add/$', views.add, name='api_user_add' ),
	url(r'^update/$', views.update, name='api_rank_update' ),
	url(r'^delete/$', views.delete, name='api_user_delete' ),
	url(r'^detail/$', views.detail, name='api_user_detail' ),
	url(r'^rules/$', views.rules, name='api_user_rules' ),
]