from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.bio_statuses, name='api_bio_status_list' )
]