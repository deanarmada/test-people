from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.regions, name='api_region_list' )
]