#django
from django.db.models import Q
from django.utils import timezone

#Rest Framework
from rest_framework import serializers

#People BIO
from crew_menu.bio.models import *

#API country
from test_people.api.countries.serializers import *

class CommonSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(CommonSerializer, self).__init__(*args, **kwargs)

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)


	updated_by = serializers.SerializerMethodField()

	def get_updated_by(self, obj):
		try:
			user_code = obj.updated_by.bio.user_code
		except:
			user_code = obj.updated_by.username

		return {
			'user_code' : user_code,
			'first_name' : obj.updated_by.first_name,
			'last_name' : obj.updated_by.last_name,
		}

class PrincipalContactInfoSerializer(CommonSerializer):
	principal_contact_info_first_name = serializers.CharField(max_length=60,)
	principal_contact_info_middle_name = serializers.CharField(max_length=60, required=False, allow_null=True, allow_blank=True)
	principal_contact_info_designation = serializers.CharField(max_length=60,)
	principal_contact_info_email_address = serializers.EmailField(max_length=50,)
	principal_contact_info_contact_number_1 = serializers.CharField(max_length=60, )
	principal_contact_info_contact_number_2 = serializers.CharField(max_length=60,required=False, allow_null=True, allow_blank=True)
	principal_contact_info_contact_number_3 = serializers.CharField(max_length=60,required=False, allow_null=True, allow_blank=True)
	principal_contact_info_contact_number_4 = serializers.CharField(max_length=60,required=False, allow_null=True, allow_blank=True)

	class Meta:
		model = PrincipalContactInfo
		exclude = ('updated_by', 'principal_contact_info_principal')
		extra_kwargs = {
			'principal_contact_info_principal': {'read_only': True}
		}

class PrincipalCountrySerializer(CountrySerializer):
	id = serializers.IntegerField()

	class Meta:
		model = CountrySerializer().Meta().model #this is not the way I think?
		fields = ('id','country_name','country_code')
		extra_kwargs = {
			'country_name': {'read_only': True},
			'country_code': {'read_only': True},
		}		

class PrincipalAddressesSerializer(CommonSerializer):
	principal_address = serializers.CharField(max_length=75,)
	principal_address_city = serializers.CharField(max_length=50,)
	principal_address_country = PrincipalCountrySerializer()
	principal_address_state = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	principal_address_zip = serializers.IntegerField(required=False, allow_null=True,) 
	principal_address_contact_number_1 = serializers.CharField(max_length=20,)
	principal_address_contact_number_2 = serializers.CharField(max_length=20, required=False, allow_null=True, allow_blank=True)
	principal_address_contact_number_3 = serializers.CharField(max_length=20, required=False, allow_null=True, allow_blank=True)

	class Meta:
		model = PrincipalAddresses
		exclude = ('updated_by', 'principal_address_principal')
		extra_kwargs = {
			'principal_address_principal': {'read_only': True}
		}

class PrincipalMasterSerializer(serializers.ModelSerializer):
	class Meta:
		model = Principals
		fields = ('id', 'principal_code')
		extra_kwargs = {
			'principal_code': 
				{'read_only': True}, 
			'id' : 
				{'required' : False}
		}

class PrincipalSerializer(CommonSerializer):
	principal_code = serializers.CharField(min_length=3, max_length=6,)
	principal_master = PrincipalMasterSerializer()
	principal_full = serializers.CharField(max_length=50)
	principal_sss = serializers.CharField(min_length=10, max_length=10)
	principal_philhealth = serializers.CharField(min_length=12, max_length=12)
	principal_email_address = serializers.EmailField(max_length=50)
	principal_contact_infos = PrincipalContactInfoSerializer(many=True)
	principal_addresses = PrincipalAddressesSerializer(many=True)

	class Meta:
		model = Principals
		fields = '__all__'

	def get_principal_contact_infos(self, obj):
		try:
			dpa = PrincipalContactInfo.objects.get(principal_contact_info_principal_id=obj.id, principal_contact_info_designation="DPA")
			return {
				'first_name' : dpa.principal_contact_info_first_name,
				'middle_name' : dpa.principal_contact_info_middle_name,
				'last_name' : dpa.principal_contact_info_last_name,
			}
		except PrincipalContactInfo.DoesNotExist:
			return {
				'first_name' : '',
				'middle_name' : '',
				'last_name' : '',
			}

	def get_is_delete(self, obj):
		child = Principals.objects.filter(~Q(principal_sys_status='D'), principal_master=obj.id) #principal with child and status is not D
		if len(child) > 0:
			return False
		else:
			return True
		

	def validate_principal_code(self, value):
		principal = Principals.objects.filter(~Q(principal_sys_status='D'), ~Q(principal_sys_status=''), principal_code = value.upper()) #not D and not empty string 
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			principal = principal.filter(~Q(id = self.instance.id))

		if len(principal) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def create(self, data):
		data = self.fix_data(data) #get principal object instance and updated_by
		principal_contact_infos = data.pop('principal_contact_infos')
		principal_addresses = data.pop('principal_addresses')
		principal = Principals.objects.create(**data)

		#Insert contact info
		for contact_info in principal_contact_infos:
			PrincipalContactInfo.objects.create(updated_by=data.get('updated_by'), principal_contact_info_principal=principal, **contact_info)

		#Insert Addresses
		for address in principal_addresses:
			country = address.pop('principal_address_country')
			try:
				country = Countries.objects.get(id=country.get('id', None))
			except Countries.DoesNotExist:
				country = Countries.objects.get(id=1) #So we won't have invalid data

			address['principal_address_country'] = country #get country instance
			PrincipalAddresses.objects.create(updated_by=data.get('updated_by'), principal_address_principal=principal, **address)

		return principal

	def update(self,  instance, validated_data):
		i_data = self.initial_data
		data = self.fix_data(validated_data) #get principal object instance and updated_by
		#just remove these
		data.pop('principal_contact_infos') 
		data.pop('principal_addresses')

		#Get data from inital data
		principal_contact_infos = i_data.pop('principal_contact_infos') 
		principal_addresses = i_data.pop('principal_addresses')

		for principal in data: setattr(instance, principal, data[principal])
		instance.save()

		#Create, Update or delete Contacts
		for contact_info in principal_contact_infos:
			to_delete = contact_info.pop('delete', False)
			if to_delete:
				PrincipalContactInfo.objects.filter(pk=contact_info.get('id')).delete() #delete
			else:
				contact_info.update({'updated_by' : data.get('updated_by')})
				contact_info.update({'principal_contact_info_principal' : instance})
				contact_info.update({'date_updated' : timezone.now()})
				PrincipalContactInfo.objects.update_or_create(defaults=contact_info, id=contact_info.get('id', None))

		#Create, Update or delete Addresses
		for address in principal_addresses:
			to_delete = address.pop('delete', False)
			if to_delete:
				PrincipalAddresses.objects.filter(pk=address.get('id')).delete() #delete
			else:
				country = address.pop('principal_address_country')
				try:
					country = Countries.objects.get(id=country.get('id', None))
				except Countries.DoesNotExist:
					country = Countries.objects.get(id=1) #So we won't have invalid data

				address['principal_address_country'] = country #get country instance
				address.update({'updated_by' : data.get('updated_by')})
				address.update({'principal_address_principal' : instance})
				address.update({'date_updated' : timezone.now()})
				PrincipalAddresses.objects.update_or_create(defaults=address, id=address.get('id', None))

		return instance

	def fix_data(self, data):
		principal_master = self.initial_data.get('principal_master', None)
		try:
			principal_master = Principals.objects.get(id=principal_master.get('id', None))
		except Principals.DoesNotExist:
			principal_master = None

		data['principal_master'] = principal_master #Instance
		data['principal_code'] = data.get('principal_code', '').upper()
		data.update({'updated_by' : self.context.get('logged_user')})

		return data