from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.principals, name='api_principal_list' ),
	url(r'^add/$', views.add, name='api_principal_add' ),
	url(r'^update/$', views.update, name='api_principal_update' ),
	url(r'^delete/$', views.delete, name='api_principal_delete' ),
	url(r'^detail/$', views.detail, name='api_principal_detail' ),
	url(r'^rules/$', views.rules, name='api_principal_rules' ),
]