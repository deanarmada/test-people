#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes

#test_people app
from .serializers import *
from test_people.decorators import token_required

@api_view(['POST'])
@parser_classes((JSONParser, FormParser, MultiPartParser))
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        principal_logo = request.data.get('principal_logo', None)
        data = json.loads(request.data.get('data', None)) #need to convert json string to python dictionary
        data.update({'principal_logo' : principal_logo}) #add to python dictionary
        logged_user = User.objects.get(pk=data.get("updated_by", 1))

        #sample data
        # data = {
        #     "principal_master" : { "id": 22}, 
        #     "principal_code" : "HSM31",
        #     "principal_full" : "testasdasd",
        #     "principal_sss" : "123456789112",
        #     "principal_philhealth" : "12345678911234",
        #     "principal_email_address" : "allan.bernabe@gmail.com",
        #     "principal_contact_infos" : [{
        #         "principal_contact_info_first_name" : "Allan Jed",
        #         "principal_contact_info_middle_name" : "Altamira",
        #         "principal_contact_info_last_name" : "Bernabe",
        #         "principal_contact_info_email_address" : "asd@asd.com",
        #         "principal_contact_info_designation" : "DPA",
        #         "principal_contact_info_contact_number_1" : "8556654",
        #         "principal_contact_info_contact_number_2" : "2214478",
        #         "principal_contact_info_contact_number_3" : "2214478",
        #         "principal_contact_info_contact_number_4" : "2214478",
        #         "updated_by" : 1
        #     },
        #     {
        #         "principal_contact_info_first_name" : "Allan Jed 2",
        #         "principal_contact_info_middle_name" : "Altamira 2",
        #         "principal_contact_info_last_name" : "Bernabe 2",
        #         "principal_contact_info_email_address" : "asd@asd2.com",
        #         "principal_contact_info_designation" : "Alternate DPA",
        #         "principal_contact_info_contact_number_1" : "8556654",
        #         "principal_contact_info_contact_number_2" : "2214478",
        #         "principal_contact_info_contact_number_3" : "2214478",
        #         "principal_contact_info_contact_number_4" : "2214478",
        #         "updated_by" : 1
        #     }],
        #     "principal_addresses" : [{
        #         "hello" : "its me",
        #         "principal_address" : "Beim Strohhause 27",
        #         "principal_address_city" : "Hamburg",
        #         "principal_address_country" : {"id" : 1},
        #         "principal_address_state" : None,
        #         "principal_address_zip" : "20097",
        #         "principal_address_contact_number_1" : "8556654",
        #         "principal_address_contact_number_2" : "8556654",
        #         "principal_address_contact_number_3" : "8556654",
        #         "updated_by" : 1
        #     }],
        #     "updated_by" : 1
        # }

        #principal = PrincipalSerializer(data=data, context={'logged_user': logged_user}) #Uncomment this for testing
        principal = PrincipalSerializer(data=data, context={'logged_user': logged_user})
        #principal = PrincipalSerializer(Principals.objects.filter(pk=46), many=True, context={'logged_user': logged_user}) #Uncomment this for testing

        #TESTING PURPOSES
        # return Response({
        #     'errors' : [],
        #     'data' : principal.data,
        # }, status=200)

        if principal.is_valid():
            principal.save()
            return Response({
                'errors' : [],
                'data' : principal.data,
            }, status=200)
            return
        else:
            return Response({
                'errors' : principal.errors,
                'data' : [],
            }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    if request.method == 'POST':
        principal_logo = request.data.get('principal_logo', None)
        data = json.loads(request.data.get('data', '{}')) #need to convert json string to python dictionary
        if principal_logo is not None: data.update({'principal_logo' : principal_logo}) #add to python dictionary

        principal = Principals.objects.get(pk=data['id'])
        logged_user = User.objects.get(pk=data.get("updated_by", 1))
        fields = data.get('fields', None)

        if fields is not None:
            return _update(request, fields=fields, logged_user=logged_user, principal=principal) #update field directly

        #sample data
        # data = {
        #    "principal_code":"HSM30",
        #    "principal_full":"TEST",
        #    "principal_master": {
        #       "id":None
        #    },
        #    "principal_sss":"1234567892",
        #    "principal_philhealth":"123456789112",
        #    "principal_email_address":"allan.bernabe2@gmail.com",
        #    "principal_contact_infos":[
        #       {
        #          "id":37,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"DPA",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1053"
        #       },
        #       {
        #          "id":38,
        #          "principal_contact_info_first_name":"Jedaivokters",
        #          "principal_contact_info_middle_name":"Jedaivokters",
        #          "principal_contact_info_designation":"Alternate DPAs",
        #          "principal_contact_info_email_address":"jedaivokters@asd2.com",
        #          "principal_contact_info_contact_number_1":"888888",
        #          "principal_contact_info_contact_number_2":"",
        #          "principal_contact_info_contact_number_3":"888888",
        #          "principal_contact_info_contact_number_4":"",
        #          "principal_contact_info_last_name":"Jedaivokters",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1054"
        #       },
        #       {
        #          "id":40,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1055"
        #       },
        #       {
        #          "id":41,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1056"
        #       },
        #       {
        #          "id":42,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1057"
        #       },
        #       {
        #          "id":43,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1058"
        #       },
        #       {
        #          "id":44,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1059"
        #       },
        #       {
        #          "id":45,
        #          "principal_contact_info_first_name":"Allan Jed",
        #          "principal_contact_info_middle_name":"Altamira",
        #          "principal_contact_info_designation":"Manager",
        #          "principal_contact_info_email_address":"asd@asd.com",
        #          "principal_contact_info_contact_number_1":"8556654",
        #          "principal_contact_info_contact_number_2":"2214478",
        #          "principal_contact_info_contact_number_3":"2214478",
        #          "principal_contact_info_contact_number_4":"2214478",
        #          "principal_contact_info_last_name":"Bernabe",
        #          "date_updated":"2016-03-29 10:36",
                 
        #          "$$hashKey":"object:1060"
        #       }
        #    ],
        #    "principal_addresses":[
        #       {
        #          "id":5,
        #          "principal_address":"Test1",
        #          "principal_address_city":"Test1",
        #          "principal_address_country":{
        #             "id":1,
        #             "country_name":"Germany",
        #             "country_code":"DEU"
        #          },
        #          "principal_address_state":None,
        #          "principal_address_zip":999999,
        #          "principal_address_contact_number_1":"999999",
        #          "principal_address_contact_number_2":"999999",
        #          "principal_address_contact_number_3":"999999",
        #          "date_updated":"2016-03-29 10:36",
        #          "$$hashKey":"object:1069"
        #       }
        #    ],
        #    "updated_by":"40",
        #    "id":48
        # }

        principal = PrincipalSerializer(principal, data=data, context={'logged_user': logged_user})
        #principal = PrincipalSerializer(principal, data=data, context={'logged_user': logged_user} )#defers=('principal_contact_infos', 'principal_addresses'))

        if principal.is_valid():
            principal.save()

            return Response({
                'errors' : [],
                'data' : principal.data,
            })

        return Response({
            'errors' : principal.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def principals(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["-id"] #default ordering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:      
            filters = request.data.get('filters')

        principals = Principals.objects.filter(filter_builder(filters), Q(principal_sys_status=None) | Q(principal_sys_status='')).order_by(*order_by)
        #print (principals.query) #See generated SQL query

        total = len(principals)
        if limit != None and offset != None:
            principals = principals[int(offset):int(limit)]

        principals = PrincipalSerializer(principals, many=True, extra_field = {
            'principal_contact_infos' : serializers.SerializerMethodField(),
            'is_delete' : serializers.SerializerMethodField()
        }, defers = ('principal_addresses', ))

        return Response({
            'errors' : [],
            'data' : principals.data,
            'order_by' : order_by,
            'total' : total,
        })
        
''' Parameters:
        required:
         - id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        principal = Principals.objects.filter(id=id)
        principal = PrincipalSerializer(principal, many=True, extra_field = {
            'principal_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : principal.data,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - updated_by : User logged id 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    if request.method == 'POST':
        id = request.data.get('id')
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
        principal = Principals.objects.filter(id__in=id)

        #TODO: Add some class for dependency checker
        #Check for dependency
        for p in principal:
            child = Principals.objects.filter(~Q(principal_sys_status='D'), principal_master=p.id) #principal with child and status is not D
            if len(child) > 0:
                return Response({
                    'errors' : ['This record has a dependency record'],
                    'data' : []
                }, status=status.HTTP_400_BAD_REQUEST)
                break

        principal.update(principal_sys_status='D', updated_by=logged_user, date_updated=timezone.now())

        principal = PrincipalSerializer(principal, many=True, fields=('id', 'principal_code', 'principal_sys_status', 'updated_by', 'date_updated'), extra_field = {
            'principal_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : principal.data
        })

@api_view(['GET'])
@csrf_exempt
def rules(request):
    if request.method == 'GET':
        from test_people.utils import rules_generator

        principal = PrincipalSerializer(defers=('id','principal_master', 'principal_contact_infos', 'principal_addresses', 'principal_logo', 'updated_by', 'date_updated'))
        principal_contact = PrincipalContactInfoSerializer(defers=('id', 'updated_by', 'date_updated'))
        principal_address = PrincipalAddressesSerializer(defers=('id','principal_address_country', 'updated_by', 'date_updated'))

        data = {
            'errors' : [],
            'data' : rules_generator(principal)
        }

        data['data'].update(rules_generator(principal_contact))
        data['data'].update(rules_generator(principal_address))

        return Response(data)


def  _update(request, **kwargs):
    logged_user = kwargs['logged_user']
    fields = kwargs['fields']
    principal = kwargs['principal']

    fields.update({
        'updated_by' : logged_user,
        'date_updated' : timezone.now()
    })

    Principals.objects.filter(id=principal.id).update(**fields)
    principal = PrincipalSerializer(principal, extra_field = {
        'principal_sys_status' : serializers.CharField(read_only=True)
    })

    #TODO: Support principal contacts and addresses
    
    return Response({
        'errors' : [],
        'data' : principal.data,
    })