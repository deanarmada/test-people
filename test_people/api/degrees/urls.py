from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.degrees, name='api_degree_list' )
]