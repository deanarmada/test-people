from django.conf.urls import include, url

from . import views

urlpatterns = [
	#Start Authentication API endpoint
	url(r'^v1/login/$', views.login, name='api_login' ),
	url(r'^v1/logout/$', views.logout, name='api_logout' ),
	url(r'^v1/ranks/', include('test_people.api.ranks.urls')),
	url(r'^v1/users/', include('test_people.api.users.urls')),
	url(r'^v1/principals/', include('test_people.api.principals.urls')),
	url(r'^v1/vessel_types/', include('test_people.api.vessel_types.urls')),
	url(r'^v1/relationship_types/', include('test_people.api.relationship_types.urls')),
	#url(r'^v1/related_people/', include('test_people.api.related_people.urls')),
	url(r'^v1/area_codes/', include('test_people.api.area_codes.urls')),
	url(r'^v1/barangays/', include('test_people.api.barangays.urls')),
	url(r'^v1/bio_statuses/', include('test_people.api.bio_statuses.urls')),
	url(r'^v1/blood_types/', include('test_people.api.blood_types.urls')),
	url(r'^v1/civil_statuses/', include('test_people.api.civil_statuses.urls')),
	url(r'^v1/employee_types/', include('test_people.api.employee_types.urls')),
	url(r'^v1/employment_types/', include('test_people.api.employment_types.urls')),
	url(r'^v1/countries/', include('test_people.api.countries.urls')),
	url(r'^v1/degrees/', include('test_people.api.degrees.urls')),
	url(r'^v1/dialects/', include('test_people.api.dialects.urls')),
	url(r'^v1/hobbies/', include('test_people.api.hobbies.urls')),
	url(r'^v1/languages/', include('test_people.api.languages.urls')),
	url(r'^v1/mobile_networks/', include('test_people.api.mobile_networks.urls')),
	url(r'^v1/mobile_network_prefixes/', include('test_people.api.mobile_network_prefixes.urls')),
	url(r'^v1/municipalities/', include('test_people.api.municipalities.urls')),
	url(r'^v1/name_suffixes/', include('test_people.api.name_suffixes.urls')),
	url(r'^v1/provinces/', include('test_people.api.provinces.urls')),
	url(r'^v1/regions/', include('test_people.api.regions.urls')),
	url(r'^v1/religions/', include('test_people.api.religions.urls')),
	url(r'^v1/schools/', include('test_people.api.schools.urls')),
	url(r'^v1/separation_types/', include('test_people.api.separation_types.urls')),
	url(r'^v1/tax_statuses/', include('test_people.api.tax_statuses.urls')),
	url(r'^v1/titles/', include('test_people.api.titles.urls')),
	url(r'^v1/bio/', include('test_people.api.bio.urls')),
	url(r'^v1/options/$', views.options, name='api_options' ),
	url(r'^v1/social_medias/', include('test_people.api.social_medias.urls')),
	url(r'^v1/uid_range/', include('test_people.api.uid_range.urls')),
	url(r'^v1/zips/', include('test_people.api.zips.urls')),
	url(r'^v1/test/$', views.test, name='api_test' )
]