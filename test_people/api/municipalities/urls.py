from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.municipalities, name='api_municipality_list' )
]