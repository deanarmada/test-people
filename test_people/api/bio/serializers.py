#People Serializer
from test_people.api.serializers import *
from test_people.api.related_people.serializers import *
from test_people.api.dialects.serializers import DialectsSerializer



class BioEducationsSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	bio_education_type = serializers.CharField(max_length=20,)
	bio_education_date_completed = serializers.IntegerField(max_value=9999, allow_null=True)

	class Meta:
		model = BioEducations
		exclude = ('updated_by', 'bio_education_bio')
		extra_kwargs = {
			'bio_education_bio': {'read_only': True}
		}

class BioAddressSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	bio_fil_address = UppercaseCharField(max_length=75,)
	bio_fil_address_type = serializers.CharField(max_length=20,)

	class Meta:
		model = BioFilipinoAddress
		exclude = ('updated_by', 'bio_fil_bio')
		extra_kwargs = {
			'bio_fil_bio': {'read_only': True}
		}

class BioTelephonesSerializer(CommonSerializer):
	def to_internal_value(self, data, *args, **kwargs):
		if data.get('bio_tel_number') is None or data.get('bio_tel_number') == '':
			data['bio_tel_area_code'] = None
			return data #return as bio_tel_area_code = None

		data = super(BioTelephonesSerializer, self).to_internal_value(data) # Get validated data and return it
		
		return data


	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	#bio_tel_area_code = serializers.IntegerField()
	bio_tel_number = serializers.IntegerField(required=False, max_value=999999999999999)

	class Meta:
		model = BioTelephones
		exclude = ('updated_by', 'bio_tel_bio')
		extra_kwargs = {
			'bio_tel_bio': {'read_only': True}
		}


class BioMobilesSerializer(CommonSerializer):
	def to_internal_value(self, data, *args, **kwargs):
		if data.get('bio_mobile_number') is None or data.get('bio_tel_number') == '':
			data['bio_mobile_prefix'] = None
			return data #return as bio_mobile_prefix = None

		data = super().to_internal_value(data) # Get validated data and return it
		
		return data


	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	bio_mobile_number = serializers.IntegerField(max_value=999999999999999)

	class Meta:
		model = BioMobiles
		exclude = ('updated_by', 'bio_mobile_bio')
		extra_kwargs = {
			'bio_mobile_bio': {'read_only': True}
		}

class BioHobbiesSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	hobby = serializers.CharField(required=False, max_length=100)

	class Meta:
		model = BioHobbies
		exclude = ('updated_by', 'bio_hobby_bio')
		extra_kwargs = {
			'bio_hobby_bio': {'read_only': True}
		}

class BioSocialMediaSerializer(CommonSerializer):
	def to_internal_value(self, data, *args, **kwargs):
		if data.get('bio_social_id') is None or data.get('bio_social_id') == '':
			data['bio_social_media'] = None
			return data #return as bio_social_media = None

		data = super().to_internal_value(data) # Get validated data and return it
		
		return data

	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	bio_social_id = serializers.CharField(max_length=75,)

	class Meta:
		model = BioSocialMedia
		exclude = ('updated_by', 'bio_social_bio')
		extra_kwargs = {
			'bio_social_bio': {'read_only': True}
		}

class BioLanguageDialectsSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word

	class Meta:
		model = BioLanguageDialects
		exclude = ('updated_by', 'bio_lang_dialect_language')

class BioLanguagesSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	language = serializers.CharField(required=False, max_length=100)
	bio_dialects = BioLanguageDialectsSerializer(many=True, required=True)

	class Meta:
		model = BioLanguages
		exclude = ('updated_by', 'bio_language_bio')
		extra_kwargs = {
			'bio_language_bio': {'read_only': True}
		}


class BioSerializer(CommonSerializer):
	import string

	def auto_user_code(self, data, next=0):
		alphabet = list(self.string.ascii_lowercase)
		try:
			middle_name = data['middle_name'][0]
			if data['middle_name'] == None:
				middle_name = 'x'
		except:
			middle_name = 'x'

		try:
			user_code = data['last_name'][0] + data['first_name'][0] + middle_name + alphabet[next]
		except IndexError:
			return data['last_name'][0] + data['first_name'][0] + middle_name + 'a' #back to a, it will be validated eventually

		bio = Bio.objects.filter(user_code = user_code.lower()).filter(~Q(bio_sys_status = 'D'))
		if len(bio) != 0:
			return self.auto_user_code(data, next+1)

		return user_code

	#All fields
	def to_internal_value(self, data, *args, **kwargs):
		#Check if is in create mode
		if self.instance is None:
			#Do auto suggest here
			data['user_code'] = self.auto_user_code(data)

		data = super(BioSerializer, self).to_internal_value(data) # Get validated data and return it
		
		return data

	user_id = serializers.IntegerField()
	availability_date = DateField(required=False, allow_null=True, input_formats=['%Y-%m-%d'])
	in_company_since = DateField(input_formats=['%Y-%m-%d'])
	in_company_until = DateField(required=False, input_formats=['%Y-%m-%d'], allow_null=True)
	company_id_number = serializers.CharField(required=False, max_length=100, allow_null=True, allow_blank=True)
	#last_rank = serializers.IntegerField(required=False)
	#last_vessel = serializers.IntegerField(required=False)
	separation_date = DateField(required=False,  allow_null=True, input_formats=['%Y-%m-%d'])
	separation_comments = UppercaseCharField(max_length=50, required=False, allow_null=True)
	user_code = serializers.CharField(min_length=4, max_length=4,)
	first_name = UppercaseCharField(max_length=30,)
	middle_name = UppercaseCharField(max_length=60, required=False, allow_null=True, allow_blank=True)
	last_name = UppercaseCharField(max_length=30,)
	nickname = UppercaseCharField(max_length=12, required=True)
	gender = serializers.CharField(max_length=10, required=True)
	birthdate = serializers.DateField(input_formats=['%Y-%m-%d'])
	birthplace = UppercaseCharField(max_length=32,)
	bmi = serializers.DecimalField(max_digits=5, decimal_places=2, required=False)
	email_address = serializers.EmailField(max_length=50,)
	email_address_2 = serializers.EmailField(max_length=50, required=False, allow_null=True, allow_blank=True)
	tax_account_number = serializers.CharField(required=False, max_length=20, allow_null=True, allow_blank=True)
	sss_number = serializers.CharField(max_length=12, allow_null=True)
	pagibig_number = serializers.CharField(max_length=15, allow_null=True)
	philhealth_number = serializers.CharField(max_length=14, allow_null=True)
	#Multiple
	bio_related_people = RelatedPeopleSerializer(many=True)
	bio_address = BioAddressSerializer(many=True)
	bio_educations = BioEducationsSerializer(many=True, required=False)
	bio_telephones = BioTelephonesSerializer(many=True,)
	bio_mobiles = BioMobilesSerializer(many=True, required=False)
	bio_hobbies = BioHobbiesSerializer(many=True, required=False)
	bio_social_medias = BioSocialMediaSerializer(many=True, required=False)
	bio_languages = BioLanguagesSerializer(many=True, required=False)

	class Meta:
		model = Bio
		fields = '__all__'

	def validate_email_address(self, value):
		bio = Bio.objects.filter(email_address=value, auth_user__email = value).filter(~Q(bio_sys_status = 'D'))
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			bio = bio.filter(~Q(id = self.instance.id))

		if len(bio) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def validate_user_code(self, value):
		import re
		#Check first if valid user code
		if not re.match("^[a-z]{4}$|^[a-z]{3}\d$", value.lower()): #alpa 4 characters or alpha 3 character + 1 digit
			raise serializers.ValidationError('Invalid user code: Combination of name not valid')

		bio = Bio.objects.filter(user_code = value.lower()).filter(~Q(bio_sys_status = 'D')) 
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			bio = bio.filter(~Q(id = self.instance.id))

		if len(bio) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def validate_user_id(self, value):
		from django.core.exceptions import ObjectDoesNotExist
		# Auto Increment Suggestion (lowest available) during add but
		# range is based  on below Company and Nationality. Still editable.

		# Foreigner:  1001 to 9999
		# Filipino:
		#   10001 to 899999 if Hellespont Group (MSMI/HHAM/ISI)
		#   900000+ if non Hellespont Group

		# During update:

		# If Filipino Non-hellespont Group and changed to Hellespont Group:
		#   UID will be changed (auto suggest) based on the appropriate range.

		# If Filipino Hellespont Group and changed to Non-Hellespont Group:
		#   UID will not be changed (remain Hellespont Group range)

		filipino = Countries.objects.get(country_name__contains="Philippines").id #Get Philippines id
		#Add mode
		current_company = self.initial_data.get('current_company', None)
		nationality = self.initial_data.get('nationality', filipino)
		is_foreigner = True if nationality != filipino else False
		
		try: #not foreigner
			uidrange = UIDRange.objects.get(is_foreigner=is_foreigner, group_ids__contains=current_company)
		except ObjectDoesNotExist:
			uidrange = UIDRange.objects.get(is_foreigner=is_foreigner)

		min = uidrange.min_range
		max = uidrange.max_range
		next_uid = uidrange.next_uid

		#Update mode
		if self.instance is not None:
			#Prefix "c_" means current saved data
			c_nationality = self.instance.nationality.id
			c_current_company = self.instance.current_company.id
			c_is_foreigner = True if nationality != filipino else False

			try: #not foreigner
				uidrange = UIDRange.objects.get(is_foreigner=c_is_foreigner, group_ids__contains=c_current_company)
			except ObjectDoesNotExist:
				uidrange = UIDRange.objects.get(is_foreigner=c_is_foreigner)

			#Nothing change return
			if current_company == c_current_company and is_foreigner == c_is_foreigner and self.instance.user_id == value:
				return self.instance.user_id

			# If Filipino Hellespont Group and changed to Non-Hellespont Group:
			if c_uidrange.is_hellespont_group and not c_is_foreigner and self.instance.user_id == value:
				return self.instance.user_id #UID will not be changed (remain Hellespont Group range)
			else:
				pass
		
		#Value given is valid
		if (min <= value <= max) and value >= next_uid:
			pass #Valid value
		else:
			#Get new uid
			# raise serializers.ValidationError({
			# 	'message' : 'This field has invalid value.',
			# 	'next_id' : next_uid
			# })
			return next_uid

		#Check if value is unique
		bio = Bio.objects.filter(user_id = value).filter(~Q(bio_sys_status = 'D'))
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			bio = bio.filter(~Q(id = self.instance.id))

		if len(bio) != 0:
			# raise serializers.ValidationError({
			# 	'message' : 'This field must be unique.',
			# 	'next_id' : next_uid
			# })
			return next_uid

		return value

	@transaction.atomic #Use to handle integrity error
	def create(self, data):
		data = self.fix_data(data) #get updated_by
		
		auth_data = {
			'username' : data['user_code'],
			'first_name' : data.pop('first_name'),
			'last_name' : data.pop('last_name'),
			'email' : data['email_address'],
			'is_staff' : False,
			'is_active' : False
		}

		auth_user = User.objects.create_user(**auth_data) #create new account
		data.update({'auth_user' : auth_user}) #add auth user to bio data

		bio_related_people = data.pop('bio_related_people')
		bio_address = data.pop('bio_address')
		bio_educations = data.pop('bio_educations')
		bio_telephones = data.pop('bio_telephones')
		bio_mobiles = data.pop('bio_mobiles')
		bio_hobbies = data.pop('bio_hobbies')
		bio_social_medias = data.pop('bio_social_medias')
		bio_languages = data.pop('bio_languages')

		#Create bio record first
		bio = Bio.objects.create(**data)

		for related_people in bio_related_people:
			RelatedPeople.objects.create(updated_by=data.get('updated_by'), related_people_bio=bio, **related_people)

		for address in bio_address:
			BioFilipinoAddress.objects.create(updated_by=data.get('updated_by'), bio_fil_bio=bio, **address)

		for education in bio_educations:
			BioEducations.objects.create(updated_by=data.get('updated_by'), bio_education_bio=bio, **education)

		for telephone in bio_telephones:
			if telephone.get('bio_tel_area_code') is None:
				continue #skip none bio_tel_area_code

			BioTelephones.objects.create(updated_by=data.get('updated_by'), bio_tel_bio=bio, **telephone)

		for mobile in bio_mobiles:
			if mobile.get('bio_mobile_prefix') is None:
				continue #skip none bio_mobile_prefix

			BioMobiles.objects.create(updated_by=data.get('updated_by'), bio_mobile_bio=bio, **mobile)

		for hobby in bio_hobbies:
			#Check if we will create new hobby instance
			if hobby.get('bio_hobby', None) == None:
				value = hobby.pop('hobby') #remove hobby
				hobby['bio_hobby'] = Hobbies.objects.create(hobby=value, updated_by=data.get('updated_by')) #create new hobby instance

			BioHobbies.objects.create(updated_by=data.get('updated_by'), bio_hobby_bio=bio, **hobby)

		for social_media in bio_social_medias:
			if social_media.get('bio_social_media') is None:
				continue #skip none bio_social_media
			BioSocialMedia.objects.create(updated_by=data.get('updated_by'), bio_social_bio=bio, **social_media)

		for language in bio_languages:
			dialects = language.pop('bio_dialects')

			#Check if we will create new language instance
			if language.get('bio_language', None) == None:
				value = language.pop('language') #remove language
				language['bio_language'] = Languages.objects.create(language=value, updated_by=data.get('updated_by')) #create new language instance

			bio_lang = BioLanguages.objects.create(updated_by=data.get('updated_by'), bio_language_bio=bio, **language)

			#If there are dialects add it.
			for dialect in dialects:
				BioLanguageDialects.objects.create(updated_by=data.get('updated_by'), bio_lang_dialect_language=bio_lang, **dialect)

		#Add new incremental
		uid_range = UIDRange.objects.get(group_ids__contains=data.get('current_company').id)
		if data.get('user_id') == uid_range.next_uid:
			uid_range.next_uid += 1
			uid_range.save() #save new value

		return bio

	@transaction.atomic #Use to handle integrity error
	def update(self,  instance, validated_data):
		#TODO: FINISH THIS
		data = self.fix_data(validated_data) #get updated_by
	  	
		bio_related_people = data.pop('bio_related_people')
		bio_address = data.pop('bio_address')
		bio_educations = data.pop('bio_educations')
		bio_telephones = data.pop('bio_telephones')
		bio_mobiles = data.pop('bio_mobiles')
		bio_hobbies = data.pop('bio_hobbies')
		bio_social_medias = data.pop('bio_social_medias')
		bio_languages = data.pop('bio_languages')
		
		auth_data = {
			'username' : data['user_code'],
			'first_name' : data.pop('first_name'),
			'last_name' : data.pop('last_name'),
			'email' : data['email_address'],
		}
		#Update django user table
		for auth_user in auth_data: setattr(instance.auth_user, auth_user, auth_data[auth_user])
		instance.auth_user.save()

		#update BIO record
		for bio in data: setattr(instance, bio, data[bio])
		instance.save()

		#Create, Update or delete Related People
		for related_people in bio_related_people:
			id = related_people.get('_id', None)

			try: 
				related_people.pop('_id') #remove
			except: 
				pass
			
			to_delete = related_people.pop('deleted', False)
			if to_delete and id != None:
				RelatedPeople.objects.filter(pk=id).delete() #delete
			else:
				related_people.update({'updated_by' : data.get('updated_by')})
				related_people.update({'related_people_bio' : instance})
				related_people.update({'date_updated' : timezone.now()})
				RelatedPeople.objects.update_or_create(defaults=related_people, id=id)

		#Create, Update or delete address
		for address in bio_address:
			id = address.get('_id', None)

			try: 
				address.pop('_id') #remove
			except: 
				pass

			
			to_delete = address.pop('deleted', False)
			if to_delete and id != None:
				BioFilipinoAddress.objects.filter(pk=id).delete() #delete
			else:
				address.update({'updated_by' : data.get('updated_by')})
				address.update({'bio_fil_bio' : instance})
				address.update({'date_updated' : timezone.now()})
				BioFilipinoAddress.objects.update_or_create(defaults=address, id=id)

		#Create, Update or delete educations
		for education in bio_educations:
			id = education.get('_id', None)

			try: 
				education.pop('_id') #remove
			except: 
				pass

			to_delete = education.pop('deleted', False)
			if to_delete and id != None:
				BioEducations.objects.filter(pk=id).delete() #delete
			else:
				education.update({'updated_by' : data.get('updated_by')})
				education.update({'bio_education_bio' : instance})
				education.update({'date_updated' : timezone.now()})
				BioEducations.objects.update_or_create(defaults=education, id=id)

		#Create, Update or delete telephones
		for telephone in bio_telephones:
			id = telephone.get('_id', None)

			try: 
				telephone.pop('_id') #remove
			except: 
				pass
			
			to_delete = telephone.pop('deleted', False)
			if to_delete and id != None:
				BioTelephones.objects.filter(pk=id).delete() #delete
			else:
				if telephone.get('bio_tel_area_code') is None and id == None:
					continue #skip none bio_tel_area_code and id that is also None

				if telephone.get('bio_tel_area_code') is None: #Make number to blank or null
					telephone['bio_tel_number'] = None

				telephone.update({'updated_by' : data.get('updated_by')})
				telephone.update({'bio_tel_bio' : instance})
				telephone.update({'date_updated' : timezone.now()})
				BioTelephones.objects.update_or_create(defaults=telephone, id=id)

		#Create, Update or delete mobiles
		for mobile in bio_mobiles:
			id = mobile.get('_id', None)

			try: 
				mobile.pop('_id') #remove
			except: 
				pass
			
			to_delete = mobile.pop('deleted', False)
			if to_delete and id != None:
				BioMobiles.objects.filter(pk=id).delete() #delete
			else:
				mobile.update({'updated_by' : data.get('updated_by')})
				mobile.update({'bio_mobile_bio' : instance})
				mobile.update({'date_updated' : timezone.now()})
				BioMobiles.objects.update_or_create(defaults=mobile, id=id)

		#Create, Update or delete mobiles
		for hobby in bio_hobbies:
			id = hobby.get('_id', None)

			try: 
				hobby.pop('_id') #remove
			except: 
				pass
			
			to_delete = hobby.pop('deleted', False)
			if to_delete and id != None:
				BioHobbies.objects.filter(pk=id).delete() #delete
			else:
				try: 
					value = hobby.pop('hobby') #remove hobby
				except: 
					pass
			
				#Check if we will create new hobby instance
				if hobby.get('bio_hobby', None) == None:
					hobby['bio_hobby'] = Hobbies.objects.create(hobby=value, updated_by=data.get('updated_by')) #create new hobby instance

				hobby.update({'updated_by' : data.get('updated_by')})
				hobby.update({'bio_hobby_bio' : instance})
				hobby.update({'date_updated' : timezone.now()})
				BioHobbies.objects.update_or_create(defaults=hobby, id=id)

		#Create, Update or delete mobiles
		for social_media in bio_social_medias:
			id = social_media.get('_id', None)
			
			try: 
				social_media.pop('_id') #remove
			except:
				pass

			to_delete = social_media.pop('deleted', False)
			if to_delete and id != None:
				BioSocialMedia.objects.filter(pk=id).delete() #delete
			else:
				social_media.update({'updated_by' : data.get('updated_by')})
				social_media.update({'bio_social_bio' : instance})
				social_media.update({'date_updated' : timezone.now()})
				BioSocialMedia.objects.update_or_create(defaults=social_media, id=id)

		#Create, Update or delete languages
		for language in bio_languages:
			dialects = language.pop('bio_dialects')
			id = language.get('_id', None)

			try: 
				language.pop('_id') #remove
			except: 
				pass

			to_delete = language.pop('deleted', False)
			
			if to_delete and id != None:
				BioLanguages.objects.filter(pk=id).delete() #delete
			else:
				value = None; #set Default
				try:
					value = language.pop('language') #remove language
				except:
					pass

				#Check if we will create new language instance
				if language.get('bio_language', None) == None:
					language['bio_language'] = Languages.objects.create(language=value, updated_by=data.get('updated_by')) #create new language instance

				language.update({'updated_by' : data.get('updated_by')})
				language.update({'bio_language_bio' : instance})
				language.update({'date_updated' : timezone.now()})
				obj_lang, new_lang = BioLanguages.objects.update_or_create(defaults=language, id=id)

			#Create, update or delete dialect from language 
			for dialect in dialects:
				obj_lang_dialect = BioLanguageDialects.objects.filter(id=dialect.get('_id')) #check first if still exist
				to_delete = dialect.pop('deleted', False)
				id = dialect.get('_id', None)
				try:
					dialect.pop('_id') #remove
				except:
					pass

				if (len(obj_lang_dialect) == 0 and id != None): #we need to check the id too
					continue
				else:
					if to_delete and id != None:
						BioLanguageDialects.objects.filter(pk=id).delete() #delete
					else:
						dialect.update({'updated_by' : data.get('updated_by')})
						dialect.update({'bio_lang_dialect_language' : obj_lang})
						dialect.update({'date_updated' : timezone.now()})
						BioLanguageDialects.objects.update_or_create(defaults=dialect, id=id)

		#Add new incremental
		uid_range = UIDRange.objects.get(group_ids__contains=data.get('current_company').id)
		if data.get('user_id') == uid_range.next_uid:
			uid_range.next_uid += 1
			uid_range.save() #save new value
		
		return instance

	def fix_data(self, data):
		data['user_code'] = data.get('user_code', '').lower()
		data.update({'updated_by' : self.context.get('logged_user')})

		return data

class BioListSerializer(BioSerializer):
	first_name = serializers.SerializerMethodField()
	last_name = serializers.SerializerMethodField()
	employee_type = serializers.SerializerMethodField()
	last_rank = serializers.SerializerMethodField()
	bio_status = serializers.SerializerMethodField()

	class Meta:
		model = Bio
		fields = ("id", "user_id", "first_name", "middle_name", "last_name", "user_code", "employee_type", "last_rank", "bio_status", "updated_by", "date_updated")

	def get_bio_status(self, obj):
		try:
			bio_status = obj.bio_status.bio_status_name
		except:
			bio_status = None

		return bio_status

	def get_last_rank(self, obj):
		try:
			last_rank = obj.last_rank.rank_code
		except:
			last_rank = None

		return last_rank

	def get_employee_type(self, obj):
		try:
			employee_type = obj.employee_type.employee_type
		except:
			employee_type = None

		return employee_type

	def get_first_name(self, obj):
		return obj.auth_user.first_name

	def get_last_name(self, obj):
		return obj.auth_user.last_name

#For view Serializer
class BioEducationsSerializerView(BioEducationsSerializer):
	bio_education_degree = GenericSerializerMethodField(extra={ 'field' : 'degree_name'})
	bio_education_school = GenericSerializerMethodField(extra={ 'field' : 'school_name'})

class BioTelephonesSerializerView(BioTelephonesSerializer):
	bio_tel_area_code = GenericSerializerMethodField(extra={ 'field' : 'code'})

class BioSocialMediaSerializerView(BioSocialMediaSerializer):
	bio_social_media = GenericSerializerMethodField(extra={ 'field' : 'social_media_name'})

class BioAddressSerializerView(BioAddressSerializer):
	bio_fil_zip = serializers.SerializerMethodField()
	def get_bio_fil_zip(self, obj):
		#Try get barangay
		try:
			barangay = obj.bio_fil_zip.barangay.barangay.upper()
			barangay_id = obj.bio_fil_zip.barangay.id
		except:
			barangay = None
			barangay_id = None

		return {
			'region' : obj.bio_fil_zip.municipality.province.region.region.upper(),
			'region_id' : obj.bio_fil_zip.municipality.province.region.id,
			'province' : obj.bio_fil_zip.municipality.province.province.upper(),
			'province_id' : obj.bio_fil_zip.municipality.province.id,
			'municipality' : obj.bio_fil_zip.municipality.municipality.upper(),
			'municipality_id' : obj.bio_fil_zip.municipality.id,
			'barangay' : barangay,
			'barangay_id' : barangay_id,
			'zip_code' : obj.bio_fil_zip.zip,
			'zip_id' : obj.bio_fil_zip.id
		}

class BioMobilesSerializerView(BioMobilesSerializer):
	bio_mobile_prefix = GenericSerializerMethodField(extra={ 'field' : 'prefix'})
	bio_mobile_network = serializers.SerializerMethodField()

	def get_bio_mobile_network(self, obj):
		#since its not required we need to have a try and except
		try:
			network = obj.bio_mobile_prefix.network.network
		except:
			network = None

		return {
			'id' : None if network == None else obj.bio_mobile_prefix.network.id,
			'label' : network
		}

class BioHobbiesSerializerView(BioHobbiesSerializer):
	hobby = serializers.SerializerMethodField()

	def get_hobby(self, obj):
		try:
			hobby = obj.bio_hobby.hobby
		except:
			hobby = None

		return hobby

class BioLanguageDialectsSerializerView(BioLanguageDialectsSerializer):
	dialect = serializers.SerializerMethodField()

	def get_dialect(self, obj):
		return obj.bio_lang_dialect_dialect.dialect

class BioLanguagesSerializerView(BioLanguagesSerializer):
	language = serializers.SerializerMethodField()
	bio_dialects = BioLanguageDialectsSerializerView(many=True)

	def get_language(self, obj):
		return obj.bio_language.language

class BioViewSerializer(BioListSerializer):
	gender_label = serializers.SerializerMethodField()
	no_of_dependents = serializers.SerializerMethodField()
	hobbies = serializers.SerializerMethodField()
	languages = serializers.SerializerMethodField()
	dialects = serializers.SerializerMethodField()

	#Override BioListSerializer
	#See common serializer class method property()
	employee_type = GenericSerializerMethodField(extra={ 'field' : 'employee_type'})
	employment_type = GenericSerializerMethodField(extra={ 'field' : 'employment_type'})
	last_rank = serializers.SerializerMethodField()
	bio_status = GenericSerializerMethodField(extra={ 'field' : 'bio_status_name'})
	title = GenericSerializerMethodField(extra={ 'field' : 'title'})
	current_company = serializers.SerializerMethodField()
	nationality = GenericSerializerMethodField(extra={ 'field' : 'country_name'})
	blood_type = GenericSerializerMethodField(extra={ 'field' : 'blood_type'})
	separation_type = GenericSerializerMethodField(extra={ 'field' : 'separation_type_name'})
	religion = GenericSerializerMethodField(extra={ 'field' : 'religion'})
	name_suffix = GenericSerializerMethodField(extra={ 'field' : 'suffix'})
	civil_status = GenericSerializerMethodField(extra={ 'field' : 'civil_status_name'})
	tax_status = GenericSerializerMethodField(extra={ 'field' : 'tax_name'})
	referred_by = serializers.SerializerMethodField()
	#multiple
	bio_related_people = serializers.SerializerMethodField()
	bio_educations = BioEducationsSerializerView(many=True)
	bio_telephones = serializers.SerializerMethodField()
	bio_address = BioAddressSerializerView(many=True)
	bio_mobiles = BioMobilesSerializerView(many=True)
	bio_hobbies = BioHobbiesSerializerView(many=True)
	bio_social_medias = BioSocialMediaSerializerView(many=True)
	bio_languages = BioLanguagesSerializerView(many=True)
	#Special case
	bio_dialects = serializers.SerializerMethodField()

	def get_is_hellespont_group(self, obj):
		return BioTelephonesSerializerView(BioTelephones.objects.filter(bio_tel_bio=obj.id).order_by('id'), many=True).data

	def get_bio_telephones(self, obj):
		return BioTelephonesSerializerView(BioTelephones.objects.filter(bio_tel_bio=obj.id).order_by('id'), many=True).data

	def get_bio_related_people(self, obj):
		return RelatedPeopleSerializerView(RelatedPeople.objects.filter(related_people_bio=obj.id).order_by('-id'), many=True).data

	def get_bio_dialects(self, obj):
		#Get dialect of a bio entry
		lang_container = []
		languages = obj.bio_languages.all()
		for language in languages:
			lang_container.append(language.bio_language.id)

		#TODO make it more DRY
		extra_field = {
			'bio_lang_dialect_dialect' : serializers.SerializerMethodField()
		}

		return DialectsSerializer(Dialects.objects.filter(language_id__in=lang_container), many=True, fields=('bio_lang_dialect_dialect', 'language', 'dialect'), extra_field=extra_field).data

	def get_last_rank(self, obj):
		try:
			rank = obj.last_rank.rank_code + ' - ' + obj.last_rank.rank_description
		except:
			rank = None

		return {
			'id' : None if rank == None else obj.last_rank.id,
			'label' : rank
		}

	def get_current_company(self, obj):
		try:
			principal = obj.current_company.principal_code + ' - ' + obj.current_company.principal_full
			is_hellespont_group = obj.current_company.principal_is_hellespont_group
			
		except:
			principal = None
			is_hellespont_group = None

		return {
			'id' : None if principal == None else obj.current_company.id,
			'label' : principal,
			'is_hellespont_group' : is_hellespont_group
		}

	def get_gender_label(self, obj):
		label = {
			'M' : 'Male',
			'F' : 'Female'
		}
		try:
			return 	label[obj.gender]
		except:
			return None

	def get_no_of_dependents(self, obj):
		try:
			return obj.bio_related_people.filter(related_people_is_dependent='Y').count()
		except:
			return 0

	#Comma separated hobbies
	def get_hobbies(self, obj):
		try:
			return ', '.join([hobby.bio_hobby.hobby for hobby in obj.bio_hobbies.all()])
		except:
			return None

	#Comma separated languages
	def get_languages(self, obj):
		try:
			return ', '.join([language.bio_language.language for language in obj.bio_languages.all()])
		except:
			return None

	#Comma separated languages
	def get_dialects(self, obj):
		try:
			dialects = []
			for language in obj.bio_languages.all():
				 for bio_dialect in language.bio_dialects.all():
				 	dialects.append(bio_dialect.bio_lang_dialect_dialect.dialect)

			return ', '.join(dialects)
		except:
			return None

	def get_referred_by(self, obj):
		try:
			full_name = obj.referred_by.user_code + ' - ' + obj.referred_by.auth_user.last_name + ', ' + obj.referred_by.auth_user.first_name
		except:
			full_name = None

		#Assumed that every model instance have a id attribute :D
		return {
			'id' : None if full_name == None else obj.referred_by.id,
			'label' : full_name
		}

	class Meta:
		model = Bio
		#fields = ('availability_date', 'in_company_since', 'birthdate')
		exclude = ('auth_user',)
