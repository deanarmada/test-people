#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required


@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        photo = request.data.get('photo', None)
        data = json.loads(request.data.get('data', '')) #need to convert json string to python dictionary
        logged_user = User.objects.get(pk=data.get("updated_by", 1))
        data.update({'photo' : photo}) #add to python dictionary
        #data = test_data()

        bio = BioSerializer(data=data, context={'logged_user': logged_user})

        if bio.is_valid():
            bio.save()
            return Response({
                'errors' : [],
                'data' : [],
            }, status=200)
            return
        else:
            return Response({
                'errors' : bio.errors,
                'data' : [],
            }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    if request.method == 'POST':
        photo = request.data.get('photo', None)
        data = json.loads(request.data.get('data', '{}')) #need to convert json string to python dictionary
        if photo is not None: data.update({'photo' : photo}) #add to python dictionary
        #data = test_data() #enable sample data
        bio_obj = Bio.objects.get(pk=data['id'])
        logged_user = User.objects.get(pk=data.get("updated_by", 1))
        fields = data.get('fields', None)

        if fields is not None:
            return
            #return _update(request, fields=fields, logged_user=logged_user, related_person=related_person) #update field directly

        bio = BioSerializer(bio_obj, data=data, context={'logged_user': logged_user})

        if bio.is_valid():
            bio.save()

            return Response({
                'errors' : [],
                'data' : [],
            })

        return Response({
            'errors' : bio.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def bio(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["-id"] #default ordering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:      
            filters = request.data.get('filters')

        bio = Bio.objects.filter(filter_builder(filters)).filter(~Q(bio_sys_status='D')).order_by(*order_by)
        #print (bio.query) #See generated SQL query

        total = len(bio)
        if limit != None and offset != None:
            bio = bio[int(offset):int(limit)]

        bio = BioListSerializer(bio, many=True,)

        return Response({
            'errors' : [],
            'data' : bio.data,
            'order_by' : order_by,
            'total' : total,
        })
        
''' Parameters:
        required:
         - id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        bio = Bio.objects.filter(id=id).filter(~Q(bio_sys_status='D'))
        bio = BioViewSerializer(bio, many=True)

        return Response({
            'errors' : [],
            'data' : bio.data,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - updated_by : User logged id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
  if request.method == 'POST':
    id = request.data.get('id')
    logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
    bio = Bio.objects.filter(id__in=id)
    bio.update(bio_sys_status='D', updated_by=logged_user, date_updated=timezone.now())

    return Response({
      'errors' : [],
      'data' : []
    })

@api_view(['GET'])
@csrf_exempt
def rules(request):
    if request.method == 'GET':
        from test_people.utils import rules_generator

        defers = (
            'id',
            'auth_user', 
            'photo', 
            'employee_type', 
            'employment_type', 
            'title', 
            'bio_status', 
            'current_company', 
            'last_rank', 
            'separation_type', 
            'name_suffix', 
            'nationality', 
            'blood_type', 
            'religion', 
            'civil_status', 
            'tax_status', 
            'referred_by', 
            'bio_related_people', 
            'bio_address', 
            'bio_educations', 
            'bio_telephones', 
            'bio_mobiles', 
            'bio_hobbies', 
            'bio_social_medias', 
            'bio_languages'
        )

        bio = BioSerializer(defers=defers)
        related_people = RelatedPeopleSerializer(defers=('id', 'related_people_name_suffix', 'related_people_tel_area_code', 'related_people_tel_area_code_2', 'related_people_mobile_prefix', 'related_people_mobile_prefix_2', 'related_people_zip', 'related_people_relationship_type', 'related_people_nationality'))
        address = BioAddressSerializer(defers=('id','bio_fil_zip', 'updated_by', 'date_updated'))
        educations = BioEducationsSerializer(defers=('id','bio_education_degree','bio_education_school', 'updated_by', 'date_updated'))
        telephones = BioTelephonesSerializer(defers=('id','bio_tel_area_code', 'updated_by', 'date_updated'))
        hobbies = BioHobbiesSerializer(defers=('id','bio_hobby', 'updated_by', 'date_updated'))
        social_medias = BioSocialMediaSerializer(defers=('id','bio_social_media', 'updated_by', 'date_updated'))
        languages = BioLanguagesSerializer(defers=('id','bio_dialects', 'bio_language', 'updated_by', 'date_updated'))
        dialects = BioLanguageDialectsSerializer(defers=('id','bio_lang_dialect_dialect', 'updated_by', 'date_updated'))

        data = {
            'errors' : [],
            'data' : rules_generator(bio)
        }

        data['data'].update(rules_generator(related_people))
        data['data'].update(rules_generator(address))
        data['data'].update(rules_generator(educations))
        data['data'].update(rules_generator(telephones))
        data['data'].update(rules_generator(hobbies))
        data['data'].update(rules_generator(social_medias))
        data['data'].update(rules_generator(dialects))

        #Manually added rule
        append_rules = {
            'employee_type': { 
                'rules' : {
                    'required' : True
                }
            },
            'employment_type' : {
                'rules': {
                    'required' : False
                }
            },
            'title' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_status' : {
                'rules': {
                    'required' : False
                }
            },
            'current_company' : {
                'rules': {
                    'required' : True
                }
            },
            'last_rank' : {
                'rules': {
                    'required' : False
                }
            },
            'nationality' : {
                'rules': {
                    'required' : True
                }
            },
            'blood_type' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_tel_area_code' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_mobile_prefix' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_mobile_number' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_social_media' : {
                'rules': {
                    'required' : False
                }
            },
            'religion' : {
                'rules': {
                    'required' : True
                }
            },
            'civil_status' : {
                'rules': {
                    'required' : True
                }
            },
            'tax_status' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_education_degree' : {
                'rules': {
                    'required' : False
                }
            },
            'bio_education_school' : {
                'rules': {
                    'required' : True
                }
            },
            'related_people_relationship_type' : {
                'rules': {
                    'required' : True
                }
            },
            'related_people_name_suffix' : {
                'rules': {
                    'required' : False
                }
            },
            'related_people_nationality' : {
                'rules': {
                    'required' : True
                }
            }
        }

        data['data'].update(append_rules)

        return Response(data)

def  _update(request, **kwargs):
    logged_user = kwargs['logged_user']
    fields = kwargs['fields']
    vessel_type = kwargs['vessel_type']

    fields.update({
        'updated_by' : logged_user,
        'date_updated' : timezone.now()
    })

    VesselTypes.objects.filter(id=vessel_type.id).update(**fields)
    vessel_type = VesselTypeSerializer(vessel_type)
    
    return Response({
        'errors' : [],
        'data' : vessel_type.data,
    }) 


def test_data():  
    data={
      "id": 72,
      "user_id": 9,
      "availability_date": None,
      "in_company_since": "2016-06-01",
      "in_company_until": "2017-06-01",
      "company_id_number": "hs33556",
      "separation_date": None,
      "separation_comments": None,
      "user_code": "asdf",
      "first_name": "ALLAN JED",
      "middle_name": "TIERRA",
      "last_name": "BERNABE",
      "nickname": "JED",
      "gender": "M",
      "birthdate": "1988-11-17",
      "birthplace": "PASAY",
      "bmi": "0.00",
      "email_address": "allan.bernabe_2@gmail.com",
      "email_address_2": "",
      "tax_account_number": "221148587000",
      "sss_number": "4588778996",
      "pagibig_number": "11455696997",
      "philhealth_number": "568999978744",
      "bio_related_people": [
        {
          "_id": 108,
          "related_people_first_name": "LENNIE",
          "related_people_middle_name": "ALTAMIRA",
          "related_people_last_name": "TIERRA",
          "related_people_married_last_name": "",
          "related_people_married_middle_name": "",
          "related_people_marriage_date": None,
          "related_people_marriage_place": None,
          "related_people_tel_number": "8542754",
          "related_people_tel_number_2": None,
          "related_people_mobile_number": "5568975",
          "related_people_mobile_number_2": None,
          "related_people_address": "24 MATA STREET VILLAMOR",
          "related_people_province_barangay": "5611",
          "related_people_birthdate": "1953-06-06",
          "related_people_birthplace": "pasay",
          "related_people_is_beneficiary": "N",
          "related_people_is_dependent": "Y",
          "related_people_is_next_of_kin": "Y",
          "related_people_is_emergency": "Y",
          "related_people_is_working": "N",
          "related_people_is_student": "N",
          "related_people_is_deceased": "N",
          "related_people_zip": 6246,
          "related_people_mobile_network": {
            "label": "Sun",
            "id": 3
          },
          "related_people_mobile_network_2": {
            "label": None,
            "id": None
          },
          "related_people_name_suffix": None,
          "related_people_tel_area_code": 36,
          "related_people_tel_area_code_2": None,
          "related_people_mobile_prefix": 58,
          "related_people_mobile_prefix_2": None,
          "related_people_relationship_type": 9,
          "related_people_nationality": 2,
          "related_people_group": "PHILHEALTH DEPENDENT, NEXT OF KIN, EMERGENCY",
          "related_people_is_working_label": "NO",
          "related_people_is_student_label": "NO",
          "related_people_is_deceased_label": "NO",
          "related_people_occupation": None
        },
        {
          "_id": 44,
          "related_people_first_name": "ALBERTO",
          "related_people_middle_name": "",
          "related_people_last_name": "BERNABE",
          "related_people_married_last_name": "",
          "related_people_married_middle_name": "",
          "related_people_marriage_date": None,
          "related_people_marriage_place": None,
          "related_people_tel_number": None,
          "related_people_tel_number_2": None,
          "related_people_mobile_number": "5568997",
          "related_people_mobile_number_2": None,
          "related_people_address": None,
          "related_people_province_barangay": None,
          "related_people_birthdate": "1955-06-01",
          "related_people_birthplace": "bataan",
          "related_people_is_beneficiary": "N",
          "related_people_is_dependent": "N",
          "related_people_is_next_of_kin": "N",
          "related_people_is_emergency": "N",
          "related_people_is_working": "N",
          "related_people_is_student": "N",
          "related_people_is_deceased": "N",
          "related_people_zip": None,
          "related_people_mobile_network": {
            "label": "Smart",
            "id": 2
          },
          "related_people_mobile_network_2": {
            "label": None,
            "id": None
          },
          "related_people_name_suffix": None,
          "related_people_tel_area_code": None,
          "related_people_tel_area_code_2": None,
          "related_people_mobile_prefix": 47,
          "related_people_mobile_prefix_2": None,
          "related_people_relationship_type": 8,
          "related_people_nationality": 2,
          "related_people_group": "",
          "related_people_is_working_label": "NO",
          "related_people_is_student_label": "NO",
          "related_people_is_deceased_label": "NO",
          "related_people_occupation": None
        },
        {
          "_id": 109,
          "related_people_first_name": "MARITESS",
          "related_people_middle_name": "cardenas",
          "related_people_last_name": "duque",
          "related_people_married_last_name": "bernabe",
          "related_people_married_middle_name": "duque",
          "related_people_marriage_date": "2017-04-22",
          "related_people_marriage_place": "PASAY",
          "related_people_tel_number": None,
          "related_people_tel_number_2": None,
          "related_people_mobile_number": "8162864",
          "related_people_mobile_number_2": None,
          "related_people_address": "24 MATA STREET VILLAMOR",
          "related_people_province_barangay": "5611",
          "related_people_birthdate": "1988-09-16",
          "related_people_birthplace": "taguig",
          "related_people_is_beneficiary": "Y",
          "related_people_is_dependent": "Y",
          "related_people_is_next_of_kin": "Y",
          "related_people_is_emergency": "Y",
          "related_people_is_working": "N",
          "related_people_is_student": "N",
          "related_people_is_deceased": "N",
          "related_people_zip": 6246,
          "related_people_mobile_network": {
            "label": "Globe",
            "id": 1
          },
          "related_people_mobile_network_2": {
            "label": None,
            "id": None
          },
          "related_people_name_suffix": None,
          "related_people_tel_area_code": None,
          "related_people_tel_area_code_2": None,
          "related_people_mobile_prefix": 2,
          "related_people_mobile_prefix_2": None,
          "related_people_relationship_type": 7,
          "related_people_nationality": 2,
          "related_people_group": "PHILHEALTH DEPENDENT, OWWA BENEFICIARY, NEXT OF KIN, EMERGENCY",
          "related_people_is_working_label": "NO",
          "related_people_is_student_label": "NO",
          "related_people_is_deceased_label": "NO",
          "related_people_occupation": None
        },
      ],
      "bio_address": [
        {
          "_id": 48,
          "bio_fil_address": "24 MATA STREET VILLAMOR",
          "bio_fil_address_type": "Current",
          "bio_fil_zip": 6246,
          "bio_fil_province_barangay": "5611"
        },
        {
          "_id": 47,
          "bio_fil_address": "24 MATA STREET VILLAMOR",
          "bio_fil_address_type": "Permanent",
          "bio_fil_zip": 6246,
          "bio_fil_province_barangay": "5611"
        }
      ],
      "bio_educations": [
        {
          "_id": 24,
          "bio_education_type": "others",
          "bio_education_date_completed": 2009,
          "bio_education_degree": 1,
          "bio_education_school": 3
        }
      ],
      "bio_telephones": [
        {
          "_id": 54,
          "bio_tel_number": "8542754",
          "bio_tel_area_code": 36
        },
      ],
      "bio_mobiles": [
        {
          "_id": 63,
          "bio_mobile_number": 2972092,
          "bio_mobile_prefix": 3,
          "bio_mobile_network": {
            "label": "Globe",
            "id": 1
          }
        },
      ],
      "bio_hobbies": [
        {
          "_id": 26,
          "hobby": "Swimming",
          "bio_hobby": 135
        }
      ],
      "bio_social_medias": [
        {
          "_id" : 26, 
          "bio_social_media" : 1,
          "bio_social_id" : "http://facebook.com/235"
        }
      ],
      "bio_languages": [
        {
          "_id": 65,
          "language": "Filipino",
          "bio_dialects": [
            # {
            #   "_id": None,
            #   "dialect": "Tagalog",
            #   "bio_lang_dialect_dialect": 16
            # }
          ],
          "bio_language": 1
        },
        {
          "_id": 32,
          "language": "English",
          "bio_dialects": [],
          "bio_language": 2
        } 
      ],
      "employee_type": 3,
      "last_rank": None,
      "bio_status": None,
      "no_of_dependents": 2,
      "hobbies": "Swimming",
      "languages": "Filipino, English",
      "dialects": "Tagalog",
      "employment_type": None,
      "title": None,
      "current_company": 48,
      "nationality": 2,
      "blood_type": 7,
      "separation_type": None,
      "religion": 1,
      "name_suffix": None,
      "civil_status": 7,
      "tax_status": 12,
      "referred_by": None,
      "photo": None,
      "photo_date_updated": None,
      "ex_crew": None,
      "is_blacklisted": None,
      "updated_by" : 40
    }

    return data
