from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.bio, name='api_bio_list' ),
	url(r'^add/$', views.add, name='api_bio_add' ),
	url(r'^update/$', views.update, name='api_bio_update' ),
	url(r'^delete/$', views.delete, name='api_bio_delete' ),
	url(r'^detail/$', views.detail, name='api_bio_detail' ),
	url(r'^rules/$', views.rules, name='api_bio_rules' ),
]