from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.blood_types, name='api_blood_type_list' )
]