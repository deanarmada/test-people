#django
from django.db.models import Q

#Rest Framework
from rest_framework import serializers

#People
from test_people.models import *


class CommonSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(CommonSerializer, self).__init__(*args, **kwargs)

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)


	updated_by = serializers.SerializerMethodField()

	def get_updated_by(self, obj):
		try:
			user_code = obj.updated_by.bio.user_code
		except:
			user_code = obj.updated_by.username

		return {
			'user_code' : user_code,
			'first_name' : obj.updated_by.first_name,
			'last_name' : obj.updated_by.last_name,
		}

class LanguagesSerializer(CommonSerializer):
	
	class Meta:
		model = Languages
		fields = '__all__'

