from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.languages, name='api_language_list' )
]