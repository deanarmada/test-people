from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.schools, name='api_school_list' )
]