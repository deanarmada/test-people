from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.name_suffixes, name='api_name_suffix_list' )
]