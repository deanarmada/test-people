from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.vessel_types, name='api_vessel_type_list' ),
	url(r'^add/$', views.add, name='api_vessel_type_add' ),
	url(r'^update/$', views.update, name='api_vessel_type_update' ),
	url(r'^delete/$', views.delete, name='api_principal_delete' ),
	url(r'^detail/$', views.detail, name='api_vessel_type_detail' ),
	url(r'^rules/$', views.rules, name='api_vessel_type_rules' ),
]