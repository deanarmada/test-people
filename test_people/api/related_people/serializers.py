from test_people.api.serializers import *

class RelatedPeopleSerializer(CommonSerializer):
	_id = serializers.IntegerField(required=False, allow_null=True) #Work around to support update function
	deleted = serializers.BooleanField(required=False) #Note: DO not make name attribute to delete, its a reserve word
	related_people_first_name = UppercaseCharField(max_length=30,)
	related_people_middle_name = UppercaseCharField(max_length=60, required=False, allow_null=True, allow_blank=True)
	related_people_last_name = UppercaseCharField(max_length=30,)
	related_people_married_last_name = UppercaseCharField(max_length=30, required=False, allow_null=True, allow_blank=True)
	related_people_married_middle_name = UppercaseCharField(max_length=60, required=False, allow_null=True, allow_blank=True)
	related_people_marriage_date = DateField(required=False,  allow_null=True, input_formats=['%Y-%m-%d'])
	related_people_marriage_place = UppercaseCharField(max_length=60, required=False, allow_null=True)
	related_people_tel_number = serializers.CharField(max_length=16, required=False, allow_null=True, allow_blank=True)
	related_people_tel_number_2 = serializers.CharField(max_length=16, required=False, allow_null=True, allow_blank=True)
	related_people_mobile_number = serializers.CharField(max_length=15, required=False, allow_null=True, allow_blank=True)
	related_people_mobile_number_2 = serializers.CharField(max_length=15, required=False, allow_null=True, allow_blank=True)
	related_people_address = UppercaseCharField(max_length=75, required=False, allow_null=True, allow_blank=True)
	related_people_province_barangay = UppercaseCharField(max_length=75, required=False, allow_null=True, allow_blank=True)
	related_people_birthdate = DateField(input_formats=['%Y-%m-%d'])
	related_people_birthplace = serializers.CharField(max_length=75,)
	related_people_passport_number = serializers.CharField(max_length=10, required=False, allow_null=True, allow_blank=True)
	related_people_is_beneficiary = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_dependent = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_next_of_kin = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_emergency = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_working = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_student = serializers.CharField(max_length=1, min_length=1,)
	related_people_is_deceased = serializers.CharField(max_length=1, min_length=1,)

	def to_internal_value(self, data, *args, **kwargs):
		#When Blank fix area code and prefix to None
		contact_fields = [
			('related_people_tel_number' , 'related_people_tel_area_code'),
			('related_people_tel_number_2' , 'related_people_tel_area_code_2'),
			('related_people_mobile_number_2' , 'related_people_mobile_prefix_2'),
		]

		for field in contact_fields:
			if data.get(field[0]) is None or data.get(field[0]) == '':
				data[field[1]] = None

		data = super(RelatedPeopleSerializer, self).to_internal_value(data) # Get validated data and return it		

		return data

	class Meta:
		model = RelatedPeople
		ordering = ['id']
		exclude = ('updated_by', 'related_people_bio')
		extra_kwargs = {
			'related_people_bio': {'read_only': True}
		}


class RelatedPeopleSerializerView(RelatedPeopleSerializer):
	related_people_zip = serializers.SerializerMethodField()
	related_people_mobile_network = serializers.SerializerMethodField()
	related_people_mobile_network_2 = serializers.SerializerMethodField()
	related_people_name_suffix = GenericSerializerMethodField(extra={ 'field' : 'suffix'})
	related_people_tel_area_code = GenericSerializerMethodField(extra={ 'field' : 'code'})
	related_people_tel_area_code_2 = GenericSerializerMethodField(extra={ 'field' : 'code'})
	related_people_mobile_prefix = GenericSerializerMethodField(extra={ 'field' : 'prefix'})
	related_people_mobile_prefix_2 = GenericSerializerMethodField(extra={ 'field' : 'prefix'})
	related_people_relationship_type = GenericSerializerMethodField(extra={ 'field' : 'relationship_name'})
	related_people_nationality = GenericSerializerMethodField(extra={ 'field' : 'country_name'})
	related_people_group = serializers.SerializerMethodField()
	related_people_is_working_label = serializers.SerializerMethodField()
	related_people_is_student_label = serializers.SerializerMethodField()	
	related_people_is_deceased_label = serializers.SerializerMethodField()
	related_people_relationship_type_gender = serializers.SerializerMethodField()


	#TODO: refactor ; QUICK code 
	def get_related_people_relationship_type_gender(self, obj):
		return obj.related_people_relationship_type.relationship_gender

	def get_related_people_is_working_label(self, obj):
		if obj.related_people_is_working == 'Y':
			return 'YES'
		else:
			return 'NO'

	def get_related_people_is_student_label(self, obj):
		if obj.related_people_is_student == 'Y':
			return 'YES'
		else:
			return 'NO'
			
	def get_related_people_is_deceased_label(self, obj):
		if obj.related_people_is_deceased == 'Y':
			return 'YES'
		else:
			return 'NO'

	def get_related_people_group(self, obj):
		fields = [('related_people_is_dependent',  'PHILHEALTH DEPENDENT'), 
			('related_people_is_beneficiary', 'OWWA BENEFICIARY'),
			('related_people_is_next_of_kin', 'NEXT OF KIN'),
			('related_people_is_emergency', 'EMERGENCY')
		]

		filtered_fields = []
		separator = ', '
		for field in fields:
			if getattr(obj, field[0]) == 'Y':
				filtered_fields.append(field[1])
			else:
				continue
		
		return separator.join(filtered_fields)

	def get_related_people_mobile_network(self, obj):
		try:
			return {
				'id' : obj.related_people_mobile_prefix.network.id,
				'label' : obj.related_people_mobile_prefix.network.network
			}
		except:
			return {
				'id' : None,
				'label' : None
			}

	def get_related_people_mobile_network_2(self, obj):
		#since its not required we need to have a try and except
		try:
			network = obj.related_people_mobile_prefix_2.network.network
		except:
			network = None

		return {
			'id' : None if network == None else obj.related_people_mobile_prefix_2.network.id,
			'label' : network
		}

	def get_related_people_zip(self, obj):
		#Try get barangay
		try:
			barangay = obj.related_people_zip.barangay.barangay.upper()
			barangay_id = obj.related_people_zip.barangay.id
		except:
			barangay = None
			barangay_id = None
		try:
			return {
				'region' : obj.related_people_zip.municipality.province.region.region.upper(),
				'region_id' : obj.related_people_zip.municipality.province.region.id,
				'province' : obj.related_people_zip.municipality.province.province.upper(),
				'province_id' : obj.related_people_zip.municipality.province.id,
				'municipality' : obj.related_people_zip.municipality.municipality.upper(),
				'municipality_id' : obj.related_people_zip.municipality.id,
				'barangay' : barangay,
				'barangay_id' : barangay_id,
				'zip_code' : obj.related_people_zip.zip,
				'zip_id' : obj.related_people_zip.id
			}
		except:
			return {
				'region' : None,
				'region_id' : None,
				'province' : None,
				'province_id' : None,
				'municipality' : None,
				'municipality_id' : None,
				'barangay' : barangay,
				'barangay_id' : barangay_id,
				'zip_code' : None,
				'zip_id' : None
			}
