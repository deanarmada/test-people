from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.area_codes, name='api_area_code_list' )
]