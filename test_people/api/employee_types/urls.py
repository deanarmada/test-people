from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.employee_types, name='api_employee_types_list' )
]