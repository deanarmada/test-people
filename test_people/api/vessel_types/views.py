#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes

#test_people app
from .serializers import *
from test_people.decorators import token_required


@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
        vessel_type = VesselTypeSerializer(data=request.data, context={'logged_user': logged_user})

        if vessel_type.is_valid():
            vessel_type.save()
            return Response({
                'errors' : [],
                'data' : vessel_type.data,
            }, status=200)
            return
        else:
            return Response({
                'errors' : vessel_type.errors,
                'data' : [],
            }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    if request.method == 'POST':
        vessel_type = VesselTypes.objects.get(pk=request.data['id'])
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
        fields = request.data.get('fields', None)

        if fields is not None:
            return _update(request, fields=fields, logged_user=logged_user, vessel_type=vessel_type) #update field directly

        vessel_type = VesselTypeSerializer(vessel_type, data=request.data, context={'logged_user': logged_user})

        if vessel_type.is_valid():
            vessel_type.save()

            return Response({
                'errors' : [],
                'data' : vessel_type.data,
            })

        return Response({
            'errors' : vessel_type.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def vessel_types(request):
    if request.method == 'POST':
        from test_people.utils import tree_builder, filter_builder

        filters = None
        tree = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["tree_id", "level"] #default ordering
        query = Q() #default filtering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')
        
        
        vessel_types = VesselTypes.objects.filter(filter_builder(filters)).order_by(*order_by)
        vessel_types = vessel_types.get_descendants(include_self=True).filter(Q(vessel_type_sys_status=None) | Q(vessel_type_sys_status=''))
        # print (vessel_types.query) #See generated SQL query
        if request.data.get('tree') != None:
            tree = tree_builder(VesselTypes, vessel_types, ('children', 'vessel_type_parent', 'vessels', 'updated_by_id'))

        total = len(vessel_types)
        if limit != None and offset != None:
            vessel_types = vessel_types[int(offset):int(limit)]
        
        #tree = tree_builder(VesselTypes, vessel_types, ('children', 'vessel_type_parent', 'vessels', 'updated_by_id'))
        vessel_types = VesselTypeSerializer(vessel_types, many=True, extra_field = {
            'is_delete' : serializers.SerializerMethodField()
        })

        return Response({
            'errors' : [],
            #'data' : tree,
            'data' : vessel_types.data,
            #'total' : len(tree),
            'total' : total,
            'order_by' : order_by
        })
        
''' Parameters:
        required:
         - id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        vessel_type = VesselTypes.objects.filter(id=id)
        vessel_type = VesselTypeSerializer(vessel_type, many=True, extra_field = {
            'vessel_type_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : vessel_type.data,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - updated_by : User logged id 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    if request.method == 'POST':
        id = request.data.get('id')
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))

        vessel_type = VesselTypes.objects.filter(id__in=id)

        for v in vessel_type:
            count_children = VesselTypes.objects.filter(id=v.id).get_descendants().filter(Q(vessel_type_sys_status=None) | Q(vessel_type_sys_status=''))

            if len(count_children) > 0: #check if it has a children
                return Response({
                    'errors' : ['This record has a dependency record'],
                    'data' : []
                }, status=status.HTTP_400_BAD_REQUEST)
                break

        vessel_type.update(vessel_type_sys_status='D', updated_by=logged_user, date_updated=timezone.now())
        vessel_type = VesselTypeSerializer(vessel_type, many=True)

        return Response({
            'errors' : [],
            'data' : vessel_type.data
        })

@api_view(['GET'])
@csrf_exempt
def rules(request):
    if request.method == 'GET':
        from test_people.utils import rules_generator
        data = {
            'errors' : [],
            'data' : rules_generator(VesselTypeSerializer(fields=('vessel_type_code','vessel_type_description', 'vessel_type_full', 'vessel_type_sys_status')))
        }

        return Response(data)

def  _update(request, **kwargs):
    logged_user = kwargs['logged_user']
    fields = kwargs['fields']
    vessel_type = kwargs['vessel_type']

    fields.update({
        'updated_by' : logged_user,
        'date_updated' : timezone.now()
    })

    VesselTypes.objects.filter(id=vessel_type.id).update(**fields)
    vessel_type = VesselTypeSerializer(vessel_type)
    
    return Response({
        'errors' : [],
        'data' : vessel_type.data,
    })        