#django
from django.db.models import Q
from django.utils import timezone

#Rest Framework
from rest_framework import serializers

#People
from test_people.models import *

class CommonSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop
		tree = kwargs.pop('tree', None) #add tree data

		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(CommonSerializer, self).__init__(*args, **kwargs)

		if tree is not None:
			#print(self.data)
			def func(cls):
				print ('I am a class method')

			setattr(self, 'func', classmethod(func))
			self.func
			#self.data = tree
			return

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)



	updated_by = serializers.SerializerMethodField()

	def get_updated_by(self, obj):
		#This is special :(
		try:
			user_code = obj.updated_by.bio.user_code
		except:
			user_code = obj.updated_by.username
		
		return user_code

#Reference to a nested hierarchy
# class RecursiveField(serializers.Serializer):
#     def to_representation(self, value):
#     	print(VesselTypes.objects.get(id=value.id).get_descendants())
#     	serializer = self.parent.parent.__class__(VesselTypes.objects.get(id=value.id),context=self.context)
#     	return serializer.data

class VesselTypeSerializer(CommonSerializer):
	vessel_type_full = serializers.CharField(min_length=5, max_length=40,)
	vessel_type_code = serializers.CharField(min_length=3, max_length=6,)
	vessel_type_description = serializers.CharField(required=False, min_length=10, max_length=200,)

	class Meta:
		model = VesselTypes
		fields = '__all__'

	def get_is_delete(self, obj):
		count_children = VesselTypes.objects.filter(id=obj.id).get_descendants().filter(Q(vessel_type_sys_status=None) | Q(vessel_type_sys_status=''))
		if len(count_children) > 0: #check if it has a children
			return False
		else:
			return True

	def validate_vessel_type_code(self, value):
		vessel_type = VesselTypes.objects.filter(~Q(vessel_type_sys_status='D'), ~Q(vessel_type_sys_status=''), vessel_type_code = value.upper()) #not D and not empty string 
		if self.instance is not None: #means that it's in update mode
			#don't include current record to validate
			vessel_type = vessel_type.filter(~Q(id = self.instance.id))

		if len(vessel_type) != 0:
			raise serializers.ValidationError('This field must be unique.')

		return value

	def create(self, data):
		data = self.fix_data(data) #get vessel_type object instance and updated_by
		vessel_type = VesselTypes.objects.create(**data)

		return vessel_type

	def update(self,  instance, validated_data):
		data = self.fix_data(validated_data) #get vessel type object instance and updated_by

		for vessel_type in data: setattr(instance, vessel_type, data[vessel_type])
		instance.save()

		return instance

	def fix_data(self, data):
		data['vessel_type_code'] = data.get('vessel_type_code', '').upper()
		data.update({'updated_by' : self.context.get('logged_user')})

		return data