#django
from django.db.models import Q
from django.utils import timezone
from django.db import transaction
#django rest
from rest_framework import serializers
#people
from crew_menu.bio.models import *

#Customize serializer method Field
class GenericSerializerMethodField(serializers.SerializerMethodField):
		def __init__(self, *args, **kwargs):
			self.extra = kwargs.pop('extra', {}) #fields to add or alter
			#Set permanently as method property
			super().__init__(method_name = 'property',*args, **kwargs)

		#Override to_representation method
		def to_representation(self, value):
			self.extra['instance_name'] = self.field_name
			method = getattr(self.parent, self.method_name)
			return method(value, self.extra)

#Upper case serializer methd field
class UppercaseCharField(serializers.CharField):
    def to_internal_value(self, data):
    	data = str(data) #convert into string
    	if data == '':
    		return None

    	return data.upper()

class DateField(serializers.DateField):
		#Change data to None if string is empty
		def to_internal_value(self, data):
			if data == '':
				return None

			return super(DateField, self).to_internal_value(data)

class CommonSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop

		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(CommonSerializer, self).__init__(*args, **kwargs)
		

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)

	updated_by = serializers.SerializerMethodField()

	def get_updated_by(self, obj):
		#This is special :(
		try:
			user_code = obj.updated_by.bio.user_code
		except:
			user_code = obj.updated_by.username
		
		return user_code

	def property(self, obj, extra):
		model_instance = extra['instance_name']
		try:
			instance = getattr(obj, model_instance)
			value = getattr(instance, extra['field'])
		except:
			value = None

		#Assumed that every model instance have a id attribute :D
		return {
			'id' : None if value == None else instance.id,
			'label' : value
		}

class OptionSerializer(serializers.ModelSerializer):
	def __init__(self, *args, **kwargs):
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter
		super(OptionSerializer, self).__init__(*args, **kwargs)
		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

	class Meta:
		model = Options
		fields = '__all__'

