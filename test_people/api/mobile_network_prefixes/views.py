#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required


@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    pass

@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    pass

@api_view(['POST'])
@csrf_exempt
@token_required
def mobile_network_prefixes(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["-id"] #default ordering
        query = Q() #default filtering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        mobile_network_prefixes = MobileNetworkPrefixes.objects.filter(filter_builder(filters)).order_by(*order_by)
        #print (mobile_network_prefixes.query) #See generated SQL query

        total = len(mobile_network_prefixes)
        if limit != None or offset != None:
            mobile_network_prefixes = mobile_network_prefixes[int(offset):int(limit)]

        mobile_network_prefixes = MobileNetworkPrefixesSerializer(mobile_network_prefixes, many=True)

        return Response({
            'errors' : [],
            'data' : mobile_network_prefixes.data,
            'total' : total,
        })

@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    pass


@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    pass
