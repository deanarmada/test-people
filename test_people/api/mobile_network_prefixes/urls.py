from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.mobile_network_prefixes, name='api_mobile_network_prefix_list' )
]