from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.dialects, name='api_dialect_list' )
]