from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.social_medias, name='api_social_media_list' )
]