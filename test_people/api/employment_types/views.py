#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required


@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    pass

@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    pass

@api_view(['POST'])
@csrf_exempt
@token_required
def employment_types(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["-id"] #default ordering
        query = Q() #default filtering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        employment_types = EmploymentTypes.objects.filter(filter_builder(filters)).order_by(*order_by)
        #print (employment_types.query) #See generated SQL query

        total = len(employment_types)
        if limit != None or offset != None:
            employment_types = employment_types[int(offset):int(limit)]

        employment_types = EmploymentTypeSerializer(employment_types, many=True)

        return Response({
            'errors' : [],
            'data' : employment_types.data,
            'total' : total,
        })

@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    pass


@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    pass
