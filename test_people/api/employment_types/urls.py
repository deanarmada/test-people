from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.employment_types, name='api_employment_types_list' )
]