#python
import json

#Django Core
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token

#test_people app
from test_people.decorators import token_required
from .serializers import *

@api_view(['POST'])
@csrf_exempt
def login(request):
    if request.method == 'POST':
        if 'username' in request.data and 'password' in request.data:
            username = request.data.get('username')
            password = request.data.get('password')

            user = authenticate(username=username, password=password)

            if user:
                token, created = Token.objects.get_or_create(user=user)

                #TODO: Add permission and etc to user~
                if user.is_active:
                    return Response({
                        'errors' : [],
                        'data' : {
                            'id' : user.id,
                            'username' : user.username,
                            'first_name' : user.first_name,
                            'last_name' : user.last_name,
                            'email' : user.email
                        },
                        'token' : token.key,
                    })
                else:
                    return Response({
                        'errors' : [{
                            'messsage': 'User not active'
                        }],
                        'data' : [],
                        'token' : [],
                    }, status=status.HTTP_400_BAD_REQUEST)


        return Response({
            'errors' : [{
                'messsage': 'Invalid username or password'
            }],
            'data' : [],
            'token' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def logout(request):
    if request.method == 'POST':
        token = request.data.get('token')
        try:
            #Delete token record
            token = Token.objects.get(key=token)
            #token.delete()
            #request.token.delete()
        except Token.DoesNotExist:
             return Response({
                 'error': [{
                    'message': 'Token not found'
                 }]
             }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            'errors' : [],
            'data' : [],
        })


@api_view(['POST'])
@csrf_exempt
def options(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder
        filters = None

        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        options = Options.objects.filter(filter_builder(filters))
        print(options.query)
        options = OptionSerializer(options, many=True)

        return Response({
            'errors' : [],
            'data' : options.data,
        })

@api_view(['GET'])
@csrf_exempt
def test(request):
    from test_people.utils import tree_builder
    defers = 'children', 'vessel_type_parent', 'vessels', 'updated_by_id',

    dicts = tree_builder(VesselTypes, VesselTypes.objects.all(), defers)
    # print(json.dumps(dicts, indent=4))
    return Response({
        'errors' : [],
        'data' : dicts,
    })
