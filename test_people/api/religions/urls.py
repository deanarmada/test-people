from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.religions, name='api_religion_list' )
]