from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.uid_range, name='api_uid_range_list' ),
]