from test_people.api.serializers import *

class UIDRangeSerializer(CommonSerializer):
	def __init__(self, *args, **kwargs):
		super(UIDRangeSerializer, self).__init__()
		#Remove updated by field
		self.fields.pop('updated_by')

	class Meta:
		model = UIDRange
		fields = '__all__'

