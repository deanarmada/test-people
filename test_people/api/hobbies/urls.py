from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.hobbies, name='api_hobby_list' )
]