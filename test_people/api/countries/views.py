#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required

#TODO ADD NEW 
''' Parameters:
        required:
            - relationship_code
            - relationship_name
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    pass
''' Parameters:
        required:
            - id
            - relationship_code
            - relationship_name
            - updated_by *The ID of the user that is logged in
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    pass

@api_view(['POST'])
@csrf_exempt
@token_required
def countries(request):
    if request.method == 'POST':
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["country_name"] #default ordering
        query = Q() #default filtering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')
            # example: for testing purposes
            # field_search = {
            #     "first_name__contains" : "Al",
            #     "bio__user_code__contains": "BA"
            # }

            # Turn list of values into list of Q objects
            queries = [Q(**{key:value}) for key, value in filters.items()]
            
            # Take one Q object from the list
            query = queries.pop()
            
            # "Or" the Q object with the ones remaining in the list
            for item in queries:
                query |= item

        countries = Countries.objects.filter(query).order_by(*order_by)
        #print (users.query) #See generated SQL query

        total = len(countries)
        if limit != None or offset != None:
            countries = countries[int(offset):int(limit)]

        countries = CountrySerializer(countries, many=True)

        return Response({
            'errors' : [],
            'data' : countries.data,
            'total' : total,
        })

''' Parameters:
        required:
         - id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    pass

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - updated_by : User logged id 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    pass