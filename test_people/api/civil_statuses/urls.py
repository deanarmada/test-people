from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.civil_statuses, name='api_civil_statuses_list' )
]