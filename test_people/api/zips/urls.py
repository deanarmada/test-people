from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.zips, name='api_zip_list' )
]