#django
from django.db.models import Q

#Rest Framework
from rest_framework import serializers

#People
from test_people.models import *

class RelationshipTypeSerializer(serializers.Serializer):
	def __init__(self, *args, **kwargs):
		fields = kwargs.pop('fields', None) #fields to show
		defers = kwargs.pop('defers', None) #fields to drop
		self.extra_field = kwargs.pop('extra_field', {}) #fields to add or alter

		for key in self.extra_field:
			self.fields[key] = self.extra_field[key] #add  or alter field

		super(RelationshipTypeSerializer, self).__init__(*args, **kwargs)

		if fields is not None:
			# Drop any fields that are not specified in the `fields` argument.
			allowed = set(fields)
			existing = set(self.fields.keys())
			for field_name in existing - allowed:
				self.fields.pop(field_name)

		if defers is not None:
			#Drop fields
			for field_name in set(defers):
				self.fields.pop(field_name)

	class UserCodeValidator(object):
	    def __init__(self, base):
	        self.base = base #just to run validator ??? I think its a bug
	        self.parent = None

	    def __call__(self, value):
	    	bio = Bio.objects.filter(user_code = value.upper())

	    	if self.parent.instance is not None: #means that it's in update mode
	    		#don't include current record to validate
	    		bio = bio.filter(~Q(auth_user_id = self.parent.instance[0].id))

	    	if len(bio) != 0:
	    		raise serializers.ValidationError('This field must be unique.')

	    def set_context(self, value): #Documentation on django rest framework is b*llsh*t it says parameter must be "serializers_field" but it is actually "value"
	    	self.parent = value.parent

	id = serializers.IntegerField(read_only=True)
	relationship_code = serializers.CharField(max_length=6) #Unique? # Changed by Dean from 30 to 6 at 05-11-2016
	relationship_name = serializers.CharField(max_length=30)
	relationship_gender = serializers.CharField(max_length=3)
	updated_by = serializers.SerializerMethodField()
	date_updated = serializers.DateTimeField(read_only=True)

	def create(self, validated_data):
		validated_data.update({'updated_by' : self.context.get('logged_user')})
		return RelationshipTypes.objects.create(**validated_data)

	def update(self,  instance, validated_data):
		validated_data.update({'updated_by' : self.context.get('logged_user')})
		relationship_type = RelationshipTypes.objects.filter(id=instance.id)
		relationship_type.update(**validated_data)
		return relationship_type[0]

	def get_updated_by(self, obj):
		return {
		 	"first_name" : obj.updated_by.first_name, 
		 	"last_name" : obj.updated_by.last_name 
		}

