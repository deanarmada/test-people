from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.relationship_types, name='api_relationship_type_list' ),
	url(r'^add/$', views.add, name='api_relationship_type_add' ),
	url(r'^update/$', views.update, name='api_relationship_type_update' ),
	url(r'^delete/$', views.delete, name='api_relationship_type_delete' ),
	url(r'^detail/$', views.detail, name='api_relationship_type_detail' ),
]