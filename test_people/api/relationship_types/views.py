#python
import json

#Django Core
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q

#Rest Framework
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

#test_people app
from .serializers import *
from test_people.decorators import token_required

''' Parameters:
        required:
            - relationship_code
            - relationship_name
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def add(request):
    if request.method == 'POST':
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))

        relationship = RelationshipTypeSerializer(data=request.data, context={'logged_user': logged_user})
        if relationship.is_valid():
            relationship.save()

            return Response({
                'errors' : [],
                'data' : relationship.data,
            })

        return Response({
            'errors' : relationship.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)
''' Parameters:
        required:
            - id
            - relationship_code
            - relationship_name
            - updated_by *The ID of the user that is logged in
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def update(request):
    if request.method == 'POST':
        relationship = RelationshipTypes.objects.get(pk=request.data['id'])
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))
        fields = request.data.get('fields', None)

        #if fields is not None:
        #    return _update(request, fields=fields, logged_user=logged_user, user=user) #update field directly

        relationship = RelationshipTypeSerializer(relationship, data=request.data, context={'logged_user': logged_user})
        if relationship.is_valid():
            relationship.save()

            return Response({
                'errors' : [],
                'data' : relationship.data,
            })

        return Response({
            'errors' : relationship.errors,
            'data' : [],
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
@token_required
def relationship_types(request):
    if request.method == 'POST':
        from test_people.utils import filter_builder

        filters = None
        limit = request.data.get('limit')
        offset = request.data.get('offset')
        order_by = ["-id"] #default ordering
        query = Q() #default filtering

        if request.data.get('order_by') != None:
            order_by = request.data.get('order_by')
        if request.data.get('filters') != None:
            filters = request.data.get('filters')

        relationships = RelationshipTypes.objects.filter(filter_builder(filters), relationship_type_sys_status=None)
        #print (relationships.query) #See generated SQL query

        total = len(relationships)
        if limit != None or offset != None:
            relationships = relationships[int(offset):int(limit)]

        relationships = RelationshipTypeSerializer(relationships, many=True, extra_field = {
            'relationship_type_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : relationships.data,
            'total' : total,
        })
''' Parameters:
        required:
         - id
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def detail(request):
    if request.method == 'POST':
        id = request.data.get('id')
        relationship = RelationshipTypes.objects.filter(id=id)
        relationship = RelationshipTypeSerializer(relationship, many=True, extra_field = {
            'relationship_type_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : relationship.data,
        })

''' Parameters:
        required:
         - id : Request must be a json string e.g. [1, 2, 3]
         - updated_by : User logged id 
'''
@api_view(['POST'])
@csrf_exempt
@token_required
def delete(request):
    if request.method == 'POST':
        id = request.data.get('id')
        logged_user = User.objects.get(pk=request.data.get("updated_by", 1))

        relationship = RelationshipTypes.objects.filter(id__in=id)
        relationship.update(relationship_type_sys_status='D', updated_by=logged_user)

        user = RelationshipTypeSerializer(relationship, many=True, extra_field = {
            'relationship_type_sys_status' : serializers.CharField(read_only=True)
        })

        return Response({
            'errors' : [],
            'data' : user.data
        })


