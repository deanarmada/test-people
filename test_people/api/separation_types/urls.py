from django.conf.urls import include, url
from . import views

urlpatterns = [
	url(r'^$', views.separation_types, name='api_separation_type_list' )
]