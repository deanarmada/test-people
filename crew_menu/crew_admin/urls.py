from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='crew_admin_index' ),
	url(r'^rank-management/', include('crew_menu.crew_admin.rank_management.urls')),
	url(r'^user-management/', include('crew_menu.crew_admin.user_management.urls')),
	url(r'^principal-management/', include('crew_menu.crew_admin.principal_management.urls')),
	url(r'^vessel-type-management/', include('crew_menu.crew_admin.vessel_type_management.urls')),
]