from django.apps import AppConfig


class VesselTypeManagementConfig(AppConfig):
    name = 'vessel_type_management'
