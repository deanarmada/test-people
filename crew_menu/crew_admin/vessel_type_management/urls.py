from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='vessel_type_management_index' ),
]