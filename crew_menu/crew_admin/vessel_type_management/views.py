from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
@login_required
def index(request):
	template = 'vessel_type_management/index.html'
	# template = 'pure_html/crew_menu/crew_admin/user_management/index.html'
	context_dict = {}
	return render(request, template, context_dict)
	# return HttpResponse("RANK ADMIN INDEX")