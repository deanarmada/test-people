// Configure our routes

$('[data-toggle="tooltip"]').tooltip();   

peopleApp.config(function($routeProvider){
	$routeProvider
	// route for the homepage
	.when('/', {
		title: 'Vessel Types',
		templateUrl: '/static/vessel_type_management/html/index.html',
		controller: 'indexController',
		// Resolve is needed to complete the data first before rendering the template
		resolve: {
			vesselTypeList: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeList();
			},
		}
	})
	// route for the add page
	.when('/add', {
		title: 'Add Vessel Type',
		templateUrl: '/static/vessel_type_management/html/add.html',
		controller: 'addController',
		resolve: {
			vesselTypeParentList: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeParentList();
			},
			vesselTypeFormValidations: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeFormValidations();
			},
		}
	})
	// route for update page with parameters
	.when('/update/:id', {
		title: 'Update Vessel Type',
		templateUrl: '/static/vessel_type_management/html/update.html',
		controller: 'updateController',
		resolve: {
			vesselTypeParentList: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeParentList();
			},
			vesselTypeFormValidations: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeFormValidations();
			},
		}
	})
	// route for viewing page with parameters
	.when('/view/:id', {
		title: 'View Vessel Type',
		templateUrl: '/static/vessel_type_management/html/update.html',
		controller: 'viewController',
		resolve: {
			vesselTypeParentList: function(vesselTypeFactory){
				return vesselTypeFactory.vesselTypeParentList();
			},
		}
	})
	.otherwise({
		redirectTo: '/'
	});
});

peopleApp.controller('indexController', function($scope, $http, $rootScope, $controller,  $TreeDnDConvert, $timeout){
	// "url" and "total" variables came from the resolve on the routing template, 
	// which came from the returned key,value pairs in the vesselTypeFactory
	// $controller('modalController', {$scope: $scope});
	$controller('itemListController', {$scope: $scope});
	$scope.mainAPI = '/api/people/v1/vessel_types/';
	$scope.apiGetItems = $rootScope.domainName+$scope.mainAPI;
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/vessel_types/update/';
	$scope.apiDelete = $rootScope.domainName+'/api/people/v1/vessel_types/delete/';
	var tree;
    $scope.tree_data = {};
    $scope.my_tree = tree = {};
    $scope._filter = {};
    
    // console.log(data);
    $scope.items = data;

    $scope.expanding_property = {
        field:       "vessel_type_full",
        displayName: 'Name'
    };
    $scope.col_defs = [
        {
            field: "vessel_type_code",
            displayName: 'Code'
        },{
            displayName:  'Date Updated',
            field: "date_updated",
        },{
            displayName:  'Updated By',
            field: "updated_by",
        }, {
            displayName:  'Options',
            cellTemplate: '<view-glyphicon-button id="node.id"></view-glyphicon-button><a href="#/update/[[ node.id ]]"><span class="glyphicon '+$rootScope.glyphiconUpdate+' tree-middle-glyphicon-margin" data-toggle="tooltip" title="Update"></span></a><span ng-if="node.is_delete"  ng-click="toggleModal(node.id, node)" class="glyphicon '+$rootScope.glyphiconDelete+'" data-toggle="tooltip" title="Delete"></span><span ng-if="!node.is_delete"  class="glyphicon '+$rootScope.glyphiconDelete+' glyphicon-disabled" data-toggle="tooltip" title="Can not be deleted because of its dependencies"></span>'
            ,
        	titleClass:  'text-center',
        	cellClass:   'text-center',
        }];
    $scope.tree_data = $TreeDnDConvert.line2tree($scope.items, 'id', 'vessel_type_parent');
    $scope.treeDelete = function(id, name, url, node){
    	json = {};
    	json['id'] = [id];
    	json['updated_by'] = $rootScope.SSid;
    	$rootScope.notifications = true;
  		$http.post(url, json)
		.then(function(response){
			// name is just an indicator when restoration
			$scope.deleteArray.push({'name': response.data.data[0][name], 'id': id});
			tree.remove_node(node); // This is a function in the factory of tree-dnd
			// alert(JSON.stringify($scope.deleteArray));
			$rootScope.notifications = true;
		},function(response){
			id = document.querySelector( '#dismiss-modal' );
		    key = angular.element(id);
			key.click();
			// $rootScope.notifications = false;
			alert('Please check the following validations on the next alert and contact the creators regarding this error');
			alert(JSON.stringify(response.data.errors));
		});
    };
    $scope.treeRestore = function(event, id, field, value, url, obj, node){
		json = {};
		json['fields'] = {};
		json['fields'][field] = value;
		json['id'] = id;
		json['updated_by'] = $rootScope.SSid;
		if(obj){
			// remove retrieved item from delete to the deleteArray
			$rootScope.deleteArray = $rootScope.deleteArray.filter(function(x){
				return x.id != obj.id;
			});
			if ($rootScope.deleteArray.length == 0){
				id = document.querySelector( '#dismiss-modal' );
	    		key = angular.element(id);
	    		key.click();
			}
		}
		$http.post(url, json)
		.then(function(response){
			tree.add_node_root(node);
		},function(response){
			alert('Please check the following validations on the next alert and contact the creators regarding this error');
			alert(JSON.stringify(response.data.errors));
		});
	};
});

peopleApp.controller('reusableComponents', function($scope, $rootScope, $http){
	$scope.uniqueCheck = function(form , event, validity){
    	_this = angular.element(event.target);
    	// value = _this.val().toUpperCase();
    	value = _this.val();
    	// Be aware of the length check value
    	if ((value.length == 2 || value.length == 3) && !validity){
    		json = {};
    		json['filters'] = {};
    		json['filters']['vessel_type_code__iexact'] = value;
    		$http.post(domainName+"/api/people/v1/vessel_types/", json)
			.then(function(response){
				total = response.data.total;
				if(total){
					form.vessel_type_code.$setValidity("unique", false);
				}else{
					form.vessel_type_code.$setValidity("unique", true);
				}
			});
    	}else{
    		form.vessel_type_code.$setValidity("unique", true);
    	}
    };
});

peopleApp.controller('addController', function($scope, $controller, $rootScope, $http, $timeout){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiAdd = $rootScope.domainName+'/api/people/v1/vessel_types/add/';
	$scope.input = {};
    $scope.code_readonly = false;
    // Validations came from vesselTypeFactory
    // Example of validation expression is [[ validations.vessel_type_code.maxlength ]]
    // Technique is whatever is the submodel of the input that would be the key of the value of this json validation
	$scope.validations = validations;
    $scope.vesselTypeParentLists = [{
    	value: null,
    	label: 'Self'
    }];
    for( x in parentListData ){
    	$scope.vesselTypeParentLists.push({
    		value: parentListData[x]['id'],
    		label: "["+parentListData[x]['vessel_type_code']+"]"+" "+parentListData[x]['vessel_type_full'],
    	});
    };
});

peopleApp.controller('updateController', function($scope, $rootScope, $routeParams, $http, $controller){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/vessel_types/update/';
	$scope.title = "UPDATE";
	$scope.subTitle = "Updating";
	id = parseInt($routeParams.id);

	$http.post($rootScope.domainName+'/api/people/v1/vessel_types/detail/', { 'id':id })
	.then(function(response){
		$scope.input = {
			'vessel_type_code': response.data.data[0].vessel_type_code,
			'vessel_type_description': response.data.data[0].vessel_type_description,
			'vessel_type_full': response.data.data[0].vessel_type_full,
			'vessel_type_parent': response.data.data[0].vessel_type_parent,
		};
		$scope.id = id;
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});
	$scope.code_readonly = true;
	$scope.inUpdate = true;
	try {
    	$scope.validations = validations;
	}
	catch(err) {
	   // Catches validations so validations will not send a request when viewing
	}
    $scope.vesselTypeParentLists = [{
    	value: null,
    	label: 'Self'
    }];
    for( x in parentListData ){
    	$scope.vesselTypeParentLists.push({
    		value: parentListData[x]['id'],
    		label: "["+parentListData[x]['vessel_type_code']+"]"+" "+parentListData[x]['vessel_type_full'],
    	});
    };
});

peopleApp.controller('viewController', function($scope, $http, $controller){
	$controller('updateController', {$scope: $scope});
	$scope.title = "VIEW";
	$scope.subTitle = "Viewing";
	$scope.viewOnly = true;
});