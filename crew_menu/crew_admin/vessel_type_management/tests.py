from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from login.tests import LogInReusablesScript as LRS , LogInTestFT as LTFT

from unittest import skip

import time
import inspect

# Create your tests here.
# Command Line: python manage.py test crew_menu.crew_admin.vessel_type_management


# python manage.py test crew_menu.crew_admin.vessel_type_management.tests.VesselTypeManagementEmptyTestListPage
class VesselTypeManagementEmptyTestListPage(StaticLiveServerTestCase):
	# Used LTFT because LRS already inherits the appended json all throughout
	fixtures = LTFT.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/vessel-type-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Vessel Type Management List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)

	# @skip("Not yet prepared for testing")
	def test_IsEmpty(self):
		# User Goes to the the Vessel Type List Page
		self.set_goToListPage()
		time.sleep(1)
		# System checks the output if the table is empty
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertEquals(len(tr), 1)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))


class VesselTypeManagementTestListPage(StaticLiveServerTestCase):
	fixtures = LRS.fixtures
	# Command: python manage.py dumpdata test_people.vesseltypes --indent=2 > login/fixtures/vesseltypes.json
	fixtures.append('vesseltypes.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/vessel-type-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the User Management List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is User Management
		self.assertIn('Vessel Types', self.browser.title)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))

	# START Search Capability Testing
	# @skip("Not yet prepared for testing")
	def test_SearchTree(self):
		# pass
		self.set_goToListPage()
		search = self.browser.find_element_by_id("search-tree")
		search.send_keys("Bul")
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertEquals(len(tr), 3)
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Bulker', td[0].text)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))
	# END Search Capability Testing

	# @skip("Not yet prepared for testing")
	def test_SearchTreeWithBranch(self):
		# pass
		self.set_goToListPage()
		search = self.browser.find_element_by_id("search-tree")
		search.send_keys("Log")
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertEquals(len(tr), 3)
		td = tr[1].find_elements_by_tag_name("td")
		td2 = tr[2].find_elements_by_tag_name("td")
		self.assertEquals('Bulker', td[0].text)
		self.assertEquals('Log Bulker', td2[0].text)


# python manage.py test crew_menu.crew_admin.vessel_type_management.tests.VesselTypeManagementTestAddPage
class VesselTypeManagementTestAddPage(StaticLiveServerTestCase):
	fixtures = VesselTypeManagementTestListPage.fixtures
	# Command: python manage.py dumpdata test_people.vesseltypes --indent=2 > login/fixtures/vesseltypes.json
	fixtures.append('vesseltypes.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/vessel-type-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToAddPage(self):
		# User goes to List Page First
		VesselTypeManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Vessel Type', self.browser.title)
		time.sleep(3)

	# Used as a reusable component for update
	def _set_AddFieldsThenSave(self):
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the vessel type code field
		vessel_type_code_field = self.browser.find_element_by_name('vessel_type_code')
		vessel_type_code_field.send_keys('MA')
		# User fills up the vessel type description field
		vessel_type_description_field = self.browser.find_element_by_name('vessel_type_description')
		vessel_type_description_field.send_keys('Master Dean')
		# User fills up the vessel type department field
		vessel_type_department_field = Select(self.browser.find_element_by_name('vessel_type_department'))
		vessel_type_department_field.select_by_visible_text('Engine')
		# User fills up the order vessel type type
		vessel_type_type_field = Select(self.browser.find_element_by_name('vessel_type_type'))
		vessel_type_type_field.select_by_visible_text('Officer')
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()

	def _set_AddFields(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the vessel type code field
		vessel_type_code_field = self.browser.find_element_by_name('vessel_type_code')
		vessel_type_code_field.send_keys('MA')
		# User fills up the vessel type description field
		vessel_type_description_field = self.browser.find_element_by_name('vessel_type_description')
		vessel_type_description_field.send_keys('Master Dean')
		# User fills up the vessel type department field
		vessel_type_department_field = Select(self.browser.find_element_by_name('vessel_type_department'))
		vessel_type_department_field.select_by_visible_text('Engine')
		# User fills up the order vessel type type
		vessel_type_type_field = Select(self.browser.find_element_by_name('vessel_type_type'))
		vessel_type_type_field.select_by_visible_text('Officer')
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# TODO
	@skip("Not yet prepared for testing")
	def test_BasicAddVesselType(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('1', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Dean', tr[1].text)
		self.assertIn('Engine', tr[1].text)
		self.assertIn('Officer', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Add Vessel Type', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		vessel_type_code_field = self.browser.find_element_by_name('vessel_type_code')
		vessel_type_code_field_value = vessel_type_code_field.get_attribute("value");
		self.assertEquals(vessel_type_code_field_value, '')
		vessel_type_description_field = self.browser.find_element_by_name('vessel_type_description')
		vessel_type_description_field_value = vessel_type_description_field.get_attribute("value");
		self.assertEquals(vessel_type_description_field_value, '')
		vessel_type_department_field = self.browser.find_element_by_name('vessel_type_department')
		vessel_type_department_field_value = vessel_type_department_field.get_attribute("value");
		self.assertEquals(vessel_type_department_field_value, '')
		vessel_type_type_field = self.browser.find_element_by_name('vessel_type_type')
		vessel_type_type_field_value = vessel_type_type_field.get_attribute("value");
		self.assertEquals(vessel_type_type_field_value, '')
		time.sleep(3)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	# TODO
	@skip("Not yet prepared for testing")
	def test_ExitVesselType(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ShowNotifications(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		save = self.browser.find_element_by_id("save")
		save.click()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_AllFieldsRequired(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present
		for x in range(span_count):
			self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))


	# START Input Validations
	# Vessel Type Code must be 3-6 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_CodeLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the code field
		field = self.browser.find_element_by_name('vessel_type_code')
		field.send_keys("AB")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-vessel-type-code')
		error_text = "length must be 3 to 6"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Code must not reach four characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_CodeMaxLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the code field
		field = self.browser.find_element_by_name('vessel_type_code')
		field.send_keys("ABCDEFGHI")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(field_value, 'ABCDEFG')
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Description must be 10-100 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_DescriptionLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the description field
		field = self.browser.find_element_by_name('vessel_type_description')
		field.send_keys("ABC")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-vessel-type-description')
		error_text = "length must be 10 to 100"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Description must not reach four 101 characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_DescriptionMaxLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the description field
		field = self.browser.find_element_by_name('vessel_type_description')
		field_value = field.get_attribute("value");
		while (len(field_value) != 103):
			field_value = search.get_attribute("value");
			field.send_keys("A")
		# field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMMNNNNN")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 100)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Full must be 5-24 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_FullLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the full field
		field = self.browser.find_element_by_name('vessel_type_full')
		field.send_keys("ABC")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-vessel-type-full')
		error_text = "length must be 5 to 24"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Full must not reach four 25 characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_FullMaxLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the full field
		field = self.browser.find_element_by_name('vessel_type_full')
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMMNNNNN")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 24)
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Size Code must be 4-10 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_SizeCodeLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the size code field
		field = self.browser.find_element_by_name('vessel_type_size_code')
		field.send_keys("AB")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-vessel-type-size-code')
		error_text = "length must be 4 to 10"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Vessel Type Size Code must not reach eleven characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_SizeCodeMaxLength(self):
		# User goes to Add Vessel Type Page
		self._setGoToAddPage()
		# User types on the size code field
		field = self.browser.find_element_by_name('vessel_type_size_code')
		field.send_keys("ABCDEFGHILMNO")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(field_value, 'ABCDEFGHILM')
		print ("------------")
		print ("%s.%s DONE - 13" % (self.__class__.__name__, inspect.stack()[0][3]))
	# END Input Validations

# python manage.py test crew_menu.crew_admin.vessel_type_management.tests.VesselTypeManagementTestUpdatePage
class VesselTypeManagementTestUpdatePage(StaticLiveServerTestCase):

	fixtures = VesselTypeManagementTestListPage.fixtures
	# Command: python manage.py dumpdata test_people.vesseltypes --indent=2 > login/fixtures/vesseltypes.json
	fixtures.append('vesseltypes.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/vessel-type-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToUpdatePage(self):
		# User Goes to Add Vessel Types Page
		VesselTypeManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		VesselTypeManagementTestAddPage._set_AddFieldsThenSave(self)
		# Vessel TypeManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		# START User Clicks update on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		td_count = len(td)-1
		span = td[td_count].find_elements_by_tag_name("span")
		update_button = span[0].click()
		time.sleep(2)
		self.assertIn('Update Vessel Type', self.browser.title)
		# END User Clicks update on the first row
		# time.sleep(3)

	def _set_UpdateFields(self):
		# User goes to Update Vessel Type Page
		self._setGoToUpdatePage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(Keys.BACKSPACE, 0)
		# User fills up the vessel type code field
		vessel_type_code_field = self.browser.find_element_by_name('vessel_type_code')
		# User Makes sure the vessel type code is not editable and readonly
		vessel_type_code_field.send_keys(Keys.BACKSPACE, Keys.BACKSPACE)
		# User fills up the vessel type description field
		vessel_type_description_field = self.browser.find_element_by_name('vessel_type_description')
		vessel_type_description_field.send_keys('ssss')
		# User fills up the vessel type department field
		vessel_type_department_field = Select(self.browser.find_element_by_name('vessel_type_department'))
		vessel_type_department_field.select_by_visible_text('Deck')
		# User fills up the order vessel type type
		vessel_type_type_field = Select(self.browser.find_element_by_name('vessel_type_type'))
		vessel_type_type_field.select_by_visible_text('Rating')
		time.sleep(2)

	def _set_SaveUpdate(self):
		confirmation_save = self.browser.find_element_by_id("confirmation-save")
		confirmation_save.click()
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		self._set_SaveUpdate()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# TODO
	@skip("Not yet prepared for testing")
	def test_BasicUpdateVesselType(self):
		# User updates values from the fields
		self._set_UpdateFields()
		# User clicks the save button that opens a confirmation
		self._set_SaveUpdate()
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)
		time.sleep(2)
		# START User checks if updated values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('0', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Deanssss', tr[1].text)
		self.assertIn('Deck', tr[1].text)
		self.assertIn('Ratings', tr[1].text)
		# END User checks if updateed values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Update Vessel Type', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared except for vessel type code
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		vessel_type_code_field = self.browser.find_element_by_name('vessel_type_code')
		vessel_type_code_field_value = vessel_type_code_field.get_attribute("value");
		self.assertNotEquals(vessel_type_code_field_value, '')
		vessel_type_description_field = self.browser.find_element_by_name('vessel_type_description')
		vessel_type_description_field_value = vessel_type_description_field.get_attribute("value");
		self.assertEquals(vessel_type_description_field_value, '')
		vessel_type_department_field = self.browser.find_element_by_name('vessel_type_department')
		vessel_type_department_field_value = vessel_type_department_field.get_attribute("value");
		self.assertEquals(vessel_type_department_field_value, '')
		vessel_type_type_field = self.browser.find_element_by_name('vessel_type_type')
		vessel_type_type_field_value = vessel_type_type_field.get_attribute("value");
		self.assertEquals(vessel_type_type_field_value, '')
		time.sleep(2)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	
	# TODO
	@skip("Not yet prepared for testing")
	def test_ExitVesselType(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ShowNotifications(self):
		# User goes to Update Vessel Type Page
		self.test_ClearFields()
		self._set_SaveUpdate()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_AllFieldsRequired(self):
		# User goes to Update Vessel Type Page
		self.test_ClearFields()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present except for vessel type code
		for x in range(span_count):
			if x != 1:
				self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		self._set_SaveUpdate()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# START Input Validations
	# TODO
	@skip("Not yet prepared for testing")
	def test_OrderIsIntegerOnly(self):
		# User goes to Update Vessel Type Page
		self._setGoToUpdatePage()
		# User types on the order field
		field = self.browser.find_element_by_name('order')
		field_value = field.get_attribute("value");
		field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-vessel-type-order')
		error_text = "This field must be an integer"
		time.sleep(1)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))
	# END Input Validations

# python manage.py test crew_menu.crew_admin.vessel_type_management.tests.VesselTypeManagementTestDeletePage
class VesselTypeManagementTestDeletePage(StaticLiveServerTestCase):

	fixtures = VesselTypeManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/vessel-type-management/#/'

	def tearDown(self):
		self.browser.quit()

	def set_Delete(self, row):
		# START User Clicks delete on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		td_count = len(td)-1
		removed_text = td[1].text
		span = td[td_count].find_elements_by_tag_name("span")
		# System checks the title of the current page if it is Vessel Type Management
		self.assertIn('Vessel Types', self.browser.title)
		time.sleep(2)
		# User clicks delete icon
		span[1].click()
		time.sleep(2)
		# User clicks delete button
		delete_button = self.browser.find_element_by_id("delete")
		delete_button.click()
		# END User Clicks delete on the first row
		time.sleep(2)
		# System Checks if the vessel type is still available in the list page
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		self.assertNotEquals(removed_text, td[1].text)

	@skip("Not yet prepared for testing")
	def test_BasicDelete(self):
		# User Goes to Add Vessel Types Page
		VesselTypeManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		VesselTypeManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Currently three
	@skip("Not yet prepared for testing")
	def test_MultipleDelete(self):
		# User Goes to Add Vessel Types Page
		VesselTypeManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		VesselTypeManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_BasicRestore(self):
		# User Goes to Add Vessel Types Page
		VesselTypeManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		VesselTypeManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User clicks the undo button to restore the deleted
		undo_button = self.browser.find_element_by_css_selector(".undo-button")
		item_notification_text = self.browser.find_element_by_css_selector(".item-notification").text

		undo_button.click()
		time.sleep(1)
		# System checks if the item is restored
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals(item_notification_text, td[2].text)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_MultipleRestore(self):
		# User Goes to Add Vessel Types Page
		VesselTypeManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		VesselTypeManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# START System counts and checks for all items that can be undone
		show_deletes = self.browser.find_element_by_id("undo-delete")
		show_deletes.click()
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		# items_notifications = []
		print ("//////////////////")
		print (item_notifications[1].text)
		self.assertEquals(len(undo_buttons), 3)
		self.assertEquals(len(item_notifications), 3)
		# END System counts and checks for all items that can be undone
		for x in range(len(undo_buttons)):
			undo_buttons[x].click()
			time.sleep(2)
			# tr = self.browser.find_elements_by_tag_name("tr")
			# td = tr[1].find_elements_by_tag_name("td")
			# print (item_notifications[x].text)
			# print (td[2].text)
			# self.assertEquals(item_notifications[x].text, td[2].text)

		# System Checks that there shold be no notifications left
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		self.assertEquals(len(undo_buttons), 0)
		self.assertEquals(len(item_notifications), 0)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))