// Configure our routes

peopleApp.config(function($routeProvider){
	$routeProvider
	// route for the homepage
	.when('/', {
		title: 'Principals',
		templateUrl: '/static/principal_management/html/index.html',
		controller: 'indexController',
		// Resolve is needed to complete the data first before rendering the template
		resolve: {
			principalList: function(principalFactory){
				return principalFactory.principalList();
			},
			
		}
	})
	// route for the add page
	.when('/add', {
		title: 'Add Principal',
		templateUrl: '/static/principal_management/html/add.html',
		controller: 'addController',
		resolve: {
			principalMasterList: function(principalFactory){
				return principalFactory.principalMasterList();
			},
			principalFormValidations: function(principalFactory){
				return principalFactory.principalFormValidations();
			},
			countryList: function(countryFactory){
				return countryFactory.countryList();
			}
		}
	})
	// route for update page with parameters
	.when('/update/:id', {
		title: 'Update Principal',
		templateUrl: '/static/principal_management/html/update.html',
		controller: 'updateController',
		resolve: {
			// $route is used to get the parameter
			principalMasterList: function(principalFactory, $route){
				return principalFactory.principalMasterList($route.current.params.id);
			},
			principalFormValidations: function(principalFactory){
				return principalFactory.principalFormValidations();
			},
			countryList: function(countryFactory){
				return countryFactory.countryList();
			}
		}
	})
	// route for viewing page with parameters
	.when('/view/:id', {
		title: 'View Principal',
		templateUrl: '/static/principal_management/html/update.html',
		controller: 'viewController',
		resolve: {
			principalMasterList: function(principalFactory, $route){
				return principalFactory.principalMasterList($route.current.params.id);
			},
			// $route is used to get the parameter
			countryList: function(countryFactory){
				return countryFactory.countryList();
			}
		}
	})
	.otherwise({
		redirectTo: '/'
	});
});

peopleApp.controller('indexController', function($scope, $http, $rootScope, $controller){
	// "url" and "total" variables came from the resolve on the routing template, 
	// which came from the returned key,value pairs in the principalFactory
	// $controller('modalController', {$scope: $scope});
	$controller('itemListController', {$scope: $scope});
	$scope.mainAPI = '/api/people/v1/principals/';
	$scope.apiGetItems = $rootScope.domainName+$scope.mainAPI;
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/principals/update/';
	$scope.apiDelete = $rootScope.domainName+'/api/people/v1/principals/delete/';
	$scope.searchFilters = [{
		value: null,
		label: 'All'
	},{
		value: 'principal_code',
		label: 'Code'
	}, {
		value: 'principal_full',
		label: 'Name'
	}];
	// to get the tableHeadersLength
	$scope.tableHeaders = [
		{'name': 'ID', 'url': $scope.mainAPI, 'field':'-id', 'class':'caret-reversed initial-reversed', 'id':'principal_id'},
		{'name': 'Code', 'url': $scope.mainAPI, 'field':'-principal_code', 'class':'caret-reversed initial-reversed', 'id':'principal_code'},
		{'name': 'Name'},
		{'name': 'E-mail Address'},
		// {'name': 'DPA'},
		{'name': 'Updated By', 'url': $scope.mainAPI, 'field':'-updated_by__bio__user_code', 'class':'caret-reversed initial-reversed', 'id':'principal_updated_by'},
		{'name': 'Date Updated', 'url': $scope.mainAPI, 'field':'-date_updated', 'class':'caret-reversed initial-reversed', 'id':'principal_date_updated'},
	];

	$scope.searchItems = [];
	// Used for td colspan when results not found
	$scope.tableHeadersLength = $scope.tableHeaders.length + 2;

	$rootScope.totalPage = Math.ceil(total/$rootScope.limit);
	$scope.originalTotalPage = $rootScope.totalPage;

	$rootScope.loadPage = function(){
		// url came from principalFactory
		if(total){
			$scope.getPage(url);
		}else{
			$scope.noRecord = true;
		}
	}

	// Default fields that can be searched
	$scope.Items = ['principal_code', 'principal_full'];

	$http.post(domainName+url)
	.then(function(response){
		data = response.data.data;
		data.map(function(obj){ 
			for(x=0;x<$scope.Items.length;x++){
				$scope.searchItems.push(obj[$scope.Items[x]]); 
			}
		});
	});

	$scope.preSorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
	$scope.sorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
    $rootScope.loadPage();

    // Principal has its own update because of unique data parsing on the backend
    $scope.principalRestore = function(id, url, obj){
    	json = {};
    	json['data'] = {};
    	json['data']['updated_by'] = $rootScope.SSid;
    	json['data']['id'] = id;
    	json['data']['fields'] = {"principal_sys_status":null};
    	// stringify is needed for the data to be accepted in the backend
    	json['data'] = JSON.stringify(json['data']);
    	// alert(json);
    	if(obj){
			// remove retrieved item from delete to the deleteArray
			$rootScope.deleteArray = $rootScope.deleteArray.filter(function(x){
				return x.id != obj.id;
			});
			// alert($rootScope.deleteArray.length);
			if ($rootScope.deleteArray.length == 0){
				// $scope.showModal = false;
				id = document.querySelector( '#dismiss-modal' );
	    		key = angular.element(id);
	    		key.click();
			}
		}
		$http.post(url, json)
		.then(function(response){
			$rootScope.loadPage();
		},function(response){
			alert('Please check the following validations on the next alert and contact the creators regarding this error');
			alert(JSON.stringify(response.data.errors));
		});
    }
 
});

peopleApp.controller('reusableComponents', function($scope, $rootScope, $http, $timeout){
	$scope.uniqueCheck = function(form , event, validity){
    	_this = angular.element(event.target);
    	// value = _this.val().toUpperCase();
    	value = _this.val();
    	// Be aware of the length check value
    	if (value.length == 4 && !validity){
    		json = {};
    		json['filters'] = {};
    		json['filters']['principal_code__iexact'] = value;
    		$http.post(domainName+"/api/people/v1/principals/", json)
			.then(function(response){
				total = response.data.total;
				if(total){
					form.principal_code.$setValidity("unique", false);
				}else{
					form.principal_code.$setValidity("unique", true);
				}
			});
    	}else{
    		form.principal_code.$setValidity("unique", true);
    	}
    };
    $scope.principalMastersLists = [{
    	value: null,
    	label: 'Self'
    }];

    // Prepopulates the Main Principal selection
    for( x in masterListData ){
    	$scope.principalMastersLists.push({
    		value: masterListData[x]['id'],
    		label: "["+masterListData[x]['principal_code']+"]"+" "+masterListData[x]['principal_full'],
    	});
    };
    
    try {
    	$scope.validations = validations;
	}
	catch(err) {
	   // Catches validations so validations will not send a request when viewing
	}
    $scope.countries = data;

    $scope.flagVisibility = function(json, value){
    	if(value < json.length){
    		return true;
    	}
    }

    $scope.addNestedModelsWithFocus = function(json, event){
    	json.push({});
    	_this = angular.element(event.target);
    	// For autofocusing after adding a new contact or address
    	$timeout(function(){
        	_this.parents("div.parent-most-addNestedModel").prev().find(".principal_contact_info_designation").focus();
        }, 200);
    }

    $scope.principalContactInfos = [{'principal_contact_info_designation':'(ECO) Environment Compliance Officer'}, {'principal_contact_info_designation':'(CSO) Chief Security Officer'}, {'principal_contact_info_designation':'(DPA) Designated Person Ashore'}];
	// $scope.principalContactInfos = [{"principal_contact_info_designation":"DPA","principal_contact_info_last_name":"Armada","principal_contact_info_first_name":"Dean Christian","principal_contact_info_middle_name":"Guinto","principal_contact_info_email_address":"adgc@manship.com","principal_contact_info_contact_number_1":7992501,"principal_contact_info_contact_number_2":7992502,"principal_contact_info_contact_number_3":9277411526,"principal_contact_info_contact_number_4":9277411527},{"principal_contact_info_designation":"Alternate DPA","principal_contact_info_last_name":"Guinto","principal_contact_info_first_name":"Christian","principal_contact_info_middle_name":"Madrid","principal_contact_info_email_address":"adgc.manship@gmail.com","principal_contact_info_contact_number_1":8992501,"principal_contact_info_contact_number_2":8992502,"principal_contact_info_contact_number_3":9276411526,"principal_contact_info_contact_number_4":9276411527},{"principal_contact_info_designation":"Technical Manager","principal_contact_info_last_name":"Bernabe","principal_contact_info_first_name":"Allan","principal_contact_info_middle_name":"Jed","principal_contact_info_email_address":"bata@manshp.com","principal_contact_info_contact_number_1":12345614,"principal_contact_info_contact_number_3":9178162961,"principal_contact_info_contact_number_4":9178162861,"principal_contact_info_contact_number_2":14346456}];
	$scope.originalPrincipalAddresses = [{}];
	// $scope.originalPrincipalAddresses = [{"principal_address":"Lot 13 Block 6 Don Pio Street, Don Enrique Heights","principal_address_city":"Quezon City","principal_address_country":{"id":400},"principal_address_state":"NCR","principal_address_zip":1200,"principal_address_contact_number_1":652634,"principal_address_contact_number_2":41235,"principal_address_contact_number_3":124351},{"principal_address":"Villamor","principal_address_city":"Pasay","principal_address_country":{"id":409},"principal_address_state":"NCR","principal_address_zip":1309,"principal_address_contact_number_1":51353,"principal_address_contact_number_2":535643,"principal_address_contact_number_3":13215412}];
	$scope.principalAddresses = angular.copy($scope.originalPrincipalAddresses);

	$scope.originalPrincipalContactInfos = angular.copy($scope.principalContactInfos);
});

peopleApp.controller('addController', function($scope, $controller, $rootScope, $http, $timeout, Upload){
	$controller('reusableComponents', {$scope: $scope});
	// operation is used to separate some fields on the form-notifications.html
	$scope.operation = "add";
	$scope.apiAdd = $rootScope.domainName+'/api/people/v1/principals/add/';
	// Validations came from principalFactory
    // Example of validation expression is [[ validations.principal_code.maxlength ]]
    // Technique is whatever is the submodel of the input that would be the key of the value of this json validation
    

    // $scope.principalMastersLists.push({});

	// $scope.principal_contact_infos = [{'principal_contact_info_designation':'DPA'}, {'principal_contact_info_designation':'Alternate DPA'}];
    $scope.input = { "principal_contact_infos":$scope.principalContactInfos, "principal_addresses":$scope.principalAddresses };
    // $scope.input = { "principal_code":"HHHM","principal_full":"Hellespont", "principal_sss":"1234567890","principal_philhealth":"123512415325","principal_email_address":"armadadean@yahoo.com", "principal_contact_infos":$scope.principalContactInfos, "principal_addresses":$scope.principalAddresses };
    $scope.code_readonly = false;

    $scope.principalClearInput = function(){
    	$scope.principalContactInfos = angular.copy($scope.originalPrincipalContactInfos);
    	$scope.principalAddresses = angular.copy($scope.originalPrincipalAddresses);
    	$scope.input = { "principal_contact_infos":$scope.principalContactInfos, "principal_addresses":$scope.principalAddresses, "principal_master":{"id":null} };
    }

    $scope.removeNestedModels = function(json, inputs){
		json.splice(inputs, 1);
    }

    $scope.principalSaveInput = function(formStatus, json, url, file){
    	if (formStatus){
			$rootScope.notifications = true;
			angular.element("html, body").animate({scrollTop: 0}, "slow");
		}else{
			json['updated_by'] = $rootScope.SSid;
			if(typeof(file) == "object"){
				file.upload = Upload.upload({
			      url: url,
			      data: {
			      		"data":JSON.stringify(json),
			      		"principal_logo": file,
			      	},
			    });
			    file.upload.then(function (response) {
			      	$rootScope.notifications = false;
			      	$scope.showModal = false;
					window.location.href = '#/';
			    }, function (response) {
			      	alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
			    });
			}
			else{
				_json = {}
				_json['data'] = {};
				_json['data'] = JSON.stringify(json);
				$http.post(url, _json)
				.then(function(response){
					$rootScope.notifications = false;
					window.location.href = '#/';
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}
		}
    }
});
peopleApp.controller('updateController', function($scope, $rootScope, $routeParams, $http, $controller, Upload){
	$controller('reusableComponents', {$scope: $scope});
	id = parseInt($routeParams.id);
	// operation is used to separate some fields on the form-notifications.html
	$scope.operation = "update";
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/principals/update/';
	$scope.title = "UPDATE";
	$scope.subTitle = "Updating";
	$scope.code_readonly = true;

	$scope.removeNestedModels = function(json, inputs){
		// alert(inputs);
		// alert($scope.originalPrincipalContactInfos.length);
		if( inputs > $scope.originalPrincipalContactInfos.length ){
			json.splice(inputs, 1);
		}else{
			json["delete"] = true;
			json[inputs]['delete'] = true;
		}
    }

	$scope.principalUpdateInput = function(formStatus, json, url, file){
		if (formStatus){
			$rootScope.notifications = true;
			angular.element("html, body").animate({scrollTop: 0}, "slow");
		}else{
			// To remove the $$hashkey from ng-repeat
			json = angular.copy(json);
			json['updated_by'] = $rootScope.SSid;
			json['id'] = $scope.id;
			if(typeof(file) == "object" && file != null){
				file.upload = Upload.upload({
			      url: url,
			      data: {
			      		"data":JSON.stringify(json),
			      		"principal_logo": file,
			      	},
			    });
			    file.upload.then(function (response) {
			      	$rootScope.notifications = false;
			      	$scope.showModal = false;
					window.location.href = '#/';
			    }, function (response) {
			      	alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
			    });
			}
			else{
				_json = {}
				_json['data'] = {};
				_json['data'] = JSON.stringify(json);
				$http.post(url, _json)
				.then(function(response){
					// alert('success');
					$rootScope.notifications = false;
					window.location.href = '#/';
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}  
		}
	};

	$scope.principalClearInput = function(){
		_value = $scope.input['principal_code'];
    	$scope.principalContactInfos = angular.copy($scope.originalPrincipalContactInfos);
    	$scope.principalAddresses = angular.copy($scope.originalPrincipalAddresses);
    	$scope.input = { 'principal_code':_value, "principal_contact_infos":$scope.principalContactInfos, "principal_addresses":$scope.principalAddresses, "principal_master":{"id":null} };
    }
	
	$http.post($rootScope.domainName+'/api/people/v1/principals/detail/', { 'id':id })
	.then(function(response){
		$scope.principalContactInfos = response.data.data[0].principal_contact_infos;
		$scope.principalAddresses = response.data.data[0].principal_addresses;
		$scope.principal_logo = response.data.data[0].principal_logo;
		$scope.id = id;
		if(response.data.data[0].principal_master){
			principal_master = response.data.data[0].principal_master.id;
		}else{
			principal_master = response.data.data[0].principal_master;
		}
		$scope.input = {
			'principal_code': response.data.data[0].principal_code,
			'principal_full': response.data.data[0].principal_full,
			'principal_master': {"id": principal_master},
			'principal_sss': response.data.data[0].principal_sss,
			'principal_philhealth': response.data.data[0].principal_philhealth,
			'principal_email_address': response.data.data[0].principal_email_address,
			'principal_contact_infos': $scope.principalContactInfos,
			'principal_addresses': $scope.principalAddresses
		};
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});
});

peopleApp.controller('viewController', function($scope, $http, $controller){
	$controller('updateController', {$scope: $scope});
	$scope.title = "VIEW";
	$scope.subTitle = "Viewing";
	$scope.viewOnly = true;
});

peopleApp.controller('modalController', function($scope){
	$scope.toggleModal = function(){
    	$scope.showModal = !$scope.showModal;
    };
});