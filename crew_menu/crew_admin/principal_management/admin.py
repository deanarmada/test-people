from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, widgets, fields

from crew_menu.bio.models import Principals 

class PrincipalsResource(resources.ModelResource):
	class Meta:
		model = Principals

class PrincipalsImport(ImportExportModelAdmin):
	resource_class = PrincipalsResource


# Register your models here.
admin.site.register(Principals, PrincipalsImport)