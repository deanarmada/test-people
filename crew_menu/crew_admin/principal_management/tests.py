from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from login.tests import LogInReusablesScript as LRS , LogInTestFT as LTFT

from unittest import skip

import time
import inspect

# Create your tests here.
# Command Line: python manage.py test crew_menu.crew_admin.principal_management


class PrincipalManagementEmptyTestListPage(StaticLiveServerTestCase):
	# Used LTFT because LRS already inherits the appended json all throughout
	fixtures = LTFT.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/principal-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Principal Management List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)

	@skip("Not yet prepared for testing")
	def test_IsEmpty(self):
		# User Goes to the the Principal List Page
		self.set_goToListPage()
		time.sleep(1)
		# System checks the output if the table is empty
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('No records found', tr[1].text)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))


class PrincipalManagementTestListPage(StaticLiveServerTestCase):
	fixtures = LRS.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/principal-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the User Management List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is User Management
		self.assertIn('Principal Management', self.browser.title)

	def set_SearchAutocomplete(self, search_text):
		# User Types in the search box
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(2)
		# System looks for suggestions via values typed on the search box
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		self.assertNotEquals(len(items), 0)
		# User clicks on the first suggestion
		items[0].click()
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertNotEquals(tr[0].text, "No records found")

	def set_NoShowAutocompleteSearch(self, search_text):
		# User Types in the search box
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(2)
		# System looks for suggestions via values typed on the search box which should be none
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		self.assertEquals(len(items), 0)
		search_value = search.get_attribute("value");
		# User hits backspace until there are no more values on the searchbox
		while (search_value != ""):
			search_value = search.get_attribute("value");
			search.send_keys(Keys.BACKSPACE)

	# START Search Capability Testing
	@skip("Not yet prepared for testing")
	def test_SearchAutocompleteFIELD(self):
		pass
	# END Search Capability Testing


# python manage.py test crew_menu.crew_admin.principal_management.tests.PrincipalManagementTestAddPage
class PrincipalManagementTestAddPage(StaticLiveServerTestCase):
	fixtures = PrincipalManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/principal-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToAddPage(self):
		# User goes to List Page First
		PrincipalManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Principal', self.browser.title)
		time.sleep(3)

	# Used as a reusable component for update
	def _set_AddFieldsThenSave(self):
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the principal code field
		principal_code_field = self.browser.find_element_by_name('principal_code')
		principal_code_field.send_keys('MA')
		# User fills up the principal description field
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field.send_keys('Master Dean')
		# User fills up the principal department field
		principal_department_field = Select(self.browser.find_element_by_name('principal_department'))
		principal_department_field.select_by_visible_text('Engine')
		# User fills up the order principal type
		principal_type_field = Select(self.browser.find_element_by_name('principal_type'))
		principal_type_field.select_by_visible_text('Officer')
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()

	def _set_AddFields(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the principal code field
		principal_code_field = self.browser.find_element_by_name('principal_code')
		principal_code_field.send_keys('MA')
		# User fills up the principal description field
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field.send_keys('Master Dean')
		# User fills up the principal department field
		principal_department_field = Select(self.browser.find_element_by_name('principal_department'))
		principal_department_field.select_by_visible_text('Engine')
		# User fills up the order principal type
		principal_type_field = Select(self.browser.find_element_by_name('principal_type'))
		principal_type_field.select_by_visible_text('Officer')
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# TODO
	@skip("Not yet prepared for testing")
	def test_BasicAddPrincipal(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('1', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Dean', tr[1].text)
		self.assertIn('Engine', tr[1].text)
		self.assertIn('Officer', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Add Principal', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		principal_code_field = self.browser.find_element_by_name('principal_code')
		principal_code_field_value = principal_code_field.get_attribute("value");
		self.assertEquals(principal_code_field_value, '')
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field_value = principal_description_field.get_attribute("value");
		self.assertEquals(principal_description_field_value, '')
		principal_department_field = self.browser.find_element_by_name('principal_department')
		principal_department_field_value = principal_department_field.get_attribute("value");
		self.assertEquals(principal_department_field_value, '')
		principal_type_field = self.browser.find_element_by_name('principal_type')
		principal_type_field_value = principal_type_field.get_attribute("value");
		self.assertEquals(principal_type_field_value, '')
		time.sleep(3)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	# TODO
	@skip("Not yet prepared for testing")
	def test_ExitPrincipal(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ShowNotifications(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		save = self.browser.find_element_by_id("save")
		save.click()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_AllFieldsRequired(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present
		for x in range(span_count):
			self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))


	# START Input Validations
	# Principal Code must be 4-6 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_CodeLength(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# User types on the code field
		field = self.browser.find_element_by_name('principal_code')
		field.send_keys("ABC")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-principal-code')
		error_text = "length must be 4 to 6"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal Code must not reach four characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_CodeMaxLength(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# User types on the code field
		field = self.browser.find_element_by_name('principal_code')
		field.send_keys("ABCDEFGHI")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(field_value, 'ABCDEF')
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal Description must be 10-50 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_DescriptionLength(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# User types on the description field
		field = self.browser.find_element_by_name('principal_description')
		field.send_keys("ABC")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-principal-description')
		error_text = "length must be 10 to 50"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal Description must not reach four 51 characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_DescriptionMaxLength(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# User types on the description field
		field = self.browser.find_element_by_name('principal_description')
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMMNNNNN")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	# SSS must be exactly twelve characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_SSSExactlyTwelveCharacters(self):
		# User adds another user
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Principal', self.browser.title)
		user_code = self.browser.find_element_by_name('sss')
		user_code.send_keys("abcdefghijkl")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-sss')
		error_text = "Must be exactly 12 characters"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	# PhilHealth must be exactly twelve characters
	# TODO
	@skip("Not yet prepared for testing")
	def test_PhilHealthExactlyFourteenCharacters(self):
		# User adds another user
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Principal', self.browser.title)
		user_code = self.browser.find_element_by_name('sss')
		user_code.send_keys("abcdefghijklmn")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-sss')
		error_text = "Must be exactly 14 characters"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal email max is 50
	# TODO
	@skip("Not yet prepared for testing")
	def test_PrincipalEmailMaxCharacters(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('email')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGHHASDWERGVZCZASDWRETEWASDASDASDASDCZXAFAWE@gmail.com")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal Contact email max is 50
	# TODO
	@skip("Not yet prepared for testing")
	def test_PrincipalContactInfoEmailMaxCharacters(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('email')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGHHASDWERGVZCZASDWRETEWASDASDASDASDCZXAFAWE@gmail.com")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 13" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Principal Can Add Multiple and Dynamic Alternate DPA
	# TODO
	@skip("Not yet prepared for testing")
	def test_PrincipalAddDynamicDPA(self):
		# 	Example JSON parameter request
		# 		 data = {
		#     "principal_master" : { "id": 22}, 
		#     "principal_code" : "HSM8",
		#     "principal_full" : "testasdasd",
		#     "principal_sss" : "123456789112",
		#     "principal_philhealth" : "12345678911234",
		#     "principal_email_address" : "allan.bernabe@gmail.com",
		#     "principal_contact_infos" : [{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     },{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     },{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     }],

		#     "updated_by" : 1
		# 	}
		# User 
		# User Clicks an Add Icon
		# User goes to Add Principal Page
		# self._setGoToAddPage()
		# User clicks an add icon to add more DPA fields
		# User can save all those DPA one at a time 
		pass

	# Principal Can Add Multiple and Dynamic Addresses
	# TODO
	@skip("Not yet prepared for testing")
	def test_PrincipalAddDynamicAddress(self):
		# 	Example JSON parameter request
		# 		 data = {
		#     "principal_master" : { "id": 22}, 
		#     "principal_code" : "HSM8",
		#     "principal_full" : "testasdasd",
		#     "principal_sss" : "123456789112",
		#     "principal_philhealth" : "12345678911234",
		#     "principal_email_address" : "allan.bernabe@gmail.com",
		#     "principal_contact_infos" : [{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     },{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     },{
		#         "principal_contact_info_first_name" : 'Allan',
		#         "principal_contact_info_last_name" : 'Bernabe',
		#         "principal_contact_info_contact_number_1" : '121212121',
		#         "updated_by" : 1
		#     }],

		#     "updated_by" : 1
		# 	}
		# User 
		# User Clicks an Add Icon
		# User goes to Add Principal Page
		# self._setGoToAddPage()
		# User clicks an add icon to add more Address fields
		# User can save all those Address one at a time 
		pass

	@skip("Not yet prepared for testing")
	def test_PrincipalDeleteDynamicAddress(self):
		pass

	@skip("Not yet prepared for testing")
	def test_PrincipalDeleteDynamicDPA(self):
		pass

	@skip("Not yet prepared for testing")
	def test_PrincipalDefaultSelf(self):
		pass


		


	# END Input Validations

# python manage.py test crew_menu.crew_admin.principal_management.tests.PrincipalManagementTestUpdatePage
class PrincipalManagementTestUpdatePage(StaticLiveServerTestCase):

	fixtures = PrincipalManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/principal-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToUpdatePage(self):
		# User Goes to Add Principals Page
		PrincipalManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		PrincipalManagementTestAddPage._set_AddFieldsThenSave(self)
		# PrincipalManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		# START User Clicks update on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		td_count = len(td)-1
		span = td[td_count].find_elements_by_tag_name("span")
		update_button = span[0].click()
		time.sleep(2)
		self.assertIn('Update Principal', self.browser.title)
		# END User Clicks update on the first row
		# time.sleep(3)

	def _set_UpdateFields(self):
		# User goes to Update Principal Page
		self._setGoToUpdatePage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(Keys.BACKSPACE, 0)
		# User fills up the principal code field
		principal_code_field = self.browser.find_element_by_name('principal_code')
		# User Makes sure the principal code is not editable and readonly
		principal_code_field.send_keys(Keys.BACKSPACE, Keys.BACKSPACE)
		# User fills up the principal description field
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field.send_keys('ssss')
		# User fills up the principal department field
		principal_department_field = Select(self.browser.find_element_by_name('principal_department'))
		principal_department_field.select_by_visible_text('Deck')
		# User fills up the order principal type
		principal_type_field = Select(self.browser.find_element_by_name('principal_type'))
		principal_type_field.select_by_visible_text('Rating')
		time.sleep(2)

	def _set_SaveUpdate(self):
		confirmation_save = self.browser.find_element_by_id("confirmation-save")
		confirmation_save.click()
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		self._set_SaveUpdate()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# TODO
	@skip("Not yet prepared for testing")
	def test_BasicUpdatePrincipal(self):
		# User updates values from the fields
		self._set_UpdateFields()
		# User clicks the save button that opens a confirmation
		self._set_SaveUpdate()
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		time.sleep(2)
		# START User checks if updated values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('0', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Deanssss', tr[1].text)
		self.assertIn('Deck', tr[1].text)
		self.assertIn('Ratings', tr[1].text)
		# END User checks if updateed values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Update Principal', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared except for principal code
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		principal_code_field = self.browser.find_element_by_name('principal_code')
		principal_code_field_value = principal_code_field.get_attribute("value");
		self.assertNotEquals(principal_code_field_value, '')
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field_value = principal_description_field.get_attribute("value");
		self.assertEquals(principal_description_field_value, '')
		principal_department_field = self.browser.find_element_by_name('principal_department')
		principal_department_field_value = principal_department_field.get_attribute("value");
		self.assertEquals(principal_department_field_value, '')
		principal_type_field = self.browser.find_element_by_name('principal_type')
		principal_type_field_value = principal_type_field.get_attribute("value");
		self.assertEquals(principal_type_field_value, '')
		time.sleep(2)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	
	# TODO
	@skip("Not yet prepared for testing")
	def test_ExitPrincipal(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ShowNotifications(self):
		# User goes to Update Principal Page
		self.test_ClearFields()
		self._set_SaveUpdate()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_AllFieldsRequired(self):
		# User goes to Update Principal Page
		self.test_ClearFields()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present except for principal code
		for x in range(span_count):
			if x != 1:
				self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		self._set_SaveUpdate()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# START Input Validations
	# TODO
	@skip("Not yet prepared for testing")
	def test_OrderIsIntegerOnly(self):
		# User goes to Update Principal Page
		self._setGoToUpdatePage()
		# User types on the order field
		field = self.browser.find_element_by_name('order')
		field_value = field.get_attribute("value");
		field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-principal-order')
		error_text = "This field must be an integer"
		time.sleep(1)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))
	# END Input Validations

	@skip("Not yet prepared for testing")
	def test_PrincipalDeleteDynamicAddress(self):
		pass

	@skip("Not yet prepared for testing")
	def test_PrincipalDeleteDynamicDPA(self):
		pass

	# This issue occurred when User tried to save after adding then deleting that added contact
	# What happened was the supposed to be deleted will still go to the server because it is just hidden 
	@skip("Not yet prepared for testing")
	def test_PrincipalDeleteAnAddedContactThenSave(self):
		pass

# python manage.py test crew_menu.crew_admin.principal_management.tests.PrincipalManagementTestDeletePage
class PrincipalManagementTestDeletePage(StaticLiveServerTestCase):

	fixtures = PrincipalManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/principal-management/#/'

	def tearDown(self):
		self.browser.quit()

	def set_Delete(self, row):
		# START User Clicks delete on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		td_count = len(td)-1
		removed_text = td[1].text
		span = td[td_count].find_elements_by_tag_name("span")
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		time.sleep(2)
		# User clicks delete icon
		span[1].click()
		time.sleep(2)
		# User clicks delete button
		delete_button = self.browser.find_element_by_id("delete")
		delete_button.click()
		# END User Clicks delete on the first row
		time.sleep(2)
		# System Checks if the principal is still available in the list page
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		self.assertNotEquals(removed_text, td[1].text)

	@skip("Not yet prepared for testing")
	def test_BasicDelete(self):
		# User Goes to Add Principals Page
		PrincipalManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		PrincipalManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Currently three
	@skip("Not yet prepared for testing")
	def test_MultipleDelete(self):
		# User Goes to Add Principals Page
		PrincipalManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		PrincipalManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_BasicRestore(self):
		# User Goes to Add Principals Page
		PrincipalManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		PrincipalManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User clicks the undo button to restore the deleted
		undo_button = self.browser.find_element_by_css_selector(".undo-button")
		item_notification_text = self.browser.find_element_by_css_selector(".item-notification").text

		undo_button.click()
		time.sleep(1)
		# System checks if the item is restored
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals(item_notification_text, td[2].text)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_MultipleRestore(self):
		# User Goes to Add Principals Page
		PrincipalManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		PrincipalManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# START System counts and checks for all items that can be undone
		show_deletes = self.browser.find_element_by_id("undo-delete")
		show_deletes.click()
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		# items_notifications = []
		print ("//////////////////")
		print (item_notifications[1].text)
		self.assertEquals(len(undo_buttons), 3)
		self.assertEquals(len(item_notifications), 3)
		# END System counts and checks for all items that can be undone
		for x in range(len(undo_buttons)):
			undo_buttons[x].click()
			time.sleep(2)
			# tr = self.browser.find_elements_by_tag_name("tr")
			# td = tr[1].find_elements_by_tag_name("td")
			# print (item_notifications[x].text)
			# print (td[2].text)
			# self.assertEquals(item_notifications[x].text, td[2].text)

		# System Checks that there shold be no notifications left
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		self.assertEquals(len(undo_buttons), 0)
		self.assertEquals(len(item_notifications), 0)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))




# First attempt of complete input json data 
# {"principal_contact_infos":[
# 		{
# 			"principal_contact_info_designation":"DPA",
# 			"principal_contact_info_last_name":"Armada",
# 			"principal_contact_info_first_name":"Dean Christian",
# 			"principal_contact_info_middle_name":"Guinto",
# 			"principal_contact_info_email_address":"armadadean@yahoo.com",
# 			"principal_contact_info_contact_number_1":"7992501",
# 			"principal_contact_info_contact_number_2":"7992502",
# 			"principal_contact_info_contact_number_3":"09277411526",
# 			"principal_contact_info_contact_number_4":"09277411527"
# 		},
# 		{
# 			"principal_contact_info_designation":"Alternate DPA",
# 			"principal_contact_info_last_name":"Viray",
# 			"principal_contact_info_first_name":"Gayle",
# 			"principal_contact_info_middle_name":"Mindora",
# 			"principal_contact_info_email_address":"viray@yahoo.com",
# 			"principal_contact_info_contact_number_1":"87000",
# 			"principal_contact_info_contact_number_2":"87001",
# 			"principal_contact_info_contact_number_3":"09178772069",
# 			"principal_contact_info_contact_number_4":"09178772068"
# 		},
# 		{
# 			"principal_contact_info_designation":"Crew Manager",
# 			"principal_contact_info_last_name":"Toledo",
# 			"principal_contact_info_first_name":"Ericka",
# 			"principal_contact_info_middle_name":"Caloocan",
# 			"principal_contact_info_email_address":"toledo@yahoo.com",
# 			"principal_contact_info_contact_number_1":"12345",
# 			"principal_contact_info_contact_number_2":"54321",
# 			"principal_contact_info_contact_number_3":"09175501293",
# 			"principal_contact_info_contact_number_4":"09175501294"
# 		},
# 			{"principal_contact_info_designation":"CEO",
# 			"principal_contact_info_last_name":"Gerona",
# 			"principal_contact_info_first_name":"Christian",
# 			"principal_contact_info_middle_name":"Gray",
# 			"principal_contact_info_email_address":"gerona@yahoo.com",
# 			"principal_contact_info_contact_number_1":"13579",
# 			"principal_contact_info_contact_number_2":"24680",
# 			"principal_contact_info_contact_number_3":"09067574430",
# 			"principal_contact_info_contact_number_4":"09067574431"
# 		}
# 	],
# 	"principal_addresses":[
# 		{
# 			"principal_address":"Lot 13 Block 6 Don Pio Street",
# 			"principal_address_city":"Dummy city-0",
# 			"principal_address_country":"Dummy country-0",
# 			"principal_address_state":"Dummy state-0",
# 			"principal_address_contact_number_1":555555,
# 			"principal_address_contact_number_2":444444,
# 			"principal_address_contact_number_3":33333
# 		},
# 		{
# 			"principal_address":"Bonfacio Global City, Unit 260, Serendra 2",
# 			"principal_address_city":"Dummy city-1",
# 			"principal_address_country":"Dummy country-1",
# 			"principal_address_state":"Dummy state-1",
# 			"principal_address_contact_number_1":11111,
# 			"principal_address_contact_number_2":22222,
# 			"principal_address_contact_number_3":88888
# 		},
# 		{
# 			"principal_address":"Tarlac Province",
# 			"principal_address_city":"Dummy city-2",
# 			"principal_address_country":"Dummy country-2",
# 			"principal_address_state":"Dummy state-2",
# 			"principal_address_contact_number_1":9876,
# 			"principal_address_contact_number_2":41241,
# 			"principal_address_contact_number_3":344212
# 		}
# 	],
# 	"principal_full":"Hellespont",
# 	"principal_master":"HSM",
# 	"principal_email_address":"hsm@yahoo.com"
# 	"principal_sss":"4214124124",
# 	"principal_philhealth":"2312314124124"
# }