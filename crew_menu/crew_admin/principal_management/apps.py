from django.apps import AppConfig


class PrincipalManagementConfig(AppConfig):
    name = 'principal_management'
