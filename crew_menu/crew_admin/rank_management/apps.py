from django.apps import AppConfig


class RankAdminConfig(AppConfig):
    name = 'rank_admin'
