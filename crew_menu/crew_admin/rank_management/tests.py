from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from login.tests import LogInReusablesScript as LRS , LogInTestFT as LTFT

from unittest import skip

import time
import inspect

# Create your tests here.
# Command Line: python manage.py test crew_menu.crew_admin.rank_management

# python manage.py test crew_menu.crew_admin.rank_management.tests.RankManagementEmptyTestListPage
class RankManagementEmptyTestListPage(StaticLiveServerTestCase):
	# Used LTFT because LRS already inherits the appended json all throughout
	fixtures = LTFT.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/rank-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Rank List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)

	def test_IsEmpty(self):
		# User Goes to the the Rank List Page
		self.set_goToListPage()
		time.sleep(1)
		# System checks the output if the table is empty
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('No records found', tr[1].text)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))



# python manage.py test crew_menu.crew_admin.rank_management.tests.RankManagementTestListPage
class RankManagementTestListPage(StaticLiveServerTestCase):
	fixtures = LRS.fixtures
	# Command: python manage.py dumpdata test_people.ranks --indent=2 > login/fixtures/ranks.json
	fixtures.append('ranks.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/rank-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Rank List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)

	def set_SearchAutocomplete(self, search_text):
		# User Types in the search box
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(1)
		# System looks for suggestions via values typed on the search box
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		self.assertNotEquals(len(items), 0)
		# User clicks on the first suggestion
		items[0].click()
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertNotEquals(tr[0].text, "No records found")

	def set_NoShowAutocompleteSearch(self, search_text):
		# User Types in the search box
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(1)
		# System looks for suggestions via values typed on the search box which should be none
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		self.assertEquals(len(items), 0)
		search_value = search.get_attribute("value");
		# User hits backspace until there are no more values on the searchbox
		while (search_value != ""):
			search_value = search.get_attribute("value");
			search.send_keys(Keys.BACKSPACE)
		
	def set_SelectField(self, field):
		# User Selects a Field
		field_select = Select(self.browser.find_element_by_id("field-select"))
		field_select.select_by_visible_text(field)

	def test_CountColumns(self):
		# User Goes to the the Rank List Page
		self.set_goToListPage()
		# System checks the maximum length number of items in a single page
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertEquals(len(tr)-1, 10) # minus one to disregard the header
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))


	# When the option filter of search is not selected, it should be globally suggest through all the fields on the option
	def test_SearchAutocompleteAll(self):
		self.set_goToListPage()
		self.set_SearchAutocomplete("CMA")
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects code, it should only suggest through code on the option
	def test_SearchAutocompleteCode(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("Code")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a description
		self.set_NoShowAutocompleteSearch("Catering")
		time.sleep(1)
		# User tests if there will be suggestions when I entered a code
		self.set_SearchAutocomplete("CMA")
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects description, it should only suggest through description on the option
	def test_SearchAutocompleteDescription(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("Description")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a description
		self.set_NoShowAutocompleteSearch("CMA")
		# User tests if there will be suggestions when I entered a code
		self.set_SearchAutocomplete("Catering")	
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Exit Button
	# This is to be tested manually as the Exit button only works when it is opened via new tab
	# Test new tab windows is not possible on selenium
	# def test_RankExit(self):
	# 	# User clicks the Exit button location on the lower right of the page
	# 	pass

	# The default sorted fields
	def test_InitialSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System looks at the sorted fields upon page render
		remove_rank_order = self.browser.find_element_by_id('remove_rank_order') # System looks if the remove in rank order is visible
		priority_rank_order = self.browser.find_element_by_id('priority_rank_order')
		sort_rank_order = self.browser.find_element_by_id('sort_rank_order')
		self.assertEquals(priority_rank_order.text, '1') # Sysem looks if this its rank order is equal to 1
		sort_rank_order_class = sort_rank_order.get_attribute("class");
		self.assertNotIn(sort_rank_order_class, 'caret-reversed')
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Checks all the sortable fields
	def test_Sortables(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System looks all the sortable fields upon page render
		sort_rank_order = self.browser.find_element_by_id('sort_rank_order')
		sort_rank_type = self.browser.find_element_by_id('sort_rank_type')
		sort_rank_updated_by = self.browser.find_element_by_id('sort_rank_updated_by')
		sort_rank_date_updated = self.browser.find_element_by_id('sort_rank_date_updated')
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via rank order
	def test_SortRankOrder(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the rank order
		sort_rank_order = self.browser.find_element_by_id('sort_rank_order')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('2', td[0].text)
		# User sorts the rank order
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search_value = search.get_attribute("value")
		self.assertNotEquals(len(search_value), 0)
		self.set_SelectField("Code")
		sort_rank_order.click()
		time.sleep(2)
		search_value = self.browser.find_element_by_id("search").get_attribute("value")
		self.assertEquals(len(search_value), 0)
		field_select = self.browser.find_element_by_id("field-select")
		field_select_selected = field_select.find_element_by_css_selector("option:checked")
		self.assertEquals(field_select_selected.text,'All')
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_rank_order = self.browser.find_element_by_id('sort_rank_order')
		sort_rank_order_class = sort_rank_order.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_rank_order_class)
		# System Checks the value of the first row of the rank order after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('2', td[0].text)
		# User sorts again on the reversed order
		sort_rank_order.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_rank_order = self.browser.find_element_by_id('sort_rank_order')
		sort_rank_order_class = sort_rank_order.get_attribute("class");
		self.assertIn('caret-reversed', sort_rank_order_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('2', td[0].text)
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via rank department
	def test_SortRankDepartment(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the rank department
		sort_rank_department = self.browser.find_element_by_id('sort_rank_department')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Engine', td[3].text)
		# User sorts the rank department
		sort_rank_department.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_rank_department = self.browser.find_element_by_id('sort_rank_department')
		sort_rank_department_class = sort_rank_department.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_rank_department_class)
		# System Checks the value of the first row of the rank department after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('Engine', td[3].text)
		# User sorts again on the reversed order
		sort_rank_department.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_rank_department = self.browser.find_element_by_id('sort_rank_department')
		sort_rank_department_class = sort_rank_department.get_attribute("class");
		self.assertIn('caret-reversed', sort_rank_department_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Deck', td[3].text)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via rank type
	def test_SortRankType(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the rank type
		sort_rank_type = self.browser.find_element_by_id('sort_rank_type')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Officer', td[4].text)
		# User sorts the rank type
		sort_rank_type.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_rank_type = self.browser.find_element_by_id('sort_rank_type')
		sort_rank_type_class = sort_rank_type.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_rank_type_class)
		# System Checks the value of the first row of the rank type after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('Crew', td[4].text)
		# User sorts again on the reversed order
		sort_rank_type.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_rank_type = self.browser.find_element_by_id('sort_rank_type')
		sort_rank_type_class = sort_rank_type.get_attribute("class");
		self.assertIn('caret-reversed', sort_rank_type_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Crew', td[4].text)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via updated by
	def test_SortUpdatedBy(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the rank updated by
		sort_rank_updated_by = self.browser.find_element_by_id('sort_rank_updated_by')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('bjnb', td[5].text)
		# User sorts the rank updated by
		sort_rank_updated_by.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_rank_updated_by = self.browser.find_element_by_id('sort_rank_updated_by')
		sort_rank_updated_by_class = sort_rank_updated_by.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_rank_updated_by_class)
		# System Checks the value of the first row of the rank updated by after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('adgb', td[5].text)
		# User sorts again on the reversed order
		sort_rank_updated_by.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_rank_updated_by = self.browser.find_element_by_id('sort_rank_updated_by')
		sort_rank_updated_by_class = sort_rank_updated_by.get_attribute("class");
		self.assertIn('caret-reversed', sort_rank_updated_by_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('adgb', td[5].text)
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via date updated
	def test_SortDateUpdated(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the rank date updated
		sort_rank_date_updated = self.browser.find_element_by_id('sort_rank_date_updated')
		tr = self.browser.find_elements_by_tag_name("tr")
		td_initial_value = tr[1].find_elements_by_tag_name("td")[6].text
		# User sorts the rank date updated
		sort_rank_date_updated.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_rank_date_updated = self.browser.find_element_by_id('sort_rank_date_updated')
		sort_rank_date_updated_class = sort_rank_date_updated.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_rank_date_updated_class)
		# System Checks the value of the first row of the rank date updated after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotIn(td_initial_value, td[6].text)
		# User sorts again on the reversed order
		sort_rank_date_updated.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_rank_date_updated = self.browser.find_element_by_id('sort_rank_date_updated')
		sort_rank_date_updated_class = sort_rank_date_updated.get_attribute("class");
		self.assertIn('caret-reversed', sort_rank_date_updated_class)
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))


	# Multi Sorting
	def test_MultiSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User clicks on the label or the arrow and the sorting of the item list table will change 
		sort_date_updated = self.browser.find_element_by_id('sort_rank_date_updated')
		sort_date_updated.click()
		priority_rank_date_updated = self.browser.find_element_by_id('priority_rank_date_updated')
		self.assertEquals(priority_rank_date_updated.text, '1') # Sysem looks if this its date updated is equal to 1
		# User clicks on a second label until he is satisfied
		sort_rank_type = self.browser.find_element_by_id('sort_rank_type')
		sort_rank_type.click()
		priority_rank_type = self.browser.find_element_by_id('priority_rank_type')
		self.assertEquals(priority_rank_type.text, '1') # Sysem looks if this its rank type is equal to 1
		priority_rank_date_updated = self.browser.find_element_by_id('priority_rank_date_updated')
		self.assertEquals(priority_rank_date_updated.text, '2') # Sysem looks if this its date updated is equal to 2
		# User clicks on a third label until he is satisfied
		sort_rank_updated_by = self.browser.find_element_by_id('sort_rank_updated_by')
		sort_rank_updated_by.click()
		priority_rank_updated_by = self.browser.find_element_by_id('priority_rank_updated_by')
		self.assertEquals(priority_rank_updated_by.text, '1') # Sysem looks if this its updated by is equal to 1
		priority_rank_type = self.browser.find_element_by_id('priority_rank_type')
		self.assertEquals(priority_rank_type.text, '2') # Sysem looks if this its rank type is equal to 2
		priority_rank_date_updated = self.browser.find_element_by_id('priority_rank_date_updated')
		self.assertEquals(priority_rank_date_updated.text, '3') # Sysem looks if this its date updated is equal to 3
		# System must prioritize the sorting and the latest that he clicked must be the most prioritized
		# User must be informed via prioritization number order being visible
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Cancel Sorting
	def test_CancelSorting(self):
		# User DoesMulti Sorting
		self.test_MultiSorting()
		# Upon Multi Sorting, user clicks a remove icon near the label field
		remove_updated_by = self.browser.find_element_by_id('remove_rank_updated_by')
		remove_updated_by.click()
		try:
			priority_rank_updated_by = self.browser.find_element_by_id('priority_rank_updated_by')
			self.fail("SHOULD BE NO AVAILABLE UPDATED BY PRIORITY")
		except:
			pass
		# The second priority should be now the number one priority
		priority_rank_type = self.browser.find_element_by_id('priority_rank_type')
		self.assertEquals(priority_rank_type.text, '1') # Sysem looks if this its rank type is equal to 1

		# Upon Multi Sorting, user clicks a remove icon near the label field the second time
		remove_rank_type = self.browser.find_element_by_id('remove_rank_type')
		remove_rank_type.click()
		try:
			priority_rank_type = self.browser.find_element_by_id('priority_rank_type')
			self.fail("SHOULD BE NO AVAILABLE RANK TYPE PRIORITY")
		except:
			pass
		# The second priority should be now the number one priority
		priority_rank_date_updated = self.browser.find_element_by_id('priority_rank_date_updated')
		self.assertEquals(priority_rank_date_updated.text, '1') # Sysem looks if this its date updated is equal to 3
		# Upon Multi Sorting, user clicks a remove icon near the label field the third time
		remove_date_updated = self.browser.find_element_by_id('remove_rank_date_updated')
		remove_date_updated.click()
		try:
			priority_rank_date_updated = self.browser.find_element_by_id('priority_rank_date_updated')
			self.fail("SHOULD BE NO AVAILABLE RANK DATE UPDATED PRIORITY")
		except:
			pass
		# The priority should be now the rank order
		priority_rank_order = self.browser.find_element_by_id('priority_rank_order')
		self.assertEquals(priority_rank_order.text, '1') # Sysem looks if this its rank type is equal to 1
		# System must remove the sorting of that certain label field
		# Priority Indicator must be gone
		# Remove Icon must be gone
		# Order arrow indicator must be facing down 
		# Search and Search Filter must Revert back to Original
		print ("------------")
		print ("%s.%s DONE - 13" % (self.__class__.__name__, inspect.stack()[0][3]))


	# Clearing resets the Sorting to Original
	# This is to be tested manually as this is very complicated in terms of combinations and returns
	# Source http://192.168.0.50:8000/crew-menu/crew-admin/rank-management/#/
	def test_ClearResetSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User tries to type in the search field
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search_value = search.get_attribute("value")
		self.assertNotEquals(len(search_value), 0)
		# User selectes a filter search field
		self.set_SelectField("Code")
		# User clicks the clear field
		clear_sort = self.browser.find_element_by_id('clear-sort')
		clear_sort.click()
		time.sleep(2)
		# System should reset the search and the search filter
		search_value = self.browser.find_element_by_id("search").get_attribute("value")
		self.assertEquals(len(search_value), 0)
		field_select = self.browser.find_element_by_id("field-select")
		field_select_selected = field_select.find_element_by_css_selector("option:checked")
		self.assertEquals(field_select_selected.text,'All')
		# The sort must be reverted to the original one including the arrows
		priority_rank_order = self.browser.find_element_by_id('priority_rank_order')
		self.assertEquals(priority_rank_order.text, '1') # Sysem looks if this its rank type is equal to 1
		print ("------------")
		print ("%s.%s DONE - 14" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Searching resets the Sorting to Original
	def test_SearchResetSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User searches on the search field
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search.send_keys(Keys.RETURN)
		# The sort must be reverted to the original one including the arrows
		try:
			priority_rank_order = self.browser.find_element_by_id('priority_rank_order')
			self.assertEquals(priority_rank_order.text, '1') # Sysem looks if this its rank type is
			self.fail("RANK ORDER MUST NOT BE PRESENT")
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 15" % (self.__class__.__name__, inspect.stack()[0][3]))

# python manage.py test crew_menu.crew_admin.rank_management.tests.RankManagementTestAddPage
class RankManagementTestAddPage(StaticLiveServerTestCase):

	fixtures = RankManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/rank-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToAddPage(self):
		# User goes to List Page First
		RankManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Rank', self.browser.title)
		time.sleep(3)

	# Used as a reusable component for update
	def _set_AddFieldsThenSave(self):
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the rank code field
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field.send_keys('MA')
		# User fills up the rank description field
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field.send_keys('Master Dean')
		# User fills up the rank department field
		rank_department_field = Select(self.browser.find_element_by_name('rank_department'))
		rank_department_field.select_by_visible_text('Engine')
		# User fills up the order rank type
		rank_type_field = Select(self.browser.find_element_by_name('rank_type'))
		rank_type_field.select_by_visible_text('Officer')
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()

	def _set_AddFields(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the rank code field
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field.send_keys('MA')
		# User fills up the rank description field
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field.send_keys('Master Dean')
		# User fills up the rank department field
		rank_department_field = Select(self.browser.find_element_by_name('rank_department'))
		rank_department_field.select_by_visible_text('Engine')
		# User fills up the order rank type
		rank_type_field = Select(self.browser.find_element_by_name('rank_type'))
		rank_type_field.select_by_visible_text('Officer')
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# COMPLETE
	def test_BasicAddRank(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('1', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Dean', tr[1].text)
		self.assertIn('Engine', tr[1].text)
		self.assertIn('Officer', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Add Rank', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field_value = rank_code_field.get_attribute("value");
		self.assertEquals(rank_code_field_value, '')
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field_value = rank_description_field.get_attribute("value");
		self.assertEquals(rank_description_field_value, '')
		rank_department_field = self.browser.find_element_by_name('rank_department')
		rank_department_field_value = rank_department_field.get_attribute("value");
		self.assertEquals(rank_department_field_value, '')
		rank_type_field = self.browser.find_element_by_name('rank_type')
		rank_type_field_value = rank_type_field.get_attribute("value");
		self.assertEquals(rank_type_field_value, '')
		time.sleep(3)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	# COMPLETE
	def test_ExitRank(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ShowNotifications(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		save = self.browser.find_element_by_id("save")
		save.click()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_AllFieldsRequired(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present
		for x in range(span_count):
			self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))


	# Rank Order must accept integer only
	# COMPLETE
	def test_OrderIsIntegerOnly(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		# User types on the order field
		field = self.browser.find_element_by_name('order')
		field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-rank-order')
		error_text = "This field must be an integer"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Order must not accept '.'
	# COMPLETE
	def test_OrderNoDecimal(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('order')
		# User types on the order field
		field.send_keys(".....")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(field_value, '')
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Max Rank Order is 99.
	# COMPLETE
	def test_OrderMax99(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('order')
		# User types on the order field
		time.sleep(2)
		field.send_keys(9999999)
		time.sleep(1)
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-rank-order')
		error_text = "Max order is 99"
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("maximum of 99")
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Code must be unique
	# COMPLETE
	def test_CodeAlreadyExists(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('rank_code')
		# User types on the code field
		field.send_keys("ss")
		time.sleep(2)
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-rank-code')
		error_text = "This code already exists"
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("Code - already exists")
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))



	# Rank Code must be 2-3 in length
	# COMPLETE
	def test_CodeLength(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('rank_code')
		# User types on the code field
		field.send_keys("A")
		time.sleep(2)
		# System checks if there is an error and there should be an error
		error = self.browser.find_element_by_id('error-rank-code')
		error_text = "Code length must be 2 or 3"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("length must be 2 or 3")
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Code must not reach four characters
	# COMPLETE
	def test_CodeMaxLength(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('rank_code')
		# User types on the code field
		field.send_keys("ABCD")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(field_value, 'ABC')
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))


	# Rank Description max is 24
	# COMPLETE
	def test_DescriptionMaxCharacters(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('rank_description')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGG")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 24)
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))


# python manage.py test crew_menu.crew_admin.rank_management.tests.RankManagementTestUpdatePage
class RankManagementTestUpdatePage(StaticLiveServerTestCase):

	fixtures = RankManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/rank-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToUpdatePage(self):
		# User Goes to Add Ranks Page
		RankManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		RankManagementTestAddPage._set_AddFieldsThenSave(self)
		# RankManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		# START User Clicks update on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		td_count = len(td)-1
		span = td[td_count].find_elements_by_tag_name("span")
		update_button = span[1].click()
		time.sleep(2)
		self.assertIn('Update Rank', self.browser.title)
		# END User Clicks update on the first row
		# time.sleep(3)

	def _set_UpdateFields(self):
		# User goes to Update Rank Page
		self._setGoToUpdatePage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(Keys.BACKSPACE, 0)
		# User fills up the rank code field
		rank_code_field = self.browser.find_element_by_name('rank_code')
		# User Makes sure the rank code is not editable and readonly
		rank_code_field.send_keys(Keys.BACKSPACE, Keys.BACKSPACE)
		# User fills up the rank description field
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field.send_keys('ssss')
		# User fills up the rank department field
		rank_department_field = Select(self.browser.find_element_by_name('rank_department'))
		rank_department_field.select_by_visible_text('Deck')
		# User fills up the order rank type
		rank_type_field = Select(self.browser.find_element_by_name('rank_type'))
		rank_type_field.select_by_visible_text('Rating')
		time.sleep(2)

	def _set_SaveUpdate(self):
		confirmation_save = self.browser.find_element_by_id("confirmation-save")
		confirmation_save.click()
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		self._set_SaveUpdate()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# COMPLETE
	def test_BasicUpdateRank(self):
		# User updates values from the fields
		self._set_UpdateFields()
		# User clicks the save button that opens a confirmation
		self._set_SaveUpdate()
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)
		time.sleep(2)
		# START User checks if updated values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('0', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Deanssss', tr[1].text)
		self.assertIn('Deck', tr[1].text)
		self.assertIn('Ratings', tr[1].text)
		# END User checks if updateed values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Update Rank', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared except for rank code
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field_value = rank_code_field.get_attribute("value");
		self.assertNotEquals(rank_code_field_value, '')
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field_value = rank_description_field.get_attribute("value");
		self.assertEquals(rank_description_field_value, '')
		rank_department_field = self.browser.find_element_by_name('rank_department')
		rank_department_field_value = rank_department_field.get_attribute("value");
		self.assertEquals(rank_department_field_value, '')
		rank_type_field = self.browser.find_element_by_name('rank_type')
		rank_type_field_value = rank_type_field.get_attribute("value");
		self.assertEquals(rank_type_field_value, '')
		time.sleep(2)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	
	# COMPLETE
	def test_ExitRank(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ShowNotifications(self):
		# User goes to Update Rank Page
		self.test_ClearFields()
		self._set_SaveUpdate()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_AllFieldsRequired(self):
		# User goes to Update Rank Page
		self.test_ClearFields()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present except for rank code
		for x in range(span_count):
			if x != 1:
				self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		self._set_SaveUpdate()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Order must accept integer only
	# COMPLETE
	def test_OrderIsIntegerOnly(self):
		# User goes to Update Rank Page
		self._setGoToUpdatePage()
		# User types on the order field
		field = self.browser.find_element_by_name('order')
		field_value = field.get_attribute("value");
		field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-rank-order')
		error_text = "This field must be an integer"
		time.sleep(1)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Order must not accept '.'
	# COMPLETE
	def test_OrderNoDecimal(self):
		# User goes to Update Rank Page
		self._setGoToUpdatePage()
		field = self.browser.find_element_by_name('order')
		# User types on the order field
		field.send_keys(".....")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertNotIn(field_value, '.....')
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Max Rank Order is 99.
	# COMPLETE
	def test_OrderMax99(self):
		# User goes to Update Rank Page
		self._setGoToUpdatePage()
		field = self.browser.find_element_by_name('order')
		# User types on the order field
		field.send_keys(9999999)
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-rank-order')
		error_text = "Max order is 99"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("Order - maximum of 99")
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Rank Description max is 24
	# COMPLETE
	def test_DescriptionMaxCharacters(self):
		# User goes to Update Rank Page
		self._setGoToUpdatePage()
		field = self.browser.find_element_by_name('rank_description')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGG")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 24)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))


# python manage.py test crew_menu.crew_admin.rank_management.tests.RankManagementTestDeletePage
class RankManagementTestDeletePage(StaticLiveServerTestCase):

	fixtures = RankManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/rank-management/#/'

	def tearDown(self):
		self.browser.quit()

	def set_Delete(self, row):
		# START User Clicks delete on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		td_count = len(td)-1
		removed_text = td[1].text
		span = td[td_count].find_elements_by_tag_name("span")
		# System checks the title of the current page if it is Rank
		self.assertIn('Rank', self.browser.title)
		time.sleep(2)
		# User clicks delete icon
		span[2].click()
		time.sleep(2)
		# User clicks delete button
		delete_button = self.browser.find_element_by_id("delete")
		delete_button.click()
		# END User Clicks delete on the first row
		time.sleep(2)
		# System Checks if the rank is still available in the list page
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		self.assertNotEquals(removed_text, td[1].text)

	def test_BasicDelete(self):
		# User Goes to Add Ranks Page
		RankManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		RankManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Currently three
	def test_MultipleDelete(self):
		# User Goes to Add Ranks Page
		RankManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		RankManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	def test_BasicRestore(self):
		# User Goes to Add Ranks Page
		RankManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		RankManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User clicks the undo button to restore the deleted
		undo_button = self.browser.find_element_by_css_selector(".undo-button")
		item_notification_text = self.browser.find_element_by_css_selector(".item-notification").text

		undo_button.click()
		time.sleep(1)
		# System checks if the item is restored
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals(item_notification_text, td[2].text)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	def test_MultipleRestore(self):
		# User Goes to Add Ranks Page
		RankManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		RankManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# START System counts and checks for all items that can be undone
		show_deletes = self.browser.find_element_by_id("undo-delete")
		show_deletes.click()
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		# items_notifications = []
		self.assertEquals(len(undo_buttons), 3)
		self.assertEquals(len(item_notifications), 3)
		# END System counts and checks for all items that can be undone
		for x in range(len(undo_buttons)):
			undo_buttons[x].click()
			time.sleep(2)
			# tr = self.browser.find_elements_by_tag_name("tr")
			# td = tr[1].find_elements_by_tag_name("td")
			# print (item_notifications[x].text)
			# print (td[2].text)
			# self.assertEquals(item_notifications[x].text, td[2].text)

		# System Checks that there shold be no notifications left
		undo_buttons = self.browser.find_elements_by_css_selector(".undo-button")
		item_notifications = self.browser.find_elements_by_css_selector(".item-notification")
		self.assertEquals(len(undo_buttons), 0)
		self.assertEquals(len(item_notifications), 0)
		# System checks if the undo delete button is gone because there is no more items left to restore
		try:
			show_deletes = self.browser.find_element_by_id("undo-delete")
			self.fail('UNDO DELETE MUST NOT BE VISIBLE!')
		except:
			pass
		# System checks if the modal is gone because there is no more items left to restore
		try:
			dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
			dismiss_modal.click()
			self.fail('MODAL MUST BE AUTOMATICALLY DISMISSED WHEN THERE ARE NO MORE TO UNDO!')
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))
		
		
# Token must be included to make interaction with API
# Users must be able to add rank
# Order must be an integer only
# Type must be at least 5 characters
# Error Notifications must shown upon validations
# Users must be able to delete
# Updating a rank involves of clicking the pencil button and pre-valued inputs based on the item must show
# TotalPage count must be real-time and collaborate with deleted items

# Search Capability
# When an option filter of search is not selected, it should be globally search through all the fields on the option
# Must search specifically in that field if that search filter option is selected
# 1.) Problem, document.querySelector( '#field-select' ) will not work anymore if the tag became a model
# 1.) Solution, replace it with $scope.searchList.value to just get the json.value's value
# ----------------------------------------------------------
# When an option filter of search is not selected, autocomplete drop down values must be involved on every field
# autocomplete drop down values must be specific in that field if that search filter option is selected
# 1.) Problem, value of the options is relational like: "bio__user_code", "bio__middle_name" which affects the mapping function of searchFilter function on he main.js
# 1.) Solution, manipulate the string by removing the "bio__" using string indexes
# ----------------------------------------------------------
# When searched make sure that it follows the limit offset and outputs only the entered search
# 1.) Problem, forgot to put the limit offset paramter passed as a request and do the math with the total search
# 1.) Solution, merged the limit offset on the json request and included the calculation of the total upon response
# ----------------------------------------------------------
# When an option filter of search is not selected, autocomplete drop down values must be involved on every field
# autocomplete drop down values must be specific in that field if that search filter option is selected
# Does not do anythinging if enetered an empty input
# Search can be done by clicking on the autocomplete
# 1.) Problem, trigger is fast enough to just declare the value as what is written on the input box
# 2.) Solution, use $timeout on the 'autoComplete' directive in main.js
# If none of the items is equal to that entered input, there should be a notification
# Notifications must be gone when routed to another template
# 1.) Problem, $rootScope.Notifications is still set to true when routing is applied
# 2.) Solution, set $rootScope.Notifications to false on '$routeChangeSuccess' at main.js
# If entered in an empty searchbox it must be the same function as clear

# Rank Code must be unique and uppercase - **
# Rank Code must be 2-3 in length - **

# Rank Order must not accept '.' - **
# Max Rank Order is 99. - **
# Sort Ranks from Highest (1) to Lowest (max) - **
# Rank Description max is 24 - **