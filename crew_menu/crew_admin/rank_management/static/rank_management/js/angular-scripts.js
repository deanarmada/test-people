// Configure our routes

peopleApp.config(function($routeProvider){
	$routeProvider
	// route for the homepage
	.when('/', {
		title: 'Ranks',
		templateUrl: '/static/rank_management/html/index.html',
		controller: 'indexController',
		// Resolve is needed to complete the data first before rendering the template
		resolve: {
			rankList: function(rankFactory){
				return rankFactory.rankList();
			},
		}
	})
	// route for the add page
	.when('/add', {
		title: 'Add Rank',
		templateUrl: '/static/rank_management/html/add.html',
		controller: 'addController',
		resolve: {
			rankFormValidations: function(rankFactory){
				return rankFactory.rankFormValidations();
			},
		}
	})
	// route for update page with parameters
	.when('/update/:id', {
		title: 'Update Rank',
		templateUrl: '/static/rank_management/html/update.html',
		controller: 'updateController',
		resolve: {
			rankFormValidations: function(rankFactory){
				return rankFactory.rankFormValidations();
			},
		}
	})
	// route for viewing page with parameters
	.when('/view/:id', {
		title: 'View Rank',
		templateUrl: '/static/rank_management/html/update.html',
		controller: 'viewController',
	})
	.otherwise({
		redirectTo: '/'
	});
});

peopleApp.controller('indexController', function($scope, $http, $rootScope, $controller){
	// "url" and "total" variables came from the resolve on the routing template, 
	// which came from the returned key,value pairs in the rankFactory
	// $controller('modalController', {$scope: $scope});
	$controller('itemListController', {$scope: $scope});
	$scope.mainAPI = '/api/people/v1/ranks/';
	$scope.apiGetItems = $rootScope.domainName+$scope.mainAPI;
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/ranks/update/';
	$scope.apiDelete = $rootScope.domainName+'/api/people/v1/ranks/delete/';
	$scope.searchFilters = [{
		value: null,
		label: 'All'
	},{
		value: 'rank_code',
		label: 'Code'
	}, {
		value: 'rank_description',
		label: 'Description'
	}];
	// to get the tableHeadersLength
	$scope.tableHeaders = [
		{'name': 'No.', 'url': $scope.mainAPI, 'field':'-rank_order', 'class':'caret-reversed initial-reversed', 'id':'rank_order'},
		{'name': 'Code'},
		{'name': 'Description'},
		{'name': 'Department', 'url': $scope.mainAPI, 'field':'-rank_department', 'class':'caret-reversed initial-reversed', 'id':'rank_department'},
		{'name': 'Type', 'url': $scope.mainAPI, 'field':'-rank_type', 'class':'caret-reversed initial-reversed', 'id':'rank_type'},
		{'name': 'Updated By', 'url': $scope.mainAPI, 'field':'-updated_by__bio__user_code', 'class':'caret-reversed initial-reversed', 'id':'rank_updated_by'},
		{'name': 'Date Updated', 'url': $scope.mainAPI, 'field':'-date_updated', 'class':'caret-reversed initial-reversed', 'id':'rank_date_updated'},
	];
	$scope.searchItems = [];
	// Used for td colspan when results not found
	$scope.tableHeadersLength = $scope.tableHeaders.length + 2;

	$rootScope.totalPage = Math.ceil(total/$rootScope.limit);
	$scope.originalTotalPage = $rootScope.totalPage;

	$rootScope.loadPage = function(){
		// url came from rankFactory
		if(total){
			$scope.getPage(url);
		}else{
			$scope.noRecord = true;
		}
	}

	// Default fields that can be searched
	$scope.Items = ['rank_code', 'rank_description'];

	$http.post(domainName+url)
	.then(function(response){
		data = response.data.data;
		data.map(function(obj){ 
			for(x=0;x<$scope.Items.length;x++){
				$scope.searchItems.push(obj[$scope.Items[x]]); 
			}
		});
	});

	// console.log($scope.searchItems);
	// alert(JSON.stringify($scope.searchItems));
	
	$scope.preSorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
	$scope.sorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
    $rootScope.loadPage();
    
});

peopleApp.controller('reusableComponents', function($scope, $rootScope, $http){
	$scope.uniqueCheck = function(form , event, validity){
    	_this = angular.element(event.target);
    	// value = _this.val().toUpperCase();
    	value = _this.val();
    	// Be aware of the length check value
    	if ((value.length == 2 || value.length == 3) && !validity){
    		json = {};
    		json['filters'] = {};
    		json['filters']['rank_code__iexact'] = value;
    		$http.post(domainName+"/api/people/v1/ranks/", json)
			.then(function(response){
				total = response.data.total;
				if(total){
					form.rank_code.$setValidity("unique", false);
				}else{
					form.rank_code.$setValidity("unique", true);
				}
			});
    	}else{
    		form.rank_code.$setValidity("unique", true);
    	}
    };
});

peopleApp.controller('addController', function($scope, $controller, $rootScope, $http, $timeout){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiAdd = $rootScope.domainName+'/api/people/v1/ranks/add/';
	$scope.input = {};
    $scope.code_readonly = false;
    // Validations came from rankFactory
    // Example of validation expression is [[ validations.rank_code.maxlength ]]
    // Technique is whatever is the submodel of the input that would be the key of the value of this json validation
    $scope.validations = validations;
});

peopleApp.controller('updateController', function($scope, $rootScope, $routeParams, $http, $controller){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/ranks/update/';
	$scope.title = "UPDATE";
	id = parseInt($routeParams.id);

	$http.post($rootScope.domainName+'/api/people/v1/ranks/detail/', { 'id':id })
	.then(function(response){
		$scope.input = {
			// sample pre-valued field
			'rank_order': response.data.data[0].rank_order,
			'rank_code': response.data.data[0].rank_code,
			'rank_description': response.data.data[0].rank_description,
			'rank_department': response.data.data[0].rank_department,
			'rank_type': response.data.data[0].rank_type,
		};
		$scope.id = id;
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});
	$scope.code_readonly = true;
	try {
    	$scope.validations = validations;
	}
	catch(err) {
	   // Catch validations so validations will not send a request when viewing
	}
});

peopleApp.controller('viewController', function($scope, $http, $controller){
	$controller('updateController', {$scope: $scope});
	$scope.title = "VIEW";
	$scope.viewOnly = true;
});