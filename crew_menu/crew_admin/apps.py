from django.apps import AppConfig


class CrewAdminConfig(AppConfig):
    name = 'crew_admin'
