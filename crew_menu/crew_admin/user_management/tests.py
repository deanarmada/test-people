from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from login.tests import LogInReusablesScript as LRS , LogInTestFT as LTFT

from unittest import skip

import time
import inspect

# Create your tests here.
# Command Line: python manage.py test crew_menu.crew_admin.user_management

# python manage.py test crew_menu.crew_admin.user_management.tests.UserManagementTestListPage
class UserManagementTestListPage(StaticLiveServerTestCase):
	fixtures = LRS.fixtures
	
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.browser.maximize_window()
		self.url = '/crew-menu/crew-admin/user-management/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the User List Page
		time.sleep(2)
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is User
		self.assertIn('User', self.browser.title)

	def set_SearchAutocomplete(self, search_text):
		# User Types in the search box
		# time.sleep(3)
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(2)
		# System looks for suggestions via values typed on the search box
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		self.assertNotEquals(len(items), 0)
		# User clicks on the first suggestion
		if (len(items) == 1):
			search.send_keys(Keys.RETURN)
		else:
			items[0].click()
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertNotEquals(tr[0].text, "No records found")

	def set_NoShowAutocompleteSearch(self, search_text):
		# User Types in the search box
		search = self.browser.find_element_by_id("search")
		search.send_keys(search_text)
		time.sleep(2)
		# System looks for suggestions via values typed on the search box which should be none
		autocomplete = self.browser.find_element_by_css_selector(".ui-autocomplete")
		items = autocomplete.find_elements_by_css_selector(".ui-menu-item a")
		# self.assertEquals(len(items), 0)
		search_value = search.get_attribute("value");
		# User hits backspace until there are no more values on the searchbox
		while (search_value != ""):
			search_value = search.get_attribute("value");
			search.send_keys(Keys.BACKSPACE)
		
	def set_SelectField(self, field):
		# User Selects a Field
		field_select = Select(self.browser.find_element_by_id("field-select"))
		field_select.select_by_visible_text(field)

	def test_CountColumns(self):
		# User Goes to the the User List Page
		self.set_goToListPage()
		# System checks the maximum length number of items in a single page
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertEquals(len(tr)-1, 10) # minus one to disregard the header
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))


	# When the option filter of search is not selected, it should be globally suggest through all the fields on the option
	def test_SearchAutocompleteAll(self):
		self.set_goToListPage()
		self.set_SearchAutocomplete("adgc")
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects code, it should only suggest through code on the option
	def test_SearchAutocompleteCode(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("Code")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a first name
		self.set_NoShowAutocompleteSearch("Dean")
		time.sleep(1)
		# User tests if there will be suggestions when I entered a code
		self.set_SearchAutocomplete("adgc")
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects first name, it should only suggest through first name on the option
	def test_SearchAutocompleteFirstName(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("First Name")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a code
		self.set_NoShowAutocompleteSearch("adgc")
		# User tests if there will be suggestions when I entered a first name
		self.set_SearchAutocomplete("Dean")	
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects middle name, it should only suggest through middle name on the option
	def test_SearchAutocompleteMiddleName(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("Middle Name")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a code
		self.set_NoShowAutocompleteSearch("adgc")
		# User tests if there will be suggestions when I entered a first name
		self.set_SearchAutocomplete("Gui")	
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When the option filter selects last name, it should only suggest through last name on the option
	def test_SearchAutocompleteLastName(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User selects "Code" on the search filter
		self.set_SelectField("Last Name")
		time.sleep(2)
		# User tests if there will be suggestion when I entered a code
		self.set_NoShowAutocompleteSearch("adgc")
		# User tests if there will be suggestions when I entered a first name
		self.set_SearchAutocomplete("Arma")	
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Exit Button
	# This is to be tested manually as the Exit button only works when it is opened via new tab
	# Test new tab windows is not possible on selenium
	# def test_UserExit(self):
	# 	pass

	# The default sorted fields
	def test_InitialSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System looks at the sorted fields upon page render
		remove_user_status = self.browser.find_element_by_id('remove_user_status') # System looks if the remove in status is visible
		priority_user_status = self.browser.find_element_by_id('priority_user_status')
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		self.assertEquals(priority_user_status.text, '1') # Sysem looks if this its status is equal to 1
		sort_user_status_class = sort_user_status.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_status_class)
		self.assertNotIn('initial-reversed', sort_user_status_class)
		remove_user_code = self.browser.find_element_by_id('remove_user_code') # System looks if the remove in rank order is visible
		priority_user_code = self.browser.find_element_by_id('priority_user_code')
		sort_user_code = self.browser.find_element_by_id('sort_user_code')
		self.assertEquals(priority_user_code.text, '2') # Sysem looks if this its rank order is equal to 1
		sort_user_code_class = sort_user_code.get_attribute("class")
		self.assertIn('caret-reversed', sort_user_code_class, )
		
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Checks all the sortable fields
	def test_Sortables(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System looks all the sortable fields upon page render
		sort_user_id = self.browser.find_element_by_id('sort_user_id')
		sort_user_code = self.browser.find_element_by_id('sort_user_code')
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		sort_user_updated_by = self.browser.find_element_by_id('sort_user_updated_by')
		sort_user_date_updated = self.browser.find_element_by_id('sort_user_date_updated')
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via user id
	def test_SortUserId(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the user id
		sort_user_id = self.browser.find_element_by_id('sort_user_id')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('3', td[0].text)
		# User sorts the user id
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search_value = search.get_attribute("value")
		self.assertNotEquals(len(search_value), 0)
		self.set_SelectField("Code")
		time.sleep(2)
		sort_user_id.click()
		time.sleep(2)
		search_value = self.browser.find_element_by_id("search").get_attribute("value")
		self.assertEquals(len(search_value), 0)
		field_select = self.browser.find_element_by_id("field-select")
		field_select_selected = field_select.find_element_by_css_selector("option:checked")
		self.assertEquals(field_select_selected.text,'All')
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_user_id = self.browser.find_element_by_id('sort_user_id')
		sort_user_id_class = sort_user_id.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_id_class)
		# System Checks the value of the first row of the user id after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('3', td[0].text)
		# User sorts again on the reversed order
		sort_user_id.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_user_id = self.browser.find_element_by_id('sort_user_id')
		sort_user_id_class = sort_user_id.get_attribute("class");
		self.assertIn('caret-reversed', sort_user_id_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('2', td[0].text)
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via user code
	def test_SortUserCode(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the user code
		sort_user_code = self.browser.find_element_by_id('sort_user_code')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('adgc', td[1].text)
		# User sorts the user code
		sort_user_code.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_user_code = self.browser.find_element_by_id('sort_user_code')
		sort_user_code_class = sort_user_code.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_code_class)
		# System Checks the value of the first row of the user code after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('adgc', td[1].text)
		# User sorts again on the reversed order
		sort_user_code.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_user_code = self.browser.find_element_by_id('sort_user_code')
		sort_user_code_class = sort_user_code.get_attribute("class");
		self.assertIn('caret-reversed', sort_user_code_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('acaa', td[1].text)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via user status
	def test_SortUserStatus(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the user status
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Active', td[4].text)
		# User sorts the user status
		sort_user_status.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		sort_user_status_class = sort_user_status.get_attribute("class");
		self.assertIn('caret-reversed', sort_user_status_class)
		# System Checks the value of the first row of the user status after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('Active', td[4].text)
		# User sorts again on the reversed order
		sort_user_status.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		sort_user_status_class = sort_user_status.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_status_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('Active', td[4].text)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via updated by
	def test_SortUpdatedBy(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the user updated by
		sort_user_updated_by = self.browser.find_element_by_id('sort_user_updated_by')
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertEquals('admin', td[5].text)
		# User sorts the user updated by
		sort_user_updated_by.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_user_updated_by = self.browser.find_element_by_id('sort_user_updated_by')
		sort_user_updated_by_class = sort_user_updated_by.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_updated_by_class)
		# System Checks the value of the first row of the user updated by after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotEquals('adgc', td[5].text)
		# User sorts again on the reversed order
		sort_user_updated_by.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_user_updated_by = self.browser.find_element_by_id('sort_user_updated_by')
		sort_user_updated_by_class = sort_user_updated_by.get_attribute("class");
		self.assertIn('caret-reversed', sort_user_updated_by_class)
		# System Checks the value of the first row after sorting again
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		time.sleep(2)
		self.assertEquals('adgc', td[5].text)
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Sorting via date updated
	def test_SortDateUpdated(self):
		# User goes to List Page First
		self.set_goToListPage()
		# System Checks the initial value of the first row of the user date updated
		sort_user_date_updated = self.browser.find_element_by_id('sort_user_date_updated')
		tr = self.browser.find_elements_by_tag_name("tr")
		td_initial_value = tr[1].find_elements_by_tag_name("td")[6].text
		# User sorts the user date updated
		sort_user_date_updated.click()
		time.sleep(2)
		# System checks if the arrow has reversed
		sort_user_date_updated = self.browser.find_element_by_id('sort_user_date_updated')
		sort_user_date_updated_class = sort_user_date_updated.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_date_updated_class)
		# System Checks the value of the first row of the user date updated after sorting
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		self.assertNotIn(td_initial_value, td[6].text)
		# User sorts again on the reversed order
		sort_user_date_updated.click()
		time.sleep(2)
		# System Checks the caret again if it has returned to the orignial position
		sort_user_date_updated = self.browser.find_element_by_id('sort_user_date_updated')
		sort_user_date_updated_class = sort_user_date_updated.get_attribute("class");
		self.assertIn('caret-reversed', sort_user_date_updated_class)
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Multi Sorting
	def test_MultiSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User clicks on the label or the arrow and the sorting of the item list table will change 
		sort_date_updated = self.browser.find_element_by_id('sort_user_date_updated')
		sort_date_updated.click()
		priority_user_date_updated = self.browser.find_element_by_id('priority_user_date_updated')
		self.assertEquals(priority_user_date_updated.text, '1') # Sysem looks if this its date updated is equal to 1
		# User clicks on a second label until he is satisfied
		sort_user_id = self.browser.find_element_by_id('sort_user_id')
		sort_user_id.click()
		priority_user_id = self.browser.find_element_by_id('priority_user_id')
		self.assertEquals(priority_user_id.text, '1') # Sysem looks if this its user id is equal to 1
		priority_user_date_updated = self.browser.find_element_by_id('priority_user_date_updated')
		self.assertEquals(priority_user_date_updated.text, '2') # Sysem looks if this its date updated is equal to 2
		# User clicks on a third label until he is satisfied
		sort_user_updated_by = self.browser.find_element_by_id('sort_user_updated_by')
		sort_user_updated_by.click()
		priority_user_updated_by = self.browser.find_element_by_id('priority_user_updated_by')
		self.assertEquals(priority_user_updated_by.text, '1') # Sysem looks if this its updated by is equal to 1
		priority_user_id = self.browser.find_element_by_id('priority_user_id')
		self.assertEquals(priority_user_id.text, '2') # Sysem looks if this its user id is equal to 2
		priority_user_date_updated = self.browser.find_element_by_id('priority_user_date_updated')
		self.assertEquals(priority_user_date_updated.text, '3') # Sysem looks if this its date updated is equal to 3
		# System must prioritize the sorting and the latest that he clicked must be the most prioritized
		# User must be informed via prioritization number order being visible
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Clearing resets the Sorting to Original
	def test_ClearResetSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User tries to type in the search field
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search_value = search.get_attribute("value")
		self.assertNotEquals(len(search_value), 0)
		# User selectes a filter search field
		self.set_SelectField("Code")
		# User clicks the clear field
		clear_sort = self.browser.find_element_by_id('clear-sort')
		clear_sort.click()
		time.sleep(2)
		# System should reset the search and the search filter
		search_value = self.browser.find_element_by_id("search").get_attribute("value")
		self.assertEquals(len(search_value), 0)
		field_select = self.browser.find_element_by_id("field-select")
		field_select_selected = field_select.find_element_by_css_selector("option:checked")
		self.assertEquals(field_select_selected.text,'All')
		# The sort must be reverted to the original one including the arrows
		priority_user_code = self.browser.find_element_by_id('priority_user_code')
		self.assertEquals(priority_user_code.text, '2') # System looks if this this label which is user no is equal to 2
		priority_user_status = self.browser.find_element_by_id('priority_user_status')
		self.assertEquals(priority_user_status.text, '1') # System looks if this this label which is user no is equal to 2
		# System checks if the arrow has reversed
		sort_user_status = self.browser.find_element_by_id('sort_user_status')
		sort_user_status_class = sort_user_status.get_attribute("class");
		self.assertNotIn('caret-reversed', sort_user_status_class)
		print ("------------")
		print ("%s.%s DONE - 14" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Searching resets the Sorting to Original
	def test_SearchResetSorting(self):
		# User goes to List Page First
		self.set_goToListPage()
		# User searches on the search field
		search = self.browser.find_element_by_id("search")
		search.send_keys("AA")
		search.send_keys(Keys.RETURN)
		time.sleep(2)
		# The sort must be reverted to the original one including the arrows
		try:
			priority_user_code = self.browser.find_element_by_id('priority_user_code')
			self.assertEquals(priority_user_code.text, '2') # System looks if this its user code is equal to 1
			self.fail("USER ID PRIORITY MUST NOT BE PRESENT")
		except:
			pass
		try:
			priority_user_status = self.browser.find_element_by_id('priority_user_status')
			self.assertEquals(priority_user_status.text, '1') # System looks if this its user status is equal to 1
			self.fail("USER STATUS PRIORITY MUST NOT BE PRESENT")
		except:
			pass
		print ("------------")
		print ("%s.%s DONE - 15" % (self.__class__.__name__, inspect.stack()[0][3]))

# python manage.py test crew_menu.crew_admin.user_management.tests.UserManagementTestAddPage
class UserManagementTestAddPage(StaticLiveServerTestCase):

	fixtures = UserManagementTestListPage.fixtures
	fixtures.append('email_notification_template.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/user-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToAddPage(self):
		# User goes to List Page First
		UserManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add User', self.browser.title)
		time.sleep(3)

	# Used as a reusable component for update
	def _set_AddFieldsThenSave(self):
		# User goes to Add User Page
		# self._setGoToAddPage()
		# User fills up the last name field
		last_name_field = self.browser.find_element_by_name('last_name')
		last_name_field.send_keys("Doe")
		# User fills up the first name field
		first_name_field = self.browser.find_element_by_name('first_name')
		first_name_field.send_keys("John")
		# User fills up the middle name field
		middle_name_field = self.browser.find_element_by_name('middle_name')
		middle_name_field.send_keys("Love")
		# User fills up the user code field
		user_code_field = self.browser.find_element_by_name('user_code')
		user_code_field.send_keys("aaaa")
		# User fills up the email field
		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys("bara@gmail.com")

		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)

	def _set_AddFields(self):
		# User goes to Add User Page
		self._setGoToAddPage()
		# User fills up the last name field
		last_name_field = self.browser.find_element_by_name('last_name')
		last_name_field.send_keys("Doe")
		# User fills up the first name field
		first_name_field = self.browser.find_element_by_name('first_name')
		first_name_field.send_keys("John")
		# User fills up the middle name field
		middle_name_field = self.browser.find_element_by_name('middle_name')
		middle_name_field.send_keys("Love")
		# User fills up the user code field
		user_code_field = self.browser.find_element_by_name('user_code')
		user_code_field.send_keys("aaaa")
		# User fills up the email field
		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys("bara@gmail.com")
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# COMPLETE
	def test_BasicAddUser(self):
		# User inputs values to fields then save
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		# System checks the title of the current page if it is User
		self.assertEquals('User', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('Doe', tr[1].text)
		self.assertIn('John', tr[1].text)
		self.assertIn('Love', tr[1].text)
		self.assertIn('aaaa', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Add User', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared
		last_name = self.browser.find_element_by_name('last_name')
		last_name_value = last_name.get_attribute("value");
		self.assertEquals(last_name_value, '')
		first_name = self.browser.find_element_by_name('first_name')
		first_name_value = first_name.get_attribute("value");
		middle_name = self.browser.find_element_by_name('middle_name')
		middle_name_value = middle_name.get_attribute("value");
		self.assertEquals(middle_name_value, '')
		user_code = self.browser.find_element_by_name('user_code')
		user_code_value = user_code.get_attribute("value");
		self.assertEquals(user_code_value, '')
		email = self.browser.find_element_by_name('email')
		email_value = email.get_attribute("value");
		self.assertEquals(email_value, '')
		time.sleep(2)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	# COMPLETE
	def test_ExitUser(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is User
		self.assertIn('User', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ShowNotifications(self):
		# User goes to Add User Page
		self._setGoToAddPage()
		save = self.browser.find_element_by_id("save")
		save.click()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_AllFieldsRequired(self):
		# User goes to Add User Page
		self._setGoToAddPage()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		print (span_count)
		# For every span.errors css system looks if field is required is present
		for x in range(span_count):
			self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Email field max of 50.
	# COMPLETE
	def test_ValidEmail(self):
		# User goes to Add User Page
		self._setGoToAddPage()
		# User types on the email field
		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-email')
		error_text = "Must be a valid email"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Cannot add emails that already exists
	# TODO
	def test_EmailAlreadyExists(self):
		# User adds another user
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add User', self.browser.title)
		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys("bara@gmail.com")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-email')
		error_text = "Email already exists"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("already exists")
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Email max is 50
	# COMPLETE
	def test_EmailMaxCharacters(self):
		# User goes to Add Rank Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('email')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGHHASDWERGVZCZASDWRETEWASDASDASDASDCZXAFAWE@gmail.com")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Code must be exactly four characters
	# COMPLETE
	def test_CodeExactlyFourCharacters(self):
		# User adds another user
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add User', self.browser.title)
		user_code = self.browser.find_element_by_name('user_code')
		user_code.send_keys("aaa")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-user-code')
		error_text = "Must be exactly 4 characters"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	# User Code must be unique
	# COMPLETE
	def test_CodeAlreadyExists(self):
		# User goes to Add Rank Page
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add User', self.browser.title)
		field = self.browser.find_element_by_name('user_code')
		# User types on the code field
		field.send_keys("aaaa")
		time.sleep(2)
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-user-code')
		error_text = "This code already exists"
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("Code - already exists")
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

# python manage.py test crew_menu.crew_admin.user_management.tests.UserManagementTestUpdatePage
class UserManagementTestUpdatePage(StaticLiveServerTestCase):

	fixtures = UserManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/crew-admin/user-management/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToUpdatePage(self):
		# User Adds values to fields
		UserManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		UserManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(3)
		# START User Clicks update on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		td_count = len(td)-1
		span = td[td_count].find_elements_by_tag_name("span")
		update_button = span[1].click()
		time.sleep(2)
		self.assertIn('Update User', self.browser.title)
		# END User Clicks update on the first row
		# time.sleep(3)

	def _set_UpdateFields(self):
		# User goes to Update User Page
		self._setGoToUpdatePage()
		# User fills up the last name field
		last_name_field = self.browser.find_element_by_name('last_name')
		last_name_field.send_keys("last")
		# User fills up the first name field
		first_name_field = self.browser.find_element_by_name('first_name')
		first_name_field.send_keys("first")
		# User fills up the middle name field
		middle_name_field = self.browser.find_element_by_name('middle_name')
		middle_name_field.send_keys("middle")
		# User Makes sure the user code is not editable and readonly
		user_code_field = self.browser.find_element_by_name('user_code')
		user_code_field.send_keys("aaaa")
		# User fills up the email field
		email_field = self.browser.find_element_by_name('email')
		email_field.send_keys("ss")
		time.sleep(2)

	def _set_SaveUpdate(self):
		confirmation_save = self.browser.find_element_by_id("confirmation-save")
		confirmation_save.click()
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		self._set_SaveUpdate()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# COMPLETE
	def test_BasicUpdateUser(self):
		# User updates values from the fields
		self._set_UpdateFields()
		# User clicks the save button that opens a confirmation
		self._set_SaveUpdate()
		self.assertEquals('User', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('Doelast', tr[1].text)
		self.assertIn('Johnfirst', tr[1].text)
		self.assertIn('Lovemiddle', tr[1].text)
		self.assertIn('aaaa', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Update User', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared except for user code and email
		last_name_field = self.browser.find_element_by_name('last_name')
		last_name_field_value = last_name_field.get_attribute("value");
		self.assertEquals(last_name_field_value, '')
		middle_name_field = self.browser.find_element_by_name('middle_name')
		middle_name_field_value = middle_name_field.get_attribute("value");
		self.assertEquals(middle_name_field_value, '')
		first_name_field = self.browser.find_element_by_name('first_name')
		first_name_field_value = first_name_field.get_attribute("value");
		self.assertEquals(first_name_field_value, '')
		user_code_field = self.browser.find_element_by_name('user_code')
		user_code_field_value = user_code_field.get_attribute("value");
		self.assertNotEquals(user_code_field_value, '')
		email_field = self.browser.find_element_by_name('email')
		email_field_value = email_field.get_attribute("value");
		self.assertNotEquals(email_field_value, '')
		time.sleep(2)
		# END System makes sure that the fields are cleared except for user code and email
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ExitRank(self):
		# User inputs values to fields
		self._set_UpdateFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Rank Management
		self.assertIn('User', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_ShowNotifications(self):
		# User goes to Update Rank Page
		self.test_ClearFields()
		self._set_SaveUpdate()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# COMPLETE
	def test_AllFieldsRequired(self):
		# User goes to Update Rank Page
		self.test_ClearFields()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present except for rank code
		for x in range(span_count):
			if x == 0 and x == 1:
				self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		self._set_SaveUpdate()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Email field max of 50.
	# TODO
	def test_ValidEmail(self):
		# User goes to Update User Page
		self._setGoToUpdatePage()
		email_field = self.browser.find_element_by_name('email')
		email_field_value = email_field.get_attribute("value");
		# User hits backspace until there are no more values on the email field
		while (email_field_value != ""):
			email_field_value = email_field.get_attribute("value");
			email_field.send_keys(Keys.BACKSPACE)
		email_field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-email')
		error_text = "Must be a valid email"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Cannot add emails that already exists
	# COMPLETE
	def test_EmailAlreadyExists(self):
		# User goes to Update User Page
		self._setGoToUpdatePage()
		email_field = self.browser.find_element_by_name('email')
		email_field_value = email_field.get_attribute("value");
		# User hits backspace until there are no more values on the email field
		while (email_field_value != ""):
			email_field_value = email_field.get_attribute("value");
			email_field.send_keys(Keys.BACKSPACE)
		email_field.send_keys("deanarmada@gmail.com")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-email')
		error_text = "Email already exists"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications("already exists")
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Email max is 50
	# TODO
	def test_EmailMaxCharacters(self):
		# User goes to Update User Page
		self._setGoToUpdatePage()
		field = self.browser.find_element_by_name('email')
		# User types 26 characters on the description field
		field.send_keys("AAAABBBBCCCCDDDDEEEEFFFFGGHHASDWERGVZCZASDWRETEWASDASDASDASDCZXAFAWE@gmail.com")
		# System checks if this is visible after typing
		field_value = field.get_attribute("value");
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	# When you add again your own email it must not return an error
	# TODO
	def test_ErrorMustNotReturnOnSelfEmail(self):
		# User goes to Update User Page
		self._setGoToUpdatePage()
		email_field = self.browser.find_element_by_name('email')
		email_field_value = email_field.get_attribute("value");
		# User hits backspace until there are no more values on the email field
		while (email_field_value != ""):
			email_field_value = email_field.get_attribute("value");
			email_field.send_keys(Keys.BACKSPACE)
		email_field.send_keys("bara@gmail.com")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-email')
		error_text = "Email already exists"
		time.sleep(2)
		self.assertNotIn("* "+error_text, error.text, )
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

# python manage.py test crew_menu.crew_admin.user_management.tests.UserManagementTestDeletePage
class UserManagementTestDeletePage(StaticLiveServerTestCase):

	fixtures = UserManagementTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.browser.maximize_window()
		self.url = '/crew-menu/crew-admin/user-management/#/'

	def tearDown(self):
		self.browser.quit()

	def set_Delete(self, row):
		# START User Clicks delete on the first row
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		td_count = len(td)-1
		removed_text = td[1].text
		span = td[td_count].find_elements_by_tag_name("span")
		# System checks the title of the current page if it is User
		self.assertIn('User', self.browser.title)
		time.sleep(2)
		# User clicks delete icon
		span[2].click()
		time.sleep(2)
		# User clicks delete button
		delete_button = self.browser.find_element_by_id("deactivate")
		delete_button.click()
		# END User Clicks delete on the first row
		time.sleep(2)
		# System Checks if the user is still available in the list page
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		self.assertNotEquals(removed_text, td[1].text)

	def set_Restore(self, row):
		# START User Clicks delete on the first row
		UserManagementTestListPage.set_SearchAutocomplete(self, "aaaa")
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		td_count = len(td)-1
		removed_text = td[1].text
		time.sleep(2)
		span = td[td_count].find_elements_by_tag_name("span")
		# System checks the title of the current page if it is User
		self.assertIn('User', self.browser.title)
		time.sleep(2)
		# User clicks restore icon
		span[2].click()
		# User clicks restore button
		restore_button = self.browser.find_element_by_id("restore")
		restore_button.click()
		# END User Clicks restore on the first row
		time.sleep(2)
		# System Checks if the user is still available in the list page
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[row].find_elements_by_tag_name("td")
		self.assertEquals(removed_text, td[1].text)

	def test_BasicDelete(self):
		# User Goes to Add Users Page
		UserManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		UserManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# Currently three
	def test_MultipleDelete(self):
		# User Goes to Add Users Page
		UserManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		UserManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		# User Clicks delete on the first row
		self.set_Delete(1)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the second
		self.set_Delete(2)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		time.sleep(1)
		# User Clicks delete on the first row again which used to be the third
		self.set_Delete(3)
		# User ignores and closes the restore modal
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))

	def test_BasicRestore(self):
		# User Goes to Add Users Page
		UserManagementTestAddPage._setGoToAddPage(self)
		# User Adds values to fields
		UserManagementTestAddPage._set_AddFieldsThenSave(self)
		time.sleep(2)
		tr = self.browser.find_elements_by_tag_name("tr")
		td = tr[1].find_elements_by_tag_name("td")
		# User Clicks delete on the first row
		self.set_Delete(1)
		dismiss_modal = self.browser.find_element_by_id("dismiss-modal")
		dismiss_modal.click()
		# User clicks the restore on the first row
		self.set_Restore(1)
		
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))




# Token must be included to make interaction with API
# Users must be able to add user
# Order must be an integer only
# Type must be at least 5 characters
# Error Notifications must shown upon validations
# Users must be able to delete
#  1.) Problem, Jed's Backend API coding is_active?
#  1.) Solution, To be consulted with Jed
# ----------------------------------------------------------
# Users must be able to perform multiple delete through checkbox
# Multiple Delete buton must be hidden unless a checkbox is ticked
# Updating a user involves of clicking the pencil button and pre-valued inputs based on the item must show
# TotalPage count must be real-time and collaborate with deleted items
# USER CODES must be UNIQUE
# Username must be the same as User Codes
# Passwords are now removed and an auto-email will be sent instead

# Search Capability
# When an option filter of search is not selected, it should be globally search through all the fields on the option
# Must search specifically in that field if that search filter option is selected
# 1.) Problem, document.querySelector( '#field-select' ) will not work anymore if the tag became a model
# 1.) Solution, replace it with $scope.searchList.value to just get the json.value's value
# ----------------------------------------------------------
# When an option filter of search is not selected, autocomplete drop down values must be involved on every field
# autocomplete drop down values must be specific in that field if that search filter option is selected
# 1.) Problem, value of the options is relational like: "bio__user_code", "bio__middle_name" which affects the mapping function of searchFilter function on he main.js
# 1.) Solution, manipulate the string by removing the "bio__" using string indexes
# ----------------------------------------------------------
# When searched make sure that it follows the limit offset and outputs only the entered search
# 1.) Problem, forgot to put the limit offset paramter passed as a request and do the math with the total search
# 1.) Solution, merged the limit offset on the json request and included the calculation of the total upon response
# ----------------------------------------------------------
# Does not do anythinging if enetered an empty input
# Search can be done by clicking on the autocomplete
# 1.) Problem, trigger is fast enough to just declare the value as what is written on the input box
# 2.) Solution, use $timeout on the 'autoComplete' directive in main.js
# If none of the items is equal to that entered input, there should be a notification
# Notifications must be gone when routed to another template
# 1.) Problem, $rootScope.Notifications is still set to true when routing is applied
# 2.) Solution, set $rootScope.Notifications to false on '$routeChangeSuccess' at main.js
# If entered in an empty searchbox it must be the same function as clear

# Email field max of 30 can be increased to 50.
# Page Numbers not working/refreshed correctly with Clear button.
# User code, User ID Number, and Email is used to detect duplicates.
# User ID Number and Email can be edited but still must be unique.
# Existing user code should not be editable.

# Dynamic Icon between active and inactive users