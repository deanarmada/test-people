// Configure our routes
peopleApp.config(function($routeProvider){
	$routeProvider
	// route for the homepage
	.when('/', {
		title: 'User Accounts',
		templateUrl: '/static/user_management/html/index.html',
		controller: 'indexController',
		resolve: {
			userList: function(userFactory){
				return userFactory.userList();
			},
		}
	})
	// route for the add page
	.when('/add', {
		title: 'Add User Account',
		templateUrl: '/static/user_management/html/add.html',
		controller: 'addController',
		resolve: {
			userFormValidations: function(userFactory){
				return userFactory.userFormValidations();
			},
		}
	})
	// route for update page with parameters
	.when('/update/:id', {
		title: 'Update User Account',
		templateUrl: '/static/user_management/html/update.html',
		controller: 'updateController',
		resolve: {
			userFormValidations: function(userFactory){
				return userFactory.userFormValidations();
			},
		}
	})
	.otherwise({
		redirectTo: '/'
	});
});

peopleApp.controller('indexController', function($scope, $http, $rootScope, $controller){
	$controller('itemListController', {$scope: $scope});
	$scope.mainAPI = '/api/people/v1/users/';
	$scope.apiGetItems = $rootScope.domainName+$scope.mainAPI;
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/users/update/';
	$scope.apiDelete = $rootScope.domainName+'/api/people/v1/users/delete/';
	$scope.searchFilters = [{
		value: null,
		label: 'All'
	},{
		value: 'bio__user_code',
		label: 'Code'
	},{
		value: 'first_name',
		label: 'First Name'
	}, {
		value: 'bio__middle_name',
		label: 'Middle Name'
	}, {
		value: 'last_name',
		label: 'Last Name'
	}];
	// to get the tableHeadersLength
	$scope.tableHeaders = [
		{'name': 'No.', 'url': $scope.mainAPI, 'field':'-id', 'class':'caret-reversed initial-reversed', 'id':'user_id'},
		{'name': 'Code', 'url': $scope.mainAPI, 'field':'-user_code', 'class':'caret-reversed initial-reversed', 'id':'user_code'},
		{'name': 'Name', 'class':'caret-reversed initial-reversed'},
		// {'name': 'Group', 'url': $scope.mainAPI, 'field':'-rank_department'},
		{'name': 'Group', 'class':'caret-reversed initial-reversed'},
		{'name': 'Status', 'url': $scope.mainAPI, 'field':'-is_active', 'id':'user_status'},
		{'name': 'Updated By', 'url': $scope.mainAPI, 'field':'-bio__updated_by__bio__user_code', 'class':'caret-reversed initial-reversed', 'id':'user_updated_by'},
		{'name': 'Date Updated', 'url': $scope.mainAPI, 'field':'-date_updated', 'class':'caret-reversed initial-reversed', 'id':'user_date_updated'},
	];
	$scope.searchItems = [];
	// Used for td colspan when results not found
	$scope.tableHeadersLength = $scope.tableHeaders.length + 2;

	// total came from rankFactory
	
	$rootScope.totalPage = Math.ceil(total/$rootScope.limit);
	$scope.originalTotalPage = $rootScope.totalPage;

	$rootScope.loadPage = function(){
		// url came from rankFactory
		if(total){
			$scope.getPage(url);
		}else{
			$scope.noRecord = true;
		}
	}

	$scope.Items = ['user_code', 'first_name', 'middle_name', 'last_name'];



	$http.post(domainName+url)
	.then(function(response){
		data = response.data.data;
		data.map(function(obj){ 
			for(x=0;x<$scope.Items.length;x++){
				$scope.searchItems.push(obj[$scope.Items[x]]); 
			}
		});
	});

	$scope.preSorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
	$scope.sorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
    $rootScope.loadPage();
});


peopleApp.controller('reusableComponents', function($scope, $rootScope, $http, $timeout){
	$scope.uniqueCheck = function(form, field, length, event, validity){
    	_this = angular.element(event.target);
    	value = _this.val();
    	if (typeof(length) == 'number'){
    		condition = value.length == length && !validity;
    		form = form.user_code;
    	}else{
    		// condition = value.length >= 30 && value.length <= 50;
    		if($scope.originalEmail == undefined){
	    		condition = validity;
	    	}else{
	    		condition = validity && $scope.originalEmail != value;
	    	}
    		form = form.email;
    	}
    	// $timeout(function() { 
    		if (condition){
	    		json = {};
	    		json['filters'] = {};
	    		json['filters'][field] = value;
	    		$http.post(domainName+"/api/people/v1/users/", json)
				.then(function(response){
					total = response.data.total;
					if(total){
						$timeout(function() {
							form.$setValidity("unique", false);
						}, 500);
					}else{
						$timeout(function() {
							form.$setValidity("unique", true);
						}, 500);
					}
				});
	    	}else{
	    		form.$setValidity("unique", true);
	    	}
    	// }, 500);
    	
    };
});

peopleApp.controller('addController', function($scope, $rootScope, $http, $controller){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiAdd = $rootScope.domainName+'/api/people/v1/users/add/';
	$scope.input = {};
    $scope.code_readonly = false;
    $scope.validations = validations;
    console.log($scope.validations);
});

peopleApp.controller('updateController', function($scope, $rootScope, $routeParams, $http, $controller){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/users/update/';
	id = parseInt($routeParams.id);

	$http.post($rootScope.domainName+'/api/people/v1/users/detail/', { 'id':id })
	.then(function(response){
		$scope.input = {
			// sample pre-valued field
			'last_name': response.data.data[0].last_name,
			'first_name': response.data.data[0].first_name,
			'middle_name': response.data.data[0].middle_name,
			'user_code': response.data.data[0].user_code,
			'username': response.data.data[0].user_code,
			'email': response.data.data[0].email,
		};
		$scope.id = id;
		$scope.originalEmail = $scope.input.email;
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});
    $scope.code_readonly = true;
    $scope.validations = validations;
    console.log($scope.validations);
});