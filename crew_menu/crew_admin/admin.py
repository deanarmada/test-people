from django.contrib import admin

from test_people.models import VesselTypes

# Register your models here.
admin.site.register(VesselTypes)