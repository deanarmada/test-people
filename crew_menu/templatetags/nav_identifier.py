from django import template

from django.contrib.auth.models import User
from crew_menu.bio.models import *

register = template.Library()

@register.simple_tag(takes_context=True)
def nav_identifier(context):
	request = context['request']
	try:
		# code = Bio.objects.get(auth_user=request.user)
		return request.user
	except:
		return "NO CODE YET"

@register.simple_tag(takes_context=True)
def session_id(context):
	request = context['request']
	print (request.user.id)
	return request.user.id