from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse

from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def index(request):
	template = 'crew_menu/index.html'
	# template = 'pure_html/crew_menu/index.html'
	context_dict = {}
	return render(request, template, context_dict)
	# return HttpResponse("CREW MENU INDEX")

# Create your views here.
def report(request):
	# template = 'crew_menu/index.html'
	template = 'pure_html/crew_menu/reports.html'
	context_dict = {}
	return render(request, template, context_dict)
	# return HttpResponse("CREW MENU INDEX")