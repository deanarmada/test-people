from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources, widgets, fields

from . models import *

class BioStatusesResource(resources.ModelResource):
	class Meta:
		model = BioStatuses

class BioStatusesImport(ImportExportModelAdmin):
	resource_class = BioStatusesResource

class SeparationTypesResource(resources.ModelResource):
	class Meta:
		model = SeparationTypes

class SeparationTypesImport(ImportExportModelAdmin):
	resource_class = SeparationTypesResource

# Register your models here.
admin.site.register(Bio)
admin.site.register(BioStatuses,  BioStatusesImport)
admin.site.register(SeparationTypes, SeparationTypesImport)
admin.site.register(RelatedPeople)
admin.site.register(BioHobbies)
admin.site.register(BioFilipinoAddress)
admin.site.register(BioSocialMedia)
admin.site.register(BioMobiles)
admin.site.register(BioEducations)