from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
@login_required
def index(request):
	template = 'bio/index.html'
	# template = 'pure_html/crew_menu/bio/index.html'
	context_dict = {}
	return render(request, template, context_dict)
	# return HttpResponse("BIO INDEX")