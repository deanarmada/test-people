# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-16 06:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0001_initial'),
        ('bio', '0002_auto_20160205_0142'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cba',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('cba_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('cba_code', models.CharField(blank=True, max_length=12, null=True)),
                ('cba_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'cba',
            },
        ),
        migrations.CreateModel(
            name='CertificateGroupDetails',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('certificate_group_detail_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_group_detail_record', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_group_detail_weight', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_group_detail_tag', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'certificate_group_details',
            },
        ),
        migrations.CreateModel(
            name='CertificateGroups',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('certificate_group_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_group_code', models.CharField(blank=True, max_length=4, null=True)),
                ('certificate_group_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'certificate_groups',
            },
        ),
        migrations.CreateModel(
            name='Certificates',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('certificate_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_code', models.CharField(blank=True, max_length=4, null=True)),
                ('certificate_full', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_book', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_main', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_expires', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_interval_years', models.CharField(blank=True, max_length=45, null=True)),
                ('certificate_interval_months', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'certificates',
            },
        ),
        migrations.CreateModel(
            name='Dependents',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('dependent_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('dependent_sys_record', models.IntegerField(blank=True, null=True)),
                ('dependent_birthdate', models.DateField(blank=True, null=True)),
                ('dependent_first_name', models.CharField(blank=True, max_length=45, null=True)),
                ('dependent_last_name', models.CharField(blank=True, max_length=45, null=True)),
                ('dependent_father_name', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.IntegerField()),
            ],
            options={
                'db_table': 'dependents',
            },
        ),
        migrations.CreateModel(
            name='Documents',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('document_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('document_record', models.IntegerField(blank=True, null=True)),
                ('document_certificate', models.IntegerField(blank=True, null=True)),
                ('document_number', models.CharField(blank=True, max_length=45, null=True)),
                ('document_issue', models.DateField(blank=True, null=True)),
                ('document_expire', models.DateField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'documents',
            },
        ),
        migrations.CreateModel(
            name='ExitInfo',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('exit_info_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_months_employ_shore', models.IntegerField(blank=True, null=True)),
                ('exit_info_last_employer_shore', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_position_shore', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_months_employ_abroad', models.IntegerField(blank=True, null=True)),
                ('exit_info_last_employer_abroad', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_last_position_abroad', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_last_employer_address_abroad', models.CharField(blank=True, max_length=45, null=True)),
                ('exit_info_allot_percent', models.IntegerField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'exit_info',
            },
        ),
        migrations.CreateModel(
            name='LicenseDetails',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('license_detail_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('license_detail_record', models.IntegerField(blank=True, null=True)),
                ('license_detail_book_number', models.CharField(blank=True, max_length=45, null=True)),
                ('license_detail_issue', models.DateField(blank=True, null=True)),
                ('license_detail_expire', models.DateField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'license_details',
            },
        ),
        migrations.CreateModel(
            name='LicenseHead',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('license_head_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_bio', models.IntegerField(blank=True, null=True)),
                ('license_head_rp_book_number', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_rp_book_issue', models.DateField(blank=True, null=True)),
                ('license_head_rp_book_expire', models.DateField(blank=True, null=True)),
                ('license_head_rp_book_place', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_rp_license', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_rp_issue', models.DateField(blank=True, null=True)),
                ('license_head_rp_expire', models.DateField(blank=True, null=True)),
                ('license_head_reg_card', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_reg_issue', models.DateTimeField(blank=True, null=True)),
                ('license_head_reg_expire', models.DateTimeField(blank=True, null=True)),
                ('license_head_passport', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_passport_issue', models.DateField(blank=True, null=True)),
                ('license_head_passport_expire', models.DateField(blank=True, null=True)),
                ('license_head_passport_place', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_usa_visa', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_usa_expire', models.DateField(blank=True, null=True)),
                ('license_head_pending_legal', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_yellow_card', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_yellow_card_expire', models.DateField(blank=True, null=True)),
                ('license_head_dutch_visa', models.CharField(blank=True, max_length=45, null=True)),
                ('license_head_dutch_expire', models.DateField(blank=True, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'license_head',
            },
        ),
        migrations.CreateModel(
            name='PrincipalAcctCategories',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('principal_acct_category_name', models.CharField(blank=True, max_length=45, null=True)),
            ],
            options={
                'db_table': 'principal_acct_categories',
            },
        ),
        migrations.CreateModel(
            name='PrincipalAddresses',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('principal_address', models.CharField(blank=True, max_length=45, null=True)),
            ],
            options={
                'db_table': 'principal_addresses',
            },
        ),
        migrations.CreateModel(
            name='Principals',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('principal_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('principal_code', models.CharField(blank=True, max_length=6, null=True)),
                ('principal_full', models.CharField(blank=True, max_length=45, null=True)),
                ('principal_sss', models.CharField(blank=True, max_length=45, null=True)),
                ('principal_accred_no', models.CharField(blank=True, max_length=45, null=True)),
                ('principal_telephone', models.CharField(blank=True, max_length=45, null=True)),
                ('principal_acct_expenses', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'principals',
            },
        ),
        migrations.CreateModel(
            name='RequiredCertificates',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('required_certificate_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('required_certificate_record', models.IntegerField(blank=True, null=True)),
                ('required_certificate_source', models.CharField(blank=True, max_length=45, null=True)),
                ('required_certificate_interval', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'required_certificates',
            },
        ),
        migrations.CreateModel(
            name='VesselAssignEquipments',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'vessel_assign_equipments',
            },
        ),
        migrations.CreateModel(
            name='VesselEquipments',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_equipment_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_equipment_code', models.CharField(blank=True, max_length=4, null=True)),
                ('vessel_equipment_full', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'vessel_equipments',
            },
        ),
        migrations.CreateModel(
            name='VesselGroupDetails',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_group_details_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_group_detail_record', models.CharField(blank=True, max_length=8, null=True)),
                ('vessel_group_detail_tags', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'vessel_group_details',
            },
        ),
        migrations.CreateModel(
            name='VesselGroups',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_group_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_group_code', models.CharField(blank=True, max_length=8, null=True)),
                ('vessel_group_full', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_group_group_by', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_group_status', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'vessel_groups',
            },
        ),
        migrations.CreateModel(
            name='Vessels',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('vessel_sys_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_code', models.CharField(blank=True, max_length=4, null=True)),
                ('vessel_full', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_registry', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_reg_number', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_grt', models.IntegerField(blank=True, null=True)),
                ('vessel_nrt', models.IntegerField(blank=True, null=True)),
                ('vessel_dwt', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_engine', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_horsepower', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_class', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_yard', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_year_built', models.IntegerField(blank=True, null=True)),
                ('vessel_trades', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_ums', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_call_sign', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_owner_ship', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_imo', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_status', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_end', models.DateField(blank=True, null=True)),
                ('vessel_reason', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_main_cat', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_pays_nat', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_pays_fmy', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_pays_pno', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_pays_tax', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_p_post_date', models.DateField(blank=True, null=True)),
                ('vessel_r_post_date', models.DateField(blank=True, null=True)),
                ('vessel_e_post_date', models.DateField(blank=True, null=True)),
                ('vessel_tfs_fleet', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_tfs_ship', models.CharField(blank=True, max_length=4, null=True)),
                ('vessel_email_1', models.CharField(blank=True, max_length=45, null=True)),
                ('vessel_email_2', models.CharField(blank=True, max_length=45, null=True)),
                ('date_updated', models.DateTimeField()),
            ],
            options={
                'db_table': 'vessels',
            },
        ),
        migrations.RenameField(
            model_name='biostatuses',
            old_name='code',
            new_name='bio_status_code',
        ),
        migrations.RenameField(
            model_name='biostatuses',
            old_name='description',
            new_name='bio_status_description',
        ),
        migrations.RenameField(
            model_name='biostatuses',
            old_name='name',
            new_name='bio_status_name',
        ),
        migrations.RemoveField(
            model_name='bio',
            name='bio_statuses',
        ),
        migrations.RemoveField(
            model_name='bio',
            name='civil_statuses',
        ),
        migrations.RemoveField(
            model_name='bio',
            name='nautical_degrees',
        ),
        migrations.RemoveField(
            model_name='bio',
            name='nautical_schools',
        ),
        migrations.RemoveField(
            model_name='bio',
            name='tax_statuses',
        ),
        migrations.AddField(
            model_name='bio',
            name='bio_status',
            field=models.ForeignKey(db_column='bio_status', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='bio_bio_status', to='bio.BioStatuses'),
        ),
        migrations.AddField(
            model_name='bio',
            name='civil_status',
            field=models.ForeignKey(db_column='civil_status', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='bio_civil_status', to='test_people.CivilStatuses'),
        ),
    ]
