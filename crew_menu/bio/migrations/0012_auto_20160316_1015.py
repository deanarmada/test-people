# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-16 10:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bio', '0011_auto_20160316_1012'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='principalcontactinfo',
            table='principal_contact_info',
        ),
    ]
