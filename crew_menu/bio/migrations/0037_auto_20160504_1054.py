# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-04 10:54
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('test_people', '0028_auto_20160503_0826'),
        ('bio', '0036_auto_20160503_0826'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='biolanguagesdialects',
            name='bio_lang_dialect_dialect',
        ),
        migrations.RemoveField(
            model_name='biolanguagesdialects',
            name='bio_lang_dialect_language',
        ),
        migrations.RemoveField(
            model_name='biolanguagesdialects',
            name='updated_by',
        ),
        migrations.DeleteModel(
            name='BioLanguagesDialects',
        ),
        migrations.CreateModel(
            name='BioLanguageDialects',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('bio_lang_dialect_dialect', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='test_people.Dialects')),
                ('updated_by', models.ForeignKey(db_column='updated_by', on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'bio_languages_dialects',
            },
        ),
    ]
