# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-05 10:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0029_auto_20160505_1009'),
        ('bio', '0041_auto_20160505_0814'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='biofilipinoaddress',
            name='bio_fil_barangay',
        ),
        migrations.RemoveField(
            model_name='biofilipinoaddress',
            name='bio_fil_municipality',
        ),
        migrations.RemoveField(
            model_name='relatedpeople',
            name='related_people_barangay',
        ),
        migrations.RemoveField(
            model_name='relatedpeople',
            name='related_people_municipality',
        ),
        migrations.AddField(
            model_name='biofilipinoaddress',
            name='bio_fil_zip',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='test_people.Zip'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='relatedpeople',
            name='related_people_is_student',
            field=models.CharField(blank=True, max_length=1, null=True),
        ),
        migrations.AddField(
            model_name='relatedpeople',
            name='related_people_occupation',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='relatedpeople',
            name='related_people_zip',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='test_people.Zip'),
        ),
    ]
