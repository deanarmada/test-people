# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-12 03:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('test_people', '0036_socialmedia'),
        ('bio', '0042_auto_20160505_1009'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='biosocialmedia',
            name='bio_social_name',
        ),
        migrations.AddField(
            model_name='biofilipinoaddress',
            name='bio_fil_province_barangay',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='biosocialmedia',
            name='bio_social_media',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='test_people.SocialMedia'),
            preserve_default=False,
        ),
    ]
