# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-18 07:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bio', '0013_auto_20160317_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='principals',
            name='principal_full',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
