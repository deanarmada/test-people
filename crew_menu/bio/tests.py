from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, LiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from login.tests import LogInReusablesScript as LRS , LogInTestFT as LTFT

from unittest import skip

import time
import inspect

# Create your tests here.
# Command Line: python manage.py test crew_menu.bio

# python manage.py test crew_menu.bio.tests.BioEmptyTestListPage
class BioEmptyTestListPage(StaticLiveServerTestCase):
	# Used LTFT because LRS already inherits the appended json all throughout
	fixtures = LTFT.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/bio/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Bio List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Bio
		self.assertIn('Crew Information Management', self.browser.title)

	def test_IsEmpty(self):
		# User Goes to the the Bio List Page
		self.set_goToListPage()
		time.sleep(1)
		# System checks the output if the table is empty
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('No records found', tr[1].text)
		print ("------------")
		print ("%s.%s DONE" % (self.__class__.__name__, inspect.stack()[0][3]))

# python manage.py test crew_menu.bio.tests.BioTestListPage
class BioTestListPage(StaticLiveServerTestCase):
	fixtures = LRS.fixtures
	# Command: python manage.py dumpdata test_people.bio --indent=2 > login/fixtures/bio.json
	fixtures.append('bio.json')

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/bio/'

	def tearDown(self):
		self.browser.quit()

	def set_goToListPage(self):
		# self._setup_login_with_access
		# User Logs in
		LRS._setupLogIn(self)
		# Server goes to the Bio List Page
		self.browser.get(self.live_server_url+self.url)
		time.sleep(2)
		# System checks the title of the current page if it is Bio
		self.assertIn('Crew Information Management', self.browser.title)

# python manage.py test crew_menu.bio.tests.BioTestAddPage
class BioTestAddPage(StaticLiveServerTestCase):

	fixtures = BioTestListPage.fixtures

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
		self.url = '/crew-menu/bio/#/'

	def tearDown(self):
		self.browser.quit()

	def _setGoToAddPage(self):
		# User goes to List Page First
		RankManagementTestListPage.set_goToListPage(self)
		time.sleep(3)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add Crew Information Management', self.browser.title)
		time.sleep(3)

	# Used as a reusable component for update
	def _set_AddFieldsThenSave(self):
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the rank code field
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field.send_keys('MA')
		# User fills up the rank description field
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field.send_keys('Master Dean')
		# User fills up the rank department field
		rank_department_field = Select(self.browser.find_element_by_name('rank_department'))
		rank_department_field.select_by_visible_text('Engine')
		# User fills up the order rank type
		rank_type_field = Select(self.browser.find_element_by_name('rank_type'))
		rank_type_field.select_by_visible_text('Officer')
		time.sleep(2)
		save = self.browser.find_element_by_id("save")
		save.click()

	def _set_AddFields(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		# User fills up the order field
		order_field = self.browser.find_element_by_name('order')
		order_field.send_keys(1)
		# User fills up the rank code field
		rank_code_field = self.browser.find_element_by_name('rank_code')
		rank_code_field.send_keys('MA')
		# User fills up the rank description field
		rank_description_field = self.browser.find_element_by_name('rank_description')
		rank_description_field.send_keys('Master Dean')
		# User fills up the rank department field
		rank_department_field = Select(self.browser.find_element_by_name('rank_department'))
		rank_department_field.select_by_visible_text('Engine')
		# User fills up the order rank type
		rank_type_field = Select(self.browser.find_element_by_name('rank_type'))
		rank_type_field.select_by_visible_text('Officer')
		time.sleep(2)

	def _set_CheckNotifications(self, text):
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notifications = self.browser.find_element_by_id('login-notifications')
		time.sleep(2)
		self.assertIn(text, error_notifications.text, )

	# TODO
	@skip("Not yet prepared for testing")
	def test_BasicAddPrincipal(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		time.sleep(2)
		# START User checks if added values are present in the table
		tr = self.browser.find_elements_by_tag_name("tr")
		self.assertIn('1', tr[1].text)
		self.assertIn('MA', tr[1].text)
		self.assertIn('Master Dean', tr[1].text)
		self.assertIn('Engine', tr[1].text)
		self.assertIn('Officer', tr[1].text)
		# END User checks if added values are present in the table
		print ("------------")
		print ("%s.%s DONE - 1" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ClearFields(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		clear = self.browser.find_element_by_id("clear")
		clear.click()
		self.assertIn('Add Principal', self.browser.title)
		time.sleep(2)
		# START System makes sure that the fields are cleared
		order_field = self.browser.find_element_by_name('order')
		order_field_value = order_field.get_attribute("value");
		self.assertEquals(order_field_value, '')
		principal_code_field = self.browser.find_element_by_name('principal_code')
		principal_code_field_value = principal_code_field.get_attribute("value");
		self.assertEquals(principal_code_field_value, '')
		principal_description_field = self.browser.find_element_by_name('principal_description')
		principal_description_field_value = principal_description_field.get_attribute("value");
		self.assertEquals(principal_description_field_value, '')
		principal_department_field = self.browser.find_element_by_name('principal_department')
		principal_department_field_value = principal_department_field.get_attribute("value");
		self.assertEquals(principal_department_field_value, '')
		principal_type_field = self.browser.find_element_by_name('principal_type')
		principal_type_field_value = principal_type_field.get_attribute("value");
		self.assertEquals(principal_type_field_value, '')
		time.sleep(3)
		# END System makes sure that the fields are cleared
		print ("------------")
		print ("%s.%s DONE - 2" % (self.__class__.__name__, inspect.stack()[0][3]))
	# TODO
	@skip("Not yet prepared for testing")
	def test_ExitPrincipal(self):
		# User inputs values to fields
		self._set_AddFields()
		# User clicks clear
		cancel = self.browser.find_element_by_id("cancel")
		cancel.click()
		time.sleep(2)
		# System checks the title of the current page if it is Principal Management
		self.assertIn('Principal Management', self.browser.title)
		print ("------------")
		print ("%s.%s DONE - 3" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_ShowNotifications(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		save = self.browser.find_element_by_id("save")
		save.click()
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		error_notification_divs_count = len(error_notification_divs)
		self.assertNotEquals(0, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 4" % (self.__class__.__name__, inspect.stack()[0][3]))

	# TODO
	@skip("Not yet prepared for testing")
	def test_AllFieldsRequired(self):
		# User goes to Add Principal Page
		self._setGoToAddPage()
		# System searches all the span.errors css
		span = self.browser.find_elements_by_css_selector("span.errors")
		# System counts all the span.errors css
		span_count = len(span)
		# For every span.errors css system looks if field is required is present
		for x in range(span_count):
			self.assertIn("* This field is required", span[x].text)
		# User clicks the save button
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		error_notification_div = self.browser.find_element_by_id('login-notifications')
		error_notification_divs = error_notification_div.find_elements_by_css_selector("div")
		time.sleep(2)
		error_notification_divs_count = len(error_notification_divs)
		self.assertEquals(span_count, error_notification_divs_count)
		print ("------------")
		print ("%s.%s DONE - 5" % (self.__class__.__name__, inspect.stack()[0][3]))

	# START Input Validations
	# Principal Code must be 4-6 in length
	# TODO
	@skip("Not yet prepared for testing")
	def test_CodeExactlyFourCharacters(self):
		# User adds another user
		self._set_AddFields()
		# self._set_AddFieldsThenSave()
		# User clicks save
		save = self.browser.find_element_by_id("save")
		save.click()
		time.sleep(2)
		add_page_button = self.browser.find_element_by_id("add-page")
		add_page_button.click()
		time.sleep(2)
		self.assertIn('Add User', self.browser.title)
		user_code = self.browser.find_element_by_name('user_code')
		user_code.send_keys("aaa")
		time.sleep(2)
		error = self.browser.find_element_by_id('error-user-code')
		error_text = "Must be exactly 4 characters"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 6" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_FirstNameMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('first_name')
		field_value = field.get_attribute("value");
		while(len(field_value) != 32 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 30)
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_MiddleNameMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('middle_name')
		field_value = field.get_attribute("value");
		while(len(field_value) != 62 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 60)
		print ("------------")
		print ("%s.%s DONE - 7" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_LastNameMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('last_name')
		field_value = field.get_attribute("value");
		while(len(field_value) != 32 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 30)
		print ("------------")
		print ("%s.%s DONE - 8" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_SuffixMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('suffix')
		field_value = field.get_attribute("value");
		while(len(field_value) != 7 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 5)
		print ("------------")
		print ("%s.%s DONE - 9" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_SeparationCommentsMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('separation_comments')
		field_value = field.get_attribute("value");
		while(len(field_value) != 52 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 10" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_NickNameMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('nickname')
		field_value = field.get_attribute("value");
		while(len(field_value) != 14 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 12)
		print ("------------")
		print ("%s.%s DONE - 11" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_EmailMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('email_address')
		field_value = field.get_attribute("value");
		while(len(field_value) != 52 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_ValidEmail(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		email_field = self.browser.find_element_by_name('email_address')
		email_field_value = email_field.get_attribute("value");
		# User hits backspace until there are no more values on the email field
		while (email_field_value != ""):
			email_field_value = email_field.get_attribute("value");
			email_field.send_keys(Keys.BACKSPACE)
		email_field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-email-address')
		error_text = "Must be a valid email"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 13" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_Email2MaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('email_address_2')
		field_value = field.get_attribute("value");
		while(len(field_value) != 52 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 12" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_ValidEmail2(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		email_field = self.browser.find_element_by_name('email_address_2')
		email_field_value = email_field.get_attribute("value");
		# User hits backspace until there are no more values on the email field
		while (email_field_value != ""):
			email_field_value = email_field.get_attribute("value");
			email_field.send_keys(Keys.BACKSPACE)
		email_field.send_keys("eeeee")
		# System checks if an error appears
		error = self.browser.find_element_by_id('error-email-address-2')
		error_text = "Must be a valid email"
		time.sleep(2)
		self.assertIn("* "+error_text, error.text, )
		self._set_CheckNotifications(error_text)
		print ("------------")
		print ("%s.%s DONE - 13" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_ReligionMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('religion')
		field_value = field.get_attribute("value");
		while(len(field_value) != 52 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 50)
		print ("------------")
		print ("%s.%s DONE - 14" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_DialectMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('dialect')
		field_value = field.get_attribute("value");
		while(len(field_value) != 102 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 100)
		print ("------------")
		print ("%s.%s DONE - 15" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_HobbiesMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('hobbies')
		field_value = field.get_attribute("value");
		while(len(field_value) != 102 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 100)
		print ("------------")
		print ("%s.%s DONE - 16" % (self.__class__.__name__, inspect.stack()[0][3]))


	@skip("Not yet prepared for testing")
	def test_MarriagePlaceMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('marriage_place')
		field_value = field.get_attribute("value");
		while(len(field_value) != 34 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 32)
		print ("------------")
		print ("%s.%s DONE - 17" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_TaxAccountNumberMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('tax_account_number')
		field_value = field.get_attribute("value");
		while(len(field_value) != 22 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 20)
		print ("------------")
		print ("%s.%s DONE - 18" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_SSSNumberMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('sss_number')
		field_value = field.get_attribute("value");
		while(len(field_value) != 14 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 12)
		print ("------------")
		print ("%s.%s DONE - 19" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_PagIbigNumberMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('pagibig_number')
		field_value = field.get_attribute("value");
		while(len(field_value) != 17 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 15)
		print ("------------")
		print ("%s.%s DONE - 20" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_PhilhealthNumberMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('philhealth_number')
		field_value = field.get_attribute("value");
		while(len(field_value) != 16 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 14)
		print ("------------")
		print ("%s.%s DONE - 21" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_GeneralCommentsMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('general_comments')
		field_value = field.get_attribute("value");
		while(len(field_value) != 62 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 60)
		print ("------------")
		print ("%s.%s DONE - 22" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_ContactNumberMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('contact_number')
		field_value = field.get_attribute("value");
		while(len(field_value) != 18 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 16)
		print ("------------")
		print ("%s.%s DONE - 21" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_ContactNumber2MaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('contact_number_2')
		field_value = field.get_attribute("value");
		while(len(field_value) != 18 ):
			field_value = field.get_attribute("value");
			field.send_keys("2")
		self.assertEquals(len(field_value), 16)
		print ("------------")
		print ("%s.%s DONE - 21" % (self.__class__.__name__, inspect.stack()[0][3]))

	@skip("Not yet prepared for testing")
	def test_BioAddressMaxCharacters(self):
		# User goes to Add Bio Page
		self._setGoToAddPage()
		field = self.browser.find_element_by_name('bio_address')
		field_value = field.get_attribute("value");
		while(len(field_value) != 77 ):
			field_value = field.get_attribute("value");
			field.send_keys("A")
		self.assertEquals(len(field_value), 75)
		print ("------------")
		print ("%s.%s DONE - 22" % (self.__class__.__name__, inspect.stack()[0][3]))

	# END Input Validations