// Configure our routes

peopleApp.config(function($routeProvider){
	$routeProvider
	// route for the homepage
	.when('/', {
		title: 'BioData Management',
		templateUrl: '/static/bio/html/index.html',
		controller: 'indexController',
		// Resolve is needed to complete the data first before rendering the template
		resolve: {
			bioList: function(bioFactory){
				return bioFactory.bioList();
			},
		}
	})
	.when('/generate-report', {
		title: 'BioData Report',
		templateUrl: '/static/bio/html/generate-report.html',
		controller: 'reportController',
		// resolve: {

		// }
	})
	// route for the add page
	.when('/add', {
		title: 'Add BioData Management',
		templateUrl: '/static/bio/html/add-update.html',
		controller: 'addController',
		resolve: {
			bioFormValidations: function(bioFactory){
				return bioFactory.bioFormValidations();
			},
			rankListComplete: function(rankFactory){
				return rankFactory.rankCompleteList();
			},
			principalListComplete: function(principalFactory){
				return principalFactory.principalCompleteList();
			},
			employeeTypeList: function(employeeTypeFactory){
				return employeeTypeFactory.employeeTypeList();
			},
			employmentTypeList: function(employmentTypeFactory){
				return employmentTypeFactory.employmentTypeList();
			},
			titleList: function(titleFactory){
				return titleFactory.titleList();
			},
			bioStatusList: function(bioStatusFactory){
				return bioStatusFactory.bioStatusList();
			},
			separationTypeList: function(separationTypeFactory){
				return separationTypeFactory.separationTypeList();
			},
			nameSuffixList: function(nameSuffixFactory){
				return nameSuffixFactory.nameSuffixList();
			},
			// Also used for Nationality
			countryList: function(countryFactory){
				return countryFactory.countryList();
			},
			bloodTypeList: function(bloodTypeFactory){
				return bloodTypeFactory.bloodTypeList();
			},
			religionList: function(religionFactory){
				return religionFactory.religionList();
			},
			civilStatusList: function(civilStatusFactory){
				return civilStatusFactory.civilStatusList();
			},
			taxStatusList: function(taxStatusFactory){
				return taxStatusFactory.taxStatusList();
			},
			areaCodeList: function(areaCodeFactory){
				return areaCodeFactory.areaCodeList();
			},
			mobilePrefixNetworkList: function(mobilePrefixNetworkFactory){
				return mobilePrefixNetworkFactory.mobilePrefixNetworkList();
			},
			regionList: function(regionFactory){
				return regionFactory.regionList();
			},
			hobbyList: function(hobbyFactory){
				return hobbyFactory.hobbyList();
			},
			languageList: function(languageFactory){
				return languageFactory.languageList();
			},
			dialectList: function(dialectFactory){
				return dialectFactory.dialectList();
			},
			relationshipTypesList: function(relationshipTypesFactory){
				return relationshipTypesFactory.relationshipTypesList();
			},
			degreesList: function(degreesFactory){
				return degreesFactory.degreesList();
			},
			schoolsList: function(schoolsFactory){
				return schoolsFactory.schoolsList();
			},
			socialMediaList: function(socialMediaFactory){
				return socialMediaFactory.socialMediaList();
			},
			newBioIdList: function(newBioIdFactory){
				return newBioIdFactory.newBioIdList();
			},
		}
	})
	// route for update page with parameters
	.when('/update/:id', {
		title: 'Update BioData Management',
		templateUrl: '/static/bio/html/add-update.html',
		controller: 'updateController',
		resolve: {
			bioFormValidations: function(bioFactory){
				return bioFactory.bioFormValidations();
			},
			rankListComplete: function(rankFactory){
				return rankFactory.rankCompleteList();
			},
			principalListComplete: function(principalFactory){
				return principalFactory.principalCompleteList();
			},
			employeeTypeList: function(employeeTypeFactory){
				return employeeTypeFactory.employeeTypeList();
			},
			employmentTypeList: function(employmentTypeFactory){
				return employmentTypeFactory.employmentTypeList();
			},
			titleList: function(titleFactory){
				return titleFactory.titleList();
			},
			bioStatusList: function(bioStatusFactory){
				return bioStatusFactory.bioStatusList();
			},
			separationTypeList: function(separationTypeFactory){
				return separationTypeFactory.separationTypeList();
			},
			nameSuffixList: function(nameSuffixFactory){
				return nameSuffixFactory.nameSuffixList();
			},
			// Also used for Nationality
			countryList: function(countryFactory){
				return countryFactory.countryList();
			},
			bloodTypeList: function(bloodTypeFactory){
				return bloodTypeFactory.bloodTypeList();
			},
			religionList: function(religionFactory){
				return religionFactory.religionList();
			},
			civilStatusList: function(civilStatusFactory){
				return civilStatusFactory.civilStatusList();
			},
			taxStatusList: function(taxStatusFactory){
				return taxStatusFactory.taxStatusList();
			},
			areaCodeList: function(areaCodeFactory){
				return areaCodeFactory.areaCodeList();
			},
			mobilePrefixNetworkList: function(mobilePrefixNetworkFactory){
				return mobilePrefixNetworkFactory.mobilePrefixNetworkList();
			},
			regionList: function(regionFactory){
				return regionFactory.regionList();
			},
			hobbyList: function(hobbyFactory){
				return hobbyFactory.hobbyList();
			},
			languageList: function(languageFactory){
				return languageFactory.languageList();
			},
			dialectList: function(dialectFactory){
				return dialectFactory.dialectList();
			},
			relationshipTypesList: function(relationshipTypesFactory){
				return relationshipTypesFactory.relationshipTypesList();
			},
			degreesList: function(degreesFactory){
				return degreesFactory.degreesList();
			},
			schoolsList: function(schoolsFactory){
				return schoolsFactory.schoolsList();
			},
			socialMediaList: function(socialMediaFactory){
				return socialMediaFactory.socialMediaList();
			},
			newBioIdList: function(newBioIdFactory){
				return newBioIdFactory.newBioIdList();
			},
		}
	})
	// // route for viewing page with parameters
	// .when('/view/:id', {
	.when('/view/:id', {
		title: 'View BioData Management',
		templateUrl: '/static/bio/html/view.html',
		controller: 'viewController',
		resolve: {
			relationshipTypesList: function(relationshipTypesFactory){
				return relationshipTypesFactory.relationshipTypesList();
			},
		}
	})
	.otherwise({
		redirectTo: '/'
	});
});

peopleApp.controller('reusableIndexReportController', function($scope, $http){
	
	// Used in generate report( filters section )
	$scope.advancedFilterRows = [];
	// Used in generate report(  sort section )
	$scope.advancedSortRows = [{}];
	$scope.fieldOptions = [];
	$scope.operatorOptions = {};
	$scope.valueOptions = {};

	// Short fort avancedFilterRequest
	// $scope.aFR = [];

	$scope.advancedFieldChange = function(count){
		$http.get("/static/js/configs/advancedFilter.json?query='+forceReload").then(function(response){
			response.data.map(function(obj){
				if(obj['field_value'] == $scope.advancedFilterRows[count]["field"]){
					$scope.operatorOptions[count] = [];
					obj["operators"].map(function(obj){
						for(x in obj){
							$scope.operatorOptions[count].push({
								label: x,
								value: obj[x],
							});
						}
					});

					$scope.valueOptions[count] = [];
					if(obj['url']){
						$http.post(domainName+"/api/people/v1"+obj['url']).then(function(response){
							response.data.data.map(function(_obj){
								$scope.valueOptions[count].push({
									label: _obj[obj["label_index"]],
									value: _obj["id"],
								});
							});
						});
					}else{
						$scope.advancedFilterRows[count]["textbox"] = 1;
					}
				}
			});
		});
	};

	$http.get("/static/js/configs/advancedFilter.json").then(function(response){
		// COMMENTS ARE SUPPOSED TO BE PUT ON THE JSON FILE BUT COMMENTS ARE NOT ALLOWED THERE AND WILL RESULT TO A SYNTAX ERROR

		// Syntax that fills up the Options of the Field Selection
		response.data.map(function(obj){
			$scope.fieldOptions.push({
				label: obj['field_label'],
				value: obj['field_value'],
			});
		});

		$scope.advancedFilterRows.push({});

		// Syntax that fills up the operators of the selected field
		// $scope.operatorOptions[0] = [];
		// response.data[0]["operators"].map(function(obj){
		// 	for(x in obj){
		// 		$scope.operatorOptions[0].push({
		// 			label: x,
		// 			value: obj[x],
		// 		});
		// 	}
		// });

		// Syntax that fills up the value options on the corresponding field (fetches from the API)
		// $scope.valueOptions[0] = [];
		// $http.post(domainName+"/api/people/v1/ranks/").then(function(response){
		// 	response.data.data.map(function(obj){
		// 		$scope.valueOptions[0].push({
		// 			label: obj["rank_code"],
		// 			value: obj["id"],
		// 		});
		// 	});
		// });

		// Default values on the first row
		// // The first object in the json is the default selection of field
		// // The first object in the operators key of the default json is the default operator
		// $scope.advancedFilterRows.push({
		// 	"field": $scope.fieldOptions[0]['value'],
		// 	"operator": $scope.operatorOptions[0][0]['value'],
		// });
	});
});

peopleApp.controller('reportController', function($scope, $http){
	$controller('reusableIndexReportController', {$scope: $scope});
});

peopleApp.controller('indexController', function($scope, $http, $rootScope, $controller){
	// "url" and "total" variables came from the resolve on the routing template, 
	// which came from the returned key,value pairs in the rankFactory
	// $controller('modalController', {$scope: $scope});
	$controller('itemListController', {$scope: $scope});
	$controller('reusableIndexReportController', {$scope: $scope});
	$scope.mainAPI = '/api/people/v1/bio/';
	$scope.apiGetItems = $rootScope.domainName+$scope.mainAPI;
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/bio/update/';
	$scope.apiDelete = $rootScope.domainName+'/api/people/v1/bio/delete/';
	$scope.searchFilters = [{
		value: null,
		label: 'All'
	},{
		value: 'user_code',
		label: 'Code'
	},{
		value: 'auth_user__first_name',
		label: 'First Name'
	}, {
		value: 'middle_name',
		label: 'Middle Name'
	}, {
		value: 'auth_user__last_name',
		label: 'Last Name'
	}];
	// to get the tableHeadersLength
	$scope.tableHeaders = [
		{'name': 'Code'},
		{'name': 'Name'},
		{'name': 'Rank', 'url': $scope.mainAPI, 'field':'-last_rank', 'class':'caret-reversed initial-reversed', 'id':'last_rank'},
		{'name': 'Status', 'url': $scope.mainAPI, 'field':'-bio_status', 'id':'bio_status', 'class':'caret-reversed initial-reversed', 'id':'bio_status'},
		// {'name': 'Yrs on Service', 'url': $scope.mainAPI, 'field':'-updated_by__bio__user_code', 'class':'caret-reversed initial-reversed', 'id':'rank_updated_by'},
		{'name': 'Updated By', 'url': $scope.mainAPI, 'field':'-updated_by__bio__user_code', 'class':'caret-reversed initial-reversed', 'id':'bio_updated_by'},
		{'name': 'Date Updated', 'url': $scope.mainAPI, 'field':'-date_updated', 'class':'caret-reversed initial-reversed', 'id':'bio_date_updated'},
	];
	$scope.searchItems = [];
	// Used for td colspan when results not found
	$scope.tableHeadersLength = $scope.tableHeaders.length + 2;

	$rootScope.totalPage = Math.ceil(total/$rootScope.limit);
	$scope.originalTotalPage = $rootScope.totalPage;
	// $rootScope.totalPage = 1;
	// $scope.originalTotalPage = 1;

	$rootScope.loadPage = function(){
		// url came from rankFactory
		if(total){
			$scope.getPage(url);
		}else{
			$scope.noRecord = true;
		}
	}

	// Default fields that can be searched
	$scope.Items = ['user_code', 'first_name', 'middle_name', 'last_name'];

	$scope.prototypeItems = [{
		'code':'JDGA',
		'name':'Jacob, Dexter Grafilo',
		'rank':'CA',
		'status':'Onboard',
		'years':10,
		'updated_by':'OMMA',
		'date_updated':'YYYY-MM-DD HH:MM'
	},{
		'code':'SECA',
		'name':'Surilla, Eric Caballegan',
		'rank':'EO',
		'status':'Waiting',
		'years':15,
		'updated_by':'NJBA',
		'date_updated':'YYYY-MM-DD HH:MM'
	},{
		'code':'SRMA',
		'name':'Soriano, Rogelio Jr. Medenilla',
		'rank':'SS',
		'status':'Separated',
		'years':3,
		'updated_by':'DMCB',
		'date_updated':'YYYY-MM-DD HH:MM'
	}];

	$http.post(domainName+url)
	.then(function(response){
		data = response.data.data;
		data.map(function(obj){ 
			for(x=0;x<$scope.Items.length;x++){
				$scope.searchItems.push(obj[$scope.Items[x]]); 
			}
		});
	});
	
	$scope.preSorts = angular.copy(order_by); // angular.copy is used to avoid binding of array 
	$scope.sorts = angular.copy(order_by); // angular.copy is used to avoid binding of array
	// $scope.preSorts = "";
	// $scope.sorts = "";
    $rootScope.loadPage();
});

peopleApp.controller('reusableComponents', function($scope, $rootScope, $http, $location, $anchorScroll, $routeParams){

	// used to store all the array converted from their json object
	$scope.convertedArrays = {};
	// $scope.originalEducationalBackgrounds = [{},{}];
  	$scope.originalBioEducations = [{}];
	$scope.bioEducations = angular.copy($scope.originalBioEducations);

	$scope.originalBioMobiles = [{},{}];
	$scope.bioMobiles = angular.copy($scope.originalBioMobiles);

	$scope.originalBioSocialMedia = [{},{}];
	$scope.bioSocialMedia = angular.copy($scope.originalBioSocialMedia);

	$scope.originalBioAddresses = [{},{}];
	$scope.bioAddresses = angular.copy($scope.originalBioAddresses);

	$scope.rankOptions = [];
	$scope.companyOptions = [];
	$scope.employeeTypeOptions = [];
	$scope.employeeTypes = {}; // Used to do conditionals on divs
	$scope.employmentTypeOptions = [];
	$scope.titleOptions = [];
	$scope.bioStatusOptions = [];
	$scope.separationTypeOptions = [];
	$scope.nameSuffixOptions = [];
	$scope.nationalityOptions = [];
	$scope.bloodTypeOptions = [];
	$scope.religionOptions = [];
	$scope.civilStatusOptions = [];
	$scope.taxStatusOptions = [];
	$scope.areaCodeOptions = [];
	$scope.mobilePrefixNetworkOptions = [];
	$scope.regionOptions = [];
	$scope.provinceOptions = [];
	$scope.hobbyOptions = [];
	$scope.languageOptions = [];
	$scope.relationshipTypesOptions = [];
	$scope.socialMediaOptions = [];
	$scope.hobbyItems = [];
	$scope.languageItems = [];
	$scope.dialectItems = [];
	$scope.genderRelationshipType = {};
	$scope.mobileNetworkType = {};
	$scope.filter = {}; // Used to store all the dynamic filters (Used in addresses)

	$scope.schools = schools;
	$scope.degrees = degrees;
	url = "/api/people/v1/bio/";
	uid_endpoint = "/api/people/v1/uid_range/";
	$scope.lazyLoadReferrers = function(){
		$scope.referrers = [];
		$http.post(domainName+url).then(function(response){
			$scope.referrers = response.data.data;
			console.log(data);
		});
	}

	$scope.manila = manila;
	$scope.married = married;
	// Gets the id of the "Philippines" in the country selection
 	// To make the philippines the default selected option 
 	$scope.philippines = philippines;
 	// Used to determine if the baranggay is a selectbox or a textfield
 	// TODO: Optimize and remove
	$scope.ncr = ncr;
	$scope.wife = wife;
	$scope.husband = husband;
	$scope.mother = mother;
	$scope.father = father;
	$scope.new_crew = new_crew;
	$scope.seafarer = seafarer;
	$scope.staff = staff;
	$scope.other_prefix = other_prefix;


	$scope.uidGenerator = function(){
		if($scope.input.current_company){ // Conditional is used to make sure that this only runs when a comapny is selected
			if($scope.input.nationality == $scope.philippines){
				is_foreigner = 0;
			}else{
				is_foreigner = 1;
			}
			uid_json = {};
			uid_json["filters"] = {};
			if(!is_foreigner){
				uid_json["filters"]["1|group_ids__contains"] = $scope.input.current_company.toString();
			}
			uid_json["filters"]["2&|is_foreigner"] = is_foreigner;
			$http.post(domainName+uid_endpoint, uid_json).then(function(response){
				x = response.data.data[0].is_hellespont_group;
				// If hellespont group, UID will be unchangeable
				uid = response.data.data[0].next_uid;
				if($scope.title == "UPDATE"){ // In update there are a lot of twist before changing the UID
					// If the company changed is reverted back to its original then the original user-id will return on the user_id input
					// If the current company is on hellespont group then the user_id input will always remain the same
					if(($scope.original_current_company == $scope.input.current_company) || $scope.original_current_company_is_hellespont_group){
						$scope.input.user_id = $scope.original_user_id;
					}else{
						$scope.input.user_id = uid;
					}
				}else{
					$scope.input.user_id = uid;
				}
			});
		}
	}

	// START PREPOPULATES
		// Prepopulates the Rank selection
		_ranks = [ ranks_complete, $scope.rankOptions, 'rank_code', 'rank_description' ];
		// Prepopulates the Principals selection
		_principals = [ principals_complete, $scope.companyOptions, 'principal_code', 'principal_full' ];
		// Prepopulates the Bio Status selection
		_bio_status = [ bio_statuses, $scope.bioStatusOptions, 'bio_status_code', 'bio_status_name' ];
		// Prepopulates the Separation Type selection
		_separation_types = [ separation_types, $scope.separationTypeOptions, 'separation_type_code', 'separation_type_name' ];
		// Prepopulates the Area code selection
		_area_codes = [ area_codes, $scope.areaCodeOptions, 'code', 'area_name' ];
		// Prepopulates the Civil Status selection
		// _civil_statuses = [ civil_statuses, $scope.civilStatusOptions, 'civil_status_code', 'civil_status_name' ];
		prepopulatesArrayWithCode = [_ranks, _principals, _bio_status, _separation_types, _area_codes];
		for(y=0;y<prepopulatesArrayWithCode.length;y++){
			for( x in prepopulatesArrayWithCode[y][0] ){
				prepopulatesArrayWithCode[y][1].push({
		    		value: prepopulatesArrayWithCode[y][0][x]['id'],
		    		label: prepopulatesArrayWithCode[y][0][x][prepopulatesArrayWithCode[y][2]]+" - "+prepopulatesArrayWithCode[y][0][x][prepopulatesArrayWithCode[y][3]],
		    	});
			}
		}

		// Prepopulates the Civil Status selection
		for( x in civil_statuses ){
	    	$scope.civilStatusOptions.push({
	    		value: civil_statuses[x]['id'],
	    		label: civil_statuses[x]['civil_status_code']+" - "+civil_statuses[x]['civil_status_name'],
	    	});
	    };
		// Prepopulates the Employee Type selection
	    for( x in employee_types ){
	    	$scope.employeeTypeOptions.push({
	    		value: employee_types[x]['id'],
	    		label: employee_types[x]['employee_type'],
	    	});
	    	$scope.employeeTypes[employee_types[x]['id']] = employee_types[x]['employee_type'];
	    };
	    // Prepopulates the Employment Type selection
	    for( x in employment_types ){
	    	$scope.employmentTypeOptions.push({
	    		value: employment_types[x]['id'],
	    		label: employment_types[x]['employment_type'],
	    	});
	    };
	    // Prepopulates the Title selection
	    for( x in titles ){
	    	$scope.titleOptions.push({
	    		value: titles[x]['id'],
	    		label: titles[x]['title'],
	    	});
	    };
	    // Prepopulates the Name Suffixes selection
	    for( x in name_suffixes ){
	    	$scope.nameSuffixOptions.push({
	    		value: name_suffixes[x]['id'],
	    		label: name_suffixes[x]['suffix'],
	    	});
	    };
	    // Prepopulates the Countrt selection
	    for( x in countries ){
	    	$scope.nationalityOptions.push({
	    		value: countries[x]['id'],
	    		label: countries[x]['country_name'],
	    	});
	    };
	    // Prepopulates the Blood Type selection
	    for( x in blood_types ){
	    	$scope.bloodTypeOptions.push({
	    		value: blood_types[x]['id'],
	    		label: blood_types[x]['blood_type'],
	    	});
	    };
	    // Prepopulates the Religion selection
	    for( x in religions ){
	    	$scope.religionOptions.push({
	    		value: religions[x]['id'],
	    		label: religions[x]['religion'],
	    	});
	    };
	    
	    // Prepopulates the Tax Status selection
	    for( x in tax_statuses ){
	    	$scope.taxStatusOptions.push({
	    		value: tax_statuses[x]['id'],
	    		label: tax_statuses[x]['tax_name'],
	    	});
	    };
	    // Prepopulates the Mobile Network Prefixes selection
	    for( x in mobile_network_prefixes ){
	    	$scope.mobilePrefixNetworkOptions.push({
	    		value: mobile_network_prefixes[x]['id'],
	    		label: mobile_network_prefixes[x]['prefix'],
	    	});
	    	// mobileNetworkType is used for determining the network
	    	$scope.mobileNetworkType[mobile_network_prefixes[x]['id']] = mobile_network_prefixes[x]['network']['label'];
	    };
	    // Prepopulates the Region selection
	    for( x in regions ){
	    	$scope.regionOptions.push({
	    		value: regions[x]['id'],
	    		label: regions[x]['region'],
	    	});
	    };
	    // Prepopulates the Relation selection
	    // Creates a json object to determine the gender of a relationship type
	    for( x in relationship_types ){
	    	$scope.relationshipTypesOptions.push({
	    		value: relationship_types[x]['id'],
	    		label: relationship_types[x]['relationship_name'],
	    	});
	    	// genderRelationshipType is used for determining the gender
	    	$scope.genderRelationshipType[relationship_types[x]['id']] = relationship_types[x]['relationship_gender'];
	    };
	    for( x in social_media ){
	    	$scope.socialMediaOptions.push({
	    		value: social_media[x]['id'],
	    		label: social_media[x]['social_media_name'],
	    	});
	    };
	    // Prepopulates the Hobbies selection
	    hobbies.map(function(obj){ 
			$scope.hobbyItems.push({
					"bio_hobby":obj["id"],
					"hobby":obj["hobby"]
				}
			); 
		});
		// Prepopulates the Languages selection
	 	languages.map(function(obj){ 
			// $scope.languageItems.push(obj["language"]);
			$scope.languageItems.push({
					"bio_language":obj["id"],
					"language":obj["language"]
				}
			); 
		});
		$scope.originalRelationTypes = [{
			'related_people_relationship_type': $scope.father, 'related_people_tel_area_code': $scope.manila, 'related_people_tel_area_code_2': $scope.manila, 'related_people_is_dependent': 'N', 'related_people_is_beneficiary': 'N', 'related_people_is_next_of_kin': 'N', 'related_people_is_emergency': 'N'
		},{
			'related_people_relationship_type': $scope.mother, 'related_people_tel_area_code': $scope.manila, 'related_people_tel_area_code_2': $scope.manila, 'related_people_is_dependent': 'N', 'related_people_is_beneficiary': 'N', 'related_people_is_next_of_kin': 'N', 'related_people_is_emergency': 'N'
		}];
		$scope.relationTypes = angular.copy($scope.originalRelationTypes);

		$scope.originalBioTelephones = [{ 'bio_tel_area_code': $scope.manila },{ 'bio_tel_area_code': $scope.manila }];
		$scope.bioTelephones = angular.copy($scope.originalBioTelephones);
    // END PREPOPULATES

	// Ex: alert( tagsJSONToArray([{'text':'dean'}, {'text':'armada',}, {'text':'guinto'}]));
	$scope.tagsJSONToArray = function (obj, model){
		try{
			objLength = obj.length;
			array = [];
			for(x=0;x<objLength;x++){
				array.push(obj[x][model]);
			}
			$scope.convertedArrays[model] = array
		}catch(err){
			// DO NOTHING
		}
	};
	$scope.languageRequired = function(form){
		// Try-Except is used because input.languages returns an error if languages is not yet a key in the input object
		try{
			if($scope.convertedArrays['language'].length > 0){
				form.languages.$setValidity("required", true);
				// return false;
			}else{
				form.languages.$setValidity("required", false);
				// return true;
			}
		}catch(err){
			form.languages.$setValidity("required", false);
			// return true;
		}	
	};
	$scope.dialectRequired = function(form){
		// Try-Except is used because input.languages returns an error if languages is not yet a key in the convertedArrays object
		try{
			form.dialects.$setValidity("required", true);
			arrayLanguages = $scope.convertedArrays['language'];
			if(arrayLanguages.length > 0){
				x = arrayLanguages.indexOf('Filipino');
				if(x != -1){
					form.dialects.$setValidity("required", false);
					try{
						arrayDialects = $scope.convertedArrays['dialect'];
						if(arrayDialects.length > 0){
							form.dialects.$setValidity("required", true);
						}
					}catch(err){
						// DO NOTHING
					}
					return true;
				}
			}
		}catch(err){
			form.dialects.$setValidity("required", true);
		}
	};
	// $scope.uniqueCheck = function(form , event, validity){
 //    	_this = angular.element(event.target);
 //    	// value = _this.val().toUpperCase();
 //    	value = _this.val();
 //    	// Be aware of the length check value
 //    	if ((value.length == 4 ) && !validity){
 //    		json = {};
 //    		json['filters'] = {};
 //    		json['filters']['user_code__iexact'] = value;
 //    		$http.post(domainName+"/api/people/v1/bio/", json)
	// 		.then(function(response){
	// 			total = response.data.total;
	// 			if(total){
	// 				form.user_code.$setValidity("unique", false);
	// 			}else{
	// 				form.user_code.$setValidity("unique", true);
	// 			}
	// 		});
 //    	}else{
 //    		form.user_code.$setValidity("unique", true);
 //    	}
 //    };

	$scope.anchorTagScroll = function(id) {
      // call $anchorScroll()
      // This method is used to avoid adding more hashes in the url address bar
      $anchorScroll(id);
      // }
    };

    $scope.load_items = function(query, parent_object, key) { 
     	query = query.toLowerCase(); 
     	var result = []; 
     	angular.forEach(parent_object, function(item, index){
     		if(item[key].toLowerCase().indexOf(query) != -1){ 
     			result.push(item); 
     		} 
     	});
     	return result; 
    };
    $scope.bioHobbyAdded = function(tag){
    	if(tag['bio_hobby']){
    		$scope.input['bio_hobbies'].push({'bio_hobby': tag['bio_hobby']});
    	}else{
    		$scope.input['bio_hobbies'].push({'bio_hobby': null, 'hobby':tag['hobby']});
    	}
    };
    $scope.bioLanguageAdded = function(tag){
    	url = "/api/people/v1/dialects/";
    	json = {};
		json['filters'] = {};
		if(tag['bio_language']){
			// $scope.dialectItems = [];
			json['filters']['language'] = tag['bio_language'];
			$http.post(domainName+url, json).then(function(response){
				data = response.data.data;
				console.log(data);
				total = response.data.total;
				if(total){
					data.map(function(obj){ 
						$scope.dialectItems.push({
								"language": obj["language"],
								"bio_lang_dialect_dialect": obj["id"],
								"dialect": obj["dialect"]
							}
						); 
					});
				}
				$scope.input['bio_languages'].push({'bio_language': tag['bio_language'], 'bio_dialects': []});
			});
		}else{
			$scope.input['bio_languages'].push({'bio_language': null, 'language': tag['language'], 'bio_dialects': []});
		}
    };
    $scope.bioDialectAdded = function(tag){
    	url = "/api/people/v1/dialects/";
    	json = {};
		json['filters'] = {};
		json['filters']['id'] = tag['bio_lang_dialect_dialect'];
		$http.post(domainName+url, json).then(function(response){
			data = response.data.data[0];
			total = response.data.total;
			languageId = data['language'];
			if(total){
				$scope.input['bio_languages'].map(function(obj){
					if(obj['bio_language'] == languageId){
						obj['bio_dialects'].push({ "bio_lang_dialect_dialect" : data['id'] })
					}
				});
			}
		});
    };
    
    
    // triggers when a tag is removed in the hobbies
    $scope.tagHobbyRemove = function(tag, proxy_key, tags_key){
    	proxy_object = $scope.input[proxy_key];
    	arrayPos = proxy_object.indexOf(tag);
		if($scope.title == 'ADD'){
			$scope.input[tags_key].splice(arrayPos, 1);
		}else{
			if($scope.originalBioHobbies.length > arrayPos){
				$scope.input[tags_key][arrayPos]["deleted"] = true;
			}else{
				$scope.input[tags_key].splice(arrayPos, 1);
			}	
		}
    };
    // triggers when a tag is removed in the languages field
    $scope.tagLanguageRemove = function(tag, proxy_key, tags_key, language_id_key){
    	proxy_object = $scope.input[proxy_key];
    	arrayPos = proxy_object.indexOf(tag);
		if($scope.title == 'ADD'){
			$scope.input[tags_key].splice(arrayPos, 1);
		}else{
			if($scope.originalBioLanguages.length > arrayPos){
				$scope.input[tags_key][arrayPos]["deleted"] = true;
			}else{
				$scope.input[tags_key].splice(arrayPos, 1);
			}	
		}
		// START script that will remove all the dialects of that certain removed language
		key = tag[language_id_key]
		$scope.dialectItems.map(function(obj){
			if(obj['language'] == key){
				$scope.dialectItems.splice(obj);
			}
		});
		ids = [];
		$scope.input.bio_dialects_proxy.map(function(obj){
			if(obj['language'] == tag["bio_language"]){
				index = $scope.input.bio_dialects_proxy.indexOf(obj);
				ids.push(index);
			}
		});
		ids = ids.sort().reverse(); // reverse is used to ease and avoid wrong splice positions
		while(ids.length){
			$scope.input.bio_dialects_proxy.splice(ids[0], 1);
			ids.splice(0, 1);
		}
		// END script that will remove all the dialects of that certain removed language
    };

    // triggers when a tag is removed in the dialect field
    $scope.bioDialectRemove = function(tag){
    	languageId = tag['language'];
    	dialectId = tag['bio_lang_dialect_dialect'];
    	$scope.input['bio_languages'].map(function(obj){
    		if(obj['bio_language'] == languageId){
    			obj['bio_dialects'].map(function(obj2d){
    				if(obj2d['bio_lang_dialect_dialect'] == dialectId){
    					arrayPos = obj['bio_dialects'].indexOf(obj2d);
    					if($scope.title == 'ADD'){
    						obj['bio_dialects'].splice(arrayPos, 1);
    					}else{
    						if($scope.originalBioDialects.length > arrayPos){
    							
    							obj['bio_dialects'][arrayPos]["deleted"] = true;
    						}else{
    							obj['bio_dialects'].splice(arrayPos, 1);
    						}
    					}
    				}
    			});
    		}
    	});
    };
    // Deletes fields(declared in the deletions array) to ensure value consistencies whenver employee type value changes
    $scope.changeEmployeeType = function(){
    	$scope.bioEducations = angular.copy($scope.originalBioEducations); // Declared to make sure that values from the fields are also deleted
    	// Method to be used when only a part is to be cleared
    	$scope.input["bio_educations"] = $scope.bioEducations; // Variable is used because if the angular.copy is used straight ahead then the value of the key will not be updated upon re-entering
    	deletions =  [ 'bio_status', 'employment_type', 'title', 'company_id_number', 'availability_date', 'last_rank', 'last_vessel' ];
    	for(x=0;x<deletions.length;x++){
    		if($scope.input[deletions[x]]){ delete $scope.input[deletions[x]]; }
    	}
    	// new_crew
    	if($scope.input.employee_type == seafarer){
    		$scope.input.bio_status = new_crew;
    	}
    };
    // functions to determine the gender of a selected relationship type, initially used to conditionally show the maiden fields on the relations section
    $scope.determineGender = function(id){
    	gender = $scope.genderRelationshipType[id];
    	return gender;
    };
    $scope.determineNetwork = function(id){
    	network = $scope.mobileNetworkType[id];
    	return network;
    };
    // Validations came from bioFactory
    // Example of validation expression is [[ validations.rank_code.maxlength ]]
    // Technique is whatever is the submodel of the input that would be the key of the value of this json validation
    $scope.validations = validations;
    // console.log($scope.validations);
    $scope.marriedName = function(model){
    	// model is the relationTypeModel object passed as a parameter
    	firstName = 'related_people_married_first_name';
    	middleName = 'related_people_married_middle_name';
    	if(model['marriedName']){
    		model[firstName] = model['related_people_first_name'];
    		model[middleName] = model['related_people_last_name'];
    	}else{
    		model[firstName] = '';
    		model[middleName] = '';
    	}
    };
    $scope.civilStatusMarriedBool = function(){
    	if($scope.input.civil_status == $scope.married){
    		marriedCounter = 0;
	    	$scope.relationTypes.map(function(obj){
	    		if(obj['related_people_relationship_type'] == $scope.wife || obj['related_people_relationship_type'] == $scope.husband){
	    			marriedCounter++;
	    		}
	    	});
	    	if(!marriedCounter){
	    		if($scope.input.gender == 'M'){
	    			$scope.relationTypes.splice(2, 0, {'related_people_relationship_type': $scope.wife, 'related_people_tel_area_code': $scope.manila, 'related_people_tel_area_code_2': $scope.manila, 'related_people_is_dependent': 'N', 'related_people_is_beneficiary': 'N', 'related_people_is_next_of_kin': 'N', 'related_people_is_emergency': 'N'});
	    		}else{
	    			$scope.relationTypes.splice(2, 0, {'related_people_relationship_type': $scope.husband, 'related_people_tel_area_code': $scope.manila, 'related_people_tel_area_code_2': $scope.manila, 'related_people_is_dependent': 'N', 'related_people_is_beneficiary': 'N', 'related_people_is_next_of_kin': 'N', 'related_people_is_emergency': 'N'});
	    		}
	    	}
    	}
    };
     // params: label, endpoint, request_id(region), filter_key_identifier(field), filter_key_identifier_2d("province"), json_key_identifier("region"), 
    // Used for the dynamic choices that is dependent from also a select choice via a model named "filter"
    // Currently used in province, mobile network prefixes
    $scope.dynamicFilter = function(relation_bool_address, endpoint, json_key_identifier, request_id, filter_key_identifier, filter_key_identifier_2d, label_response_key, index, same){
    	// filter_key_identifier = acts as an additional dimensional key for the filter json object
    	// request_id = acts as the value of model object that will be passed on the endpoint
    	// json_key_identifier = acts as the additional dimensional key identifier in the json object that will be passed to endpoint
    	// filter_key_identifier_2d = acts as an extra additional dimensional key for the filter json object (the if of the model)
    	// label_key_response = acts the key that is defined to parse the label from the json object on the response
    	url = "/api/people/v1/"+endpoint+"/";
    	json = {};
		json['filters'] = {};
		json['filters'][json_key_identifier] = request_id;
		$http.post(domainName+url, json).then(function(response){
			$scope.filter[filter_key_identifier] = {};
			$scope.filter[filter_key_identifier][filter_key_identifier_2d] = response.data.data;
			removes = [];
			for( x in $scope.filter[filter_key_identifier][filter_key_identifier_2d]){
				$scope.filter[filter_key_identifier][filter_key_identifier_2d].push({
		    		value: $scope.filter[filter_key_identifier][filter_key_identifier_2d][x]['id'],
		    		label: $scope.filter[filter_key_identifier][filter_key_identifier_2d][x][label_response_key],
		    	});
		    	removes.push(x); 
		    	// removes is used to delete the unnecessary objects from the response
		    	// removes is used to make sure that the values and label of the select object are the only ones present on the select filter_key_identifier
			}
			for (x=0;x<removes.length;x++){
		    	$scope.filter[filter_key_identifier][filter_key_identifier_2d].splice(0, 1);
		    }
		});
		if(index > 0){
			if(relation_bool_address){
				json = $scope.relationTypes;
				_index = "related_people_zip";
				index_proxy = "related_people_zip_proxy";
				index_barangay = "related_people_province_barangay";
			}else{
				json = $scope.bioAddresses;
				_index = "bio_fil_zip";
				index_proxy = "bio_fil_zip_proxy";
				index_barangay = "bio_fil_province_barangay";
			}
			if(!same){
				json[index-1][_index] =  '';
				json[index-1][index_proxy] =  '';
				json[index-1][index_barangay] =  '';
			}	
		}
    };
    // zipFilter($index, 'bio_fil_zip', 'bioAddress.bio_fil_zip_proxy, bioAddress.municipality)
    $scope.zipFilter = function(relation_bool, index, municipality_id, barangay_id ){
    	url = "/api/people/v1/zips/";
    	json = {};
		json['filters'] = {};
		if(barangay_id){
			json['filters']["1|barangay"] = barangay_id;
			json['filters']["2&|municipality"] = municipality_id;
		}else{
			json['filters']["municipality"] = municipality_id;
		}
		$http.post(domainName+url, json).then(function(response){
			if(relation_bool){
				json = $scope.relationTypes;
				_index = "related_people_zip";
				index_proxy = "related_people_zip_proxy";
			}else{
				json = $scope.bioAddresses;
				_index = "bio_fil_zip";
				index_proxy = "bio_fil_zip_proxy";
			}
			json[index][_index] =  response.data.data[0].id;
			json[index][index_proxy] =  response.data.data[0].zip;
		});
    };
    $scope.formGroupValidationChecker = function(json){
		emergencyCounter = 0;
    	kinCounter = 0;
		json.map(function(obj){
    		emergency = obj['related_people_is_emergency']; // The bool value of the is_emergency (Y or N)
    		kin = obj['related_people_is_next_of_kin']; // The bool value of the is_next_of_kin (Y or N)
    		if(emergency == 'Y'){
    			emergencyCounter++;
    		}
    		if(kin == 'Y'){
    			kinCounter++;
    		}
    	});
    	if(!emergencyCounter || !kinCounter){
    		$scope.relationGroupValidation = true;
		}else{
			$scope.relationGroupValidation = false;
		}
	}

    $scope.uniqueCheck = function(form , event, field){
    	_this = angular.element(event.target);
    	// value = _this.val().toUpperCase();
    	value = _this.val();
    	name = _this.attr("name");
    	_id =  $routeParams.id;
    	// Be aware of the length check value
    	// if ((value.length == 2 || value.length == 3) && !validity){
    		json = {};
    		json['filters'] = {};
    		if(field == 'email'){
    			json['filters']['1|email_address'] = value;
    			json['filters']['2&|auth_user__email'] = value;
    			if(_id){
    				json['filters']["3&~|id"] = _id;
    			}
    		}else if(field == 'uid'){
    			json['filters']['1|user_id'] = value;
    			if(_id){
    				json['filters']["3&~|id"] = _id;
    			}
    		}
    		// console.log(json);
    		$http.post(domainName+"/api/people/v1/bio/", json)
			.then(function(response){
				// console.log(response);
				total = response.data.total;
				if(total){
					// alert("daemn");
					form[name].$setValidity("unique", false);
				}else{
					// alert("armada");
					form[name].$setValidity("unique", true);
				}
			});
    	// }else{
    		form[name].$setValidity("unique", true);
    	// }
    };

    $scope.prefixTypeChange = function(old_val, obj, obj_index){
    	if(old_val == $scope.other_prefix){
    		obj[obj_index] = undefined;
    	}
    };
    $scope.emptyTelephoneChange = function(val, obj, obj_index){
    	if(!val){
    		obj[obj_index] = null;
    	}
    };
    $scope.addRelationNestedModels = function(json, more){
    	json.push({'related_people_is_dependent': 'N', 'related_people_is_beneficiary': 'N', 'related_people_is_next_of_kin': 'N', 'related_people_is_emergency': 'N'});
    };
    $scope.sameAsAbove = function(this_model, primary, receiver, region_id_attr, province_id_attr, municipality_id_attr, related_people){
		// this_model is the value of the checkbox
		// primary is the permanent address model
		// receiver can be the current address model or other relation addresses model
		// region_id_attr is the tag id that will be trigger change
		// related_people is a boolean that if true it indicates that it will return for the related people key
		if(related_people){
			indexAddress = 'related_people_address';
			index_barangay = 'related_people_province_barangay';
			index_zip_proxy = 'related_people_zip_proxy';
			index_zip = 'related_people_zip';
		}else{
			indexAddress = 'bio_fil_address';
			index_barangay = 'bio_fil_province_barangay';
			index_zip_proxy = 'bio_fil_zip_proxy';
			index_zip = 'bio_fil_zip';
		}

		console.log("_------");
		console.log(this_model);

		if(this_model){
			receiver[indexAddress] = primary['bio_fil_address'];
			receiver['region'] = primary['region'];
			receiver['province'] = primary['province'];
			receiver['municipality'] = primary['municipality'];
			receiver[index_barangay] = primary['bio_fil_province_barangay'];
			receiver[index_zip_proxy] = primary['bio_fil_zip_proxy'];
			receiver[index_zip] = primary['bio_fil_zip'];
			receiver['addressReadonly'] = true;
			receiver['regionReadonly'] = true;
			receiver['provinceReadonly'] = true;
			receiver['cityReadonly'] = true;
			receiver['barangayReadonly'] = true;
		}else{
			receiver[indexAddress] = '';
			receiver['region'] = '';
			receiver['province'] = '';
			receiver['municipality'] = '';
			receiver[index_barangay] = '';
			receiver[index_zip_proxy] = '';
			receiver[index_zip] = '';
			receiver['addressReadonly'] = false;
			receiver['regionReadonly'] = false;
			receiver['provinceReadonly'] = false;
			receiver['cityReadonly'] = false;
			receiver['barangayReadonly'] = false;
		}
		// $scope.watch is used to trigger the ng-change
		$scope.$watch(receiver, function(newValue, oldValue){
			$scope.dynamicFilter(related_people, 'provinces', 'region', receiver['region'], region_id_attr, 'province', 'province', related_people, 1);
			$scope.dynamicFilter(related_people, 'municipalities', 'province', receiver['province'], province_id_attr, 'municipality', 'municipality', related_people, 1);
			$scope.dynamicFilter(related_people, 'barangays', 'municipality', receiver['municipality'], municipality_id_attr, 'barangay', 'barangay', related_people, 1)
	    });
	};
});

peopleApp.controller('addController', function($scope, $controller, $rootScope, $http, $timeout, Upload){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiAdd = $rootScope.domainName+'/api/people/v1/bio/add/';
	$scope.title = "ADD";
	// $scope.originalInput = {"user_id":'00001', 'employee_type':$scope.employeeTypeOptions[0]['value'], "bio_educations": $scope.bioEducations};
	// The code above does not work
	// $scope.input = {"user_id":new_bio_id, 'employee_type': seafarer, 'bio_status': new_crew, 'gender':'M'}; ----- This is commented because user_id has a new automated suggestion process
	$scope.input = {'employee_type': seafarer, 'bio_status': new_crew, 'gender':'M'};
	$scope.input["bio_educations"] = $scope.bioEducations;
	$scope.input["bio_telephones"] = $scope.bioTelephones;
	$scope.input["bio_mobiles"] = $scope.bioMobiles;
	$scope.input["bio_social_medias"] = $scope.bioSocialMedia;
	$scope.input["bio_address"] = $scope.bioAddresses;
	$scope.input["bio_hobbies"] = [];
	$scope.input["bio_languages"] = [];
	$scope.input["bio_related_people"] = $scope.relationTypes;
	$scope.input["nationality"] = $scope.philippines;
    $scope.code_readonly = false;

    $scope.removeNestedModels = function(json, inputs, multiple_inputs){
    	// typeof condition included to avoid conflict with update's originalObj
    	if(multiple_inputs && typeof(multiple_inputs) == "number"){
    		json.splice(inputs, multiple_inputs);
    	}else{
    		json.splice(inputs, 1);
    	}
    };
    // default Deceased Group Value
    // Turnes all the Relaion Group Value to null if true
    $scope.dDGV = function(this_model){
    	if(this_model['related_people_is_deceased']){
   			this_model['related_people_is_dependent'] = 'N';
			this_model['related_people_is_beneficiary'] = 'N';
			this_model['related_people_is_next_of_kin'] = 'N';
			this_model['related_people_is_emergency'] = 'N';
			delete this_model['related_people_tel_area_code'];
			delete this_model['related_people_tel_number'];
			delete this_model['related_people_tel_area_code_2'];
			delete this_model['related_people_tel_number_2'];
			delete this_model['related_people_mobile_prefix'];
			delete this_model['related_people_mobile_number'];
			delete this_model['related_people_mobile_prefix_2'];
			delete this_model['related_people_mobile_number_2'];
    	}
    };
	
    $scope.bioSaveInput = function(formStatus, json, url, file){
    	console.log(angular.copy(json));
		$scope.formGroupValidationChecker(json['bio_related_people']);
    	if(formStatus || $scope.relationGroupValidation){
    		// console.log(formStatus);
    		// console.log($scope.relationGroupValidation);
			$rootScope.notifications = true;
			angular.element("html, body").animate({scrollTop: 0}, "slow");
		}else{
	    	json = angular.copy(json); // To remove the $$hashkey from ng-repeat
	    	json['bio_telephones'] = jsonEmptyRemover(json['bio_telephones']);
	    	json['bio_mobiles'] = jsonEmptyRemover(json['bio_mobiles']);
	    	json['bio_social_medias'] = jsonEmptyRemover(json['bio_social_medias']);
	    	json['bio_educations'] = jsonEmptyRemover(json['bio_educations']);
	    	json['bio_educations'].forEach(function(entry){
				// This will put the education_type key if there are values that are input
				if(Object.keys(entry).length > 1){
					entry.bio_education_type = "others";
				}
			});
	    	console.log(json);
	    	console.log("-----");
			json['updated_by'] = $rootScope.SSid;
			if(typeof(file) == "object"){
				file.upload = Upload.upload({
			      url: url,
			      data: {
			      		"data":JSON.stringify(json),
			      		"photo": file,
			      	},
			    });
			    file.upload.then(function (response) {
			      	$rootScope.notifications = false;
			      	$scope.showModal = false;
					window.location.href = '#/';
			    }, function (response) {
			      	alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
			    });
			}else{
				_json = {}
				_json['data'] = {};
				_json['data'] = JSON.stringify(json);
				$http.post(url, _json)
				.then(function(response){
					$rootScope.notifications = false;
					window.location.href = '#/';
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}
		}
    };
    $scope.uiSelectClear = function(index, json){
    	if(json){
    		delete json[index];
    	}else{
    		delete $scope.input[index];
    	}
    };
    $scope.separationDate = function(){
    	if(!$scope.input.separation_date){
    		delete $scope.input.separation_date;
    		delete $scope.input.separation_type;
    		delete $scope.input.separation_comments;
    	}
    };
    $scope.validations = validations;
});

peopleApp.controller('updateController', function($scope, $rootScope, $routeParams, $http, $controller, $timeout, Upload){
	$controller('reusableComponents', {$scope: $scope});
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/bio/update/';
	$scope.title = "UPDATE";
	id = parseInt($routeParams.id);

	$scope.lazyLoadReferrers();
	// Disables the lazy load to remove the delay on the selection of lazy load referrers
	$scope.lazyLoadReferrers = function(){
		return false;
	};
	$http.post($rootScope.domainName+'/api/people/v1/bio/detail/', { 'id':id })
	.then(function(response){
		data = response.data.data[0];
		console.log(data);
		data.bio_related_people.reverse();
		$scope.bioAddresses = [];
		$scope.bioTelephones = [];
		$scope.bioMobiles = [];
		$scope.bioSocialMedia = [];
		$scope.bioEducations = [];
		$scope.relationTypes = [];
		for(x=0;x<data.bio_address.length;x++){
			$scope.bioAddresses.push({});
			// To prefill the values of the provinces
			$scope.dynamicFilter(0 , 'provinces', 'region', data.bio_address[x].bio_fil_zip.region_id, 'region'+x, 'province', 'province', x+1);
			// To prefill the values of municipalities
			$scope.dynamicFilter(0 , 'municipalities', 'province', data.bio_address[x].bio_fil_zip.province_id, 'province'+x, 'municipality', 'municipality', x+1);
						
			if(data.bio_address[x].bio_fil_zip.barangay_id){
				// To prefill the values of NCR barangays
				$scope.dynamicFilter(0 , 'barangays', 'municipality', data.bio_address[x].bio_fil_zip.municipality_id, 'municipality'+x, 'barangay', 'barangay', x+1)
				barangay = data.bio_address[x].bio_fil_zip.barangay_id;
			}else{
				barangay = data.bio_address[x].bio_fil_province_barangay;
			}
			$scope.bioAddresses[x] = {
				"_id": data.bio_address[x].id,
				'bio_fil_address': data.bio_address[x].bio_fil_address,
				'region': data.bio_address[x].bio_fil_zip.region_id,
				'province': data.bio_address[x].bio_fil_zip.province_id,
				'municipality': data.bio_address[x].bio_fil_zip.municipality_id,
				'bio_fil_province_barangay': barangay,
				'bio_fil_zip_proxy': data.bio_address[x].bio_fil_zip.zip_code,
				"bio_fil_zip": data.bio_address[x].bio_fil_zip.zip_id,
				"bio_fil_address_type": data.bio_address[x].bio_fil_address_type,
			};
		}
		permanent = angular.copy($scope.bioAddresses[0]);
		delete permanent["_id"];
		delete permanent["bio_fil_address_type"];
		current = angular.copy($scope.bioAddresses[1]);
		delete current["_id"];
		delete current["bio_fil_address_type"];
		bool = JSON.stringify(permanent) === JSON.stringify(current); // Determines if both permanent and current are the same
		if(bool){
			$timeout(function() {
	        	angular.element("#same-as-above-permanent").trigger("click"); 
	    	}, 1000);
		}
		
		$scope.originalBioAddresses = angular.copy($scope.bioAddresses);
		for(x=0;x<data.bio_telephones.length;x++){
			$scope.bioTelephones.push({});
			$scope.bioTelephones[x] = {
				"_id": data.bio_telephones[x].id,
				'bio_tel_number': data.bio_telephones[x].bio_tel_number,
				'bio_tel_area_code': data.bio_telephones[x].bio_tel_area_code.id,
			};
		}
		$scope.originalBioTelephones = angular.copy($scope.bioTelephones);
		for(x=0;x<data.bio_mobiles.length;x++){
			$scope.bioMobiles.push({});
			$scope.bioMobiles[x] = {
				"_id": data.bio_mobiles[x].id,
				'bio_mobile_number': data.bio_mobiles[x].bio_mobile_number,
				'bio_mobile_prefix': data.bio_mobiles[x].bio_mobile_prefix.id,
			};
		}
		$scope.originalBioMobiles = angular.copy($scope.bioMobiles);
		for(x=0;x<data.bio_social_medias.length;x++){
			$scope.bioSocialMedia.push({});
			$scope.bioSocialMedia[x] = {
				"_id": data.bio_social_medias[x].id,
				'bio_social_id': data.bio_social_medias[x].bio_social_id,
				'bio_social_media': data.bio_social_medias[x].bio_social_media.id,
			};
		}
		$scope.originalBioSocialMedia = angular.copy($scope.bioSocialMedia);
		for(x=0;x<data.bio_educations.length;x++){
			$scope.bioEducations.push({});
			$scope.bioEducations[x] = {
				"_id": data.bio_educations[x].id,
				'bio_education_date_completed': data.bio_educations[x].bio_education_date_completed,
				'bio_education_degree': data.bio_educations[x].bio_education_degree.id,
				'bio_education_school': data.bio_educations[x].bio_education_school.id,
			};
		}
		if(!$scope.bioEducations.length){
			$scope.bioEducations.push({});
		}
		$scope.originalBioEducations = angular.copy($scope.bioEducations);

		// Source: http://stackoverflow.com/questions/6211613/testing-whether-a-value-is-odd-or-even
		// Makes Sure that the fields that are displayed is even
		loopEvenlyDisplayed($scope.bioTelephones);
		loopEvenlyDisplayed($scope.bioMobiles);
		loopEvenlyDisplayed($scope.bioSocialMedia);
		relatedSameIds = []; // This is where all the ids with same as permanent will be stored
		relatedDeceased = []; // This is where all the ids with deceased will be stored
		relatedWorking = []; // This is where all the ids with working will be stored
		relatedStudying = []; // This is where all the ids with studying will be stored
		relatedMarriedName = [];
		for(x=0;x<data.bio_related_people.length;x++){
			if(data.bio_related_people[x].related_people_relationship_type){
				$scope.relationTypes.push({});
				// To prefill the values of the provinces
				$scope.dynamicFilter(1 , 'provinces', 'region', data.bio_related_people[x].related_people_zip.region_id, 'related_region'+x, 'province', 'province', x+1);
				// To prefill the values of municipalities
				$scope.dynamicFilter(1 , 'municipalities', 'province', data.bio_related_people[x].related_people_zip.province_id, 'related_province'+x, 'municipality', 'municipality', x+1);
				if(data.bio_related_people[x].related_people_zip.barangay_id){
					// To prefill the values of NCR barangays
					$scope.dynamicFilter(1 , 'barangays', 'municipality', data.bio_related_people[x].related_people_zip.municipality_id, 'related_municipality'+x, 'barangay', 'barangay', x+1);
					barangay = data.bio_related_people[x].related_people_zip.barangay_id;
				}else{
					barangay = data.bio_related_people[x].related_people_province_barangay;
				}
				relAddress = {"bio_fil_address":data.bio_related_people[x].related_people_address,"region":data.bio_related_people[x].related_people_zip.region_id,"province":data.bio_related_people[x].related_people_zip.province_id,"municipality":data.bio_related_people[x].related_people_zip.municipality_id,"bio_fil_province_barangay":barangay,"bio_fil_zip_proxy":data.bio_related_people[x].related_people_zip.zip_code,"bio_fil_zip":data.bio_related_people[x].related_people_zip.zip_id};
				bool = JSON.stringify(permanent) === JSON.stringify(relAddress); // Determines if both permanent and current are the same
				if(bool){
					relatedSameIds.push(x);
				}
				if(data.bio_related_people[x].related_people_is_deceased == 'Y'){
					relatedDeceased.push(x);
				}
				if(data.bio_related_people[x].related_people_is_working_label == 'YES'){
					relatedWorking.push(x);
				}
				if(data.bio_related_people[x].related_people_is_student_label == 'YES'){
					relatedStudying.push(x);
				}
				if(data.bio_related_people[x].related_people_married_last_name){
					relatedMarriedName.push(x);
				}
				$scope.relationTypes[x] = {
					"_id": data.bio_related_people[x].id,
					"related_people_first_name": data.bio_related_people[x].related_people_first_name,
					"related_people_middle_name": data.bio_related_people[x].related_people_middle_name,
					"related_people_last_name": data.bio_related_people[x].related_people_last_name,
					"related_people_married_last_name": data.bio_related_people[x].related_people_married_last_name,
					"related_people_married_middle_name": data.bio_related_people[x].related_people_married_middle_name,
					"related_people_marriage_date": data.bio_related_people[x].related_people_marriage_date,
					"related_people_marriage_place": data.bio_related_people[x].related_people_marriage_place,
					"related_people_tel_number": data.bio_related_people[x].related_people_tel_number,
					"related_people_tel_number_2": data.bio_related_people[x].related_people_tel_number_2,
					"related_people_mobile_number": data.bio_related_people[x].related_people_mobile_number,
					"related_people_mobile_number_2": data.bio_related_people[x].related_people_mobile_number_2,
					"related_people_address": data.bio_related_people[x].related_people_address,
					"region": data.bio_related_people[x].related_people_zip.region_id,
					"province": data.bio_related_people[x].related_people_zip.province_id,
					"municipality": data.bio_related_people[x].related_people_zip.municipality_id,
					"related_people_province_barangay": barangay,
					"related_people_zip_proxy": data.bio_related_people[x].related_people_zip.zip_code,
					"related_people_zip": data.bio_related_people[x].related_people_zip.zip_id,
					"related_people_birthdate": data.bio_related_people[x].related_people_birthdate,
					"related_people_birthplace": data.bio_related_people[x].related_people_birthplace,
					"related_people_is_beneficiary": data.bio_related_people[x].related_people_is_beneficiary,
					"related_people_is_dependent": data.bio_related_people[x].related_people_is_dependent,
					"related_people_is_next_of_kin": data.bio_related_people[x].related_people_is_next_of_kin,
					"related_people_is_emergency": data.bio_related_people[x].related_people_is_emergency,
					"related_people_is_working": data.bio_related_people[x].related_people_is_working_label,
					"related_people_is_student": data.bio_related_people[x].related_people_is_student_label,
					"related_people_is_deceased": data.bio_related_people[x].related_people_is_deceased,
					"related_people_mobile_network": data.bio_related_people[x].related_people_mobile_network,
					"related_people_mobile_network_2": data.bio_related_people[x].related_people_mobile_network_2,
					"related_people_occupation": data.bio_related_people[x].related_people_occupation,
					"related_people_name_suffix": data.bio_related_people[x].related_people_name_suffix.id,
					"related_people_tel_area_code": data.bio_related_people[x].related_people_tel_area_code.id,
					"related_people_tel_area_code_2": data.bio_related_people[x].related_people_tel_area_code_2.id,
					"related_people_mobile_prefix": data.bio_related_people[x].related_people_mobile_prefix.id,
					"related_people_mobile_prefix_2": data.bio_related_people[x].related_people_mobile_prefix_2.id,
					"related_people_relationship_type": data.bio_related_people[x].related_people_relationship_type.id,
					"related_people_nationality": data.bio_related_people[x].related_people_nationality.id,
					"relation_group": data.bio_related_people[x].related_people_group,
					"related_people_passport_number": data.bio_related_people[x].related_people_passport_number,
				};
			}
		}
		$timeout(function() {
			for(var x=0;x<relatedSameIds.length;x++){
				angular.element("#same-as-above-permanent-"+relatedSameIds[x]).trigger("click");
			}
			for(var x=0;x<relatedDeceased.length;x++){
				angular.element("#deceased-"+relatedDeceased[x]).trigger("click");
			}
			for(var x=0;x<relatedWorking.length;x++){
				angular.element("#working-"+relatedWorking[x]).trigger("click");
			}
			for(var x=0;x<relatedStudying.length;x++){
				angular.element("#studying-"+relatedStudying[x]).trigger("click");
			}
			for(var x=0;x<relatedMarriedName.length;x++){
				angular.element("#married-name-"+relatedMarriedName[x]).trigger("click");
			}
		}, 1000);
		$scope.originalRelationTypes = angular.copy($scope.relationTypes);
		// START Agreed with Jed to do this in the front-end as this is only needed because of Angular's ng-tags-input's standards
		bio_dialects_proxy = [];
		data.bio_languages.map(function(obj){
			console.log(obj);
			obj.bio_dialects.map(function(_obj){
				// [{"language":1,"bio_lang_dialect_dialect":11,"dialect":"Maguindanao"}]
				bio_dialects_proxy.push({"bio_lang_dialect_dialect":_obj.bio_lang_dialect_dialect, "dialect":_obj.dialect, "language":obj.bio_language});
			});
		});
		// END Agreed with Jed to do this in the front-end as this is only needed because of Angular's ng-tags-input's standards
		$scope.input = {
			'user_id': data.user_id,
			'availability_date': data.availability_date,
			'in_company_since': data.in_company_since,
			'in_company_until': data.in_company_until,
			'company_id_number': data.company_id_number,
			'separation_date': data.separation_date,
			'separation_comments': data.separation_comments,
			'user_code': data.user_code,
			'first_name': data.first_name,
			'middle_name': data.middle_name,
			'last_name': data.last_name,
			'nickname': data.nickname,
			'gender': data.gender,
			'birthdate': data.birthdate,
			'birthplace': data.birthplace,
			'bmi': data.bmi,
			'email_address': data.email_address,
			'email_address_2': data.email_address_2,
			'tax_account_number': data.tax_account_number,
			'sss_number': data.sss_number,
			'pagibig_number': data.pagibig_number,
			'philhealth_number': data.philhealth_number,
			'employee_type': data.employee_type.id,
			'last_rank': data.last_rank.id,
			'bio_status': data.bio_status.id,
			'photo_date_updated': data.photo_date_updated,
			'ex_crew': data.ex_crew,
			'is_blacklisted': data.is_blacklisted,
			'date_updated': data.date_updated,
			'employment_type': data.employment_type.id,
			'title': data.title.id,
			'current_company': data.current_company.id,
			'separation_type': data.separation_type.id,
			'name_suffix': data.name_suffix.id,
			'nationality': data.nationality.id,
			'blood_type': data.blood_type.id,
			'religion': data.religion.id,
			'civil_status': data.civil_status.id,
			'tax_status': data.tax_status.id,
			'referred_by': data.referred_by.id,
			'bio_hobbies_proxy': angular.copy(data.bio_hobbies),
			'bio_hobbies': data.bio_hobbies,
			'bio_languages_proxy': angular.copy(data.bio_languages),
			'bio_languages': data.bio_languages,
			'bio_dialects_proxy': bio_dialects_proxy,
			'dependentCount': data.no_of_dependents,
			'bio_educations' : $scope.bioEducations,
			'bio_telephones' : $scope.bioTelephones,
			'bio_mobiles' : $scope.bioMobiles,
			'bio_social_medias' : $scope.bioSocialMedia,
			'bio_address' : $scope.bioAddresses,
			'bio_related_people' : $scope.relationTypes,
		};
		$scope.original_current_company = angular.copy($scope.input.current_company);
		$scope.original_current_company_is_hellespont_group = data.current_company.is_hellespont_group;
		$scope.original_user_id = angular.copy($scope.input.user_id);
		if(data.photo){ // Conditional used to avoid null value of $scope.photo,, because null causes an error upon updating
			$scope.photo = data.photo;	
		}
		$scope.originalBioHobbies = angular.copy(data.bio_hobbies);
		$scope.originalBioLanguages = angular.copy(data.bio_languages);
		$scope.dialectItems = data.bio_dialects;
		$scope.originalBioDialects = angular.copy(bio_dialects_proxy);
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});

	// $timeout(function() {
 //         angular.element("#same-as-above-permanent").trigger("click"); 
 //    }, 1000);

	$scope.code_readonly = true;
	try {
    	$scope.validations = validations;
	}
	catch(err) {
	   // Catch validations so validations will not send a request when viewing
	}
	$scope.removeNestedModels = function(json, inputs, originalObj, specialCond){
		// specialCond is used for those with multiple inputs
		// what happens is originalObj are integers that wil become the number of occurence of the deletion
		// and specialCond will be the object
		// this occurs on example mobile, telephones and social media
		if(specialCond){
			if(inputs+1 > specialCond.length){
	    		json.splice(inputs, originalObj);
	    	}else{
	    		json["deleted"] = true;
	    		json[inputs]['deleted'] = true;
	    		json[inputs+1]['deleted'] = true;
	    	}
		}else{	
			if( inputs > originalObj.length ){
				json.splice(inputs, 1);
			}else{
				json["deleted"] = true;
				json[inputs]['deleted'] = true;
			}
		}
    };

    $scope.bioUpdateInput = function(formStatus, json, url, file){
    	// alert(json);
		$scope.formGroupValidationChecker(json['bio_related_people']);
    	if(formStatus || $scope.relationGroupValidation){
			$rootScope.notifications = true;
			angular.element("html, body").animate({scrollTop: 0}, "slow");
		}else{
	    	json = angular.copy(json); // To remove the $$hashkey from ng-repeat
	    	json['bio_telephones'] = jsonEmptyRemover(json['bio_telephones']);
	    	json['bio_mobiles'] = jsonEmptyRemover(json['bio_mobiles']);
	    	json['bio_social_medias'] = jsonEmptyRemover(json['bio_social_medias']);
			json['updated_by'] = $rootScope.SSid;
			json['id'] = $routeParams.id;

			// START manually adding _id to those that cannot be
			add_id(json["bio_hobbies"]);
			add_id(json["bio_languages"]);
			json["bio_languages"].map(function(obj){
				obj["bio_dialects"].map(function(_obj){
					_obj["_id"] = _obj["id"] 
				});
			});

			console.log(json["bio_dialects"]);

			if(typeof(file) == "object"){
				file.upload = Upload.upload({
			      url: url,
			      data: {
			      		"data":JSON.stringify(json),
			      		"photo": file,
			      	},
			    });
			    file.upload.then(function (response) {
			      	$rootScope.notifications = false;
			      	$scope.showModal = false;
					window.location.href = '#/';
			    }, function (response) {
			      	alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
			    });
			}else{
				_json = {}
				_json['data'] = {};
				_json['data'] = JSON.stringify(json);
				$http.post(url, _json)
				.then(function(response){
					$rootScope.notifications = false;
					window.location.href = '#/';
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}
		}
    };
    $scope.uiSelectClear = function(index, json){
    	if(json){
    		json[index] = "";
    	}else{
    		$scope.input[index] = "";
    	}
    };
    $scope.separationDate = function(){
    	if(!$scope.input.separation_date){
    		$scope.input.separation_date = null;
    		$scope.input.separation_type = null;
    		$scope.input.separation_comments = null;
    	}
    };
});

peopleApp.controller('viewController', function($scope, $http, $controller, $routeParams, $rootScope){
	// $controller('updateController', {$scope: $scope});
	// $controller('reusableComponents', {$scope: $scope});
	$scope.apiUpdate = $rootScope.domainName+'/api/people/v1/bio/update/';
	$scope.title = "VIEW";
	$scope.viewOnly = true;
	$scope.page = "status";
	$scope.wife = wife;
	$scope.husband = husband;
	$scope.mother = mother;
	$scope.father = father;
	$scope.changeContent = function(page){
  	$scope.page = page;
  }
  id = parseInt($routeParams.id);
  $http.post($rootScope.domainName+'/api/people/v1/bio/detail/', { 'id':id })
	.then(function(response){
		data = response.data.data[0];
		console.log(data);
		$scope.bioAddresses = [];
		$scope.bioTelephones = [];
		$scope.bioMobiles = [];
		$scope.bioSocialMedia = [];
		$scope.bioEducations = [];
		$scope.relationTypes = [];

		for(x=0;x<data.bio_address.length;x++){
			$scope.bioAddresses.push({});
			if(data.bio_address[x].bio_fil_zip.barangay_id){
				barangay = data.bio_address[x].bio_fil_zip.barangay;
			}else{
				barangay = data.bio_address[x].bio_fil_province_barangay;
			}
			$scope.bioAddresses[x] = {
				'bio_fil_address': data.bio_address[x].bio_fil_address,
				'region': data.bio_address[x].bio_fil_zip.region,
				'province': data.bio_address[x].bio_fil_zip.province,
				'municipality': data.bio_address[x].bio_fil_zip.municipality,
				'bio_fil_province_barangay': barangay,
				'bio_fil_zip_proxy': data.bio_address[x].bio_fil_zip.zip_code,
			};
		}


		for(x=0;x<data.bio_telephones.length;x++){
			$scope.bioTelephones.push({});
			$scope.bioTelephones[x] = {
				'bio_tel_number': data.bio_telephones[x].bio_tel_number,
				'bio_tel_area_code': data.bio_telephones[x].bio_tel_area_code,
			};
		}
		for(x=0;x<data.bio_mobiles.length;x++){
			$scope.bioMobiles.push({});
			$scope.bioMobiles[x] = {
				'bio_mobile_number': data.bio_mobiles[x].bio_mobile_number,
				'bio_mobile_network': data.bio_mobiles[x].bio_mobile_network,
				'bio_mobile_prefix': data.bio_mobiles[x].bio_mobile_prefix,
			};
		}
		for(x=0;x<data.bio_social_medias.length;x++){
			$scope.bioSocialMedia.push({});
			$scope.bioSocialMedia[x] = {
				'bio_social_id': data.bio_social_medias[x].bio_social_id,
				'bio_social_media': data.bio_social_medias[x].bio_social_media,
			};
		}
		for(x=0;x<data.bio_educations.length;x++){
			$scope.bioEducations.push({});
			$scope.bioEducations[x] = {
				'bio_education_date_completed': data.bio_educations[x].bio_education_date_completed,
				'bio_education_degree': data.bio_educations[x].bio_education_degree,
				'bio_education_school': data.bio_educations[x].bio_education_school,
			};
		}
		// Source: http://stackoverflow.com/questions/6211613/testing-whether-a-value-is-odd-or-even
		// Makes Sure that the fields that are displayed is even
		loopEvenlyDisplayed($scope.bioTelephones);
		loopEvenlyDisplayed($scope.bioMobiles);
		loopEvenlyDisplayed($scope.bioSocialMedia);
		
		for(x=0;x<data.bio_related_people.length;x++){
			if(data.bio_related_people[x].related_people_relationship_type){
				$scope.relationTypes.push({});
				if(data.bio_related_people[x].related_people_zip.barangay_id){
					barangay = data.bio_related_people[x].related_people_zip.barangay;
				}else{
					barangay = data.bio_related_people[x].related_people_province_barangay;
				}
				$scope.relationTypes[x] = {
					"related_people_first_name": data.bio_related_people[x].related_people_first_name,
					"related_people_middle_name": data.bio_related_people[x].related_people_middle_name,
					"related_people_last_name": data.bio_related_people[x].related_people_last_name,
					"related_people_married_last_name": data.bio_related_people[x].related_people_married_last_name,
					"related_people_married_middle_name": data.bio_related_people[x].related_people_married_middle_name,
					"related_people_marriage_date": data.bio_related_people[x].related_people_marriage_date,
					"related_people_marriage_place": data.bio_related_people[x].related_people_marriage_place,
					"related_people_tel_number": data.bio_related_people[x].related_people_tel_number,
					"related_people_tel_number_2": data.bio_related_people[x].related_people_tel_number_2,
					"related_people_mobile_number": data.bio_related_people[x].related_people_mobile_number,
					"related_people_mobile_number_2": data.bio_related_people[x].related_people_mobile_number_2,
					"related_people_address": data.bio_related_people[x].related_people_address,
					"related_people_birthdate": data.bio_related_people[x].related_people_birthdate,
					"related_people_birthplace": data.bio_related_people[x].related_people_birthplace,
					"related_people_is_beneficiary": data.bio_related_people[x].related_people_is_beneficiary,
					"related_people_is_dependent": data.bio_related_people[x].related_people_is_dependent,
					"related_people_is_next_of_kin": data.bio_related_people[x].related_people_is_next_of_kin,
					"related_people_is_emergency": data.bio_related_people[x].related_people_is_emergency,
					"related_people_is_working": data.bio_related_people[x].related_people_is_working_label,
					"related_people_is_student": data.bio_related_people[x].related_people_is_student_label,
					"related_people_is_deceased": data.bio_related_people[x].related_people_is_deceased,
					"zip_code": data.bio_related_people[x].related_people_zip.zip_code,
					"province": data.bio_related_people[x].related_people_zip.province,
					"barangay": barangay,
					"region": data.bio_related_people[x].related_people_zip.region,
					"municipality": data.bio_related_people[x].related_people_zip.municipality,
					"related_people_mobile_network": data.bio_related_people[x].related_people_mobile_network,
					"related_people_mobile_network_2": data.bio_related_people[x].related_people_mobile_network_2,
					"related_people_occupation": data.bio_related_people[x].related_people_occupation,
					"related_people_name_suffix": data.bio_related_people[x].related_people_name_suffix,
					"related_people_tel_area_code": data.bio_related_people[x].related_people_tel_area_code,
					"related_people_tel_area_code_2": data.bio_related_people[x].related_people_tel_area_code_2,
					"related_people_mobile_prefix": data.bio_related_people[x].related_people_mobile_prefix,
					"related_people_mobile_prefix_2": data.bio_related_people[x].related_people_mobile_prefix_2,
					"related_people_relationship_type": data.bio_related_people[x].related_people_relationship_type,
					"related_people_nationality": data.bio_related_people[x].related_people_nationality,
					"relation_group": data.bio_related_people[x].related_people_group,
					"related_people_passport_number": data.bio_related_people[x].related_people_passport_number,
					"related_people_relationship_type_gender": data.bio_related_people[x].related_people_relationship_type_gender,
				}
			}
		}
		// TAKE NOTE: If you used .label on the select fields some of the ng-if condition will hide the fields like Ranks because of employee Type
		$scope.input = {
			'user_id': data.user_id,
			'availability_date': data.availability_date,
			'in_company_since': data.in_company_since,
			'in_company_until': data.in_company_until,
			'company_id_number': data.company_id_number,
			'separation_date': data.separation_date,
			'separation_comments': data.separation_comments,
			'user_code': data.user_code,
			'first_name': data.first_name,
			'middle_name': data.middle_name,
			'last_name': data.last_name,
			'nickname': data.nickname,
			'gender': data.gender_label,
			'birthdate': data.birthdate,
			'birthplace': data.birthplace,
			'bmi': data.bmi,
			'email_address': data.email_address,
			'email_address_2': data.email_address_2,
			'tax_account_number': data.tax_account_number,
			'sss_number': data.sss_number,
			'pagibig_number': data.pagibig_number,
			'philhealth_number': data.philhealth_number,
			'employee_type': data.employee_type,
			'last_rank': data.last_rank,
			'bio_status': data.bio_status,
			'photo_date_updated': data.photo_date_updated,
			'ex_crew': data.ex_crew,
			'is_blacklisted': data.is_blacklisted,
			'date_updated': data.date_updated,
			'employment_type': data.employment_type,
			'title': data.title,
			'current_company': data.current_company,
			'separation_type': data.separation_type,
			'name_suffix': data.name_suffix,
			'nationality': data.nationality,
			'blood_type': data.blood_type,
			'religion': data.religion,
			'civil_status': data.civil_status,
			'tax_status': data.tax_status,
			'referred_by': data.referred_by,
			'bio_hobbies_proxy': data.hobbies,
			'bio_languages_proxy': data.languages,
			'bio_dialects_proxy': data.dialects,
			'dependentCount': data.no_of_dependents,
		};
		$scope.photo = data.photo;
	},function(response){
		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		alert(JSON.stringify(response.data.errors));
	});
});

peopleApp.controller('modalController', function($scope){
	$scope.toggleModal = function(){
    	$scope.showModal = !$scope.showModal;
    };
});