# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
#django core
from django.db import models

from test_people.models import *

#BIO models
class Bio(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    bio_sys_status = models.CharField(max_length=45, blank=True, null=True)
    auth_user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True,)
    employee_type = models.ForeignKey(EmployeeTypes, on_delete=models.DO_NOTHING, blank=True, null=True,)
    employment_type = models.ForeignKey(EmploymentTypes, on_delete=models.DO_NOTHING, blank=True, null=True,)
    title = models.ForeignKey(Titles, on_delete=models.DO_NOTHING, blank=True, null=True,)
    bio_status = models.ForeignKey('BioStatuses', on_delete=models.DO_NOTHING, db_column='bio_status', related_name='bio_bio_status', null=True, blank=True)
    availability_date = models.DateField(blank=True, null=True)
    current_company = models.ForeignKey('Principals', on_delete=models.DO_NOTHING, blank=True, null=True,)
    in_company_since = models.DateField(blank=True, null=True)
    in_company_until = models.DateField(blank=True, null=True)
    company_id_number = models.CharField(max_length=100, blank=True, null=True)
    last_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='bio_rank')
    #last_vessel = models.ForeignKey(Vessels, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='bio_vessel')
    separation_date = models.DateField(blank=True, null=True)
    separation_type = models.ForeignKey('SeparationTypes', on_delete=models.DO_NOTHING, blank=True, null=True,)
    separation_comments = models.CharField(max_length=50, blank=True, null=True)
    user_code = models.CharField(max_length=4, blank=True, null=True)
    photo = models.ImageField(upload_to='bio/photos', blank=True, null=True)
    photo_date_updated = models.DateTimeField(blank=True, null=True)
    middle_name = models.CharField(max_length=45, blank=True, null=True)
    name_suffix = models.ForeignKey(NameSuffixes, on_delete=models.DO_NOTHING, null=True, blank=True)
    nickname = models.CharField(max_length=45, blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    birthplace = models.CharField(max_length=45, blank=True, null=True)
    nationality = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, null=True, blank=True)
    bmi = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    blood_type = models.ForeignKey(BloodTypes, on_delete=models.DO_NOTHING, null=True, blank=True)
    email_address = models.CharField(max_length=45, blank=True, null=True)
    email_address_2 = models.CharField(max_length=45, blank=True, null=True)
    religion = models.ForeignKey(Religions, on_delete=models.DO_NOTHING, null=True, blank=True)
    ex_crew = models.CharField(max_length=45, blank=True, null=True)
    civil_status = models.ForeignKey(CivilStatuses, on_delete=models.DO_NOTHING, db_column='civil_status', related_name='bio_civil_status', null=True, blank=True)
    tax_status = models.ForeignKey(TaxStatuses, on_delete=models.DO_NOTHING, db_column='tax_status', related_name='bio_tax_status', null=True, blank=True)
    tax_account_number = models.CharField(max_length=50, blank=True, null=True)
    sss_number = models.CharField(max_length=45, blank=True, null=True)
    pagibig_number = models.CharField(max_length=45, blank=True, null=True)
    philhealth_number = models.CharField(max_length=45, blank=True, null=True)
    is_blacklisted = models.CharField(max_length=1, blank=True, null=True)
    referred_by = models.ForeignKey('self', on_delete=models.DO_NOTHING,blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='bio_updated_by')

    def __str__(self):
        return '%s' % (self.user_code)

    def save(self, size=(500,500), *args, **kwargs):
        if not self.photo:
            super(Bio, self).save(*args, **kwargs)
            return

        from django.utils import timezone #import timezone

        #TODO: Should we remove old photo?
        self.photo_date_updated = timezone.now() #when replaced update datetime

        super(Bio, self).save(*args, **kwargs)

        from PIL import Image #import image

        pw = self.photo.width
        ph = self.photo.height
        nw = self.photo.width
        nh = self.photo.height
        
        # only do this if the ImageFielde needs resizing
        filename = str(self.photo.path)
        image = Image.open(filename)

        if (pw, ph) != (nw, nh):
            #filename = str(self.photo.path)
            pr = float(pw) / float(ph)
            nr = float(nw) / float(nh)
            
            if pr > nr:
                # photo aspect is wider than destination ratio
                tw = int(round(nh * pr))
                image = image.resize((tw, nh), Image.ANTIALIAS)
                l = int(round(( tw - nw ) / 2.0))
                image = image.crop((l, 0, l + nw, nh))
            elif pr < nr:
                # photo aspect is taller than destination ratio
                th = int(round(nw / pr))
                image = image.resize((nw, th), Image.ANTIALIAS)
                t = int(round(( th - nh ) / 2.0))
                # print((0, t, nw, t + nh))
                image = image.crop((0, t, nw, t + nh))
            else:
                # photo aspect matches the destination ratio
                image = image.resize(size, Image.ANTIALIAS)
                
            image.save(filename)
        image.save(filename, quality=50)

    class Meta:
        db_table = 'bio'


class BioFilipinoAddress(models.Model):
    bio_fil_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_address') #Default to filipino related name as bio_address
    bio_fil_address = models.CharField(max_length=80, blank=True, null=True)
    bio_fil_province_barangay = models.CharField(max_length=80, blank=True, null=True)
    bio_fil_zip = models.ForeignKey(Zip, on_delete=models.DO_NOTHING,)
    bio_fil_address_type = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,)

    class Meta:
        db_table = 'bio_filipino_address'

class BioEducations(models.Model):
    bio_education_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_educations')
    bio_education_degree = models.ForeignKey(Degrees, on_delete=models.DO_NOTHING, blank=True, null=True,)
    bio_education_school = models.ForeignKey(Schools, on_delete=models.DO_NOTHING, blank=True, null=True,)
    bio_education_date_completed = models.PositiveIntegerField(null=True)
    bio_education_type = models.CharField(max_length=20, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_educations'

class BioTelephones(models.Model):
    bio_tel_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_telephones')
    bio_tel_area_code = models.ForeignKey(AreaCodes, on_delete=models.DO_NOTHING, blank=True, null=True,) 
    bio_tel_number = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_telephones'

class BioMobiles(models.Model):
    bio_mobile_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_mobiles')
    bio_mobile_prefix = models.ForeignKey(MobileNetworkPrefixes, on_delete=models.DO_NOTHING, blank=True, null=True,)
    bio_mobile_number = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_mobiles'  

class BioHobbies(models.Model):
    bio_hobby_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_hobbies')
    bio_hobby = models.ForeignKey(Hobbies, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_hobbies'

class BioSocialMedia(models.Model):
    bio_social_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_social_medias')
    bio_social_media = models.ForeignKey(SocialMedia, blank=True, null=True)
    bio_social_id = models.CharField(max_length=100, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_social_media'

class BioLanguages(models.Model):
    bio_language_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, related_name='bio_languages')
    bio_language = models.ForeignKey(Languages, on_delete=models.DO_NOTHING, blank=True, null=True)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_languages'

class BioLanguageDialects(models.Model):
    bio_lang_dialect_language = models.ForeignKey(BioLanguages, on_delete=models.CASCADE, related_name='bio_dialects')
    bio_lang_dialect_dialect = models.ForeignKey(Dialects, on_delete=models.DO_NOTHING)
    date_updated = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')

    class Meta:
        db_table = 'bio_languages_dialects'

class BioEmergencyAddress(models.Model):
    bio = models.ForeignKey(Bio, on_delete=models.DO_NOTHING)
    address = models.CharField(max_length=45, blank=True, null=True)
    cities = models.ForeignKey(Cities, on_delete=models.DO_NOTHING, blank=True, null=True)
    countries = models.ForeignKey(Countries, on_delete=models.DO_NOTHING)
    states = models.ForeignKey(States, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'bio_emergency_address'


class BioProvincialAddress(models.Model):
    id = models.IntegerField(primary_key=True)
    bio = models.ForeignKey(Bio, on_delete=models.DO_NOTHING)
    address = models.CharField(max_length=45, blank=True, null=True)
    cities = models.ForeignKey(Cities, on_delete=models.DO_NOTHING, blank=True, null=True)
    countries = models.ForeignKey(Countries, on_delete=models.DO_NOTHING)
    states = models.ForeignKey(States, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'bio_provincial_address'


class BioStatuses(models.Model):
    bio_status_code = models.CharField(max_length=45, blank=True, null=True)
    bio_status_name = models.CharField(max_length=45, blank=True, null=True)
    bio_status_description = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.bio_status_name

    class Meta:
        db_table = 'bio_statuses'

class SeparationTypes(models.Model):
    separation_type_sys_status = models.CharField(max_length=45, blank=True, null=True)
    separation_type_code = models.CharField(max_length=45, blank=True, null=True)
    separation_type_name = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.separation_type_name

    class Meta:
        db_table = 'separation_types'

class RelatedPeople(models.Model):
    related_people_bio = models.ForeignKey(Bio, on_delete=models.CASCADE, blank=True, null=True, related_name='bio_related_people')
    related_people_first_name = models.CharField(max_length=80)
    related_people_middle_name = models.CharField(max_length=80)
    related_people_last_name = models.CharField(max_length=80)
    related_people_married_last_name = models.CharField(max_length=80)
    related_people_married_middle_name = models.CharField(max_length=80)
    related_people_marriage_date = models.DateField(null=True, blank=True)
    related_people_marriage_place = models.CharField(max_length=80, null=True, blank=True)
    related_people_name_suffix = models.ForeignKey(NameSuffixes, on_delete=models.DO_NOTHING, null=True, blank=True)
    related_people_tel_area_code = models.ForeignKey(AreaCodes, on_delete=models.DO_NOTHING, blank=True, null=True)
    related_people_tel_area_code_2 = models.ForeignKey(AreaCodes, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='related_tel_area_code_2')
    related_people_tel_number = models.CharField(max_length=45, blank=True, null=True)
    related_people_tel_number_2 = models.CharField(max_length=45, blank=True, null=True)
    related_people_mobile_prefix = models.ForeignKey(MobileNetworkPrefixes, on_delete=models.DO_NOTHING, blank=True, null=True)
    related_people_mobile_number = models.CharField(max_length=45, blank=True, null=True)
    related_people_mobile_prefix_2 = models.ForeignKey(MobileNetworkPrefixes, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='related_mobile_prefix_2')
    related_people_mobile_number_2 = models.CharField(max_length=45, blank=True, null=True)
    related_people_address = models.CharField(max_length=80, blank=True, null=True)
    related_people_province_barangay = models.CharField(max_length=80, blank=True, null=True)
    related_people_zip = models.ForeignKey(Zip, on_delete=models.DO_NOTHING, blank=True, null=True)
    related_people_relationship_type = models.ForeignKey(RelationshipTypes, on_delete=models.DO_NOTHING, blank=True, null=True)
    related_people_birthdate = models.DateField(blank=True, null=True)
    related_people_birthplace = models.CharField(max_length=32, blank=True, null=True)
    related_people_nationality = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, null=True, blank=True)
    related_people_passport_number = models.CharField(max_length=15, blank=True, null=True)
    related_people_is_beneficiary = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_dependent = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_next_of_kin = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_emergency = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_working = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_student = models.CharField(max_length=1, blank=True, null=True)
    related_people_is_deceased = models.CharField(max_length=1, blank=True, null=True)
    related_people_occupation = models.CharField(max_length=100, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'related_people'       

#need to move to its proper sub-apps

class Cba(models.Model):
    id = models.IntegerField(primary_key=True)
    cba_sys_status = models.CharField(max_length=45, blank=True, null=True)
    cba_code = models.CharField(max_length=12, blank=True, null=True)
    cba_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', blank=True, null=True, related_name='cba_updated_by')
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'cba'

class CertificateGroupDetails(models.Model):
    id = models.IntegerField(primary_key=True)
    certificate_group_detail_sys_status = models.CharField(max_length=45, blank=True, null=True)
    certificate_group_detail_record = models.CharField(max_length=45, blank=True, null=True)
    certificate_group_detail_certificate_group = models.ForeignKey('CertificateGroups', on_delete=models.DO_NOTHING, db_column='certificate_group_detail_certificate_group', blank=True, null=True)
    certificate_group_detail_certificate = models.ForeignKey('Certificates', on_delete=models.DO_NOTHING, db_column='certificate_group_detail_certificate', blank=True, null=True)
    certificate_group_detail_weight = models.CharField(max_length=45, blank=True, null=True)
    certificate_group_detail_vessel_category = models.ForeignKey(VesselCategories, on_delete=models.DO_NOTHING, db_column='certificate_group_detail_vessel_category', blank=True, null=True)
    certificate_group_detail_tag = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='certificate_group_detail_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'certificate_group_details'


class CertificateGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    certificate_group_sys_status = models.CharField(max_length=45, blank=True, null=True)
    certificate_group_code = models.CharField(max_length=4, blank=True, null=True)
    certificate_group_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='certificate_group_created_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'certificate_groups'


class Certificates(models.Model):
    id = models.IntegerField(primary_key=True)
    certificate_sys_status = models.CharField(max_length=45, blank=True, null=True)
    certificate_code = models.CharField(max_length=4, blank=True, null=True)
    certificate_full = models.CharField(max_length=45, blank=True, null=True)
    certificate_book = models.CharField(max_length=45, blank=True, null=True)
    certificate_main = models.CharField(max_length=45, blank=True, null=True)
    certificate_expires = models.CharField(max_length=45, blank=True, null=True)
    certificate_interval_years = models.CharField(max_length=45, blank=True, null=True)
    certificate_interval_months = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='certificate_created_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'certificates'

class Dependents(models.Model):
    id = models.IntegerField(primary_key=True)
    dependent_sys_status = models.CharField(max_length=45, blank=True, null=True)
    dependent_sys_record = models.IntegerField(blank=True, null=True)
    dependent_bio = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='dependent_bio', blank=True, null=True)
    dependent_relationship = models.ForeignKey(RelationshipTypes, on_delete=models.DO_NOTHING, db_column='dependent_relationship', blank=True, null=True)
    dependent_birthdate = models.DateField(blank=True, null=True)
    dependent_first_name = models.CharField(max_length=45, blank=True, null=True)
    dependent_last_name = models.CharField(max_length=45, blank=True, null=True)
    dependent_father_name = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='dependent_updated_by')
    date_updated = models.IntegerField()

    class Meta:
        db_table = 'dependents'          

class Documents(models.Model):
    id = models.IntegerField(primary_key=True)
    document_sys_status = models.CharField(max_length=45, blank=True, null=True)
    document_record = models.IntegerField(blank=True, null=True)
    document_bio = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='document_bio', blank=True, null=True)
    document_certificate = models.IntegerField(blank=True, null=True)
    document_number = models.CharField(max_length=45, blank=True, null=True)
    document_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, db_column='document_rank', blank=True, null=True)
    document_issue = models.DateField(blank=True, null=True)
    document_expire = models.DateField(blank=True, null=True)
    document_other_country = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, db_column='document_other_country', blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='document_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'documents'

class ExitInfo(models.Model):
    id = models.IntegerField(primary_key=True)
    exit_info_sys_status = models.CharField(max_length=45, blank=True, null=True)
    exit_info_bio = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='exit_info_bio', blank=True, null=True)
    exit_info_months_employ_shore = models.IntegerField(blank=True, null=True)
    exit_info_last_employer_shore = models.CharField(max_length=45, blank=True, null=True)
    exit_info_position_shore = models.CharField(max_length=45, blank=True, null=True)
    exit_info_months_employ_abroad = models.IntegerField(blank=True, null=True)
    exit_info_last_employer_abroad = models.CharField(max_length=45, blank=True, null=True)
    exit_info_last_position_abroad = models.CharField(max_length=45, blank=True, null=True)
    exit_info_last_employer_address_abroad = models.CharField(max_length=45, blank=True, null=True)
    exit_info_allot_percent = models.IntegerField(blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='exit_info_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'exit_info'

class LicenseDetails(models.Model):
    id = models.IntegerField(primary_key=True)
    license_detail_sys_status = models.CharField(max_length=45, blank=True, null=True)
    license_detail_record = models.IntegerField(blank=True, null=True)
    license_detail_license_head = models.ForeignKey('LicenseHead', on_delete=models.DO_NOTHING, db_column='license_detail_license_head', blank=True, null=True)
    license_detail_other_country = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, db_column='license_detail_other_country', blank=True, null=True)
    license_detail_book_number = models.CharField(max_length=45, blank=True, null=True)
    license_detail_book_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, db_column='license_detail_book_rank', blank=True, null=True)
    license_detail_issue = models.DateField(blank=True, null=True)
    license_detail_expire = models.DateField(blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='license_details_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'license_details'


class LicenseHead(models.Model):
    id = models.IntegerField(primary_key=True)
    license_head_sys_status = models.CharField(max_length=45, blank=True, null=True)
    license_head_bio = models.IntegerField(blank=True, null=True)
    license_head_rp_book_number = models.CharField(max_length=45, blank=True, null=True)
    license_head_rp_book_issue = models.DateField(blank=True, null=True)
    license_head_rp_book_expire = models.DateField(blank=True, null=True)
    license_head_rp_book_place = models.CharField(max_length=45, blank=True, null=True)
    license_head_rp_license = models.CharField(max_length=45, blank=True, null=True)
    license_head_rp_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, db_column='license_head_rp_rank', blank=True, null=True, related_name='license_head_rp_rank')
    license_head_rp_issue = models.DateField(blank=True, null=True)
    license_head_rp_expire = models.DateField(blank=True, null=True)
    license_head_reg_card = models.CharField(max_length=45, blank=True, null=True)
    license_head_reg_issue = models.DateTimeField(blank=True, null=True)
    license_head_reg_expire = models.DateTimeField(blank=True, null=True)
    license_head_reg_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, db_column='license_head_reg_rank', blank=True, null=True)
    license_head_passport = models.CharField(max_length=45, blank=True, null=True)
    license_head_passport_issue = models.DateField(blank=True, null=True)
    license_head_passport_expire = models.DateField(blank=True, null=True)
    license_head_passport_place = models.CharField(max_length=45, blank=True, null=True)
    license_head_usa_visa = models.CharField(max_length=45, blank=True, null=True)
    license_head_usa_expire = models.DateField(blank=True, null=True)
    license_head_pending_legal = models.CharField(max_length=45, blank=True, null=True)
    license_head_yellow_card = models.CharField(max_length=45, blank=True, null=True)
    license_head_yellow_card_expire = models.DateField(blank=True, null=True)
    license_head_dutch_visa = models.CharField(max_length=45, blank=True, null=True)
    license_head_dutch_expire = models.DateField(blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='license_head_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'license_head'


class PrincipalAcctCategories(models.Model):
    id = models.IntegerField(primary_key=True)
    principal_acct_category_name = models.CharField(max_length=45, blank=True, null=True)
    principal_acct_category_principal = models.ForeignKey('Principals', on_delete=models.DO_NOTHING, db_column='principal_acct_category_principal', blank=True, null=True)

    class Meta:
        db_table = 'principal_acct_categories'

class Principals(models.Model):
    principal_sys_status = models.CharField(max_length=45, blank=True, null=True)
    principal_code = models.CharField(max_length=6, blank=True, null=True)
    principal_master = models.ForeignKey('self', on_delete=models.DO_NOTHING, db_column='principal_master', blank=True, null=True)
    principal_full = models.CharField(max_length=50, blank=True, null=True)
    principal_sss = models.CharField(max_length=45, blank=True, null=True)
    principal_philhealth = models.CharField(max_length=45, blank=True, null=True)
    principal_email_address = models.CharField(max_length=45, blank=True, null=True)
    principal_logo = models.ImageField(upload_to='principals/logo', blank=True, null=True)
    principal_is_hellespont_group = models.BooleanField(default=False)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.principal_code)

    def save(self, size=(500,500), *args, **kwargs):
        if not self.principal_logo:
            super(Principals, self).save(*args, **kwargs)
            return
            
        super(Principals, self).save(*args, **kwargs)

        from PIL import Image #import image

        pw = self.principal_logo.width
        ph = self.principal_logo.height
        nw = self.principal_logo.width
        nh = self.principal_logo.height
        
        # only do this if the image needs resizing
        filename = str(self.principal_logo.path)
        image = Image.open(filename)

        if (pw, ph) != (nw, nh):
            #filename = str(self.principal_logo.path)
            pr = float(pw) / float(ph)
            nr = float(nw) / float(nh)
            
            if pr > nr:
                # photo aspect is wider than destination ratio
                tw = int(round(nh * pr))
                image = image.resize((tw, nh), Image.ANTIALIAS)
                l = int(round(( tw - nw ) / 2.0))
                image = image.crop((l, 0, l + nw, nh))
            elif pr < nr:
                # photo aspect is taller than destination ratio
                th = int(round(nw / pr))
                image = image.resize((nw, th), Image.ANTIALIAS)
                t = int(round(( th - nh ) / 2.0))
                # print((0, t, nw, t + nh))
                image = image.crop((0, t, nw, t + nh))
            else:
                # photo aspect matches the destination ratio
                image = image.resize(size, Image.ANTIALIAS)
                
            image.save(filename)
        image.save(filename, quality=50)

    class Meta:
        db_table = 'principals'

class PrincipalContactInfo(models.Model):
    principal_contact_info_first_name = models.CharField(max_length=60)
    principal_contact_info_middle_name = models.CharField(max_length=60, blank=True, null=True)
    principal_contact_info_last_name = models.CharField(max_length=60)
    principal_contact_info_designation = models.CharField(max_length=70)
    principal_contact_info_email_address = models.CharField(max_length=50, blank=True, null=True)
    principal_contact_info_contact_number_1 = models.CharField(max_length=50)
    principal_contact_info_contact_number_2 = models.CharField(max_length=50, blank=True, null=True)
    principal_contact_info_contact_number_3 = models.CharField(max_length=50, blank=True, null=True)
    principal_contact_info_contact_number_4 = models.CharField(max_length=50, blank=True, null=True)
    principal_contact_info_principal = models.ForeignKey(Principals, on_delete=models.DO_NOTHING, related_name='principal_contact_infos')
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('id',)
        db_table = 'principal_contact_info'

class PrincipalAddresses(models.Model):
    principal_address = models.CharField(max_length=80, blank=True, null=True)
    principal_address_city = models.CharField(max_length=50)
    principal_address_country = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, db_column='principal_address_country', blank=True, null=True)
    principal_address_state = models.CharField(max_length=50, blank=True, null=True)
    principal_address_zip = models.IntegerField(blank=True, null=True)
    principal_address_principal = models.ForeignKey(Principals, on_delete=models.DO_NOTHING, db_column='principal_address_principal', related_name='principal_addresses')
    principal_address_contact_number_1 = models.CharField(max_length=30, blank=True, null=True)
    principal_address_contact_number_2 = models.CharField(max_length=30, blank=True, null=True)
    principal_address_contact_number_3 = models.CharField(max_length=30, blank=True, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, db_column='updated_by')
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'principal_addresses'

class RequiredCertificates(models.Model):
    id = models.IntegerField(primary_key=True)
    required_certificate_sys_status = models.CharField(max_length=45, blank=True, null=True)
    required_certificate_record = models.IntegerField(blank=True, null=True)
    required_certificate_principal = models.ForeignKey(Principals, on_delete=models.DO_NOTHING, db_column='required_certificate_principal', blank=True, null=True)
    required_certificate_vessel_category = models.ForeignKey(VesselCategories, on_delete=models.DO_NOTHING, db_column='required_certificate_vessel_category', blank=True, null=True)
    required_certificate_rank = models.ForeignKey(Ranks, on_delete=models.DO_NOTHING, db_column='required_certificate_rank', blank=True, null=True)
    required_certificate_certificate_group = models.ForeignKey(CertificateGroups, on_delete=models.DO_NOTHING, db_column='required_certificate_certificate_group', blank=True, null=True)
    required_certificate_source = models.CharField(max_length=45, blank=True, null=True)
    required_certificate_interval = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='required_certificate_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'required_certificates'        

class VesselAssignEquipments(models.Model):
    id = models.IntegerField(primary_key=True)
    vessel_assign_equipment_vessel = models.ForeignKey('VesselGroups', on_delete=models.DO_NOTHING, db_column='vessel_assign_equipment_vessel', blank=True, null=True)
    vessel_assign_equipment_equipment = models.ForeignKey('VesselEquipments', on_delete=models.DO_NOTHING, db_column='vessel_assign_equipment_equipment', blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_assignment_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'vessel_assign_equipments'        

class VesselEquipments(models.Model):
    id = models.IntegerField(primary_key=True)
    vessel_equipment_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_equipment_code = models.CharField(max_length=4, blank=True, null=True)
    vessel_equipment_full = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', blank=True, null=True, related_name='vessel_equipment_updated_by')
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'vessel_equipments'


class VesselGroupDetails(models.Model):
    id = models.IntegerField(primary_key=True)
    vessel_group_details_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_group_detail_record = models.CharField(max_length=8, blank=True, null=True)
    vessel_group_detail_vessel_group = models.ForeignKey('VesselGroups', on_delete=models.DO_NOTHING, db_column='vessel_group_detail_vessel_group', blank=True, null=True)
    vessel_group_detail_vessel = models.ForeignKey('Vessels', on_delete=models.DO_NOTHING, db_column='vessel_group_detail_vessel', blank=True, null=True)
    vessel_group_detail_tags = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_group_detail_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'vessel_group_details'


class VesselGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    vessel_group_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_group_code = models.CharField(max_length=8, blank=True, null=True)
    vessel_group_full = models.CharField(max_length=45, blank=True, null=True)
    vessel_group_group_by = models.CharField(max_length=45, blank=True, null=True)
    vessel_group_principal = models.ForeignKey(Principals, on_delete=models.DO_NOTHING, db_column='vessel_group_principal', blank=True, null=True)
    vessel_group_status = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey(Bio, on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_group_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'vessel_groups'        

class Vessels(models.Model):
    id = models.IntegerField(primary_key=True)
    vessel_sys_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_code = models.CharField(max_length=4, blank=True, null=True)
    vessel_type = models.ForeignKey(VesselTypes, on_delete=models.DO_NOTHING, db_column='vessel_type', blank=True, null=True)
    vessel_full = models.CharField(max_length=45, blank=True, null=True)
    vessel_principal = models.ForeignKey(Principals, on_delete=models.DO_NOTHING, db_column='vessel_principal', blank=True, null=True)
    vessel_registry = models.CharField(max_length=45, blank=True, null=True)
    vessel_country = models.ForeignKey(Countries, on_delete=models.DO_NOTHING, db_column='vessel_country', blank=True, null=True)
    vessel_reg_number = models.CharField(max_length=45, blank=True, null=True)
    vessel_grt = models.IntegerField(blank=True, null=True)
    vessel_nrt = models.IntegerField(blank=True, null=True)
    vessel_dwt = models.CharField(max_length=45, blank=True, null=True)
    vessel_engine = models.CharField(max_length=45, blank=True, null=True)
    vessel_horsepower = models.CharField(max_length=45, blank=True, null=True)
    vessel_class = models.CharField(max_length=45, blank=True, null=True)
    vessel_yard = models.CharField(max_length=45, blank=True, null=True)
    vessel_year_built = models.IntegerField(blank=True, null=True)
    vessel_trades = models.CharField(max_length=45, blank=True, null=True)
    vessel_ums = models.CharField(max_length=45, blank=True, null=True)
    vessel_call_sign = models.CharField(max_length=45, blank=True, null=True)
    vessel_owner_ship = models.CharField(max_length=45, blank=True, null=True)
    vessel_imo = models.CharField(max_length=45, blank=True, null=True)
    vessel_status = models.CharField(max_length=45, blank=True, null=True)
    vessel_end = models.DateField(blank=True, null=True)
    vessel_reason = models.CharField(max_length=45, blank=True, null=True)
    vessel_main_cat = models.CharField(max_length=45, blank=True, null=True)
    vessel_pays_nat = models.CharField(max_length=45, blank=True, null=True)
    vessel_pays_fmy = models.CharField(max_length=45, blank=True, null=True)
    vessel_pays_pno = models.CharField(max_length=45, blank=True, null=True)
    vessel_pays_tax = models.CharField(max_length=45, blank=True, null=True)
    vessel_p_post_date = models.DateField(blank=True, null=True)
    vessel_r_post_date = models.DateField(blank=True, null=True)
    vessel_e_post_date = models.DateField(blank=True, null=True)
    vessel_tfs_fleet = models.CharField(max_length=45, blank=True, null=True)
    vessel_tfs_ship = models.CharField(max_length=4, blank=True, null=True)
    vessel_email_1 = models.CharField(max_length=45, blank=True, null=True)
    vessel_email_2 = models.CharField(max_length=45, blank=True, null=True)
    updated_by = models.ForeignKey('Bio', on_delete=models.DO_NOTHING, db_column='updated_by', related_name='vessel_updated_by')
    date_updated = models.DateTimeField()

    class Meta:
        db_table = 'vessels'