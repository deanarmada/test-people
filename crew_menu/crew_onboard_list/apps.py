from django.apps import AppConfig


class CrewOnboardListConfig(AppConfig):
    name = 'crew_onboard_list'
