from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
	# template = 'crew_menu/crew_onboard_list/index.html'
	template = 'pure_html/crew_menu/crew_onboard_list/index.html'
	context_dict = {}
	return render(request, template, context_dict)
	# return HttpResponse("CREW ONBOARD LIST INDEX")