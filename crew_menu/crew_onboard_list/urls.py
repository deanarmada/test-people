from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='crew_onboard_list_index' ),
]