from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='crew_menu_index' ),
	url(r'^reports/$', views.report, name='crew_menu_report' ), # temporary
	url(r'^bio/', include('crew_menu.bio.urls')),
	url(r'^crew-onboard-list/', include('crew_menu.crew_onboard_list.urls')),
	url(r'^crew-admin/', include('crew_menu.crew_admin.urls')),
]