from __future__ import unicode_literals

# Used for formatting the output date values of a model object
# Example output - 2015-11-04
DATE_FORMAT = "Y-m-d"
# Adds Date Input Formats
DATE_INPUT_FORMATS = ("%Y-%m-%d", )
# Used for formatting the output datetime values of a model object
# Example output - 2015-11-04 18:00 
DATETIME_FORMAT = 'Y-m-d H:i'