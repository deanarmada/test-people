from django.core.urlresolvers import reverse
from django.utils.html import mark_safe
from django.shortcuts import render
from django.utils.six import BytesIO
from django.http import HttpResponse

from urllib.request import urlopen

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status

from . serializers import StudentSerializer, TaskSerializer
from . forms import StudentForm, TaskForm, ParsleyForm
from . models import Task

from test_people.models import *

# Custom methods
# START get JSON from URL then convert to dict
def return_api(url):
	html = ""
	try:
		response = urlopen(url)
		json_data = response.read()
		stream = BytesIO(json_data)
		data = JSONParser().parse(stream)
		
		html += "<table border = '1px' class='retrieved-data-table'>"
		html += "<tr>"
		for key,values in data[0].items():
			html += "<th class='%s'>" % key
			html += key.title()
			html += "</th>"
		html += "</tr>"
		for x in data:
			html += "<tr>"
			for key, values in x.items():
				html += "<td class='%s'>" % key
				html += values
				html += "</td>"
			html += "</tr>"
		html += "</table>"
	except:
		html += "Invalid API"
	return mark_safe(html)
# END get JSON from URL then convert to dict

# Create your views here.
def index(request):
	print ("----------")
	ranks = Ranks.objects.get(id=5)
	print (reverse('testing_sample_web_service'))
	template = 'testing_sample_app/index.html'
	context_dict = {'ranks': ranks}
	return render(request, template, context_dict)

def web_service_form(request):
	web_service_form = StudentForm(request.POST or None)
	template = 'testing_sample_app/web_service_form.html'

	if request.POST:
		if web_service_form.is_valid():
			# web_service_form.save()
			student_serializer = StudentSerializer(data=request.POST)
			if student_serializer.is_valid():
				student_serializer.save()
		else:
			print (web_service_form.errors)

	context_dict = {}
	context_dict['web_service_form'] = web_service_form
	return render(request, template, context_dict)

def task_form(request):
	task_form = TaskForm(request.POST or None)
	template = 'testing_sample_app/task_form.html'

	context_dict = {}
	context_dict['task_form'] = task_form
	# context_dict['data'] = data
	context_dict['return_api'] = return_api('http://192.168.0.50:8000/testing-sample-app/tasks/?format=json')
	return render(request, template, context_dict)

def client_side_validation_form(request):
	parsley_form = ParsleyForm(request.POST or None)
	template = 'testing_sample_app/client_side_validation_form.html'
	context_dict = {}
	context_dict['parsley_form'] = parsley_form
	return render(request, template, context_dict)

# START API view methods
@api_view(['GET', 'POST'])
def task_list(request):
    """
    List all tasks, or create a new task.
    """
    if request.method == 'GET':
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else: 
        	print(serializer.errors)
        	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)






@api_view(['GET', 'PUT', 'DELETE'])
def task_detail(request, pk):
    """
    Get, udpate, or delete a specific task
    """
    try:
        task = Task.objects.get(pk=pk)
    except Task.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TaskSerializer(task)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TaskSerializer(task, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
# END API view methods


def angularjs(request):
	template = 'testing_sample_app/angularjs.html'
	context_dict = {}
	return render(request, template, context_dict)

# def djangular_form_sample(request):
# 	return HttpResponse("DEAN")