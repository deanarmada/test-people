from django.db import models

# Create your models here.
# class AuditTable(models.Model):
# 	date_created = models.DateTimeField(auto_now_add=True)
	# test_field = models.CharField(max_length=50, null=True, blank=True)

class Section(models.Model):
	name = models.CharField(max_length=50, default=None)

	def __str__(self):
		return self.name

class Student(models.Model):
	section = models.ForeignKey(Section, default=None)
	first_name = models.CharField(max_length=50, default=None)
	middle_name = models.CharField(max_length=50, default=None)
	last_name = models.CharField(max_length=50, default=None)

	def __str__(self):
		full_name = "%s %s %s" % (self.first_name, self.middle_name, self.last_name)
		return full_name


class Task(models.Model):
    # completed = models.BooleanField(default=False)
    title = models.CharField(max_length=100)
    description = models.TextField()