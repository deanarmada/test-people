from django.conf.urls import include, url

from . import views
from . routers import router

urlpatterns = [
	url(r'^$', views.index, name='testing_sample_index' ),
	# url(r'^$', views.angularjs, name='testing_sample_index' ),

	url(r'^restful-form/$', views.web_service_form, name='testing_sample_web_service' ),
	url(r'^task-form/$', views.task_form, name='testing_sample_task_form' ),
	url(r'^client-side-validation-form/$', views.client_side_validation_form, name='testing_sample_client_side_validation_form' ),

	url(r'^api/', include(router.urls)),

	url(r'^tasks/$', views.task_list, name='task-list'),
	url(r'^tasks/(?P<pk>[0-9]+)$', views.task_detail, name='task_detail'),

	url(r'^angularjs/$', views.angularjs, name='testing_sample_angularjs' ),
	# url(r'^djangular-form-sample/$', views.djangular_form_sample, name='testing_sample_djangular_form_sample'),
]