var domainName = window.location.href;
var domainName = domainName.split("/");
var domainName = domainName[0] + "//" + domainName[2];

// Function that forces the form to not submit
// Ensures that the form is not submitted in formControllers
function formSubmit(){
	return false;
}
// Function that checks whether an item is available in the array returns boolean
// Example: isInArray(1, [1,2,3]); // true
function isInArray(value, array) {
  return array.indexOf(value) > -1;
}
function ascendingArrayList(value, array){
	new_value = value.replace('-', '')
	index = array.indexOf(value);
	// alert(index);
	// Conditional statement is to make sure that it does not delete anything if field did not make any sorting yet
	// Comment the conditional to enable single sorting for each field
	if(index != -1){
		array.splice(index, 1);
	}
	array.unshift(new_value);
	return array;
}
function descendingArrayList(value, array){
	new_value = value.replace('-', '');
	index = array.indexOf(new_value);
	// Conditional statement is to make sure that it does not delete anything if field did not make any sorting yet
	// Comment the conditional to enable single sorting for each field
	if(index != -1){
		array.splice(index, 1);
	}
	array.unshift(value);
	return array;
}
// Function used to remove those JSON empty objects like the ones on the BioMobile where the empty json objects is used for ng-repeat
function jsonEmptyRemover(array){
	new_array = [];
    array.map(function(obj){
      if(Object.keys(obj).length){
       new_array.push(obj);
      }
    });
    return new_array;
}

function loopEvenlyDisplayed(json){
	while(!json.length || json.length & 1){
		json.push({});
	}
}

function add_id(array){
	array.map(function(obj){ 
		obj["_id"] = obj["id"] 
	});
}

// Compares if both arrays are identical
// Source: http://stackoverflow.com/questions/4025893/how-to-check-identical-array-in-most-efficient-way
function arraysEqual(arr1, arr2) {
    if(arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
}

$(document).tooltip({
	// Selector is needed to enable toopltip inside an angularjs routing template
    selector: '[data-toggle="tooltip"]'
});

var peopleApp = angular.module('peopleApp', ['ngRoute', 'ngAnimate', 'ui.mask', 'ngFileUpload', 'ntt.TreeDnD', 'ngTagsInput', 'ui.select', 'ngSanitize']); 

peopleApp.service('defaults', function(){
	this.perPage = 10; // This is the number of items to be shown on each page
	this.page = 1; // The current page
	this.querynum = 0; // The Starting Point of the query, hit next and this will be 10 (start will be the 10th item of the api)
	this.limit = 10; // The Ending Point of the query, hit next and this will be 20 (end will be the 20th item of the api)
	this.screenHeight = screen.height;
});

// Content headers to enable manual json input on a $http post
var jqlike_headers = {
	    headers : {
	        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
	    }
	}

// START Configurations
	peopleApp.config(function($interpolateProvider, $httpProvider) {
		// Change template tags
	    $interpolateProvider.startSymbol('[[');
	    $interpolateProvider.endSymbol(']]');
	    
	    // Enabling CORS
	    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
		$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
		// $httpProvider.defaults.withCredentials = true;

		$httpProvider.interceptors.push(function() {
        return {
	            'request': function(config) {
	                config.url = getTimeVersionedUrl(config.url);
	                return config;
	            }
	        };
	    });

	    function getTimeVersionedUrl(url) {
	        // only do for html templates of this app
	        // NOTE: the path to test for is app dependent!
	        if (!url || url.indexOf('a/app/') < 0 || url.indexOf('.html') < 0) return url;
	        // create a URL param that changes every minute
	        // and add it intelligently to the template's previous url
	        var param = 'v=' + ~~(Date.now() / 60000) % 10000; // 4 unique digits every minute
	        if (url.indexOf('?') > 0) {
	            if (url.indexOf('v=') > 0) return url.replace(/v=[0-9](4)/, param);
	            return url + '&' + param;
	        }
	        return url + '?' + param;
	    }
	});
// END Configurations

// START Global Functions and Declarations
	peopleApp.run(function($rootScope, $window, $http, $controller, $timeout, defaults) {
		// START Default Global Variables
			$rootScope.domainName = domainName;

		    $rootScope.perPage = defaults.perPage;
			$rootScope.page = defaults.page;
			$rootScope.querynum = defaults.querynum;
			$rootScope.limit = defaults.limit;
			$rootScope.SSid = SS_id;

			$rootScope.main = {
		        limit: $rootScope.limit,
		        offset: $rootScope.querynum,
		    };

		    // $rootScope.screenHeight = screen.height;

		    $rootScope.notifications = false;

		    $rootScope.noRecordMessage = "No records found";
		    $rootScope.integerOnlyMessage = "This field must be an integer";
		    $rootScope.requiredMessage = "This field is required";
		    $rootScope.relationGroupValidationMessage = "Please select at least one next of kin and one emergency relation";
		    $rootScope.errorNotificationTitle = "";
		    $rootScope.errorDuplicate = "already exists";
		    $rootScope.invalidEmail = "Invalid E-mail";
		    $rootScope.glyphiconUpdate = "glyphicon-pencil";
		    $rootScope.glyphiconDelete = "glyphicon-remove";
		    $rootScope.glyphiconRemoveSort = "glyphicon-minus-sign";
		    $rootScope.glyphiconRemoveObject = "glyphicon-minus";
		    $rootScope.glyphiconAddObject = "glyphicon-plus";
		    $rootScope.glyphiconDeactivate = "glyphicon-ban-circle";
		    $rootScope.glyphiconActivate = "glyphicon-ok";
		    $rootScope.glyphiconTelephone = "glyphicon-earphone";
		    $rootScope.glyphiconMobile = "glyphicon-phone";
		    // $rootScope.glyphiconView = "glyphicon-eye-open";\
		    // Forces reload on ng-includes
		    // Source: http://stackoverflow.com/questions/21935446/angularjs-ng-include-caching
		    $rootScope.forceReload = Math.random().toString(36).substring(7);;


		    // $controller('notificationsController', {$scope: $scope});
		    // alert($scope.name);
	    // END Default Global Variables

	    // START route configurations
	    	// Settings on the start of a route change
	    	// Basically, when you go to a different URL
			$rootScope.$on('$routeChangeStart', function (event, current, previous) {
				if (!$window.localStorage.token){
			    	window.location.href = domainName;
			    }
			    $rootScope.loadingView = true;
			    if (current.$$route && current.$$route.resolve) {
			    	$rootScope.perPage = defaults.perPage;
					$rootScope.page = defaults.page;
					$rootScope.main.limit = defaults.limit;
	    			$rootScope.main.offset = defaults.querynum;
	    			// $timeout(function() { $rootScope.showRouting = false; }, 400);
			    }
			    angular.element("html, body").animate({scrollTop: 0}, "slow");
			    // Dynamic Title Using AngularJS
			    document.title = current.$$route.title;
			});
			// Settings on the end of a route change
	    	// Basically, after you go to a different URL
		    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
		    	// Hides a loading message until resolves on the $routeProvider are finished and an extra 1 second
		    	$timeout(function() { $rootScope.loadingView = false; }, 1000);
		    	angular.element("html, body").animate({scrollTop: 0}, "slow");
		    	// $timeout(function() { $rootScope.showRouting = true; }, 600);
		    	$rootScope.showRouting = true;
		        $rootScope.notifications = false;
		    });
	    // END route configurations


	    // START default functions
		    // Close Window
		    $rootScope.close = function(){
		    	// This only works if a javascript or html code is used to open a certain window
		    	$window.close();
		    };


		    $rootScope.logOut = function(){
		    	$http.post(domainName+"/logout/", logout_token, jqlike_headers)
				.then(function(response){
					$window.localStorage.removeItem('token');
					window.location.href = domainName;
				},function(response){
					// alert('error');
					// alert(JSON.stringify(logout_token));
					$window.localStorage.removeItem('token');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
		    };

		    $rootScope.individualUpdate = function(event, id, field, value, url, obj){
				json = {};
				json['fields'] = {};
				json['fields'][field] = value;
				json['id'] = id;
				json['updated_by'] = $rootScope.SSid;
				if(obj){
					// remove retrieved item from delete to the deleteArray
					$rootScope.deleteArray = $rootScope.deleteArray.filter(function(x){
						return x.id != obj.id;
					});
					// alert($rootScope.deleteArray.length);
					if ($rootScope.deleteArray.length == 0){
						// $scope.showModal = false;
						id = document.querySelector( '#dismiss-modal' );
			    		key = angular.element(id);
			    		key.click();
					}
				}
				$http.post(url, json)
				.then(function(response){
					$rootScope.loadPage();
				},function(response){
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
				// angular.element(event.target) is similar to $(this)
				// angular.element(event.target).parent().remove();
			};

		    
		    // Get the ids of selected for deletion
		    // $rootScope.deleteMultiple = function(){
		    // 	$rootScope.array = [];
		    // 	angular.forEach($rootScope.items, function(item){
		    // 		if(item.selected){
		    // 			$rootScope.array.push({'id':item.id});
		    // 		}
		    // 	});
		    // 	alert(JSON.stringify($rootScope.array));
		    // };

		    // Hides a notification
		    $rootScope.hideNotifications = function(){
				$rootScope.notifications = false;
			};
			$rootScope.dynamicCaret = function(event){
		    	_this = angular.element(event.target);
		    	dynamic_current = _this.hasClass("caret-reversed");
		    	if(dynamic_current){
		    		_this.removeClass("caret-reversed");
		    	}else{
		    		_this.addClass("caret-reversed");
		    	}
		    }
	    // END default functions
	});
// END Global Functions and Declarations

// START Default Controllers
	// Controller for handling expressions and functions inside a form
	peopleApp.controller('formController', function($scope, $rootScope, $http, $httpParamSerializerJQLike, $window, $timeout) {
		$scope.hideLogInNotifications = function(){
			$scope.notifications = false;
		}
		$scope.clearInput = function(){
			$scope.input = {};
		};
		$scope.clearNotAll = function(unclear_field){
			// alert(typeof(unclear_field));
			if (typeof(unclear_field) == 'string'){
	    		_value = $scope.input[unclear_field];
	    	}else{
	    		num_field = unclear_field.length;
	    		_value = [];
	    		for(x=0;x<num_field;x++){
	    			_value[x] = $scope.input[unclear_field[x]];
	    		}
	    	}
			
			$scope.input = {};

			if (typeof(unclear_field) == 'string'){
	    		$scope.input[unclear_field] = _value;
	    	}else{
	    		for(x=0;x<num_field;x++){
	    			$scope.input[unclear_field[x]] = _value[x];
	    		}
	    	}
		};
		// $scope.dean = function(){
		// 	alert('dean');
		// };
		$scope.saveInput = function(formStatus, json, url, stay){
			if (formStatus){
				$rootScope.notifications = true;
			}else{
				// take note that adding additional key value that is not included on the api will result to error
				json['updated_by'] = $rootScope.SSid;
				$http.post(url, json)
				.then(function(response){
					// alert('success');
					$rootScope.notifications = false;
					if(!stay){
						window.location.href = '#/';
					}else{
						$scope.saveNotifications = true;
					}
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}
		};
		// {'fields':{'rank_sys_status':''}, 'id':3 }
		$scope.updateInput = function(formStatus, json, url){
			if (formStatus){
				$rootScope.notifications = true;
			}else{
				json['updated_by'] = $rootScope.SSid;
				json['id'] = $scope.id;
				$http.post(url, json)
				.then(function(response){
					// alert('success');
					$rootScope.notifications = false;
					$scope.showModal = false;
					$timeout(function() { window.location.href = '#/'; }, 500);
				},function(response){
					// alert('error');
					alert('Please check the following validations on the next alert and contact the creators regarding this error');
					alert(JSON.stringify(response.data.errors));
				});
			}
		};
		$scope.logIn = function(json, url){
			// json.username = json.username.toUpperCase();
			$http.post(url, $httpParamSerializerJQLike(json), jqlike_headers)
			.then(function(response){
				token = response.data.token;
				// alert(JSON.stringify(response));
				$window.localStorage.token = token;
				if (json['redirect']){
					window.location.href = json['redirect'];
				}else{
					window.location.href = 'crew-menu';
				}
			},function(response){
				// error = response.data.errors
				$scope.notifications = true;
				$scope.loginErrorMessage = "* "+response.data.errors[0].messsage; 
				// alert('Please check the following validations on the next alert and contact the creators regarding this error');
				// alert(JSON.stringify(response.data.errors));
			});
		};
		$scope.toggleModal = function(){
	    	$scope.showModal = !$scope.showModal;
	    };
	    $scope.hideSaveNotifications = function(){
				$scope.saveNotifications = false;
		};
		$scope.addNestedModels = function(json, more){
			if(more){
				for(x=0;x<more;x++){
					json.push({});
				}
			}else{
	    		json.push({});
	    	}
	    };
	    // Used in BioMobiles, BioTelephones, BioSocialMedias
	    // Removes every json object that is null
	    $scope.nullRemover = function(index, json){
	    	if($scope.title == "ADD"){
	    		if(!json[index]){
		    		delete json[index];
		    	}else if(!$scope.input[index]){
		    		delete $scope.input[index];
		    	}
	    	}else{
	    		_this = angular.element(event.target);
	    		value = _this.val();
	    		// console.log(value);
	    		x = parseInt(value, 10)
	    		if(!json[index] && !x){
	    			json[index] = null;
	    			// json["deleted"] = true;
	    		}
	    	}
	    };
	    $scope.fieldDependentRemover = function(json, independentField, dependentField){
	    	// dependentField are those that needs the independentField to be filled out before it is enabled
	    	if(!json[independentField]){
	    		delete json[dependentField];
	    	}
	    };
	    // Custom function used in deleting values in a ui-select
	    $scope.uiGlobalSelectClear = function(index, json){
	    	if(json){
	    		delete json[index];
	    	}else{
	    		delete $scope.input[index];
	    	}
	    };
	});
	peopleApp.controller('itemListController', function($scope, $rootScope, $http, $timeout, defaults) {
		// $scope.showModal = false;

		$scope.nextPageBool = true;
		if ($rootScope.page == $rootScope.totalPage){
    		$scope.nextPageBool = false;
    	}
    	// alert($scope.nextPageBool);
		$scope.previousPageBool = false;
		$scope.selectedAll = false;

		$rootScope.deleteArray = [];
		$rootScope.noRecord = false;

		// $scope.sorts = [];
		// $scope.sorts = $scope.preSorts;

		$scope.getPage = function(url){
			$http.post(domainName+url, $rootScope.main)
			.then(function(response){
				data = response.data.data;
				// START Replaces a certain value to a desired value like true to active (Used in user management)
				// var old = JSON.stringify(data).replace(/true/g, '"active"').replace(/false/g, '"inactive"');
				// data = JSON.parse(old);
				// END Replaces a certain value to a desired value like true to active 
				$rootScope.items = data;
				$rootScope.items.forEach(function(entry) {
				    entry.selected = false;
				});
			});
		};

	    $scope.nextPage = function(){
	    	$rootScope.page++;
	    	$rootScope.main.limit = $rootScope.main.limit + $rootScope.perPage;
	    	$rootScope.main.offset = $rootScope.main.offset + $rootScope.perPage;
	    	if ($rootScope.page == $rootScope.totalPage){
	    		$scope.nextPageBool = false;
	    	}
	    	if ($rootScope.page > 1){
	    		$scope.previousPageBool = true;
	    	}
	    	$rootScope.loadPage();
	    };
	    $scope.previousPage = function(){
	    	$rootScope.page--;
	    	$rootScope.main.limit = $rootScope.main.limit - $rootScope.perPage;
	    	$rootScope.main.offset = $rootScope.main.offset - $rootScope.perPage;
	    	if ($rootScope.page < $rootScope.totalPage){
	    		$scope.nextPageBool = true;
	    	}
	    	if ($rootScope.page == 1){
	    		$scope.previousPageBool = false;
	    	}
	    	$rootScope.loadPage();
	    };
	    $scope.returnPage = function(page){
	    	$rootScope.page = page;
	    	$rootScope.main.limit = $rootScope.perPage * page;
	    	$rootScope.main.offset = $rootScope.main.limit - $rootScope.perPage;
	    	if ($rootScope.page < $rootScope.totalPage){
	    		$scope.nextPageBool = true;
	    	}
	    	if ($rootScope.page == 1){
	    		$scope.previousPageBool = false;
	    	}
	    	if ($rootScope.page == $rootScope.totalPage){
	    		$scope.nextPageBool = false;
	    	}
	    	if ($rootScope.page > 1){
	    		$scope.previousPageBool = true;
	    	}
	    	$rootScope.loadPage();
	    };

	    // Checks everything
		$scope.checkAll = function(){
			angular.forEach($rootScope.items, function (item) {
	            item.selected = $scope.selectedAll;
	        });
		};
		// Source: http://stackoverflow.com/questions/22863256/angular-js-button-should-be-enabled-if-at-least-one-checkbox-is-checked-how-to
		// Hides the delete button if a checkbox is checked
		$scope.isChecked = function() {
	        for(var e in $rootScope.items) {
	        	// e is the index of the list
	             var checkBox = $rootScope.items[e];
	            if(checkBox.selected){
	                return true;
	            }
	        }
	        return false;
	    };
	    $scope.deleteItem = function(id, name, url, tree){
	    	json = {};
	    	json['id'] = [id];
	    	json['updated_by'] = $rootScope.SSid;
	    	$rootScope.notifications = true;
	  		$http.post(url, json)
			.then(function(response){
				$rootScope.loadPage();
				// name is just an indicator when restoration
				$scope.deleteArray.push({'name': response.data.data[0][name], 'id': id});
				// alert(JSON.stringify($scope.deleteArray));
				$rootScope.notifications = true;
			},function(response){
				// alert('error');
				alert('Please check the following validations on the next alert and contact the creators regarding this error');
				alert(JSON.stringify(response.data.errors));
			});
	    };
	    $scope.deleteMultiple = function(name, url){
	    	json = {};
	    	ids = [];
	    	angular.forEach($scope.items, function(item){
	    		if(item.selected){
	    			ids.push(item.id);
	    		}
	    	});
	    	json['id'] = ids;
	  		$http.post(url, json)
			.then(function(response){
				$rootScope.loadPage();
				data = response.data.data;
				responseDataCount = data.length;
				for(x=0;x<responseDataCount;x++){
					$scope.deleteArray.push({'name': data[x][name], 'id': data[x]['id']});
				}
				$rootScope.notifications = true;
			},function(response){
				// alert('error');
				alert('Please check the following validations on the next alert and contact the creators regarding this error');
				alert(JSON.stringify(response.data.errors));
			});
	    };
	    $scope.toggleModal = function(id, obj){
	    	// alert($scope.showModal)
	    	$scope.id = id;
	    	$scope.obj = obj;
	    	$rootScope.notifications = false;
	    	$scope.showModal = true;
	    	$scope.deleteStatement = "Are you sure you want to delete the selected record?";
	    	$scope.individualDelete = true;
	    	$scope.multipleDelete = false;
	    };
	    $scope.toggleDynamicModal = function(id, condition){
	    	$scope.id = id;
	    	$scope.condition = condition;
	    	$rootScope.notifications = false;
	    	$scope.showModal = true;
	    	if($scope.condition){
				$scope.statement = "Are you sure you want to deactivate the selected record?";
				$scope.operation = true;
			}else{
				$scope.statement = "Are you sure you want to restore the selected record?";
				$scope.operation = false;
			}
	    };
	    $scope.toggleModalMultiple = function(){
	    	$scope.showModal = true;
	    	$scope.deleteStatement = "Are you sure you want to delete all selected record?";
	    	$scope.individualDelete = false;
	    	$scope.multipleDelete = true;
	    };
	    $scope.toggleNotificationModal = function(){
	    	$scope.showModal = true;
	    };
	    $scope.clearSort = function(){
	    	angular.forEach(angular.element(".caret"), function(key, value){
			    sort_arrow = angular.element(key);
			    if(!sort_arrow.hasClass('initial-reversed')){
			    	sort_arrow.removeClass("caret-reversed");
			 	}else{
			 		sort_arrow.addClass("caret-reversed");
			 	}
			});
	    }
	    $scope.searchClearSort = function(){
	    	angular.forEach(angular.element(".caret"), function(key, value){
			    sort_arrow = angular.element(key);
			    if(!sort_arrow.hasClass('caret-reversed')){
			    	sort_arrow.addClass("caret-reversed");
			 	}
			});
	    }
	    $scope.search = function(url, event){
	    	// START filter declaration
    		// key is the field
    		// value is the value for that field
	    	_this = angular.element(event.target);
	    	value = _this.val();
    		key = $scope.searchList;

    		// If enter is pressed
	    	if(event.which == 13){
	    		if(value){
		    		json = {};
		    		json['filters'] = {};
		    		if (key && key.value != null){
		    			key = $scope.searchList.value;
		    			// Specific field will be search
		    			json['filters'][key+'__icontains'] = value;
		    		}else{
		    			// All the options field in the search by will be search
		    			// y = $scope.searchFilters.slice(1); // Slice is used to evade the null:all key-value pair of searchfilters model
		    			y = $scope.searchFilters.slice(1);
		    			for(x=0;x<y.length;x++){
			    			z = y[x].value;
			    			json['filters'][z+'__icontains'] = value;
			    		}
		    		}
		    		// END filter declaration
		    		// alert(JSON.stringify($rootScope.main));
		    		for (x in $rootScope.main){
		    			json[x] = $rootScope.main[x];
		    		}
		    		// alert(JSON.stringify(json));
			    	$http.post(url, json).then(function(response){;
			    		totalSearch = response.data.total
			    		if(!totalSearch){
			    			$scope.noRecord = true;
			    		}else{
			    			$scope.noRecord = false;
			    		}
			    		data = response.data.data;
						// START Replaces a certain value to a desired value like true to active (Used in user management)
						// var old = JSON.stringify(data).replace(/true/g, '"active"').replace(/false/g, '"inactive"');
						// data = JSON.parse(old);
						// END Replaces a certain value to a desired value like true to active
			    		$rootScope.items = data;

						$rootScope.items.forEach(function(entry) {
						    entry.selected = false;
						});
						$rootScope.totalPage = Math.ceil(totalSearch/$rootScope.limit);
						$rootScope.page = 1;
						if ($rootScope.page == $rootScope.totalPage){
				    		$scope.nextPageBool = false;
				    	}
				    	$scope.sorts = [];
				    	$scope.searchClearSort();
			    	},function(response){
			    		alert('Please check the following validations on the next alert and contact the creators regarding this error');
			   			alert(JSON.stringify(response.data.errors));
			    	});
			    }else{
			    	$rootScope.loadPage();
			    }
	    	}
	    };
	    $scope.clearSearchAndSearchFilterValues = function(){
	    	id = document.querySelector( '#search' );
	    	key = angular.element(id);
    		key.val("");
    		$scope.searchList = $scope.searchFilters[0];
    		$scope.searchFilter();
	    }
	    $scope.clearSearch = function(){
	    	$rootScope.totalPage = $scope.originalTotalPage;
	    	$rootScope.perPage = defaults.perPage;
			$rootScope.page = defaults.page;
			delete $rootScope.main['order_by'];
			$scope.sorts = angular.copy($scope.preSorts);
			$scope.clearSort();
			$rootScope.main.limit = defaults.limit;
			$rootScope.main.offset = defaults.querynum;
			$scope.nextPageBool = true;
			$scope.previousPageBool = false;
	    	$rootScope.loadPage();
	    	$scope.noRecord = false;
	    	$scope.clearSearchAndSearchFilterValues()
	    };
	    $scope.searchFilter = function(){
	    	replace = '__';
	    	try{
	    		value = $scope.searchList.value;
	    		_value = value.lastIndexOf(replace);
		    	if(_value != -1){
		    		value = value.slice(_value+replace.length);
		    	}
		    	$http.post(url).then(function(response){
		    		$scope.searchItems = [];
		    		data = response.data.data;
		    		data.map(function(obj){ $scope.searchItems.push(obj[value]); });
		    	},function(response){
		    		alert('Please check the following validations on the next alert and contact the creators regarding this error');
		   			alert(JSON.stringify(response.data.errors));
		    	});
	    	}
	    	catch(err){
	    		$http.post(domainName+url)
				.then(function(response){
					data = response.data.data;
					// $scope.Items = [];
					data.map(function(obj){ 
						for(x=0;x<$scope.Items.length;x++){
							// alert($scope.Items[x]);
							// alert('/');
							$scope.searchItems.push(obj[$scope.Items[x]]); 
						}	
					});
				});
	    	} 	
	    };

	    $scope.dynamicCaret = function(event, thisElement){
	    	if(thisElement){
	    		_this = thisElement;
	    	}else{
	    		_this = angular.element(event.target);
	    	}
	    	dynamic_current = _this.hasClass("caret-reversed");
	    	if(dynamic_current){
	    		_this.removeClass("caret-reversed");
	    	}else{
	    		_this.addClass("caret-reversed");
	    	}
	    }
	    
	    $scope.itemSort = function(url, field, event){
	    	_this = angular.element(event.target).find(".caret")
	    	$scope.dynamicCaret(event, _this);
	    	if(isInArray(field, $scope.sorts)){
	    		$scope.sorts = ascendingArrayList(field, $scope.sorts)
	    	}else{
	    		$scope.sorts = descendingArrayList(field, $scope.sorts)
	    	}

	    	$rootScope.main['order_by'] = $scope.sorts;
	    	$http.post(domainName+url, $rootScope.main)
	    	.then(function(response){
	    		data = response.data.data;
				$rootScope.items = data;
				$rootScope.items.forEach(function(entry) {
				    entry.selected = false;
				});
				$scope.clearSearchAndSearchFilterValues()
	    	});
	    }
	    $scope.orderPosition = function(value){
	    	arrayPosition = $scope.sorts.indexOf(value); // Gets the position on the array | same as knowing what is prioritized 1 is the most prioritized
	    	if(arrayPosition == -1){
	    		arrayPosition = $scope.sorts.indexOf(value.replace('-', '')); // System checks if it is in descending order in the array
	    		// arrayPosition = arrayPosition.toString();
	    	}
	    	if (arrayPosition == -1){
	    		arrayPosition = null; // Will just return none if it really is not in the array
	    		return arrayPosition;
	    	}
	    	return arrayPosition+1;
	    }
	    $scope.removeOrder = function(value, event){
	    	$scope.sorts.splice($scope.orderPosition(value)-1, 1);
	    	_this = angular.element(event.target).parent().parent().prev().find(".caret");
	    	dynamic_current = _this.hasClass("initial-reversed");
	    	if(!dynamic_current){
	    		_this.removeClass("caret-reversed");
	    	}else{
		 		_this.addClass("caret-reversed");
		 	}
	    	$rootScope.main['order_by'] = $scope.sorts;
	    	$http.post(domainName+url, $rootScope.main)
	    	.then(function(response){
	    		data = response.data.data;
				$rootScope.items = data;
				$rootScope.items.forEach(function(entry) {
				    entry.selected = false;
				});
				$scope.clearSearchAndSearchFilterValues()
	    	});
	    }
	    $scope.checkSortable = function(value){
	    	if(value){
	    		css = "item-sort";
	    	}else{
	    		css = "";
	    	}
	    	return css;
	    }
	    $scope.addDynamicFilter = function(json){
	    	json.push({});
	    };
	    $scope.removeDynamicFilter = function(json, inputs){
	    	json.splice(inputs, 1);
	    };
	});
// END Default Controllers

// START Default Factories
	peopleApp.factory('delayFactory', function($http, $rootScope, $q, $timeout){
		var deferred = $q.defer();
		$timeout(function(){
          deferred.resolve({
            key: function( ) {
              return;
            }
          });
        }, 1000);
		return deferred.promise;
	});
	peopleApp.factory('rankFactory', function($http, $rootScope){
		return {
			rankList: function(){ // IDEA URL
				url = "/api/people/v1/ranks/";
				return $http.post(domainName+url, $rootScope.main).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					ranks = data;
					total = response.data.total;
					order_by = response.data.order_by;
					// alert(JSON.stringify(order_by));
					data.forEach(function(entry) {
					    entry.selected = false;
					});
				});
			},
			rankCompleteList: function(){ // IDEA URL
				url = "/api/people/v1/ranks/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					ranks_complete = data;
					total = response.data.total;
					order_by = response.data.order_by;
				});
			},
			rankFormValidations: function(){
				url = "/api/people/v1/ranks/rules/";
				return $http.get(domainName+url).then(function(response){
					// START script for dynamic validations
					validations= {};
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					for (key in data){
						validations[key] = data[key]['rules']
					}
				});
			},
		}
	});
	peopleApp.factory('bioFactory', function($http, $rootScope){
		return {
			bioList: function(){ // IDEA URL
				url = "/api/people/v1/bio/";
					return $http.post(domainName+url, $rootScope.main).then(function(response){
						// data is used for consistent variables used on the admin section
						data = response.data.data;
						bio = data;
						total = response.data.total;
						order_by = response.data.order_by;
						// alert(JSON.stringify(order_by));
						data.forEach(function(entry) {
						    entry.selected = false;
						});
					});
			},
			bioFormValidations: function(){
				url = "/api/people/v1/bio/rules/";
				return $http.get(domainName+url).then(function(response){
					// START script for dynamic validations
					validations= {};
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					for (key in data){
						validations[key] = data[key]['rules']
					}
				});
			}
		}
	});
	peopleApp.factory('userFactory', function($http, $rootScope){
		return {
			userList: function(){ // IDEA URL
				url = "/api/people/v1/users/";
				return $http.post(domainName+url, $rootScope.main).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					users = data;
					total = response.data.total;
					order_by = response.data.order_by;
					// alert(JSON.stringify(order_by));
					data.forEach(function(entry) {
					    entry.selected = false;
					});
				});
			},
			userFormValidations: function(){
				url = "/api/people/v1/users/rules/";
				return $http.get(domainName+url).then(function(response){
					// START script for dynamic validations
					validations= {};
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					for (key in data){
						validations[key] = data[key]['rules']
					}
					// END script for dynamic validations
				});
			},
		}
	});
	peopleApp.factory('principalFactory', function($http, $rootScope){
		return {
			principalList: function(){ // IDEA URL
				url = "/api/people/v1/principals/";
				return $http.post(domainName+url, $rootScope.main).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					principals = data;
					total = response.data.total;
					// console.log(response);
					order_by = response.data.order_by;
					// alert(JSON.stringify(order_by));
					data.forEach(function(entry) {
					    entry.selected = false;
					});
				});
			},
			principalCompleteList: function(){ // IDEA URL
				url = "/api/people/v1/principals/";
					return $http.post(domainName+url).then(function(response){
						// data is used for consistent variables used on the admin section
						data = response.data.data;
						principals_complete = data;
						total = response.data.total;
						order_by = response.data.order_by;
					});
			},
			principalMasterList: function(filterId){ // IDEA URL
				url = "/api/people/v1/principals/";
				json = {};
	    		json['filters'] = {};
	    		if(filterId){
					json['filters']["~|id"] = filterId;
				}else{
					json['filters']['principal_master'] = null;
				}
				return $http.post(domainName+url, json).then(function(response){
					masterListData = response.data.data;
					// console.log(masterListData);
				});
			},
			principalFormValidations: function(){
				url = "/api/people/v1/principals/rules/";
				return $http.get(domainName+url).then(function(response){
					// START script for dynamic validations
					validations= {};
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					for (key in data){
						validations[key] = data[key]['rules']
					}
					// END script for dynamic validations
				});
			},
		}
	});
	// Is also used for nationality
	peopleApp.factory('countryFactory', function($http, $rootScope){
		return {
			countryList: function(){ // IDEA URL
				url = "/api/people/v1/countries/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					countries = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['country_name'].toUpperCase() == "PHILIPPINES"){
							philippines = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('employeeTypeFactory', function($http, $rootScope){
		return {
			employeeTypeList: function(){ // IDEA URL
				url = "/api/people/v1/employee_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					employee_types = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['employee_type'].toUpperCase() == "SEAFARER"){
							seafarer = obj['id'];
						}
						if(obj['employee_type'].toUpperCase() == "STAFF"){
							staff = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('employmentTypeFactory', function($http, $rootScope){
		return {
			employmentTypeList: function(){ // IDEA URL
				url = "/api/people/v1/employment_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					employment_types = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('titleFactory', function($http, $rootScope){
		return {
			titleList: function(){ // IDEA URL
				url = "/api/people/v1/titles/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					titles = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('bioStatusFactory', function($http, $rootScope){
		return {
			bioStatusList: function(){ // IDEA URL
				url = "/api/people/v1/bio_statuses/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					bio_statuses = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj["bio_status_name"].toUpperCase() == "NEW CREW"){
							new_crew = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('separationTypeFactory', function($http, $rootScope){
		return {
			separationTypeList: function(){ // IDEA URL
				url = "/api/people/v1/separation_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					separation_types = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('nameSuffixFactory', function($http, $rootScope){
		return {
			nameSuffixList: function(){ // IDEA URL
				url = "/api/people/v1/name_suffixes/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					name_suffixes = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('bloodTypeFactory', function($http, $rootScope){
		return {
			bloodTypeList: function(){ // IDEA URL
				url = "/api/people/v1/blood_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					blood_types = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('religionFactory', function($http, $rootScope){
		return {
			religionList: function(){ // IDEA URL
				url = "/api/people/v1/religions/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					religions = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('areaCodeFactory', function($http, $rootScope){
		return {
			areaCodeList: function(){ // IDEA URL
				url = "/api/people/v1/area_codes/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					area_codes = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['area_name'].toUpperCase() == "METRO MANILA"){
							manila = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('mobileNetworkFactory', function($http, $rootScope){
		return {
			mobileNetworkList: function(){ // IDEA URL
				url = "/api/people/v1/mobile_networks/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					mobile_networks = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('mobilePrefixNetworkFactory', function($http, $rootScope){
		return {
			mobilePrefixNetworkList: function(){ // IDEA URL
				url = "/api/people/v1/mobile_network_prefixes/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					mobile_network_prefixes = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['prefix'] == "+"){
							other_prefix = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('regionFactory', function($http, $rootScope){
		return {
			regionList: function(){ // IDEA URL
				url = "/api/people/v1/regions/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					regions = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj["region"].toUpperCase() == "NATIONAL CAPITAL REGION"){
							ncr = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('provinceFactory', function($http, $rootScope){
		return {
			provinceList: function(){ // IDEA URL
				url = "/api/people/v1/provinces/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					provinces = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('municipalityFactory', function($http, $rootScope){
		return {
			municipalityList: function(){ // IDEA URL
				url = "/api/people/v1/municipalities/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					municipalities = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('hobbyFactory', function($http, $rootScope){
		return {
			hobbyList: function(){ // IDEA URL
				url = "/api/people/v1/hobbies/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					hobbies = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('languageFactory', function($http, $rootScope){
		return {
			languageList: function(){ // IDEA URL
				url = "/api/people/v1/languages/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					languages = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('dialectFactory', function($http, $rootScope){
		return {
			dialectList: function(){ // IDEA URL
				url = "/api/people/v1/dialects/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					dialects = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('vesselTypeFactory', function($http, $rootScope){
		return {
			vesselTypeList: function(){ // IDEA URL
				url = "/api/people/v1/vessel_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					vessel_types = data;
					total = response.data.total;
					// console.log(response);
					order_by = response.data.order_by;
					// alert(JSON.stringify(order_by));
					data.forEach(function(entry) {
					    entry.selected = false;
					});
				});
			},
			vesselTypeParentList: function(filterId){ // IDEA URL
				url = "/api/people/v1/vessel_types/";
				json = {};
	    		json['filters'] = {};
	    		if(filterId){
					json['filters']["~|id"] = filterId;
				}else{
					json['filters']['vessel_type_parent'] = null;
				}
				return $http.post(domainName+url, json).then(function(response){
					parentListData = response.data.data;
				});
			},
			vesselTypeFormValidations: function(){
				url = "/api/people/v1/vessel_types/rules/";
				return $http.get(domainName+url).then(function(response){
					// START script for dynamic validations
					validations= {};
					// data is used for consistent variables used on the admin section
					data = response.data.data
					for (key in data){
						validations[key] = data[key]['rules']
					}
					// END script for dynamic validations
				});
			},
		}
	});
	peopleApp.factory('civilStatusFactory', function($http, $rootScope){
		return {
			civilStatusList: function(){ // IDEA URL
				url = "/api/people/v1/civil_statuses/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					civil_statuses = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['civil_status_code'] == "M"){
							married = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('taxStatusFactory', function($http, $rootScope){
		return {
			taxStatusList: function(){ // IDEA URL
				url = "/api/people/v1/tax_statuses/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					tax_statuses = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('relationshipTypesFactory', function($http, $rootScope){
		return {
			relationshipTypesList: function(){ // IDEA URL
				url = "/api/people/v1/relationship_types/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					relationship_types = data;
					total = response.data.total;
					data.map(function(obj){
						if(obj['relationship_name'].toUpperCase() == "WIFE"){
							wife = obj['id'];
						}
						if(obj['relationship_name'].toUpperCase() == "HUSBAND"){
							husband = obj['id'];
						}
						if(obj['relationship_name'].toUpperCase() == "MOTHER"){
							mother = obj['id'];
						}
						if(obj['relationship_name'].toUpperCase() == "FATHER"){
							father = obj['id'];
						}
					});
				});
			},
		}
	});
	peopleApp.factory('degreesFactory', function($http, $rootScope){
		return {
			degreesList: function(){ // IDEA URL
				url = "/api/people/v1/degrees/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					degrees = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('schoolsFactory', function($http, $rootScope){
		return {
			schoolsList: function(){ // IDEA URL
				url = "/api/people/v1/schools/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					schools = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('socialMediaFactory', function($http, $rootScope){
		return {
			socialMediaList: function(){ // IDEA URL
				url = "/api/people/v1/social_medias/";
				return $http.post(domainName+url).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					social_media = data;
					total = response.data.total;
				});
			},
		}
	});
	peopleApp.factory('newBioIdFactory', function($http, $rootScope){
		return {
			newBioIdList: function(){ // IDEA URL
				url = "/api/people/v1/bio/";
				json = {};
				json["order_by"] = ["-user_id"];
				json["limit"] = 1;
				json["offset"] = 0;
				json["filters"] = { "~|user_id" : null };
				return $http.post(domainName+url, json).then(function(response){
					// data is used for consistent variables used on the admin section
					data = response.data.data;
					// if(!data) will not work
					if(data == ""){
						data = 1;
					}else{
						data = data[0].user_id + 1;
					}
					new_bio_id = data;
					total = response.data.total;
				});
			},
		}
	});
// END Default Factories

// START Default Runs
	// $httpParamSerializerJQLike is used to manually pass JSON as a post parameter
	peopleApp.run(function($httpParamSerializerJQLike, $window, $http, $rootScope, $anchorScroll){
	    if ($window.localStorage.token){
	    	// Script to define token header authorization
	    	$http.defaults.headers.common.Authorization = "Token "+$window.localStorage.token;
	    	// Creates a logout token jquery json to be passed during the execution of logout function
	    	logout_token = $httpParamSerializerJQLike({
	    		token: $window.localStorage.token
	    	});
	    }
	    $anchorScroll.yOffset = 175;   // always scroll by 175 extra pixels
	});
// END Default Runs

// START Directives
	peopleApp.directive("loadingDiv", function($timeout) {
	    return {
	        template : '<div ng-if="loadingView" id="loading-page"><img src="/static/img/loading-x.gif"></div>',
	        // link: function (scope, element) {
            // scope.$watch("screenHeight", function () {
            //    scope.height = scope.screenHeight+"px";
            // });
        	// }
	    };
	});
	// Source: http://jsfiddle.net/alexsuch/RLQhh/
	peopleApp.directive("confirmationModal", function(){
		return {
			template: '<div class="modal fade">' + 
			  '<div class="modal-dialog">' + 
			    '<div class="modal-content modal-sm">' + 
			      '<div class="modal-header">' + 
			        // '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
			        '<span class="pull-right cursor-pointer" data-dismiss="modal" id="dismiss-modal">X</span>' +
			        '{{ title }}' + 
			      '</div>' + 
			      '<div class="modal-body" id="modal-body" ng-transclude></div>' + 
			    '</div>' + 
			  '</div>' + 
			'</div>',
			restrict: 'E',
			transclude: true,
			replace:true,
			scope:true,
			link: function postLink(scope, element, attrs) {
				scope.title = attrs.title;

				scope.$watch(attrs.visible, function(value){
				  if(value == true)
				    $(element).modal('show');
				  else
				    $(element).modal('hide');
				});

				$(element).on('shown.bs.modal', function(){
				  scope.$apply(function(){
				    scope.$parent[attrs.visible] = true;
				  });
				});

				$(element).on('hidden.bs.modal', function(){
				  scope.$apply(function(){
				    scope.$parent[attrs.visible] = false;
				  });
				});
			}
		};
	});

	peopleApp.directive('ngEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.ngEnter);
	                });
	                event.preventDefault();
	            }
	        });
	    };
	});

	peopleApp.directive('noWhiteSpace', function () {
	    return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if(event.which === 32) {
	            	return false;
	            }
	        });
	    };
	});

	peopleApp.directive('negatorDecimalNegative', function () {
	    return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if(event.which === 46 || event.which === 45) {
	            	return false;
	            }
	        });
	    };
	});

	peopleApp.directive('numbersOnly', function(){
		return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if (event.which > 31 && (event.which < 48 || event.which > 57)) {
	            	return false;
	            }
	        });
	    };
	});

	peopleApp.directive('numbersDecimalOnly', function () {
	    return function (scope, element, attrs) {
	        element.bind("keypress", function (event) {
	            if (event.which > 31 && (event.which < 48 || event.which > 57) && event.which != 46) {
	            	return false;
	            }
	        });
	    };
	});

	// peopleApp.directive('ngUpper', function () {
	//     return function (scope, element, attrs) {
	//         element.bind("keyup", function (event) {
	//             element.val(element.val().toUpperCase())
	//         });
	//     };
	// });
	peopleApp.directive('ngDisable', function () {
	    return function (scope, element, attrs) {
	        element.bind("click", function (event) {
	            // If an input is only readonly then the html can not be typed to receive values
	            if(attrs.readonly){
	            	return false;
	            }
	        });
	    };
	});
	
	// START Autocomplete Directives
	function monkeyPatchAutocomplete() {

	    // Don't really need to save the old fn, 
	    // but I could chain if I wanted to
	    var oldFn = $.ui.autocomplete.prototype._renderItem;
	  
	    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
	        var re = new RegExp( "(" + this.term + ")", "gi" );
	        var t = item.label.replace(re,"<span style='font-weight:bold;color:#04508e;'>" + this.term + "</span>");
	        return $( "<li></li>" )
	        .data( "item.autocomplete", item )
	        .append( "<a>" + t + "</a>" )
	        .appendTo( ul );
	  	};
	};
	monkeyPatchAutocomplete();

	// Removes duplicates in an array list
	function remove_duplicates_safe(arr) {
	    var obj = {};
	    var arr2 = [];
	    for (var i = 0; i < arr.length; i++) {
	        if (!(arr[i] in obj)) {
	            arr2.push(arr[i]);
	            obj[arr[i]] = true;
	        }
	    }
	    return arr2;
	}

	function split( val ) {
		return val.split( /,\s*/ );
	}

	function extractLast( term ) {
		return split( term ).pop();
	}

	peopleApp.directive('autoComplete', function($timeout){
		return {
			restrict: "A",
			scope: {
				uiItems: "="
			},
			link: function(scope, element, attrs){
				scope.$watchCollection('uiItems', function(val){
					items = scope.uiItems.filter(function(n){ return n != undefined });
					element.autocomplete({
						source: remove_duplicates_safe(items),
						select: function(){
							// Triggers enter when you select an item
							var e = jQuery.Event("keyup");
							e.which = 13;
							$timeout(function() { element.trigger(e); }, 200);
						},
						minLength:2,
					});
				});
			}
		}
	});

	// Came from Dean's plunkr http://plnkr.co/edit/2EPuhRiR9OncV8z7q018?p=preview
	peopleApp.directive('autoCompleteValidation', function($timeout){
		return {
			require: 'ngModel',
			scope: {
				uiItems: "="
			},
			link: function(scope, element, attrs, ctrl){
			  	var items;
				scope.$watchCollection('uiItems', function(val){
					items = scope.uiItems.filter(function(n){ return n != undefined });
					element.autocomplete({
						source: remove_duplicates_safe(items),
						select: function(){
						  $timeout(function() {
    						// ctrl.$setValidity('available', true);
    						ctrl.$setViewValue(1);
    						// TODO http request
    						scope.$apply();
						  }, 200);
						},
						minLength:2,
					});
				});
				
				// ctrl.$validators.available = function (modelValue, viewValue) {
				//   var value = modelValue || viewValue;
				//   if (value) {
	  	// 			var _index = items.indexOf(value);
	  	// 			return _index !== -1;
				//   }
				// }
			}
		}
	});
	// END Autocomplete Directives

	// Source: http://stackoverflow.com/questions/26842216/angularjs-password-confirmation-nomatch-not-working
	// Same Password Error Match Directive
	peopleApp.directive('validPasswordConfirmation', function(){
		return {
			require: 'ngModel', // is needed to load parsers
			scope: {
				reference: '=validPasswordConfirmation'
			},
			link: function(scope, elm, attrs, ctrl){
				ctrl.$parsers.unshift(function(viewValue, $scope){
					// viewValue is the value that you are entering every key event, try alerting it
					var noMatch = viewValue != scope.reference
					// It will trgger the validation if the noMatch is false
					// Basically, if it does not mach with the password
					ctrl.$setValidity('noMatch', !noMatch);
					return viewValue;
				});

				scope.$watch("reference", function(value){
					ctrl.$setValidity('noMatch', value === ctrl.$viewValue);
				});
			}
		}
	});
	peopleApp.directive('fileUpload', function () {
	    return {
	        scope: true,        //create a new scope
	        link: function (scope, el, attrs) {
	            el.bind('change', function (event) {
	                var files = event.target.files;
	                //iterate files since 'multiple' may be specified on the element
	                for (var i = 0;i<files.length;i++) {
	                    //emit event upward
	                    scope.$emit("fileSelected", { file: files[i] });
	                }                                       
	            });
	        }
	    };
	});
	// Used for forms with dynamic fields to make them scrollable
	peopleApp.directive('getHeightScrollable', function($timeout, $rootScope){
		return {
			restrict: 'A',
			link: function(scope, element){
				// $timeout(function() {
					scope.height = element.prop('offsetHeight');
					element.css("height", scope.height); //To manipulate css on the directive tag
					element.css("overflow-y", "scroll");
				// }, 900);
			}
		}
	});
	// Use for the sidebar to return a fixed width

	function getAffixParentWidth(width, paddRight){
		_width = width - paddRight;
		return _width;  
	}
	peopleApp.directive('getWidthAffix', function($timeout, $window){
		return {
			restrict: 'A',
			link: function(scope, element){
				angular.element($window).bind('resize', function(){			        
					element.css("width", getAffixParentWidth(element.parent().prop('offsetWidth'), parseInt(element.parent().css("padding-right")))); //To manipulate css on the directive tag
			    });
				// $timeout(function() {
					element.css("width", getAffixParentWidth(element.parent().prop('offsetWidth'), parseInt(element.parent().css("padding-right")))); //To manipulate css on the directive tag
				// }, 5000);
			}
		}
	});
	// Directive examples
	// // Use avoid repetition of update button on the item list on the index page of crew-admin apps
	peopleApp.directive('viewGlyphiconButton', function(){
		return {
			scope:{
				id: "="
			},
			template : '<a href="#/view/{{ id }}" target="_blank"><span class="glyphicon glyphicon glyphicon-eye-open" data-toggle="tooltip" title="View"></span></a>',
		}
	});
	peopleApp.directive('adminFormCancelButton', function(){
		return {
			template : '<div class="col-lg-2 " ng-class="{\'col-lg-offset-10\': viewOnly}"><a href="#/"><button class="btn form-control" id="cancel">BACK</button></a></div>',
		}
	});
	peopleApp.directive('adminUpdatePageClearButton', function(){
		return {
			template : '<div class="col-lg-2 col-lg-offset-6" ng-if="!viewOnly"><button type="button" class="btn form-control" ng-click="clearNotAll(field)" id="clear">CLEAR</button></div>',
			// template : 'xx - {{ field }}',
			restrict: 'E',
			scope: true,
			link: function(scope, element, attrs){
				scope.field = attrs.field;
			}
		}
	});
	peopleApp.directive('adminUpdatePageSaveButton', function(){
		return {
			template : '<div class="col-lg-2" ng-if="!viewOnly"><button class="btn form-control" ng-click="toggleModal()" id="confirmation-save">SAVE</button></div>',
		}
	});
	peopleApp.directive('adminAddPageClearButton', function(){
		return {
			template : '<div class="col-lg-2 col-lg-offset-6"><button class="btn form-control" ng-click="clearInput()" id="clear" type="button">CLEAR</button></div>',
		}
	});
	peopleApp.directive('adminAddPageSaveButton', function(){
		return {
			// template : '<div class="col-lg-2"><button class="btn form-control" ng-click="saveInput(formValidation, input, apiAdd)" id="save">SAVE</button></div><div class="col-lg-3"><button class="btn form-control" ng-click="saveInput(formValidation, input, apiAdd, true)" id="save">SAVE & CONTINUE</button></div>',
			template : '<div class="col-lg-2"><button class="btn form-control" ng-click="saveInput(validation, input, apiAdd)" id="save">SAVE</button></div>',
			// template : 'xx - {{ field }}',
			restrict: 'E',
			scope: true,
			link: function(scope, element, attrs){
				scope.validation = attrs.validation;
			}
		}
	});
	peopleApp.directive('datepicker', function() {
	    return {
	        restrict: 'A',
	        require : 'ngModel',
	        link : function (scope, element, attrs, ngModel) {
	        	// console.log(ngModel);
	        	datePatternCounter = [];
	            // $(function(){
	            	if(!scope.viewOnly){
	            		element.attr("placeholder", "YYYY-MM-DD");
	            	}
	                element.datepicker({
	                	changeYear: true, 
				        changeMonth: true, 
				        yearRange: "1900:+50", 
				        showButtonPanel: true,
				        closeText: 'Clear',
	                    dateFormat:'yy-mm-dd',
	                    onSelect:function (date) {
	                        ngModel.$setViewValue(date);
	                        scope.$apply();
	                    },
	                    // Custom function that clears the date value of the model when the "Clear" button is clicked on the datepicker
	                    onClose: function(date){
	                    	var event = arguments.callee.caller.caller.arguments[0];
                    		if(Number(date) && date.length == 8){
                    			// alert("dean");
                    			year = date.slice(0, 4);
                    			month = date.slice(4, 6);
                    			day = date.slice(6, 8);
                    			newDate = year+"-"+month+"-"+day;
                    			ngModel.$setViewValue(newDate);
                    			ngModel.$render();

                    			// This trigger is made to solve issue https://tree.taiga.io/project/itmanship-people/issue/225 
                    			// ("Auto hyphen does not occur when ESC is pressed")
                    			// ["Because what happens is when Esc is pressed, the datepicker UI will be closed and the input is still focused. So futher changes won't do any onclose actions"]
                    			// Please be noted that the autohyphen is triggered when the datepicker UI is closed
                    			// Easisest Solution: Put a blur trigger to surely remove the focus
                    		}else{
                    			// alert("armada");
                    			// Code if with dash
                    			// Do nothing
                    		}
                    		element.trigger('blur');
                    		// ngModel.$error.datePattern
	                    	if(event['type'] == 'click'){
	                    		ngModel.$setViewValue();
	                    		ngModel.$render();
	                    	}
	                    },
	                    beforeShow: function (e, t) {
	                    	id = document.querySelector( '#ui-datepicker-div' );
					        angular.element(id).addClass('HideTodayButton');
					        if(element.hasClass('birth_date') && !element.hasClass('ng-dirty')){
					          	element.datepicker("option", "maxDate", 0);
					        }
				        },
	                });
	            // });
	            // START Syntax to check if the date is valid or not in real time
    			ngModel.$validators.available = function (modelValue, viewValue) {
    				var today = new Date();
				  	if(ngModel.$error.required){
						check = false; 
					}else{
					    check = true;
					}
					if(modelValue){
						var check = modelValue.split('-');
	                	var y = parseInt(check[0], 10);
						var m = parseInt(check[1], 10);
						var d = parseInt(check[2], 10);
						var date = new Date(y,m-1,d);
						if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
						    check = true;
						    if(element.hasClass('birth_date')){
                				if(today < date){
                					check = false; // Returns false if the date input is more advanced on an options set to maxDate today
                				}
                			}
						} else {
						    check = false; // Returns false if the date is invalid like February 31
						}
						if(!check){
							element.parent().siblings("span.errors").html('<span class="invalid-date" ng-if="'+ngModel.$error.available+'"> * Invalid Date <br /></span>');
						}	
					}
					return check;
				}
				// END Syntax to check if the date is valid or not
	        }
	    }
	});

	// START Used to show validations upon focus ------ June 1, 2016
	function showSpecificValidations(){
		return {
			link: function(scope, element, attrs){
				element.bind("keyup", function(){
					if(element.hasClass('error')){
						element.parent().siblings("span.errors").css("display", "block");
					}else{
						element.parent().siblings("span.errors").css("display", "none");
					}
				});
				element.bind("focus", function(){
					if(scope.notifications){
						if(element.hasClass('error')){
							element.parent().siblings("span.errors").css("display", "block");
						}
					}
				});
				element.bind("blur", function(){
					element.parent().siblings("span.errors").css("display", "none");
				});
				element.bind("mouseenter", function(){
					if(scope.notifications){
						if(element.hasClass('error')){
							element.parent().siblings("span.errors").css("display", "block");
						}
					}
				});
				element.bind("mouseleave", function(){
					element.parent().siblings("span.errors").css("display", "none");
				});
			}
		}
	}
	
	peopleApp.directive("input", showSpecificValidations);
	peopleApp.directive("select", showSpecificValidations);
	peopleApp.directive("textarea", showSpecificValidations);
	peopleApp.directive("showErrors", showSpecificValidations);
	peopleApp.directive('negateSpecialCharacters', function(){
		return{
          	require: 'ngModel',
          	link: function(scope, element, attrs, ngModel){
      			attrs.$observe('ngModel', function(value){
          			scope.$watch(value, function(newValue){
          				try{
	          			  	if(attrs.negateSpecialCharacters){
	          				    if(attrs.negateSpecialCharacters == "address"){
	          				 		regex = /[^a-zA-Z0-9,-.Ññ ]+/g;
	          				    }
	          				    if(attrs.negateSpecialCharacters == "name"){
	          				    	// START negates non-characters as prefix
	          				        prefix = newValue.search(/[^a-zA-Z]/i);
	          				        if(!prefix){
	          				        	newValue = newValue.substring(1);
	          				        }
	          				       regex = /[^a-zA-Z-.Ññ ]+/g;
	          				       // END negates non-characters as prefix
	          				    }
	          				}else{
	          					regex = /[^a-zA-Z0-9 ]+/g;
	          				}
	          				clean = newValue.replace(regex, "");
	          				ngModel.$setViewValue(clean);
	          				ngModel.$render();
	          			}catch(err){
			          			// Just to catch all the regex errors on console log upon page reload
			          	}
          			});
          		});
          	}
        }
	});
	peopleApp.directive("input", function($timeout){
		return {
			link: function(scope, element, attrs, ngModel){
				if(scope.viewOnly){
					$timeout(function() {
						attrs.$observe('ngModel', function(value){
					        scope.$watch(value, function(newValue){
					          if(!newValue){				          	
					          		element.addClass("view-null-values");
					          }else{
					          	element.addClass("view-values");
					          }
					        });
					    });
				    }, 700);
				    if(!attrs.ngModel && !attrs.value){
				    	element.addClass("view-null-values");
				    }
				    if(attrs.value){
				    	element.addClass("view-values");
				    }
			    }
			}
		}
	});
	// START advance e-mail validation checker
	// HTML5's e-mail validation if full of holes like 'deanarmada@example', this will validate
	// Source: https://docs.angularjs.org/guide/forms#modifying-built-in-validators
	peopleApp.directive("input", function(){
		var EMAIL_REGEXP = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
		 return {
		 	require: '?ngModel',
		    restrict: '',
		    link: function(scope, elm, attrs, ctrl) {
		      	// only apply the validator if ngModel is present and Angular has added the email validator
		      	if (ctrl && ctrl.$validators.email) {
		        	// this will overwrite the default Angular email validator
		        	ctrl.$validators.email = function(modelValue) {
		          		return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
		        	};
		    	}
		    }
		};
	});

	peopleApp.directive("nullifyOnUpdate", function(){
		return {
			require: 'ngModel',
			link: function(scope, element, attrs, ngModel){
				if(scope.title == "UPDATE"){
					attrs.$observe('ngModel', function(value){
	          			scope.$watch(value, function(newValue){
	          				if(!newValue){
	          					ngModel.$setViewValue(null);
		          				ngModel.$render();
	          				}
	          			});
	          		});
          		}
			}
		}
	});

	// END advance e-mail validation checker
	// peopleApp.directive("nullRemoverDirective", function (){
	// 	return {
	// 		restrict: 'A',
	// 		require: 'ngModel',
	// 		link: function(scope, element, attrs, ngModel){
	// 			element.bind("keyup", function(){
	// 				if(!element.val()){
	// 					alert("dean");
	// 			        ngModel.$setViewValue(undefined);
	// 				}
	// 			});
	// 		}
	// 	}
	// });
	// END Used to show validations upon focus ------ June 1, 2016

	// peopleApp.directive('getProfileSidebarHeight', function() {
	// 	return {
	// 		restrict: 'A',
	// 		link: function(scope, element){
	// 			id = document.querySelector( '#profile-sidebar' );
	// 			sidebar_height = angular.element(id).css("height");
	// 			alert(sidebar_height);
	// 	}
	// });

	
	// FOR FURTHER REUSABALITY
	// NOTE: TOO AUTOMATIC, MAY CAUSE PROBLEM IN THE FUTURE
	// peopleApp.directive('adminUpdateViewFooter', function(){
	// 	return {
	// 		template : '<admin-update-view-page-cancel-button></admin-update-view-page-cancel-button>'
	// 	}
	// });

	// TODO IF REALLY NEEDED
	// peopleApp.directive('updateItemGlyphiconButton', function(){
	// 	return {
	// 		scope:{
	// 			id: "="
	// 		},
	// 		template : '<a href="#/update/{{ id }}"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Update"></span></a>',
	// 	}
	// });
	// // Use avoid repetition of delete button on the item list on the index page of crew-admin apps
	// // Example of using attributes as scope variables without conflicting other scopes
	// TODO IF REALLY NEEDED
	// peopleApp.directive('deleteItemGlyphiconButton', function(){
	// 	return {
	// 		template : '<span class="glyphicon glyphicon-remove" ng-click="toggleModal(id)" data-toggle="tooltip" title="Delete"></span>',
	// 		// template : 'xx - {{ id }}',
	// 		restrict: 'E',
	// 		scope: true,
	// 		link: function(scope, element, attrs){
	// 			scope.id = attrs.id;
	// 		}
	// 	}
	// });
// END Directives

// return {
// 			template : 'xx - {{ id }}',
// 			restrict: 'E',
// 			scope: true,
// 			link: function(scope, element, attrs){
// 				scope.id = attrs.id;
// 			}
// 		}

// START Filters
// Source: http://stackoverflow.com/questions/24883308/convert-birthday-to-age-in-angularjs
peopleApp.filter('ageFilter', function(){
	return function(birthday){
		var birthday = new Date(birthday);
		var today = new Date();
		var age = ((today - birthday) / (31557600000));
		var age = Math.floor( age );
		return age;
	}
});
// Multiple parameter filter
// Source: http://stackoverflow.com/questions/16227325/how-do-i-call-an-angular-js-filter-with-multiple-arguments
peopleApp.filter('sinceUntilFilter', function(){
	return function(since, until){
		var since = new Date(since);
		if(until){
			var until = new Date(until);
		}else{
			var until = new Date();
		}
		var result = ((until - since) / (31557600000));
		var result = Math.round(result * 100) / 100; // Rounds off to two decimal places;
		return result;
	}
});